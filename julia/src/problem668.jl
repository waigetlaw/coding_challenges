using FastPrimeSieve, Printf

calcSquareRootSmooth = function(limit)
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(limit)));

    nonSmooth = 0;

    for prime in primes
        if prime ≤ √limit
            nonSmooth += prime
        else
            nonSmooth += fld(limit, prime)
        end
    end
    @printf "For limit %i, there are %i square root smooth numbers." limit (limit - nonSmooth)
end

@time calcSquareRootSmooth(10^10)
