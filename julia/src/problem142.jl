#=
Find the smallest x + y + z with integers x > y > z > 0 such that x + y, x - y, x + z, x - z, y + z, y - z are all perfect squares.
=#

using Printf

function isPerfectSquare(n)
    return isinteger(sqrt(n))
end

function problem142()

    limit = 500000;

    for z ∈ 1:limit
        if mod(z, 1000) === 0 @printf "Progress: %1.2f\n" 100*z/limit end
        for y ∈ z + 1:limit
            if !(isPerfectSquare(y + z) && isPerfectSquare(y - z))
                continue;
            end
            for x ∈ y + 1:limit
                if isPerfectSquare(x + y) && isPerfectSquare(x - y) &&
                    isPerfectSquare(x + z) && isPerfectSquare(x - z)
                    @printf "x = %i, y = %i, z = %i\n" x y z
                    return x + y + z
                end
            end
        end
    end
end

@time @show problem142();

#=
x = 434657, y = 420968, z = 150568
problem142() = 1006193
 71.688204 seconds (154.73 k allocations: 10.471 MiB, 0.16% compilation time)
=#
