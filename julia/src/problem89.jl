#=
For a number written in Roman numerals to be considered valid there are basic rules which must be followed. Even though the rules allow some numbers to be expressed in more than one way there is always a "best" way of writing a particular number.

For example, it would appear that there are at least six ways of writing the number sixteen:

IIIIIIIIIIIIIIII
VIIIIIIIIIII
VVIIIIII
XIIIIII
VVVI
XVI

However, according to the rules only XIIIIII and XVI are valid, and the last example is considered to be the most efficient, as it uses the least number of numerals.

The 11K text file, roman.txt (right click and 'Save Link/Target As...'), contains one thousand numbers written in valid, but not necessarily minimal, Roman numerals; see About... Roman Numerals for the definitive rules for this problem.

Find the number of characters saved by writing each of these in their minimal form.

Note: You can assume that all the Roman numerals in the file contain no more than four consecutive identical units.
=#

function problem89()
    romanNumbers = readlines("julia/src/files/0089_roman.txt")

    denominations = Dict(
        'I' => 1,
        'V' => 5,
        'X' => 10,
        'L' => 50,
        'C' => 100,
        'D' => 500,
        'M' => 1000,
        nothing => 0
    )

    inverseDenominations = Dict(
        0 => ['I', 'V'],
        1 => ['X', 'L'],
        2 => ['C', 'D'],
        3 => ['M']
    )

    totalLettersSaved = 0;

    for romanNumber ∈ romanNumbers
        number = 0;
        for idx ∈ eachindex(romanNumber)
            currentDen = romanNumber[idx];
            nextDen = idx < length(romanNumber) ? romanNumber[idx+1] : nothing;
            if denominations[nextDen] > denominations[currentDen]
                number -= denominations[currentDen]
            else
                number += denominations[currentDen]
            end
        end
        # @printf "The roman numeral: %s is equal to %i.\n" romanNumber number

        minRomanNumber = "";
        numberCopy = number;
        unit = 0;
        while number >= 1 
            digit = mod(number, 10);
            number = fld(number, 10);
            if digit ≤ 3 || unit > 2
                minRomanNumber = repeat(inverseDenominations[unit][1], digit) * minRomanNumber;
            elseif digit == 4
                minRomanNumber = inverseDenominations[unit][1] * inverseDenominations[unit][2] * minRomanNumber;
            elseif digit == 9
                minRomanNumber = inverseDenominations[unit][1] * inverseDenominations[unit + 1][1] * minRomanNumber;
            else
                minRomanNumber = inverseDenominations[unit][2] * repeat(inverseDenominations[unit][1], digit - 5) * minRomanNumber;
            end
            unit += 1;
        end

        lettersSaved = length(romanNumber) - length(minRomanNumber)
        @printf "The original roman numeral: %s, equal to %i, can be expressed minimally as %s. The number of letters saved is: %i\n" romanNumber numberCopy minRomanNumber lettersSaved
        totalLettersSaved += lettersSaved;
    end

    @printf "\nThe total number of letters saved for all roman numbers is %i.\n" totalLettersSaved
end

@time problem89();
