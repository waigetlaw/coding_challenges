#=
An integer of the form p^q * q^p with prime numbers p ≠ q is called a hybrid-integer.
For example, 800 = 2⁵5² is a hybrid-integer.

We define C(n) to be the number of hybrid-integers less than or equal to n.
You are given C(800) = 2 and C(800^800) = 10790.

Find C(800800^800800).
=#

using Primes, FastPrimeSieve, Printf, LambertW

function convertBase(n, newBase)
    newPower::BigFloat = log10(big(n))/log10(big(newBase))
    return newPower
end

function convertBasev2(base, power, newBase)
    return Float64(power / (log(newBase)/log(base)));
end

function convertBase(base, power, newBase)
    return power * log(base) / log(newBase);
end

# n is only 800800, not the power part.
function C(n)
    primes::Array{BigInt} = [BigInt(2)]
    limit::BigInt = BigInt(n)^n
    total::BigInt = 0;

    while 2^primes[end] * primes[end]^2 < limit
        prime = nextprime(primes[end], 2);
        push!(primes, BigInt(prime));
    end

    for index ∈ eachindex(primes)
        for q ∈ primes[index + 1: end]
            p = primes[index]
            pToQ = p^q
            qToP = q^p
            if pToQ * qToP > limit
                break;
            end
            p_power = convertBase(pToQ, n)
            q_power = convertBase(qToP, n)

            if p_power + q_power <= n
                total += 1
            end
        end
    end

    return total;
end

# calculate largest prime for each prime then the no of primes in between all fit
function Cv2(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
    total::BigInt = 0;

    limit = BigInt(n)^n
    biggest_p = findfirst(p -> 2^p * p^2 > limit, primes);

    primes = primes[1:biggest_p]

    for i ∈ eachindex(primes)
        if i == length(primes)
            break
        end
        p = primes[i]
        first_q = primes[i + 1]

        if p^first_q * first_q^p > limit
            break;
        end

        for j ∈ length(primes):-1:i + 1
            q = primes[j]
            # @printf "p %i, q %i\n" p q
            if (p^q * q^p) <= limit
                total += j - i
                break
            end
            
            # p_power = convertBase(p^q, n)
            # q_power = convertBase(q^p, n)

            # if p_power + q_power <= n
            #     # @printf "%i^%i * %i^%i = %i, numbers = %i\n" p q q p p^q * q^p j - i
            #     total += j - i;
            #     break;
            # end
        end
    end
    return total;
end

function Cv3(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
    total::BigInt = 0;

    limit = BigInt(n)^n
    biggest_p = findfirst(p -> 2^p * p^2 > limit, primes);

    primes = primes[1:biggest_p]

    for index ∈ eachindex(primes)
        for q ∈ primes[index + 1: end]
            p = primes[index]
            pToQ = p^q
            qToP = q^p
            if pToQ * qToP > limit
                break;
            end
            p_power = convertBase(pToQ, n)
            q_power = convertBase(qToP, n)

            if p_power + q_power <= n
                total += 1
            end
        end
    end

    return total;
end

function Cv4(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
    total::BigInt = 0;

    limit = BigInt(n)^n
    biggest_p = findfirst(p -> 2^p * p^2 > limit, primes);

    primes = primes[1:biggest_p]

    for index ∈ eachindex(primes)
        for q ∈ primes[index + 1: end]
            p = primes[index]
            pToQ = p^q
            qToP = q^p
            if pToQ * qToP > limit
                break;
            end
            total += 1
        end
    end

    return total;
end

# x = (b/a) ω ((a/b)e^(-c/b)), where c = log_b(800)
# wrong
function Cv5(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
   
    limit = BigInt(n)^n
    biggest_p = findfirst(p -> 2^p * p^2 > limit, primes);

    primes = primes[1:biggest_p]

    total::BigInt = 0;

    for p in primes
        omega_content = (1/p)ℯ^(-log(p, 800)/p)
        x = p * lambertw(omega_content, 0)

        @printf "for %i^x * x^%i = %i, x = %i.\n" p p 800 x
    end
end

function Cv6(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
    total::BigInt = 0;

    limit = BigInt(n)^n
    biggest_p = findfirst(p -> 2^p * p^2 > limit, primes);

    primes = primes[1:biggest_p]
    for index ∈ eachindex(primes)
        if mod(index, 100) === 0
            # @printf "Progress = %1.2f.\n" 100* index/length(primes)
        end
        for q_index ∈ index + 1:length(primes)
            p = primes[index]
            q = primes[q_index]

            p_power = convertBasev2(p, q, n)
            if p_power > n
                break
            end
            q_power = convertBasev2(q, p, n)
            if p_power + q_power > n
                break
            end
            total += 1
        end
    end

    return total;
end

function Cv7(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
    total::BigInt = 0;

    limit = BigInt(n)^n
    biggest_p = findfirst(p -> 2^p * p^2 > limit, primes);

    primes = primes[1:biggest_p]
    for index ∈ eachindex(primes)
        if mod(index, 100) === 0
            # @printf "Progress = %1.2f.\n" 100* index/length(primes)
        end

        p = primes[index]
        n_power = convertBasev2(n, n, p)
        for q_index ∈ index + 1:length(primes)
            q = primes[q_index]

            if q > n_power
                break
            end

            q_power = convertBasev2(q, p, p)
            if q + q_power > n_power
                break
            end
            total += 1
        end
    end

    return total;
end

function Cv8(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
    total::BigInt = 0;

    log_limit = big(n)*log(n)
    biggest_p = findfirst(p -> big(p)*log(2) > log_limit, primes);

    primes = primes[1:biggest_p]

    for index ∈ eachindex(primes)
        if mod(index, 100) === 0
            # @printf "Progress = %1.2f.\n" 100* index/length(primes)
        end

        p = primes[index]
        log_p = log(p);

        q_index = findfirst(idx -> p*log(primes[idx]) + primes[idx]*log_p > log_limit, (index+1:length(primes)))
        if q_index !== nothing
            total += q_index - 1
        end
    end

    return total;
end

include("./utils/binarysearch.jl")

function Cv9(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
    total::BigInt = 0;

    log_limit = big(n)*log(n)
    biggest_p = findfirst(p -> big(p)*log(2) > log_limit, primes);

    primes = primes[1:biggest_p]

    for index ∈ eachindex(primes)
        if mod(index, 100) === 0
            # @printf "Progress = %1.2f.\n" 100* index/length(primes)
        end

        p = primes[index]
        log_p = log(p);
        if primes[index + 1] * log_p > log_limit
            break
        end
        q_index = binarysearchfirst(
            primes[index + 1:end],
            q_prime -> p*log(q_prime) + q_prime*log_p > log_limit ? 0 : -1
        )
        # q_index = findfirst(==(q), primes[index + 1:end])
        if q_index !== nothing
            total += q_index - 1
        end
    end

    return total;
end

function Cv10(n)
    pow = ceil(convertBase(n, 2));
    pow_limit::BigInt = n * pow;
    primes::Array{BigInt} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(pow_limit)));
    total::BigInt = 0;

    log_limit = big(n)*log(n)

    for (index, p) ∈ enumerate(primes)
        if mod(index, 100) === 0
            @printf "Progress = %1.2f.\n" 100* index/length(primes)
        end

        log_p = log(p);

        # break as the smallest q is still larger than n^n. There are no more solutions
        if p * log(primes[index + 1]) + primes[index + 1] * log_p > log_limit
            break
        end

        primes_sublist = @view (primes[index + 1:end]);
        q_index = binarysearchfirst(
            primes_sublist,
            q_prime -> p*log(q_prime) + q_prime*log_p > log_limit ? 0 : -1
        )
        total += q_index - 1 # q_index is never nothing because we know there is at least one solution for q from earlier if check
    end

    return total;
end

# @time @show C(800);
# @time @show Cv2(800);
# @time @show Cv3(800);
# @time @show Cv4(800);
# @time @show Cv6(800)
# @time @show Cv7(800);
# @time @show Cv8(800);
# @time @show Cv9(800);
# @time @show Cv10(800);

function C′(n)
    primeLimit::Int64 = ceil(n*log(n)/log(2))
    prime_numbers = primes(primeLimit)
    total = 0;

    for (p_index, p) ∈ enumerate(prime_numbers)
        # no more solutions when p ^ first q > n as numbers will only grow
        if convertBase(p, prime_numbers[p_index + 1], n) > n
            break
        end

        for q ∈ prime_numbers[p_index + 1: end]
            adjusted_p_power = convertBase(p, q, n)
            adjusted_q_power = convertBase(q, p, n)
            if adjusted_p_power + adjusted_q_power > n
                break;
            end
            total += 1
        end
    end

    return total;
end

function C′v2(n)
    primeLimit::Int64 = ceil(n*log(n)/log(2))
    prime_numbers = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(primeLimit)));
    total = 0;

    log_limit = n * log(n);

    for (p_index, p) ∈ enumerate(prime_numbers)
        log_p = log(p);
        smallest_q = prime_numbers[p_index + 1];

        # no more solutions when p ^ first q > n as numbers will only grow
        if p * log(smallest_q) + smallest_q * log_p > log_limit
            break
        end

        for q ∈ prime_numbers[p_index + 1: end]
            if p * log(q) + q * log_p > log_limit
                break;
            end
            total += 1
        end
    end

    return total;
end

function C′v3(n)
    primeLimit::Int64 = ceil(n*log(n)/log(2))
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(primeLimit)));
    total = 0;

    log_limit = n * log(n);

    for (p_index, p) ∈ enumerate(primes)
        log_p = log(p);
        smallest_q = primes[p_index + 1];

        # no more solutions when p ^ first q > n as numbers will only grow
        if p * log(smallest_q) + smallest_q * log_p > log_limit
            break
        end

        # primes_sublist = @view (primes[p_index + 1:end]);
        # q_index = binarysearchfirst(
        #     primes_sublist,
        #     q -> convertBase(p, q, n) + convertBase(q, p , n) > n ? 0 : -1
        # )
        q_index = findfirst(q -> p*log(q) + q*log_p > log_limit, primes[p_index+1:length(primes)])
        total += q_index - 1
    end

    return total;
end


function C′v4(n)
    primeLimit::Int64 = ceil(n*log(n)/log(2))
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(primeLimit)));
    total = 0;

    log_limit = n * log(n);

    for (p_index, p) ∈ enumerate(primes)
        log_p = log(p);
        smallest_q = primes[p_index + 1];

        # no more solutions when p ^ first q > n as numbers will only grow
        if p * log(smallest_q) + smallest_q * log_p > log_limit
            break
        end

        q_primes = @view primes[p_index + 1:end];
        q_index = binarysearchfirst(
            q_primes,
            q -> p * log(q) + q * log_p > log_limit ? 0 : -1
        )
        total += q_index - 1
    end

    return total;
end

# @time @show C′(80000);
# @time @show C′v2(80000);
# @time @show C′v2(800800);
# @time @show C′(800800);
@time @show C′v4(800);
@time @show C′v4(800800);


#=
v1
C(800) = 10790
  0.409499 seconds (610.36 k allocations: 58.348 MiB, 1.43% gc time)
10790

v2
Cv2(800) = 10790
 35.059684 seconds (23.86 M allocations: 5.909 GiB, 0.53% gc time)

v4
Cv4(800) = 10790
0.115726 seconds (152.20 k allocations: 39.074 MiB, 2.69% gc time)

Cv4(4000) = 139692
  9.219200 seconds (2.11 M allocations: 5.443 GiB, 0.77% gc time)

v5 solving 2^x * x^2 = 800

x=(b/a)\mathrm{W}\left((a/b)e^{-c/b}\right)

x = (b/a) ω ((a/b)e^(-c/b)), where c = log_b(800)

for 2^x x^2 = 800:

2 * w (1/2)e(-log_2(800)/2)

2 * w(0.00402562)

doesn't seem like this will be easy either because our logs would be in base of the prime p, but we need natural log...

v6 converting base by only using p and q without calculating p^q and looping index rather than new P array
Cv6(4000) = 139692
  1.065288 seconds (4.24 M allocations: 215.137 MiB, 2.76% gc time)

Cv6(50000) = 10094387
 70.522529 seconds (253.92 M allocations: 16.311 GiB, 1.76% gc time)

Cv6(800800) = 1412403576
20298.585882 seconds (35.34 G allocations: 3.227 TiB, 0.84% gc time)

v7 Try changing base of 800800 to p - should save a lot of base conversions!

v9 with binary search
Cv9(50000) = 10094387
 10.876394 seconds (12.33 M allocations: 44.249 GiB, 20.34% gc time)

Cv9(100000) = 34072752
 34.576143 seconds (26.61 M allocations: 177.018 GiB, 21.38% gc time)

Cv9(100000) = 34072752
  4.755622 seconds (3.26 M allocations: 26.008 GiB, 22.88% gc time)

V9 with views in binary search
Cv9(100000) = 34072752
  1.942990 seconds (2.88 M allocations: 8.750 GiB, 17.10% gc time)

v10 with views in code too
Cv10(100000) = 34072752
  0.643998 seconds (2.86 M allocations: 133.712 MiB, 2.98% gc time)
Cv10(800800) = 1412403576
  4.815090 seconds (16.55 M allocations: 750.328 MiB, 2.22% gc time)

v10 kai - earlier break and no primes mapping
Cv10(800800) = 1412403576
  2.838040 seconds (10.61 M allocations: 468.481 MiB, 2.55% gc time)

v11? best version after writing blog
C′v4(800800) = 1412403576
0.018886 seconds (21 allocations: 15.961 MiB)
=#
