#=
We call a positive integer double pandigital if it uses all the digits 0 to 9 exactly twice (with no leading zero). For example, 40561817703823564929 is one such number.

How many double pandigital numbers are divisible by 11?

Divisibility rule by 11 is sum of odds - sum of evens must be divisible by 11.

So we need to find how many ways there are to split all 20 numbers into groups of two that have a diff mod 11 === 0.

Also keep in mind it cannot start with a leading 0.
=#

using Combinatorics

function problem491()
    # digits = [2,2,4,4]
    digits = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,0,0]
    s = sum(digits)
    # combos = collect(multiset_combinations(digits, 2))
    perms = filter(d -> d[1] != 0, unique(multiset_permutations(digits, Int64(length(digits)/2))))
    numberDivBy11 = length(filter(p -> begin
        even = sum(p)
        odd = s - even
        return mod(even - odd, 11) == 0
    end
    ,perms))
    # perms = collect(multiset_permutations(digits, Int64(length(digits)/2)))
    # for c in combos
    #     println(combos)
    # end
    println((numberDivBy11))
end

@time @show problem491()

#=
v1 too slow and maybe incorrect...

for each combo if 10 digits (as even), we also need to multiply by combo of combinations the odds could be placed.
=#
