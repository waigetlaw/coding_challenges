#=
The Fibonacci sequence is defined by the recurrence relation:

Fₙ = F_{n-1} + F_{n-2}, where F₁ = 1 and F₂ = 1.

It turns out that F_541, which contains 113 digits, is the first Fibonacci number for which the last nine digits are 1-9 pandigital (contain all the digits 1 to 9, but not necessarily in order). And F_2749, which contains 575 digits, is the first Fibonacci number for which the first nine digits are 1-9 pandigital.

Given that Fₖ is the first Fibonacci number for which the first nine digits AND the last nine digits are 1-9 pandigital, find k.

Thoughts: lets just try brute force.
=#

using Printf

function isPandigital(F)
    if F < 10^18 return false end
    last9 = join(sort(digits(mod(F, 10^9))))
    isPandEnd = last9 === "123456789"
    if !isPandEnd
        return false
    end
    d = digits(F)
    first9 = join(sort(d[length(d)-8:end]))
    return first9 === "123456789"
end

function problem104()
    limit = 10^6;
    F_prev::BigInt = 1;
    F_current::BigInt = 1;
    k = 2;
    while k < limit
        if mod(k, 1000) === 0 @printf "Progress: %1.2f\n" 100*k/limit end
        k += 1;
        F_next::BigInt = F_prev + F_current;
        if isPandigital(F_next)
            break;
        end

        F_prev = F_current
        F_current = F_next
    end
    return k
end

@time @show problem104();

#=
problem104() = 100000
 11.216669 seconds (6.94 M allocations: 3.180 GiB, 0.96% gc time)
 
problem104() = 329468
212.916996 seconds (14.54 M allocations: 120.330 GiB, 2.54% gc time, 0.01% compilation time)

v2 with mod to get last 9
problem104() = 329468
3.926770 seconds (12.22 M allocations: 4.916 GiB, 4.22% gc time)
=#
