using Printf

function problem135(limit::Int)
    n_track = Dict{Int, Int}()
    for a=1:1:1 + limit/2, N=0:1:2*a
        n::Int=(2*a-N)*(2*a+N)
        if (n > limit)
            continue
        end
        if (!haskey(n_track, n))
            n_track[n] = N > 0 && a > N ? 2 : 1;
        else
            n_track[n] += N > 0 && a > N ? 2 : 1;
        end
    end

    ans = filter((k, v)::Pair -> v === 10, n_track)
    println(length(ans))

    one = filter((k, v)::Pair -> v === 1, n_track)

    key = sort(collect(keys(one)))
    print(key)
end

@time problem135(10^2)

# 4989
# 366.480833 seconds (3.94 M allocations: 100.658 MiB, 0.01% gc time, 0.00% compilation time)
