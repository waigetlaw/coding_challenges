#=
We define an S-number to be a natural number, n, that is a perfect square and its square root can be obtained by splitting the decimal representation of n into 2 or more numbers then adding the numbers.

For example, 81 is an S-number because √81 = 8 + 1.
6724 is an S-number: √6724 = 6 + 72 + 4.
8281 is an S-number: √8281 = 8 + 2 + 81 = 82 + 8 + 1.
9801 is an S-number: √9801 = 98 + 0 + 1.

Further we define T(N) to be the sum of all S numbers n ≤ N. You are given T(10⁴) = 41333.

Find T(10^12).
=#

using Combinatorics, Printf

function problem719(N)
    sqrtOfN = √N;
    ΣSNumbers = 0;

    for i::Int64 ∈ 1:sqrtOfN
        n::Int64 = i^2
        # @printf "√%i = %i, if the numbers %s can be summed to %i, then it is an S-number.\n" n i n i
        digits = collect(string(n))
        # @printf "Using digits %s, try to sum to %i\n" digits i
        parts = filter(arr -> length(arr) > 1, collect(partitions(digits)))
        parts = map(arr -> map(x -> parse(Int64, join(x)), arr), parts)
        parts = filter(arr -> sum(arr) == i && join(arr) == string(n), parts)
        # parts = filter(arr -> findfirst(x -> length(x) > 1 && x[1] == '0', arr) === nothing, parts)
        # partition seems to alter order too so we need to check the original order is kept
        # parts = filter(arr -> parse(Int64, join(arr)) == n, parts)

        if length(parts) > 0
            ΣSNumbers += n
            @printf "√%i = %i, partition: %s totaled to %i, therefore %i is an S-number.\n" n i parts[1] i n
        end
        # for part in parts
        #     total = 0;
        #     for number in part
        #         # total += parse(Int64, join(number))
        #         total += number
        #     end
        #     if total == i
        #         ΣSNumbers += n
        #         @printf "√%i = %i, partition: %s totaled to %i, therefore %i is an S-number.\n" n i part total n
        #         break
        #     end
        # end
    end

    @printf "\nT(%i) = %i.\n" N ΣSNumbers
end

function problem719v2(N)
    sqrtOfN = √N;
    ΣSNumbers = 0;

    for i::Int64 ∈ 1:sqrtOfN
        n::Int64 = i^2
        digits = collect(string(n))

        parts = collect(partitions(length(digits)))[2:end]
        parts = filter(arr -> length(string(i)) - 1 ≤ maximum(arr) ≤ length(string(i)), parts)

        # @printf "i = %i, parts: %s\n" i parts

        foundSol = false;

        for part ∈ parts
            if foundSol
                break
            end
            # @printf "For √%i = %i, try partition %s.\n" n i part
            unique_perms = unique(permutations(part))
 
            for perm in unique_perms
                copy_digits = digits;
                split_digits = [];
                for p in perm
                    push!(split_digits, copy_digits[1:p])
                    copy_digits = copy_digits[p+1:end]
                end

                numbers = map(arr -> parse(Int64, join(arr)), split_digits)
                if sum(numbers) == i
                    ΣSNumbers += n
                    foundSol = true;
                    @printf "√%i = %i, partition: %s totaled to %i, therefore %i is an S-number.\n" n i numbers sum(numbers) n
                    break
                end
                if foundSol
                    break
                end
            end
        end
    end

    @printf "\nT(%i) = %i.\n" N ΣSNumbers
end

@time problem719v2(10^12)

#=
For v1:

T(100000000) = 2818842841.
 37.767767 seconds (954.43 M allocations: 49.170 GiB, 14.37% gc time, 0.07% compilation time)

T(10000000) = 30940313.
2.463686 seconds (61.12 M allocations: 3.138 GiB, 13.97% gc time, 1.03% compilation time)

for v2, we use permutations and partitions to get exactly only partitions that contain numbers that are less than or equal to the same target number and at most two less than i otherwise it won't be able to sum to it. The lesser choices to loop through increases speed a lot.

T(10000000) = 30940313.
  0.153348 seconds (4.07 M allocations: 259.875 MiB, 18.48% gc time, 1.30% compilation time)

T(100000000) = 2818842841.
1.434316 seconds (39.66 M allocations: 2.715 GiB, 16.29% gc time, 0.14% compilation time)

From 37.76 seconds to 1.4 sec!!

√81 = 9, partition: [8, 1] totaled to 9, therefore 81 is an S-number.
√100 = 10, partition: [10, 0] totaled to 10, therefore 100 is an S-number.
√1296 = 36, partition: [1, 29, 6] totaled to 36, therefore 1296 is an S-number.
√2025 = 45, partition: [20, 25] totaled to 45, therefore 2025 is an S-number.
√3025 = 55, partition: [30, 25] totaled to 55, therefore 3025 is an S-number.
√6724 = 82, partition: [6, 72, 4] totaled to 82, therefore 6724 is an S-number.
√8281 = 91, partition: [82, 8, 1] totaled to 91, therefore 8281 is an S-number.
√9801 = 99, partition: [98, 1] totaled to 99, therefore 9801 is an S-number.
√10000 = 100, partition: [100, 0] totaled to 100, therefore 10000 is an S-number.
√55225 = 235, partition: [5, 5, 225] totaled to 235, therefore 55225 is an S-number.
√88209 = 297, partition: [88, 209] totaled to 297, therefore 88209 is an S-number.
√136161 = 369, partition: [1, 361, 6, 1] totaled to 369, therefore 136161 is an S-number.
√136900 = 370, partition: [1, 369, 0] totaled to 370, therefore 136900 is an S-number.
√143641 = 379, partition: [14, 364, 1] totaled to 379, therefore 143641 is an S-number.
√171396 = 414, partition: [17, 1, 396] totaled to 414, therefore 171396 is an S-number.
√431649 = 657, partition: [4, 3, 1, 649] totaled to 657, therefore 431649 is an S-number.
√455625 = 675, partition: [45, 5, 625] totaled to 675, therefore 455625 is an S-number.
√494209 = 703, partition: [494, 209] totaled to 703, therefore 494209 is an S-number.
√571536 = 756, partition: [5, 715, 36] totaled to 756, therefore 571536 is an S-number.
√627264 = 792, partition: [62, 726, 4] totaled to 792, therefore 627264 is an S-number.
√826281 = 909, partition: [826, 2, 81] totaled to 909, therefore 826281 is an S-number.
√842724 = 918, partition: [842, 72, 4] totaled to 918, therefore 842724 is an S-number.
√893025 = 945, partition: [8, 930, 2, 5] totaled to 945, therefore 893025 is an S-number.
√929296 = 964, partition: [929, 29, 6] totaled to 964, therefore 929296 is an S-number.
√980100 = 990, partition: [980, 10, 0] totaled to 990, therefore 980100 is an S-number.
√982081 = 991, partition: [982, 8, 1] totaled to 991, therefore 982081 is an S-number.
√998001 = 999, partition: [998, 1] totaled to 999, therefore 998001 is an S-number.
√1000000 = 1000, partition: [1000, 0] totaled to 1000, therefore 1000000 is an S-number.
√1679616 = 1296, partition: [1, 679, 616] totaled to 1296, therefore 1679616 is an S-number.
√2896804 = 1702, partition: [2, 896, 804] totaled to 1702, therefore 2896804 is an S-number.
√3175524 = 1782, partition: [3, 1755, 24] totaled to 1782, therefore 3175524 is an S-number.
√4941729 = 2223, partition: [494, 1729] totaled to 2223, therefore 4941729 is an S-number.
√7441984 = 2728, partition: [744, 1984] totaled to 2728, therefore 7441984 is an S-number.
√11329956 = 3366, partition: [11, 3299, 56] totaled to 3366, therefore 11329956 is an S-number.
√13293316 = 3646, partition: [1, 329, 3316] totaled to 3646, therefore 13293316 is an S-number.
√13557124 = 3682, partition: [1, 3557, 124] totaled to 3682, therefore 13557124 is an S-number.
√17073424 = 4132, partition: [1, 707, 3424] totaled to 4132, therefore 17073424 is an S-number.
√23804641 = 4879, partition: [238, 0, 4641] totaled to 4879, therefore 23804641 is an S-number.
√24068836 = 4906, partition: [2, 4068, 836] totaled to 4906, therefore 24068836 is an S-number.
√24502500 = 4950, partition: [2450, 2500] totaled to 4950, therefore 24502500 is an S-number.
√25502500 = 5050, partition: [2550, 2500] totaled to 5050, therefore 25502500 is an S-number.
√26512201 = 5149, partition: [26, 5122, 1] totaled to 5149, therefore 26512201 is an S-number.
√28005264 = 5292, partition: [28, 0, 5264] totaled to 5292, therefore 28005264 is an S-number.
√46676224 = 6832, partition: [46, 6762, 24] totaled to 6832, therefore 46676224 is an S-number.
√51710481 = 7191, partition: [5, 1, 7104, 81] totaled to 7191, therefore 51710481 is an S-number.
√52881984 = 7272, partition: [5288, 1984] totaled to 7272, therefore 52881984 is an S-number.
√54597321 = 7389, partition: [54, 5, 9, 7321] totaled to 7389, therefore 54597321 is an S-number.
√56746089 = 7533, partition: [56, 7460, 8, 9] totaled to 7533, therefore 56746089 is an S-number.
√56896849 = 7543, partition: [5, 689, 6849] totaled to 7543, therefore 56896849 is an S-number.
√57562569 = 7587, partition: [5, 7562, 5, 6, 9] totaled to 7587, therefore 57562569 is an S-number.
√60481729 = 7777, partition: [6048, 1729] totaled to 7777, therefore 60481729 is an S-number.
√63297936 = 7956, partition: [6, 3, 2, 9, 7936] totaled to 7956, therefore 63297936 is an S-number.
√70829056 = 8416, partition: [70, 8290, 56] totaled to 8416, therefore 70829056 is an S-number.
√71284249 = 8443, partition: [7, 1, 2, 8424, 9] totaled to 8443, therefore 71284249 is an S-number.
√76860289 = 8767, partition: [76, 8602, 89] totaled to 8767, therefore 76860289 is an S-number.
√78428736 = 8856, partition: [78, 42, 8736] totaled to 8856, therefore 78428736 is an S-number.
√79388100 = 8910, partition: [7, 93, 8810, 0] totaled to 8910, therefore 79388100 is an S-number.
√79887844 = 8938, partition: [7, 9, 8878, 44] totaled to 8938, therefore 79887844 is an S-number.
√84787264 = 9208, partition: [8478, 726, 4] totaled to 9208, therefore 84787264 is an S-number.
√86769225 = 9315, partition: [8, 6, 76, 9225] totaled to 9315, therefore 86769225 is an S-number.
√86955625 = 9325, partition: [8695, 5, 625] totaled to 9325, therefore 86955625 is an S-number.
√91891396 = 9586, partition: [9189, 1, 396] totaled to 9586, therefore 91891396 is an S-number.
√92563641 = 9621, partition: [9256, 364, 1] totaled to 9621, therefore 92563641 is an S-number.
√92929600 = 9640, partition: [9, 29, 2, 9600] totaled to 9640, therefore 92929600 is an S-number.
√95355225 = 9765, partition: [9535, 5, 225] totaled to 9765, therefore 95355225 is an S-number.
√98029801 = 9901, partition: [9802, 98, 1] totaled to 9901, therefore 98029801 is an S-number.
√98188281 = 9909, partition: [9818, 82, 8, 1] totaled to 9909, therefore 98188281 is an S-number.
√98366724 = 9918, partition: [9836, 6, 72, 4] totaled to 9918, therefore 98366724 is an S-number.
√98903025 = 9945, partition: [9890, 30, 25] totaled to 9945, therefore 98903025 is an S-number.
√99102025 = 9955, partition: [9910, 20, 25] totaled to 9955, therefore 99102025 is an S-number.
√99281296 = 9964, partition: [9928, 1, 29, 6] totaled to 9964, therefore 99281296 is an S-number.
√99800100 = 9990, partition: [9980, 10, 0] totaled to 9990, therefore 99800100 is an S-number.
√99820081 = 9991, partition: [9982, 8, 1] totaled to 9991, therefore 99820081 is an S-number.
√99980001 = 9999, partition: [9998, 1] totaled to 9999, therefore 99980001 is an S-number.
√100000000 = 10000, partition: [10000, 0] totaled to 10000, therefore 100000000 is an S-number.
√110502144 = 10512, partition: [1, 10502, 1, 4, 4] totaled to 10512, therefore 110502144 is an S-number.
√149377284 = 12222, partition: [1, 4937, 7284] totaled to 12222, therefore 149377284 is an S-number.
√161976529 = 12727, partition: [1, 6197, 6529] totaled to 12727, therefore 161976529 is an S-number.
√298287441 = 17271, partition: [2, 9828, 7441] totaled to 17271, therefore 298287441 is an S-number.
√300814336 = 17344, partition: [3008, 14336] totaled to 17344, therefore 300814336 is an S-number.
√493817284 = 22222, partition: [4938, 17284] totaled to 22222, therefore 493817284 is an S-number.
√494217361 = 22231, partition: [494, 21736, 1] totaled to 22231, therefore 494217361 is an S-number.
√642825316 = 25354, partition: [6, 4, 28, 25316] totaled to 25354, therefore 642825316 is an S-number.
√751527396 = 27414, partition: [7, 5, 1, 5, 27396] totaled to 27414, therefore 751527396 is an S-number.
√1133601561 = 33669, partition: [11, 33601, 56, 1] totaled to 33669, therefore 1133601561 is an S-number.
√1178342929 = 34327, partition: [11, 7, 8, 34292, 9] totaled to 34327, therefore 1178342929 is an S-number.
√1256135364 = 35442, partition: [12, 5, 61, 35364] totaled to 35442, therefore 1256135364 is an S-number.
√1336487364 = 36558, partition: [1, 3, 36487, 3, 64] totaled to 36558, therefore 1336487364 is an S-number.
√1358291025 = 36855, partition: [1, 35829, 1025] totaled to 36855, therefore 1358291025 is an S-number.
√1518037444 = 38962, partition: [1518, 0, 37444] totaled to 38962, therefore 1518037444 is an S-number.
√1553936400 = 39420, partition: [1, 55, 39364, 0] totaled to 39420, therefore 1553936400 is an S-number.
√1693240201 = 41149, partition: [16, 932, 40201] totaled to 41149, therefore 1693240201 is an S-number.
√1748410596 = 41814, partition: [1, 748, 41059, 6] totaled to 41814, therefore 1748410596 is an S-number.
√1818425449 = 42643, partition: [1, 81, 8, 42544, 9] totaled to 42643, therefore 1818425449 is an S-number.
√2084561649 = 45657, partition: [20, 8, 45616, 4, 9] totaled to 45657, therefore 2084561649 is an S-number.
√2141745841 = 46279, partition: [21, 417, 45841] totaled to 46279, therefore 2141745841 is an S-number.
√2380464100 = 48790, partition: [2380, 46410, 0] totaled to 48790, therefore 2380464100 is an S-number.
√2384857225 = 48835, partition: [238, 48572, 25] totaled to 48835, therefore 2384857225 is an S-number.
√2449458064 = 49492, partition: [24, 49458, 6, 4] totaled to 49492, therefore 2449458064 is an S-number.
√2456490969 = 49563, partition: [2, 456, 49096, 9] totaled to 49563, therefore 2456490969 is an S-number.
√2490608836 = 49906, partition: [2, 49060, 8, 836] totaled to 49906, therefore 2490608836 is an S-number.
√2505002500 = 50050, partition: [25, 0, 50025, 0] totaled to 50050, therefore 2505002500 is an S-number.
√2647514116 = 51454, partition: [26, 4, 7, 51411, 6] totaled to 51454, therefore 2647514116 is an S-number.
√2800526400 = 52920, partition: [280, 0, 52640, 0] totaled to 52920, therefore 2800526400 is an S-number.
√2805291225 = 52965, partition: [28, 0, 52912, 25] totaled to 52965, therefore 2805291225 is an S-number.
√2853162225 = 53415, partition: [28, 53162, 225] totaled to 53415, therefore 2853162225 is an S-number.
√3293956449 = 57393, partition: [3, 2, 939, 56449] totaled to 57393, therefore 3293956449 is an S-number.
√3311657209 = 57547, partition: [331, 1, 6, 57209] totaled to 57547, therefore 3311657209 is an S-number.
√3514592656 = 59284, partition: [3, 5, 1, 4, 59265, 6] totaled to 59284, therefore 3514592656 is an S-number.
√3862125316 = 62146, partition: [3, 8, 62125, 3, 1, 6] totaled to 62146, therefore 3862125316 is an S-number.
√4139764281 = 64341, partition: [41, 3, 9, 7, 64281] totaled to 64341, therefore 4139764281 is an S-number.
√4256518564 = 65242, partition: [42, 5, 65185, 6, 4] totaled to 65242, therefore 4256518564 is an S-number.
√4506705424 = 67132, partition: [4, 50, 67054, 24] totaled to 67132, therefore 4506705424 is an S-number.
√4682391184 = 68428, partition: [4, 68239, 1, 184] totaled to 68428, therefore 4682391184 is an S-number.
√4769007364 = 69058, partition: [4, 7, 69007, 36, 4] totaled to 69058, therefore 4769007364 is an S-number.
√4782690649 = 69157, partition: [4, 78, 2, 69064, 9] totaled to 69157, therefore 4782690649 is an S-number.
√4853769561 = 69669, partition: [48, 53, 7, 69561] totaled to 69669, therefore 4853769561 is an S-number.
√5272502544 = 72612, partition: [52, 72502, 54, 4] totaled to 72612, therefore 5272502544 is an S-number.
√5572174609 = 74647, partition: [5, 5, 7, 21, 74609] totaled to 74647, therefore 5572174609 is an S-number.
√5674759561 = 75331, partition: [5, 6, 74759, 561] totaled to 75331, therefore 5674759561 is an S-number.
√5906076201 = 76851, partition: [590, 60, 76201] totaled to 76851, therefore 5906076201 is an S-number.
√5947648641 = 77121, partition: [594, 76486, 41] totaled to 77121, therefore 5947648641 is an S-number.
√6039776656 = 77716, partition: [6, 39, 77665, 6] totaled to 77716, therefore 6039776656 is an S-number.
√6049417284 = 77778, partition: [60494, 17284] totaled to 77778, therefore 6049417284 is an S-number.
√6067786816 = 77896, partition: [6, 6, 77868, 16] totaled to 77896, therefore 6067786816 is an S-number.
√6322794256 = 79516, partition: [63, 22, 79425, 6] totaled to 79516, therefore 6322794256 is an S-number.
√6580129924 = 81118, partition: [65, 80129, 924] totaled to 81118, therefore 6580129924 is an S-number.
√6731382025 = 82045, partition: [6, 7, 3, 1, 3, 82025] totaled to 82045, therefore 6731382025 is an S-number.
√6826064400 = 82620, partition: [6, 82606, 4, 4, 0] totaled to 82620, therefore 6826064400 is an S-number.
√6832014336 = 82656, partition: [68320, 14336] totaled to 82656, therefore 6832014336 is an S-number.
√7261084944 = 85212, partition: [7, 261, 0, 84944] totaled to 85212, therefore 7261084944 is an S-number.
√7385855481 = 85941, partition: [7, 385, 85548, 1] totaled to 85941, therefore 7385855481 is an S-number.
√7668680041 = 87571, partition: [766, 86800, 4, 1] totaled to 87571, therefore 7668680041 is an S-number.
√7879757824 = 88768, partition: [7, 87975, 782, 4] totaled to 88768, therefore 7879757824 is an S-number.
√7887571344 = 88812, partition: [7, 88757, 1, 3, 44] totaled to 88812, therefore 7887571344 is an S-number.
√7887748969 = 88813, partition: [7, 88774, 8, 9, 6, 9] totaled to 88813, therefore 7887748969 is an S-number.
√8033895424 = 89632, partition: [80, 3, 3, 89542, 4] totaled to 89632, therefore 8033895424 is an S-number.
√8248090761 = 90819, partition: [8, 2, 48, 0, 90761] totaled to 90819, therefore 8248090761 is an S-number.
√8419163536 = 91756, partition: [84, 1, 91635, 36] totaled to 91756, therefore 8419163536 is an S-number.
√8723933604 = 93402, partition: [8, 7, 23, 93360, 4] totaled to 93402, therefore 8723933604 is an S-number.
√8756093476 = 93574, partition: [87, 5, 6, 0, 93476] totaled to 93574, therefore 8756093476 is an S-number.
√8862527881 = 94141, partition: [8, 86252, 7881] totaled to 94141, therefore 8862527881 is an S-number.
√8945565561 = 94581, partition: [8, 94556, 5, 5, 6, 1] totaled to 94581, therefore 8945565561 is an S-number.
√9048004641 = 95121, partition: [90480, 4641] totaled to 95121, therefore 9048004641 is an S-number.
√9435596769 = 97137, partition: [9, 4, 355, 96769] totaled to 97137, therefore 9435596769 is an S-number.
√9811893025 = 99055, partition: [98118, 930, 2, 5] totaled to 99055, therefore 9811893025 is an S-number.
√9845799076 = 99226, partition: [98, 45, 7, 99076] totaled to 99226, therefore 9845799076 is an S-number.
√9849371536 = 99244, partition: [98493, 715, 36] totaled to 99244, therefore 9849371536 is an S-number.
√9869031649 = 99343, partition: [98690, 3, 1, 649] totaled to 99343, therefore 9869031649 is an S-number.
√9911994481 = 99559, partition: [99, 11, 99448, 1] totaled to 99559, therefore 9911994481 is an S-number.
√9926136900 = 99630, partition: [99261, 369, 0] totaled to 99630, therefore 9926136900 is an S-number.
√9926336161 = 99631, partition: [99263, 361, 6, 1] totaled to 99631, therefore 9926336161 is an S-number.
√9940688209 = 99703, partition: [99406, 88, 209] totaled to 99703, therefore 9940688209 is an S-number.
√9953055225 = 99765, partition: [99530, 5, 5, 225] totaled to 99765, therefore 9953055225 is an S-number.
√9980010000 = 99900, partition: [99800, 100, 0] totaled to 99900, therefore 9980010000 is an S-number.
√9980209801 = 99901, partition: [99802, 98, 1] totaled to 99901, therefore 9980209801 is an S-number.
√9981808281 = 99909, partition: [99818, 82, 8, 1] totaled to 99909, therefore 9981808281 is an S-number.
√9983606724 = 99918, partition: [99836, 6, 72, 4] totaled to 99918, therefore 9983606724 is an S-number.
√9989003025 = 99945, partition: [99890, 30, 25] totaled to 99945, therefore 9989003025 is an S-number.
√9991002025 = 99955, partition: [99910, 20, 25] totaled to 99955, therefore 9991002025 is an S-number.
√9992801296 = 99964, partition: [99928, 1, 29, 6] totaled to 99964, therefore 9992801296 is an S-number.
√9998000100 = 99990, partition: [99980, 10, 0] totaled to 99990, therefore 9998000100 is an S-number.
√9998200081 = 99991, partition: [99982, 8, 1] totaled to 99991, therefore 9998200081 is an S-number.
√9999800001 = 99999, partition: [99998, 1] totaled to 99999, therefore 9999800001 is an S-number.
√10000000000 = 100000, partition: [100000, 0] totaled to 100000, therefore 10000000000 is an S-number.
√10999394884 = 104878, partition: [1, 9993, 94884] totaled to 104878, therefore 10999394884 is an S-number.
√11048532544 = 105112, partition: [1, 104853, 254, 4] totaled to 105112, therefore 11048532544 is an S-number.
√11105365924 = 105382, partition: [1, 1, 105365, 9, 2, 4] totaled to 105382, therefore 11105365924 is an S-number.
√13769379649 = 117343, partition: [1, 37693, 79649] totaled to 117343, therefore 13769379649 is an S-number.
√14937972841 = 122221, partition: [1, 49379, 72841] totaled to 122221, therefore 14937972841 is an S-number.
√16913002500 = 130050, partition: [16, 9, 130025, 0] totaled to 130050, therefore 16913002500 is an S-number.
√17413177681 = 131959, partition: [174, 131776, 8, 1] totaled to 131959, therefore 17413177681 is an S-number.
√19614002500 = 140050, partition: [19, 6, 140025, 0] totaled to 140050, therefore 19614002500 is an S-number.
√20408122449 = 142857, partition: [20408, 122449] totaled to 142857, therefore 20408122449 is an S-number.
√20601435024 = 143532, partition: [20, 6, 0, 143502, 4] totaled to 143532, therefore 20601435024 is an S-number.
√21948126201 = 148149, partition: [21948, 126201] totaled to 148149, therefore 21948126201 is an S-number.
√26754163489 = 163567, partition: [2, 67, 5, 4, 163489] totaled to 163567, therefore 26754163489 is an S-number.
√28167580224 = 167832, partition: [28, 167580, 224] totaled to 167832, therefore 28167580224 is an S-number.
√29869171929 = 172827, partition: [29, 869, 171929] totaled to 172827, therefore 29869171929 is an S-number.
√33058148761 = 181819, partition: [33058, 148761] totaled to 181819, therefore 33058148761 is an S-number.
√35010152100 = 187110, partition: [35010, 152100] totaled to 187110, therefore 35010152100 is an S-number.
√41320319076 = 203274, partition: [4, 1, 3, 203190, 76] totaled to 203274, therefore 41320319076 is an S-number.
√43470165025 = 208495, partition: [43470, 165025] totaled to 208495, therefore 43470165025 is an S-number.
√46582157241 = 215829, partition: [46, 58, 215724, 1] totaled to 215829, therefore 46582157241 is an S-number.
√62416028224 = 249832, partition: [6, 241602, 8224] totaled to 249832, therefore 62416028224 is an S-number.
√76024275625 = 275725, partition: [76, 24, 275625] totaled to 275725, therefore 76024275625 is an S-number.
√80612837776 = 283924, partition: [80, 61, 283777, 6] totaled to 283924, therefore 80612837776 is an S-number.
√92982304900 = 304930, partition: [9, 2, 9, 8, 2, 304900] totaled to 304930, therefore 92982304900 is an S-number.
√96310294921 = 310339, partition: [9, 6, 310294, 9, 21] totaled to 310339, therefore 96310294921 is an S-number.
√100316625984 = 316728, partition: [10, 0, 316625, 9, 84] totaled to 316728, therefore 100316625984 is an S-number.
√101558217124 = 318682, partition: [101558, 217124] totaled to 318682, therefore 101558217124 is an S-number.
√101873318976 = 319176, partition: [10, 187, 3, 318976] totaled to 319176, therefore 101873318976 is an S-number.
√102432002500 = 320050, partition: [1, 24, 320025, 0] totaled to 320050, therefore 102432002500 is an S-number.
√108878221089 = 329967, partition: [108878, 221089] totaled to 329967, therefore 108878221089 is an S-number.
√111332999556 = 333666, partition: [111, 332999, 556] totaled to 333666, therefore 111332999556 is an S-number.
√113366216601 = 336699, partition: [11, 336621, 66, 1] totaled to 336699, therefore 113366216601 is an S-number.
√113366890000 = 336700, partition: [11, 336689, 0] totaled to 336700, therefore 113366890000 is an S-number.
√113433566401 = 336799, partition: [1134, 335664, 1] totaled to 336799, therefore 113433566401 is an S-number.
√114758337600 = 338760, partition: [1147, 5, 8, 337600] totaled to 338760, therefore 114758337600 is an S-number.
√115228339209 = 339453, partition: [11, 5, 228, 339209] totaled to 339453, therefore 115228339209 is an S-number.
√118434404449 = 344143, partition: [1, 1, 84, 344044, 4, 9] totaled to 344143, therefore 118434404449 is an S-number.
√122349546225 = 349785, partition: [12, 2, 349546, 225] totaled to 349785, therefore 122349546225 is an S-number.
√123448227904 = 351352, partition: [123448, 227904] totaled to 351352, therefore 123448227904 is an S-number.
√124392352249 = 352693, partition: [1, 2, 439, 2, 352249] totaled to 352693, therefore 124392352249 is an S-number.
√125594398449 = 354393, partition: [1, 255943, 98449] totaled to 354393, therefore 125594398449 is an S-number.
√126970356241 = 356329, partition: [12, 69, 7, 0, 356241] totaled to 356329, therefore 126970356241 is an S-number.
√127194229449 = 356643, partition: [127194, 229449] totaled to 356643, therefore 127194229449 is an S-number.
√128017977616 = 357796, partition: [1, 280179, 77616] totaled to 357796, therefore 128017977616 is an S-number.
√132801936400 = 364420, partition: [1, 328019, 36400] totaled to 364420, therefore 132801936400 is an S-number.
√133175364624 = 364932, partition: [133, 175, 364624] totaled to 364932, therefore 133175364624 is an S-number.
√136353686121 = 369261, partition: [13, 635, 368612, 1] totaled to 369261, therefore 136353686121 is an S-number.
√136746123264 = 369792, partition: [1, 367461, 2326, 4] totaled to 369792, therefore 136746123264 is an S-number.
√136912580289 = 370017, partition: [1, 369125, 802, 89] totaled to 370017, therefore 136912580289 is an S-number.
√136999177956 = 370134, partition: [1, 369991, 77, 9, 56] totaled to 370134, therefore 136999177956 is an S-number.
√137013243409 = 370153, partition: [1, 370132, 4, 3, 4, 9] totaled to 370153, therefore 137013243409 is an S-number.
√147134383561 = 383581, partition: [1, 4, 7, 1, 3, 4, 383561] totaled to 383581, therefore 147134383561 is an S-number.
√151803744400 = 389620, partition: [15180, 374440, 0] totaled to 389620, therefore 151803744400 is an S-number.
√151838812225 = 389665, partition: [1518, 388122, 25] totaled to 389665, therefore 151838812225 is an S-number.
√152344237969 = 390313, partition: [152344, 237969] totaled to 390313, therefore 152344237969 is an S-number.
√168141002500 = 410050, partition: [16, 8, 1, 410025, 0] totaled to 410050, therefore 168141002500 is an S-number.
√174164328900 = 417330, partition: [1, 7, 416432, 890, 0] totaled to 417330, therefore 174164328900 is an S-number.
√174172675600 = 417340, partition: [17, 417267, 56, 0] totaled to 417340, therefore 174172675600 is an S-number.
√176282419600 = 419860, partition: [176, 2, 82, 419600] totaled to 419860, therefore 176282419600 is an S-number.
√183542839561 = 428419, partition: [1, 8, 3, 5, 428395, 6, 1] totaled to 428419, therefore 183542839561 is an S-number.
√188643417561 = 434331, partition: [1, 88, 6, 434175, 61] totaled to 434331, therefore 188643417561 is an S-number.
√194369883876 = 440874, partition: [1, 9, 436988, 3876] totaled to 440874, therefore 194369883876 is an S-number.
√194672441089 = 441217, partition: [1, 9, 46, 72, 441089] totaled to 441217, therefore 194672441089 is an S-number.
√204424249689 = 452133, partition: [20, 442424, 9689] totaled to 452133, therefore 204424249689 is an S-number.
√204521922081 = 452241, partition: [20, 452192, 20, 8, 1] totaled to 452241, therefore 204521922081 is an S-number.
√213018248521 = 461539, partition: [213018, 248521] totaled to 461539, therefore 213018248521 is an S-number.
√214632064656 = 463284, partition: [21, 463206, 46, 5, 6] totaled to 463284, therefore 214632064656 is an S-number.
√217930248900 = 466830, partition: [217930, 248900] totaled to 466830, therefore 217930248900 is an S-number.
√222347142369 = 471537, partition: [22, 23, 471423, 69] totaled to 471537, therefore 222347142369 is an S-number.
√224644717089 = 473967, partition: [2246, 4, 471708, 9] totaled to 473967, therefore 224644717089 is an S-number.
√224739520489 = 474067, partition: [22, 473952, 4, 89] totaled to 474067, therefore 224739520489 is an S-number.
√232910481664 = 482608, partition: [2, 32, 910, 481664] totaled to 482608, therefore 232910481664 is an S-number.
√240149002500 = 490050, partition: [24, 1, 490025, 0] totaled to 490050, therefore 240149002500 is an S-number.
√249321461041 = 499321, partition: [2, 493214, 6104, 1] totaled to 499321, therefore 249321461041 is an S-number.
√249500250000 = 499500, partition: [249500, 250000] totaled to 499500, therefore 249500250000 is an S-number.
√249906008836 = 499906, partition: [2, 499060, 8, 836] totaled to 499906, therefore 249906008836 is an S-number.
√250050002500 = 500050, partition: [25, 0, 500025, 0] totaled to 500050, therefore 250050002500 is an S-number.
√250500250000 = 500500, partition: [250500, 250000] totaled to 500500, therefore 250500250000 is an S-number.
√251501247001 = 501499, partition: [251, 501247, 1] totaled to 501499, therefore 251501247001 is an S-number.
√262975121721 = 512811, partition: [2, 629, 7, 512172, 1] totaled to 512811, therefore 262975121721 is an S-number.
√265148785476 = 514926, partition: [26, 514878, 5, 4, 7, 6] totaled to 514926, therefore 265148785476 is an S-number.
√275245031044 = 524638, partition: [27, 524503, 104, 4] totaled to 524638, therefore 275245031044 is an S-number.
√280052640000 = 529200, partition: [2800, 526400, 0] totaled to 529200, therefore 280052640000 is an S-number.
√280529122500 = 529650, partition: [28, 0, 529122, 500] totaled to 529650, therefore 280529122500 is an S-number.
√284000528889 = 532917, partition: [28, 4000, 528889] totaled to 532917, therefore 284000528889 is an S-number.
√284270248900 = 533170, partition: [284270, 248900] totaled to 533170, therefore 284270248900 is an S-number.
√285316222500 = 534150, partition: [28, 531622, 2500] totaled to 534150, therefore 285316222500 is an S-number.
√287600528656 = 536284, partition: [28, 7600, 528656] totaled to 536284, therefore 287600528656 is an S-number.
√289940248521 = 538461, partition: [289940, 248521] totaled to 538461, therefore 289940248521 is an S-number.
√301905192681 = 549459, partition: [30190, 519268, 1] totaled to 549459, therefore 301905192681 is an S-number.
√303628550625 = 551025, partition: [30, 362, 8, 550625] totaled to 551025, therefore 303628550625 is an S-number.
√305527035025 = 552745, partition: [30, 552703, 5, 2, 5] totaled to 552745, therefore 305527035025 is an S-number.
√315597891961 = 561781, partition: [31, 559789, 1961] totaled to 561781, therefore 315597891961 is an S-number.
√326571103296 = 571464, partition: [326, 571103, 29, 6] totaled to 571464, therefore 326571103296 is an S-number.
√341415838864 = 584308, partition: [3, 414, 1, 583886, 4] totaled to 584308, therefore 341415838864 is an S-number.
√341584140304 = 584452, partition: [3, 4, 1, 584140, 304] totaled to 584452, therefore 341584140304 is an S-number.
√355954231161 = 596619, partition: [35, 595423, 1161] totaled to 596619, therefore 355954231161 is an S-number.
√360760000689 = 600633, partition: [3, 607, 600006, 8, 9] totaled to 600633, therefore 360760000689 is an S-number.
√371718237969 = 609687, partition: [371718, 237969] totaled to 609687, therefore 371718237969 is an S-number.
√393595626384 = 627372, partition: [393, 595, 626384] totaled to 627372, therefore 393595626384 is an S-number.
√393900588225 = 627615, partition: [39390, 0, 588225] totaled to 627615, therefore 393900588225 is an S-number.
√409265988121 = 639739, partition: [40926, 598812, 1] totaled to 639739, therefore 409265988121 is an S-number.
√412264158084 = 642078, partition: [412, 2, 641580, 84] totaled to 642078, therefore 412264158084 is an S-number.
√413908229449 = 643357, partition: [413908, 229449] totaled to 643357, therefore 413908229449 is an S-number.
√414313643584 = 643672, partition: [41, 43, 1, 3, 643584] totaled to 643672, therefore 414313643584 is an S-number.
√418646526841 = 647029, partition: [418, 646526, 84, 1] totaled to 647029, therefore 418646526841 is an S-number.
√420744227904 = 648648, partition: [420744, 227904] totaled to 648648, therefore 420744227904 is an S-number.
√422216647524 = 649782, partition: [42, 2216, 647524] totaled to 649782, therefore 422216647524 is an S-number.
√430656187536 = 656244, partition: [43, 0, 656187, 5, 3, 6] totaled to 656244, therefore 430656187536 is an S-number.
√446667662224 = 668332, partition: [446, 667662, 224] totaled to 668332, therefore 446667662224 is an S-number.
√448944221089 = 670033, partition: [448944, 221089] totaled to 670033, therefore 448944221089 is an S-number.
√448967002500 = 670050, partition: [4, 4, 8, 9, 670025, 0] totaled to 670050, therefore 448967002500 is an S-number.
√453805669801 = 673651, partition: [45, 3805, 669801] totaled to 673651, therefore 453805669801 is an S-number.
√455674951369 = 675037, partition: [4, 5, 5, 674951, 3, 69] totaled to 675037, therefore 455674951369 is an S-number.
√464194217124 = 681318, partition: [464194, 217124] totaled to 681318, therefore 464194217124 is an S-number.
√467991441801 = 684099, partition: [4, 679914, 4180, 1] totaled to 684099, therefore 467991441801 is an S-number.
√475793689284 = 689778, partition: [475, 7, 9, 3, 689284] totaled to 689778, therefore 475793689284 is an S-number.
√483123695041 = 695071, partition: [4, 8, 3, 12, 3, 695041] totaled to 695071, therefore 483123695041 is an S-number.
√486972291556 = 697834, partition: [48, 697229, 1, 556] totaled to 697834, therefore 486972291556 is an S-number.
√508712724081 = 713241, partition: [508, 712724, 8, 1] totaled to 713241, therefore 508712724081 is an S-number.
√517363718400 = 719280, partition: [517, 363, 718400] totaled to 719280, therefore 517363718400 is an S-number.
√542707735969 = 736687, partition: [5, 4, 2, 707, 735969] totaled to 736687, therefore 542707735969 is an S-number.
√547397258769 = 739863, partition: [54, 739725, 8, 7, 69] totaled to 739863, therefore 547397258769 is an S-number.
√564375060001 = 751249, partition: [5, 643, 750600, 1] totaled to 751249, therefore 564375060001 is an S-number.
√569754742041 = 754821, partition: [5, 69, 754742, 4, 1] totaled to 754821, therefore 569754742041 is an S-number.
√575284325625 = 758475, partition: [5, 752843, 2, 5625] totaled to 758475, therefore 575284325625 is an S-number.
√575873052769 = 758863, partition: [5, 758730, 52, 7, 69] totaled to 758863, therefore 575873052769 is an S-number.
√577676002500 = 760050, partition: [5, 7, 7, 6, 760025, 0] totaled to 760050, therefore 577676002500 is an S-number.
√592977002500 = 770050, partition: [5, 9, 2, 9, 770025, 0] totaled to 770050, therefore 592977002500 is an S-number.
√609487805809 = 780697, partition: [6, 94, 8, 780580, 9] totaled to 780697, therefore 609487805809 is an S-number.
√615121784209 = 784297, partition: [61, 5, 1, 21, 784209] totaled to 784297, therefore 615121784209 is an S-number.
√623450788921 = 789589, partition: [623, 45, 0, 788921] totaled to 789589, therefore 623450788921 is an S-number.
√624687898384 = 790372, partition: [62, 468, 789838, 4] totaled to 790372, therefore 624687898384 is an S-number.
√626480165025 = 791505, partition: [626480, 165025] totaled to 791505, therefore 626480165025 is an S-number.
√626879147049 = 791757, partition: [6, 268, 791470, 4, 9] totaled to 791757, therefore 626879147049 is an S-number.
√628347923856 = 792684, partition: [6, 283, 4, 792385, 6] totaled to 792684, therefore 628347923856 is an S-number.
√632794794256 = 795484, partition: [632, 794794, 2, 56] totaled to 795484, therefore 632794794256 is an S-number.
√637987185081 = 798741, partition: [6, 3, 798718, 5, 8, 1] totaled to 798741, therefore 637987185081 is an S-number.
√646803586081 = 804241, partition: [646, 803586, 8, 1] totaled to 804241, therefore 646803586081 is an S-number.
√648047540196 = 805014, partition: [64, 804754, 196] totaled to 805014, therefore 648047540196 is an S-number.
√657427450761 = 810819, partition: [65742, 745076, 1] totaled to 810819, therefore 657427450761 is an S-number.
√658215803025 = 811305, partition: [65, 8215, 803025] totaled to 811305, therefore 658215803025 is an S-number.
√660790152100 = 812890, partition: [660790, 152100] totaled to 812890, therefore 660790152100 is an S-number.
√669420148761 = 818181, partition: [669420, 148761] totaled to 818181, therefore 669420148761 is an S-number.
√682087684996 = 825886, partition: [6, 820876, 8, 4996] totaled to 825886, therefore 682087684996 is an S-number.
√682398253476 = 826074, partition: [682, 39, 825347, 6] totaled to 826074, therefore 682398253476 is an S-number.
√691678315584 = 831672, partition: [6, 91, 6, 7, 831558, 4] totaled to 831672, therefore 691678315584 is an S-number.
√691754831524 = 831718, partition: [6, 9, 175, 4, 831524] totaled to 831718, therefore 691754831524 is an S-number.
√695832915556 = 834166, partition: [695, 832915, 556] totaled to 834166, therefore 695832915556 is an S-number.
√699108360129 = 836127, partition: [6, 99, 1, 0, 836012, 9] totaled to 836127, therefore 699108360129 is an S-number.
√699183613584 = 836172, partition: [6, 9, 9, 1, 836135, 8, 4] totaled to 836172, therefore 699183613584 is an S-number.
√718474921641 = 847629, partition: [71, 847492, 1, 64, 1] totaled to 847629, therefore 718474921641 is an S-number.
√721684230400 = 849520, partition: [7216, 842304, 0] totaled to 849520, therefore 721684230400 is an S-number.
√723428498116 = 850546, partition: [723, 4, 2, 849811, 6] totaled to 850546, therefore 723428498116 is an S-number.
√725650126201 = 851851, partition: [725650, 126201] totaled to 851851, therefore 725650126201 is an S-number.
√726185187225 = 852165, partition: [7, 261, 851872, 25] totaled to 852165, therefore 726185187225 is an S-number.
√726785235289 = 852517, partition: [7, 2, 67, 852352, 89] totaled to 852517, therefore 726785235289 is an S-number.
√728533945764 = 853542, partition: [72, 853394, 5, 7, 64] totaled to 853542, therefore 728533945764 is an S-number.
√729088853689 = 853867, partition: [72, 90, 8, 8, 853689] totaled to 853867, therefore 729088853689 is an S-number.
√734694122449 = 857143, partition: [734694, 122449] totaled to 857143, therefore 734694122449 is an S-number.
√734707836801 = 857151, partition: [73470, 783680, 1] totaled to 857151, therefore 734707836801 is an S-number.
√739686002500 = 860050, partition: [7, 3, 9, 6, 860025, 0] totaled to 860050, therefore 739686002500 is an S-number.
√748643718564 = 865242, partition: [7, 4, 864371, 856, 4] totaled to 865242, therefore 748643718564 is an S-number.
√760638645316 = 872146, partition: [7606, 3, 864531, 6] totaled to 872146, therefore 760638645316 is an S-number.
√770008005001 = 877501, partition: [77000, 800500, 1] totaled to 877501, therefore 770008005001 is an S-number.
√775718801001 = 880749, partition: [77, 571, 880100, 1] totaled to 880749, therefore 775718801001 is an S-number.
√788231454976 = 887824, partition: [7, 882314, 5497, 6] totaled to 887824, therefore 788231454976 is an S-number.
√789188819769 = 888363, partition: [7, 89, 1, 888197, 69] totaled to 888363, therefore 789188819769 is an S-number.
√791462888164 = 889642, partition: [7, 9, 1462, 888164] totaled to 889642, therefore 791462888164 is an S-number.
√792181882116 = 890046, partition: [7921, 8, 1, 882116] totaled to 890046, therefore 792181882116 is an S-number.
√821609906329 = 906427, partition: [82, 1, 6, 9, 906329] totaled to 906427, therefore 821609906329 is an S-number.
√822278985616 = 906796, partition: [8222, 7, 898561, 6] totaled to 906796, therefore 822278985616 is an S-number.
√823991907600 = 907740, partition: [8, 2, 39, 91, 907600] totaled to 907740, therefore 823991907600 is an S-number.
√824890815225 = 908235, partition: [8, 2, 48, 908152, 25] totaled to 908235, therefore 824890815225 is an S-number.
√829409096961 = 910719, partition: [82, 940, 909696, 1] totaled to 910719, therefore 829409096961 is an S-number.
√844891872400 = 919180, partition: [8, 448, 918724, 0] totaled to 919180, therefore 844891872400 is an S-number.
√847309204036 = 920494, partition: [8, 47, 30, 920403, 6] totaled to 920494, therefore 847309204036 is an S-number.
√849213854784 = 921528, partition: [84, 921385, 47, 8, 4] totaled to 921528, therefore 849213854784 is an S-number.
√850026836961 = 921969, partition: [85002, 6, 836961] totaled to 921969, therefore 850026836961 is an S-number.
√864092948356 = 929566, partition: [8, 64, 0, 929483, 5, 6] totaled to 929566, therefore 864092948356 is an S-number.
√875693566225 = 935785, partition: [87, 5, 6, 935662, 25] totaled to 935785, therefore 875693566225 is an S-number.
√883694002500 = 940050, partition: [8, 8, 3, 6, 940025, 0] totaled to 940050, therefore 883694002500 is an S-number.
√894252031201 = 945649, partition: [8, 942520, 3120, 1] totaled to 945649, therefore 894252031201 is an S-number.
√894573580761 = 945819, partition: [8, 945735, 8, 7, 61] totaled to 945819, therefore 894573580761 is an S-number.
√899483424921 = 948411, partition: [8, 9, 948342, 49, 2, 1] totaled to 948411, therefore 899483424921 is an S-number.
√901039491361 = 949231, partition: [90, 1, 3, 949136, 1] totaled to 949231, therefore 901039491361 is an S-number.
√904494200401 = 951049, partition: [9044, 942004, 1] totaled to 951049, therefore 904494200401 is an S-number.
√904800464100 = 951210, partition: [904800, 46410, 0] totaled to 951210, therefore 904800464100 is an S-number.
√906941952225 = 952335, partition: [9, 6, 94, 1, 952225] totaled to 952335, therefore 906941952225 is an S-number.
√912695533201 = 955351, partition: [9, 1, 2, 6, 955332, 1] totaled to 955351, therefore 912695533201 is an S-number.
√923594037444 = 961038, partition: [923594, 37444] totaled to 961038, therefore 923594037444 is an S-number.
√923595959521 = 961039, partition: [923, 595, 959521] totaled to 961039, therefore 923595959521 is an S-number.
√937519681536 = 968256, partition: [9, 37, 51, 968153, 6] totaled to 968256, therefore 937519681536 is an S-number.
√938495937600 = 968760, partition: [9384, 959376, 0] totaled to 968760, therefore 938495937600 is an S-number.
√940257969561 = 969669, partition: [94, 2, 5, 7, 969561] totaled to 969669, therefore 940257969561 is an S-number.
√956032217361 = 977769, partition: [956032, 21736, 1] totaled to 977769, therefore 956032217361 is an S-number.
√974940986881 = 987391, partition: [9, 7, 494, 0, 986881] totaled to 987391, therefore 974940986881 is an S-number.
√979903989801 = 989901, partition: [9, 79, 9, 3, 989801] totaled to 989901, therefore 979903989801 is an S-number.
√980296029801 = 990099, partition: [980296, 2, 9801] totaled to 990099, therefore 980296029801 is an S-number.
√982366428736 = 991144, partition: [982366, 42, 8736] totaled to 991144, therefore 982366428736 is an S-number.
√982542860289 = 991233, partition: [982542, 8602, 89] totaled to 991233, therefore 982542860289 is an S-number.
√983238829056 = 991584, partition: [983238, 8290, 56] totaled to 991584, therefore 983238829056 is an S-number.
√984990746089 = 992467, partition: [984990, 7460, 8, 9] totaled to 992467, therefore 984990746089 is an S-number.
√985276597321 = 992611, partition: [985276, 5, 9, 7321] totaled to 992611, therefore 985276597321 is an S-number.
√986382676224 = 993168, partition: [986382, 6762, 24] totaled to 993168, therefore 986382676224 is an S-number.
√988994448324 = 994482, partition: [9, 8, 8, 994448, 3, 2, 4] totaled to 994482, therefore 988994448324 is an S-number.
√989444005264 = 994708, partition: [989444, 5264] totaled to 994708, therefore 989444005264 is an S-number.
√989728512201 = 994851, partition: [989728, 5122, 1] totaled to 994851, therefore 989728512201 is an S-number.
√989925502500 = 994950, partition: [989925, 5025, 0] totaled to 994950, therefore 989925502500 is an S-number.
√993279329956 = 996634, partition: [993279, 3299, 56] totaled to 996634, therefore 993279329956 is an S-number.
√996439175524 = 998218, partition: [996439, 1755, 24] totaled to 998218, therefore 996439175524 is an S-number.
√996598896804 = 998298, partition: [996598, 896, 804] totaled to 998298, therefore 996598896804 is an S-number.
√997409679616 = 998704, partition: [997409, 679, 616] totaled to 998704, therefore 997409679616 is an S-number.
√998002998001 = 999001, partition: [998002, 998, 1] totaled to 999001, therefore 998002998001 is an S-number.
√998018982081 = 999009, partition: [998018, 982, 8, 1] totaled to 999009, therefore 998018982081 is an S-number.
√998020980100 = 999010, partition: [998020, 980, 10, 0] totaled to 999010, therefore 998020980100 is an S-number.
√998072929296 = 999036, partition: [998072, 929, 29, 6] totaled to 999036, therefore 998072929296 is an S-number.
√998110893025 = 999055, partition: [998110, 8, 930, 2, 5] totaled to 999055, therefore 998110893025 is an S-number.
√998164842724 = 999082, partition: [998164, 842, 72, 4] totaled to 999082, therefore 998164842724 is an S-number.
√998182826281 = 999091, partition: [998182, 826, 2, 81] totaled to 999091, therefore 998182826281 is an S-number.
√998416627264 = 999208, partition: [998416, 62, 726, 4] totaled to 999208, therefore 998416627264 is an S-number.
√998488571536 = 999244, partition: [998488, 5, 715, 36] totaled to 999244, therefore 998488571536 is an S-number.
√998594494209 = 999297, partition: [998594, 494, 209] totaled to 999297, therefore 998594494209 is an S-number.
√998650455625 = 999325, partition: [998650, 45, 5, 625] totaled to 999325, therefore 998650455625 is an S-number.
√998686431649 = 999343, partition: [998686, 4, 3, 1, 649] totaled to 999343, therefore 998686431649 is an S-number.
√999172171396 = 999586, partition: [999172, 17, 1, 396] totaled to 999586, therefore 999172171396 is an S-number.
√999242143641 = 999621, partition: [999242, 14, 364, 1] totaled to 999621, therefore 999242143641 is an S-number.
√999260136900 = 999630, partition: [999260, 1, 369, 0] totaled to 999630, therefore 999260136900 is an S-number.
√999262136161 = 999631, partition: [999262, 1, 361, 6, 1] totaled to 999631, therefore 999262136161 is an S-number.
√999406088209 = 999703, partition: [999406, 88, 209] totaled to 999703, therefore 999406088209 is an S-number.
√999530055225 = 999765, partition: [999530, 5, 5, 225] totaled to 999765, therefore 999530055225 is an S-number.
√999800010000 = 999900, partition: [999800, 100, 0] totaled to 999900, therefore 999800010000 is an S-number.
√999802009801 = 999901, partition: [999802, 98, 1] totaled to 999901, therefore 999802009801 is an S-number.
√999818008281 = 999909, partition: [999818, 82, 8, 1] totaled to 999909, therefore 999818008281 is an S-number.
√999836006724 = 999918, partition: [999836, 6, 72, 4] totaled to 999918, therefore 999836006724 is an S-number.
√999890003025 = 999945, partition: [999890, 30, 25] totaled to 999945, therefore 999890003025 is an S-number.
√999910002025 = 999955, partition: [999910, 20, 25] totaled to 999955, therefore 999910002025 is an S-number.
√999928001296 = 999964, partition: [999928, 1, 29, 6] totaled to 999964, therefore 999928001296 is an S-number.
√999980000100 = 999990, partition: [999980, 10, 0] totaled to 999990, therefore 999980000100 is an S-number.
√999982000081 = 999991, partition: [999982, 8, 1] totaled to 999991, therefore 999982000081 is an S-number.
√999998000001 = 999999, partition: [999998, 1] totaled to 999999, therefore 999998000001 is an S-number.
√1000000000000 = 1000000, partition: [1000000, 0] totaled to 1000000, therefore 1000000000000 is an S-number.

T(1000000000000) = 128088830547982.
35912.752332 seconds (126.96 G allocations: 10.402 TiB, 21.08% gc time, 0.00% compilation time)
=#
