using Primes

"""
totient(n)

Computes the totient of n, which is the number of positive integers up to a given integer n that are relatively prime to n. In other words, it is the number of integers k in the range 1 ≤ k ≤ n for which the greatest common divisor gcd(n, k) is equal to 1.

# Examples
```julia-repl
julia> totient(600)
160
```
"""
function totient(n)
    factors = factor(Set, n);
    ϕₙ::Int128 = Int128(n);
    for p ∈ factors
        ϕₙ *= 1 - 1//p
    end
    return ϕₙ
end

function ϕ(n) totient(n) end
