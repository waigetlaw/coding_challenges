"""
compare: must return -1 if number is too low, 0 if correct, 1 if too high
returns index of first item from list
"""
function binarysearchfirst(list, compare, found = nothing, index = 0)
    if length(list) < 3
        ans = findfirst(n -> compare(n) === 0, list)
        return ans === nothing ? found : index + ans;
    end

    mid = cld(length(list), 2)

    firstHalf = @view list[1:mid-1]
    secondHalf = @view list[mid + 1:end]

    ans = compare(list[mid])
    if ans === 0
        return binarysearchfirst(firstHalf, compare, index + mid, index)
    elseif ans === 1
        return binarysearchfirst(firstHalf, compare, found, index)
    else
        return binarysearchfirst(secondHalf, compare, found, index + mid)
    end
end

function binarysearchfirstitem(list, compare, found = nothing)
    if length(list) < 3
        ans = findfirst(n -> compare(n) === 0, list)
        return ans === nothing ? found : list[ans];
    end

    mid = cld(length(list), 2)

    firstHalf = @view list[1:mid-1]
    secondHalf = @view list[mid + 1:end]

    ans = compare(list[mid])
    if ans === 0
        return binarysearchfirstitem(firstHalf, compare, list[mid])
    elseif ans === 1
        return binarysearchfirstitem(firstHalf, compare, found)
    else
        return binarysearchfirstitem(secondHalf, compare, found)
    end
end

# arr = [2,4,6,8,10,12,14,16,18,543543,7328243423,2,4,67,2321321,5434,3467875]
# ans = binarysearchfindfirst(
#     arr,
#     n -> mod(n, 5) === 0 ? 0 : -1
# );

# println(ans)
# println(arr[ans])
