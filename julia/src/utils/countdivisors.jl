using Primes

function countdivisors(n)
    factorization = factor(n)
    divisors = 1;
    for (prime, power) in factorization
        divisors *= power + 1
    end
    return divisors
end
