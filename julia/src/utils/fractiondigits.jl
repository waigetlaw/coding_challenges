using Printf

function fractiondigits(n, k, l = 50)
    d = [];
    q, r = divrem(n, k);
    if r == 0
        return d;
    end
    while length(d) < l && r != 0
        if mod(length(d), 10^5) == 0
            @printf "Percentage done: %1.2f\n" 100*length(d)/l
        end
        r *= 10
        q, r = divrem(r, k)
        push!(d, q)
    end
    return d
end

function fractiondigitsv2(n, k, l = 50)
    d = [];
    q, r = divrem(n, k);
    if r == 0
        return d;
    end
    r *= 10
    remainders = Dict();
    while length(d) < l && r != 0
        if mod(length(d), 10^5) == 0
            @printf "Percentage done: %1.2f\n" 100*length(d)/l
        end
        # r *= 10
        q, r_new = divrem(r, k)
        r_new *= 10;

        if haskey(remainders, r)
            @printf "has cycle - r = %i, q = %i, %s\n" r q remainders
            cycle = [q];
            start = r;
            next = remainders[r][2];
            @printf "start %s, next %s\n" start next
            while next != start
                push!(cycle, remainders[next][1])
                next = remainders[next][2]
            end
            @printf "current num: %s\n" d
            @printf "cycle is %s\n" cycle

            dlength = length(d);
            require = l - dlength;
            times, extra = divrem(require, length(cycle))
            println(times, " ", extra)
            for x in times
                push!(d, cycle)
            end
            for y in extra
                # push to reach limit
            end
            break
        else
            remainders[r] = (q, r_new)
            push!(d, q)
        end
        r = r_new
    end
    return d
end

"""
n: number as the numerator
k: number as the denominator
l: number of decimals after the point required (result of n/k)

return: Dictionary containing the initial digits with length l and empty cycle or initial with cycle of digits that would repeat forever thereafter
"""
function fractiondigitsv3(n, k, l = 50)
    d::Array{Int8} = [];
    q, r = divrem(n, k);
    idx::Int128 = 1;
    if r == 0
        return Dict("initial" => [], "cycle" => []);
    end
    r *= 10
    remainders = Dict();
    while length(d) < l && r != 0
        # if mod(idx, 10^6) == 0
        #     @printf "Percentage done: %1.2f\n" 100*length(d)/l
        # end
        q, r_new = divrem(Int128(r), k)
        r_new::Int128 = Int128(r_new * 10)

        cyclestartindex = get(remainders, r, 0)
        # if r < 0 || r > k * 1000
        #     @printf "something wrong %i\n" r
        # end
        if cyclestartindex > 0
            # cyclestartindex = remainders[r]
            cycle = @view d[cyclestartindex:end]
            # @printf "has cycle: %s\n" cycle
            return Dict("initial" => d[1:cyclestartindex - 1], "cycle" => cycle)
        else
            remainders[r] = idx
            push!(d, q)
            idx += 1
        end
        r = r_new
    end
    return Dict("initial" => d, "cycle" => [])
end

# So to steal/summarize from my 2nd answer, the nth digit of x/y is the 1st digit of (10n-1x mod y)/y = floor(10 * (10n-1x mod y) / y) mod 10.
function nthdigit(x, y, n)
    return Int8(mod(floor(10 * mod(x * powermod(10, n - 1, y), y) / y), 10))
end


# @time @show nthdigit(1, 17, 10^6);
# @time @show getNthFractionalDigit(1, 17, 10^6);
