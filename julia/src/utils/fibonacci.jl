function fibonacci(n)
    known = zeros(Int64, n)
    function memoize(k)
      if known[k] != 0
        # do nothing
      elseif k == 1
        known[k] = 0
      elseif k == 2 || k == 3
        known[k] = 1
      else
        known[k] = memoize(k-1) + memoize(k-2)
      end
      return known[k]
    end
    memoize(n)
    return known
  end
