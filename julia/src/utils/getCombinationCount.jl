

function getCombinationCount(total, arr)
    possibleCombinations = 0;

    function reduce(total, arr, index = 1)
        item = arr[index];
        index += 1;
        if total == 0
            possibleCombinations += 1;
            return;
        end
        if index > length(arr)
            if mod(total, item) == 0
                possibleCombinations += 1;
                return;
            end
            return;
        end
        if item > total
            reduce(total, arr, index);
            return;
        end
        while true
            reduce(total, arr, index);
            total -= item;
            if total < 0
                break
            end
        end
    end

    reduce(total, arr);
    return possibleCombinations;
end
