"""
intPart(limit)

Computes the total integer partitions for 1 ≤ n ≤ limit

# Examples
```julia-repl
julia> intPart(10)
BigInt[1, 2, 3, 5, 7, 11, 15, 22, 30, 42]
```
"""
function intPart(limit)
    n = 1;
    pentagonal = [];
    parts = Array{BigInt}([0, 1])
    k_lim = fld(1 + sqrt(1 + 24 * limit), 6) + 1;
    for k ∈ 1: k_lim
        push!(pentagonal, Dict{String, BigInt}("p" => k*(3*k - 1)/2, "mult" => mod(k, 2) == 0 ? -1 : 1))
        push!(pentagonal, Dict{String, BigInt}("p" => (-k)*(3*(-k) - 1)/2, "mult" => mod(k, 2) == 0 ? -1 : 1))
    end

    
    for n ∈ 2:last(pentagonal)["p"]
        part_n = 0;
        for pent ∈ pentagonal
            part_index = n - pent["p"] + 1
            if part_index < 1
                break
            end
            if part_index > length(parts)
                continue
            end
            part_n += (parts[part_index] * pent["mult"])
        end
        push!(parts, part_n)
        if length(parts) ≥ limit + 2
            break
        end
    end
 
    return parts[3:end]
end
