function getdivisors(n)

    divisors = Set([1, n]);
    limit = ceil(sqrt(n))
    div = 2;
    while div <= limit
        if mod(n, div) == 0
            push!(divisors, div)
            push!(divisors, n ÷ div)
        end
        div += 1;
    end
    return divisors;
end
