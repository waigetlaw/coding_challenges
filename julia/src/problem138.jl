#=
Consider the isosceles triangle with base length, b = 16, and legs, L = 17.

By using the Pythagorean theorem it can be seen that the height of the triangle, h = sqrt(17^2 - 8^2) = 15, which is one less than the base length.

With b = 272 and L = 305, we get h = 273, which is one more than the base length, and this is the second smallest isosceles triangle with the property that  h = b +- 1.

Find SUM(L) for the twelve smallest isosceles triangles for which h = b +-1 and b, L are positive integers.

Thoughts: calc the equations we get:

base = +-2 +- sqrt(4 - 5 - 5L^2)/(5/2)

if base is int and positive, then there is an answer.

    The initial +- can be determined at the end as the negative and positive answers flip so if either is int, it will be an answer.

    lets say the sqrt part is a whole number...

   +- 2 +- N / (10/4)
   = +-  ((4) +- 2N)/5
=#


# consider N = L + x, where x is the integer diff between N and L
# we eventually get L = (x + sqrt(5x^2 + 4))/4
# notice pattern of x for sqrt = int = [8, 21, 55, 144, 377, 987, 2584, 6765, 17711, 46368, 121393, 317811, 832040, 2178309, 5702887, ...]
# x_{n+1} = 3 * x_n - x_{n-1}
using Printf

function problem138_v5(limit)
    Ls = [];

    x_prev = 1
    x = big(3)

    while (length(Ls) < limit)
        old_x = x
        x = (3 * x) - x_prev;
        x_prev = old_x;

        L = (x + sqrt(5 * x ^ 2 + 4))/4
        if (isinteger(L))
            N = L + x
            # @printf "N = %i, L = %i, x = %i\n" N L x
            # println(L)
            push!(Ls, L)
        end

    end

    println(Ls)
    @printf "sum of first %i L: %i\n" limit (sum(Ls))
end


function problem138_v4(limit)
    Ls = [];

    N = big(3);

    while (length(Ls) < limit)
        N += 1;
        N_squared = N^2;
        unit = mod(N_squared, 10);

        if (unit ≠ 9 && unit ≠ 4)
            continue;
        end

        L = sqrt(1 + ((N_squared - 4)/5))

        if (isinteger(L))
            @printf "N = %i, L = %i, N/L = %f\n" N L N/L
            println(L)
            push!(Ls, L)
        end
    end

    println(Ls)
    @printf "sum of first %i L: %i" limit (sum(Ls))
end

function problem138_v3(limit)
    Ls = [];

    N = big(-3)
    while (length(Ls) < limit)
        N += 5

        top1 = (N^2 - 4 + 5)
        top2 = ((N + 1)^2 - 4 + 5)
        if (mod(top1, 5) == 0)
            L1 = sqrt(top1 / 5);
            if (isinteger(L1))
                println(L1)
                push!(Ls, L1)
            end
        end

        if (mod(top2, 5) == 0)
            L2 = sqrt(top2 / 5);
            if (isinteger(L2))
                println(L2)
                push!(Ls, L2)
            end
        end
    end

    println(Ls)
    @printf "sum of first %i L: %i" limit (2 * sum(Ls))
end

function problem138_v2(limit)
    Ls = [];

    L = big(16)
    while (length(Ls) < limit)
        L += 1
        N = sqrt(4 - 5 + 5 * (L^2))
        if (isinteger(N))
            unit = mod(N, 10)
            if unit == 2 || unit == 3 || unit == 7 || unit == 8
                println(L)
                push!(Ls, L)
            end
        end
    end

    println(Ls)
    @printf "sum of first %i L: %i\n" limit (2 * sum(Ls))
end

function problem138(limit)
    Ls = [];

    b = 1;
    while (length(Ls) < limit)
        b += 1
        h1 = b - 1;
        h2 = b + 1;

        L1::Float64 = sqrt(big((0.5 * b)^2 + h1^2))
        L2::Float64 = sqrt(big((0.5 * b)^2 + h2^2))

        if (isinteger(L1))
            # @printf "b %i, h1 %i, L1 %i, h - b = %i\n" b h1 L1 h1 - b
            @printf "sqrt(%i^2 + %i^2) = %f\n" b/2 h1 L1
            push!(Ls, L1)
        end
        if (isinteger(L2))
            @printf "b %i, h2 %i, L2 %i h - b = %i\n" b h2 L2 h2 - b
            @printf "%i^2 + %i^2 = %f\n" b/2 h2 L2
            push!(Ls, L2)
        end
    end

    println(Ls)
    @printf "sum of first %i L: %i\n" limit (2 * sum(Ls))
end

function calcL(N)
    return sqrt((5 + big(N)^2 - 4)/5)
end

function problem138_old(limit)
    Ls = [];
    N = -2
    while (length(Ls) < limit)
        N += 5
        b1 = abs((4 + (2 * N)) / 5)
        b2 = abs((4 - (2 * (N-1))) / 5)
        # println(b1, " ", b2)
        if b1 > 0 && isinteger(b1)
            L = calcL(N)
            if isinteger(L)
                # h = sqrt(L^2 - (0.5*b1)^2)
                # @printf "N %i, b1 %i, L %i, h %i\n" N b1 L h
                @printf "sqrt(%i^2 - %i^2) = h\n" L b1/2
                push!(Ls, L)
            end

        end
        if b2 != b1 && b2 > 0 && isinteger(b2)
            L = calcL(N-1)
            if isinteger(L)
                # h = sqrt(L^2 - (0.5*b2)^2)
                # @printf "N %i, b2 %i, L %i, h %i\n" N b2 L h
                @printf "sqrt(%i^2 - %i^2) = h\n" L b2/2
                push!(Ls, L)
            end
        end
    end

    println(Ls)
    @printf "sum of first %i L: %i\n" limit (2 * sum(Ls))
end

# @time problem138_old(7)
# @time problem138(12)

# @time problem138_v2(7)

# @time problem138_v4(5)

@time problem138_v5(12)
#=
v2
b 16, h1 15, L1 17
b 272, h2 273, L2 305
b 4896, h1 4895, L1 5473
b 87840, h2 87841, L2 98209
b 1576240, h1 1576239, L1 1762289
b 28284464, h2 28284465, L2 31622993
b 59907457, h2 59907458, L2 66978574
b 91530450, h2 91530451, L2 102334155
b 123153443, h2 123153444, L2 137689736
b 144760853, h1 144760852, L1 161847553
b 154776436, h2 154776437, L2 173045317
b 176383846, h1 176383845, L1 197203134

original
N 38, b1 16, L 17, h 15
N 682, b2 272, L 305, h 273
N 12238, b1 4896, L 5473, h 4895
N 219602, b2 87840, L 98209, h 87841
N 3940598, b1 1576240, L 1762289, h 1576239
N 70711162, b2 28284464, L 31622993, h 28284465
N 228826127, b2 91530450, L 102334155, h 91530451
N 424266972, b2 169706788, L 189737958, h 169706789
N 440959613, b1 176383846, L 197203134, h 176383845
N 582381937, b2 232952774, L 260449120, h 232952775
N 599074578, b1 239629832, L 267914296, h 239629831
N 703171022, b2 281268408, L 314467641, h 281268409
N 740496902, b2 296198760, L 331160282, h 296198761
N 757189543, b1 302875818, L 338625458, h 302875817
N 794515423, b1 317806170, L 355318099, h 317806169


sqrt(17^2 - 8^2) = h
sqrt(305^2 - 136^2) = h
sqrt(5473^2 - 2448^2) = h
sqrt(98209^2 - 43920^2) = h
sqrt(1762289^2 - 788120^2) = h
sqrt(31622993^2 - 14142232^2) = h
sqrt(567451585^2 - 253772064^2) = h
Any[17.0, 305.0, 5473.0, 98209.0, 1.762289e+06, 3.1622993e+07, 5.67451585e+08]
sum of first 7 L: 1201881742355.734773 seconds (8.63 G allocations: 234.453 GiB, 14.91% gc time)

v2
17
305
5473
98209
1762289
31622993
567451585
Any[17, 305, 5473, 98209, 1762289, 31622993, 567451585]
sum of first 7 L: 1201881742377.812906 seconds (9.08 G allocations: 211.393 GiB, 14.95% gc time)

v4
N = 38, L = 17, N/L = 2.235294
17.0
N = 682, L = 305, N/L = 2.236066
305.0
N = 12238, L = 5473, N/L = 2.236068
5473.0
N = 219602, L = 98209, N/L = 2.236068
98209.0
N = 3940598, L = 1762289, N/L = 2.236068
1.762289e+06
Any[17.0, 305.0, 5473.0, 98209.0, 1.762289e+06]
sum of first 5 L: 1866293  2.268851 seconds (56.75 M allocations: 1.327 GiB, 17.91% gc time)

v5
N = 38, L = 17, N/L = 2.235294
17.0
N = 682, L = 305, N/L = 2.236066
305.0
N = 12238, L = 5473, N/L = 2.236068
5473.0
N = 219602, L = 98209, N/L = 2.236068
98209.0
N = 3940598, L = 1762289, N/L = 2.236068
1.762289e+06
Any[17.0, 305.0, 5473.0, 98209.0, 1.762289e+06]
sum of first 5 L: 1866293  1.889745 seconds (50.10 M allocations: 1.331 GiB, 15.13% gc time)

v5
N = 38, L = 17, x = 21
17.0
N = 682, L = 305, x = 377
305.0
N = 12238, L = 5473, x = 6765
5473.0
N = 219602, L = 98209, x = 121393
98209.0
N = 3940598, L = 1762289, x = 2178309
1.762289e+06
N = 70711162, L = 31622993, x = 39088169
3.1622993e+07
N = 1268860318, L = 567451585, x = 701408733
5.67451585e+08
Any[17.0, 305.0, 5473.0, 98209.0, 1.762289e+06, 3.1622993e+07, 5.67451585e+08]
sum of first 7 L: 600940871
530.845401 seconds (14.03 G allocations: 397.169 GiB, 13.81% gc time)

x pattern

Any[3, 8, 21, 55, 144, 377, 987, 2584, 6765, 17711, 46368, 121393]

v5 with x pattern
sum of first 7 L: 600940871
  0.000874 seconds (108 allocations: 5.805 KiB)
=#
