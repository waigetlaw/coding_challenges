#=
The smallest number m such that 10 divides m! is m = 5.
The smallest number m such that 25 divides m! is m = 10
.
Let s(n) be the smallest number m such that n divides m!.
So s(10) = 5 and s(25) = 10.

Let S(n) be Σs(i) for 2 ≤ i ≤ n.
S(100) = 2012.

Find S(10⁸).
=#

using Printf, Primes, DataStructures

function calcNumberOfTimesDivide(n, div)
    total = 0;
    while mod(n, div) == 0
        total += 1
        n /= div
    end
    return total;
end

function findLowestNumberWithNFactors(base, power)
    total = 0;
    n::Int128 = 0;
    while total < power
        n += base
        # factorization = global factorizationTable[n]
        # if haskey(factorization, base)
        #     total += factorization[base]
        # end
        total += calcNumberOfTimesDivide(n, base)
    end
    return n
end

function s(n)
    factorization = factor(DataStructures.SortedDict, n)
    max_m::Int128 = 1;

    for (prime, power) ∈ factorization
        m = findLowestNumberWithNFactors(prime, power)
        if m > max_m
            max_m = m
        end
    end
    return max_m;
end

function S(n)
    total::Int128 = 0;
    for i ∈ 2:n
        total += s(i)
    end
    return total;
end

@time @show S(10^8);

# calcNumberOfTimesDivide(8, 2)

#=
problem is I need to double count squares, so imagine s(8) which is 2^3

the factorial needed is 4 because 1 x 2 x 3 x 4 = 1 x 2 x 3 x 2 x 2.

But I need to double count 2 @ 4.

So... for 2, I need to know that the total count of 2's are:

1 2 3 4 5 6 7 8 9 10 (products)
0 1 0 2 0 1 0 3 0 1  total = 8x 2's.

How can I count this??

It should know s(2^8) = 10 because there are 8 2's.
Similarly how can I count how many 3's or 5's etc?

If we had a limit 10, we can count 2's by:

10/2 = 5
then 10/2^2 = 2
then 10/2^3 = 1

total 8

how can we go backwards to figure out the max number needed?

if power is < p, I think it's just p * power.

e.g. 3^3 should just be 9. 3, 6, 9 = 4 total

4^4 is just 16.

4, 8, 16 = 4 total

5^5

5, 10, 15, 20, 25 = 6 total

when power > p:

can we subtract?

so imagine 2^8

we know 4 is suffice for 2, then we need 6 more:

orrr can I change the power base??
2^8 = 4^4 so we know the answer is at most 16

v2
S(10 ^ 4) = 10125843
  0.015150 seconds (411.65 k allocations: 49.482 MiB, 22.29% gc time)
10125843

S(10 ^ 6) = 64938007616
  1.905535 seconds (44.64 M allocations: 5.284 GiB, 21.24% gc time)
64938007616

S(10 ^ 8) = 476001479068717
122.876088 seconds (1.69 G allocations: 178.233 GiB, 7.41% gc time)
=#
