#=
Define F(n) to be the number of integers x ≤ n that can be written in the form x = a²b³, where a and b are integers not necessarily different and both greater than 1.

For example, 32 = 2² × 2³ and 72 = 3² × 2³ are the only two integers less than 100 that can be written in this form. Hence, F(100) = 2.

Further you are given F(2 × 10⁴) = 130 and F(3 × 10⁶) = 2014.

Find F(9 × 10¹⁸).
=#

#=
Thoughts: Few ways to solve this - one could be calculating the prime factors, and making sure every prime factor is a power of 2 or 3.

This would work because we can always 'join' same powers together in the end to reduce it to the form a²b³.

E.g. 2^2 * 5^2 * 7^3 * 11^3 would be same as (2*5)^2 * (7*11)^3 = 10^2 * 77^3.

However, I still think this is too slow as we go up to 10^18...

Other way is to just divide by cubes and check the remainder is square. Even this will take a long time though as there are so many cubes - 1 to 3million.

There must be a way where we don't have to loop through each number and check...

perhaps find all combinations?

2^2 * x^3 and find all x that is within limit
=#

using Printf, FastPrimeSieve, Primes, DataStructures

include("./utils/binarysearch.jl")
include("./utils/getdivisors.jl")

function problem634(limit)

    a::Int128 = 2;
    b::Int128 = 2;

    numbers = [];

    while a^3 * b^2 <= limit
        a3::Int128 = a^3;
        a3b2::Int128 = a3 * b^2;
        while a3b2 <= limit
            push!(numbers, a3b2)
            b += 1;
            a3b2 = a3 * b^2;
        end
        a += 1;
        b = 2;
    end
    println(sort(unique(numbers)))
    return length(numbers)
end

function problem634log(limit)

    a::Int128 = 2;
    b::Int128 = 2;

    numbers = [];

    while a^3 * b^2 <= limit
        a3::Int128 = a^3;
        a3b2::Int128 = a3 * b^2;
        while a3b2 <= limit
            push!(numbers, a3b2)
            b += 1;
            a3b2 = a3 * b^2;
        end
        a += 1;
        b = 2;
    end
    return numbers
end

function problem634v2(limit)
    cubes = [];
    cube::Int128 = 2;

    while cube <= limit
        push!(cubes, cube^3)
        cube += 1
    end

    total::Int128 = 0;

    for cube in cubes
        remaining::Int128 = fld(limit, cube);
        sqr = floor(sqrt(remaining));
        if sqr >= 2
            total += sqr - 1;
        end
    end

    return total;
end

#=
Given b, it should find factors and simply the form b * sqrt(b) as much as possible:

e.g. b = 12:

12 * sqrt(12) = 12 * sqrt(4)sqrt(3) = 12 * 2 * sqrt(3) = (24, 3)
=#
function simp(b)
    factors = factor(Dict, b)
    mult = b;
    sq = 1;
    for (p, power) in factors
        # @printf "p %i, power %i\n" p power
        sqrpower, rem = divrem(power, 2)
        mult *= p^sqrpower
        sq *= p*rem
    end
    return (mult, sq)
end

# @time @show simp(5353434)

function problem634v3(limit)

    a::Int128 = 2;
    b::Int128 = 2;

    numbers = Set();
    # numbers = Dict();

    loglimit = log(limit)
    # loglimit = limit

    while 2log(a) + 3log(b) <= loglimit
        a2 = 2log(a);
        # a2b3::Float64 = a2 + 2log(b)
        a2b3 = a^2 * b^3
        while 2log(a) + 3log(b) <= loglimit
            (m, sq) = simp(b)
            push!(numbers, (a*m, sq))
            # push!(numbers, string(a*m)*"sqrt"*string(sq)*"a2b3="*string(a^2 * b^3))
            # if haskey(numbers, a2b3)
            #     push!(numbers[a2b3], string(a*m)*"sqrt"*string(sq)*"a2b3="*string(a)*" "*string(b))
            # else
            #     numbers[a2b3] = [string(a*m)*"sqrt"*string(sq)*"a2b3="*string(a)*" "*string(b)]
            # end
            b += 1;
            # a2b3 = a2 + 3log(b);
            a2b3 = a^2 * b^3
        end
        a += 1;
        b = 2;
    end
    # println(numbers)
    return length(numbers)
end


# find a list of b's that do not simplify sqrt any further (i.e. have no squares in them such as 2x2x3)
function problem634v4(limit)
    cubelimit::Int128 = ceil(sqrt(cld(limit, 2^2)));

    primes::Array{Int128} = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(cubelimit)));

    println("primes calced")
    bs = copy(primes);
    for (idx, p) in enumerate(primes)
        if p * p > cubelimit
            break
        end
        for p2 in @view primes[idx:end]
            pp2 = p*p2
            if pp2 <= cubelimit
                push!(bs, pp2)
            else
                break
            end
        end
    end

    println("bs calced")

    bs = sort(bs)

    maxSqr = cld(limit, 8) + 1;
    loglimit = log(limit)
    total = 0;
    numbers = []

    for a in 2:maxSqr
        for b in bs
            if a^2 * b^3 <= limit
                push!(numbers, (a^2 * b^3, a , b))
                total += 1;
            else
                # break;
            end
        end
    end
    println(sort(numbers))
    println(total)

end

function simpv2(b)
    factors = factor(Dict, b)
    mult = b;
    sq = 1;
    for (p, power) in factors
        # @printf "p %i, power %i\n" p power
        sqrpower, rem = divrem(power, 2)
        mult *= p^sqrpower
        sq *= p^rem
    end
    return (mult, sq)
end

function problem634v5(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));
    bs = map(b -> simpv2(b) , 2:blimit)
    # for b in bs
    #     println(b)
    # end

    numbers::Set{Tuple{Int128, Int128}} = Set();

    println("bs calculated")

    for a::Int128 in 2:alimit
        if mod(a, 1000000) == 0 @printf "progress: %1.2f\n" 100*a/alimit end
        if (a*bs[1][1])^2 * bs[1][2] > limit
            break
        end
        idx = binarysearchfirst(
            bs,
            b -> (a*b[1])^2 * b[2] > limit ? 0 : -1
        );
        for (m, b) in @view bs[1:idx - 1];
            push!(numbers, (a*m, b))
        end
    end

    # println(sort(unique(numbers)))
    # println(length(unique(numbers)))
    return length(numbers)
end

function problem634v6(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));
    loglimit = log(limit);

    numbers = Set();

    A = map(a-> (a, factor(Vector, a)), 2:alimit)
    B = map(b-> (b, factor(Vector, b)), 2:blimit)

    for (a, afactors) in A
        for (b, bfactors) in B
            if 2log(a) + 3log(b) <= loglimit
                push!(numbers, prod(vcat(afactors, afactors, bfactors, bfactors, bfactors)))
                # println(sort(vcat(afactors, bfactors)))
                # push!(numbers, a^2 * b^3)
            else
                break
            end
        end
    end
    # println(numbers)
    return length(numbers)
end

function problem634v7(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));

    B::Array{Int128} = map(b -> b^3, 2:blimit)

    numbers::Set{Int128} = Set();

    for a::Int128 in 2:alimit
        a2 = a^2;
        for b3::Int128 in B
            a2b3::Int128 = a2 * b3;
            if a2b3 <= limit
                push!(numbers, a2b3)
                # numbers[string(a2b3)] = true;
            else
                break
            end
        end
    end
    # println(numbers)
    return length(numbers)
end

# sqrt of the num should still be unique
# but instead of a^2b^3 it would become
function problem634v8(limit)
    alimit::Int32 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int32 = ceil(cbrt(cld(limit, 2^2)));

    B = map(b -> simpv2(b), 2:blimit)
    println(B)


    # B = [(2, (2, 2)), (3, (3, 3)), (4, (8, 1)), (5, (5, 5)), (6, (6, 6)), (7, (7, 7)), (8, (16, 2)), (9, (27, 1)), (10, (10, 10)), (11, (11, 11)), (12, (24, 3)), (13, (13, 13)), (14, (14, 14)), (15, (15, 15)), (16, (64, 1)), (17, (17, 17)), (18, (54, 2))]
    B = [(2, 2), (3, 3), (8, 1), (5, 5), (6, 6), (7, 7), (16, 2), (27, 1), (10, 10), (11, 11), (24, 3), (13, 13), (14, 14), (15, 15), (64, 1), (17, 17), (54, 2)]

    mbs = Dict();

    for (m, b) in mbs
        if haskey(mbs, b)
            push!(mbs[b], m)
        else
            mbs[b] = [m]
        end
    end

    # remove same b's but m < m_min^2?

    numbers = Dict();

    # println(alimit)
    # println(blimit)

    total = 0;
    for a::Int32 in 2:alimit
        # a2::Int128 = a^2;
        for (m, b) in B
            a2b3::Int128 = (a*m)^2 * b;
            if a2b3 <= limit
                if haskey(numbers, a2b3)
                    push!(numbers[a2b3], (a, m, b))
                else
                    numbers[a2b3] = [(a,m,b)]
                end
                total += 1;
            else
                break
            end
        end
    end
    println(numbers)
    # return length(numbers)
    return total
end

function problem634v9(limit)
    alimit::Int32 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int32 = ceil(cbrt(cld(limit, 2^2)));

    B::Array{Tuple{Int128, Int128}} = map(b -> simp(b), 2:blimit)

    println(B)
    numbers::Set{Int128} = Set();

    # a = 2;
    # nums = [];
    # total = 0;
    # while true
    #     # (m1, b1) = B[1]
    #     if a^2 > limit break end
    #     for b3 in B
    #         if a < 4 && b3 == 1 continue end
    #         a2b3::Int128 = a^2 * b3
    #         # nums += 1
    #         if a2b3 <= limit
    #             push!(nums, a2b3)
    #             @printf "%i^2 * %i^3 = %i\n" a cbrt(b3) a2b3
            
    #             total += 1
    #             # numbers[string(a2b3)] = true;
    #         else
    #             break
    #         end
    #     end
    #     a += 1;
    # end
    # # println(sort(nums))
    # return total
end

# @time @show problem634v5(2*10^4);
# @time @show problem634v3(9*10^12);

# @time @show problem634log(3*10^6);
# @time @show problem634v4(3*10^6);
# @time @show problem634v8(2*10^4);
# @show simpv2(4)

# attempts to reduce b down from b^3 to a^2c^3 where c < b
function splitb(b)
    divs = @view sort(collect(getdivisors(b)))[2:end-1]
    d_idx = findfirst(d -> isinteger(sqrt(d)), divs)
    if d_idx !== nothing
        return divs[d_idx]
    else
        return nothing
    end
end

function problem634v10(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));

    B::Array{Int128} = map(b -> b^3, filter(b -> begin
        a::Union{Int128, Nothing} = splitb(b);
        if a === nothing 
            return true 
        end
        return false;
        # a = a*round(sqrt(a));
        # return a + 2 > alimit;
    end, 2:blimit))

    total = 0;

    for a::Int128 in 2:alimit
        a2 = a^2;
        for b3::Int128 in B
            a2b3::Int128 = a2 * b3;
            if a2b3 <= limit
                total += 1
            else
                break
            end
        end
    end
    return total
end


# @time @show problem634v10(3*10^6);

function canbereduced(b)
    factors = factor(Dict, b)
    println(factors)
    if length(factors) == 1
        (p, power) = collect(factors)[1]
        a = p;
        if power >= 3
            a *= p;
            return a
        end
        return false
    end
    for (p, power) in factors
        if power >= 2
            return p^2
        end
    end
    return false
end

# @show canbereduced(56)

function problem634v11(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));

    B::Array{Int128} = filter(b3 -> begin

    
    a = canbereduced(b3)

    if a !== false
        minb = cbrt(b3^3 ÷ a^2);
        @printf "%i^3 = %i, can be reduced to: %i^2 * %i^3 = %i\n" b3 b3^3 a minb a^2 * minb^3
        return a + 2 > alimit
    end
    return true
    end, map(b -> b, 2:blimit))

    println(B)
    B = map(b -> b^3, B)
    total = 0

    nums = Dict();

    for a::Int128 in 2:alimit
        a2 = a^2;
        for b3::Int128 in B
            a2b3::Int128 = a2 * b3;
            if a2b3 <= limit

                if haskey(nums, a2b3)
                    push!(nums[a2b3], (a, Int32(cbrt(b3))))
                else
                    nums[a2b3] = [(a, Int32(cbrt(b3)))]
                end
                total += 1
            else
                break
            end
        end
    end
    println(nums)
    return total
end

function getSmallestSquareKeepingCube(factors)
    potentialSquares = [];
    mustHave = 1;

    for (base, power) in factors
        isMustHave = false;
        if mod(power, 3) != 0
            isMustHave = true
        end

        power -= 2;
        sq = base^2;
        while mod(power, 3) != 0 power > 0
            power -= 2;
            sq *= base^2;
        end

        if power >= 0 && mod(power, 3) == 0
            # @printf "must have? %s, potential %i, power left: %i\n" isMustHave sq power
            if isMustHave
                mustHave *= sq
            else
                push!(potentialSquares, sq)
            end
        end
    end

    if mustHave > 1
        return mustHave
    elseif length(potentialSquares) > 0
        return minimum(potentialSquares);
    else
        return nothing
    end
end


function isSquareSmaller(factors, a2)
    smallestSquare::Union{Nothing, Int128} = nothing;
    mustHave::Int128 = 1;

    for (base, power) in factors
        if mustHave > a2
            return false
        end
        isMustHave::Bool = mod(power, 3) != 0

        base2::Int128 = base^2
        power -= 2;
        sq = base2;
        while mod(power, 3) != 0 && power > 1
            power -= 2;
            sq *= base2;
        end

        if mod(power, 3) == 0
            if isMustHave
                mustHave *= sq
            elseif smallestSquare === nothing || sq < smallestSquare
                smallestSquare = sq;
            end
        end
    end

    if mustHave > 1
        return mustHave < a2
    else
        return smallestSquare < a2
    end
end

d = DataStructures.SortedDict()
d[2] = 13;
d[19] = 2;

# @show getSmallestSquareKeepingCube(d)

function problem634v12(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));

        # A = map(a -> (a^2, factor(SortedDict, a^2)), 2:alimit);
        # B = map(b -> (b^3, factor(SortedDict, b^3)), 2:blimit);

    A::Array{Tuple{Int128, Dict{Int64, Int64}}} = map(
        a -> begin 
            afactors = factor(Dict, a)
            map!(a->2a, values(afactors))
            return (a^2, afactors)
        end,
        2:alimit);
    B::Array{Tuple{Int128, Dict{Int64, Int64}}} = map(
        b -> begin 
            bfactors = factor(Dict, b)
            map!(b->3b, values(bfactors))
            return (b^3, bfactors)
        end,
        2:blimit);

    # println(A)
    # println(Av2)

    # nums = DataStructures.SortedDict();
    total::Int128 = 0;
    for (a2, afactors) in A
        for (b3, bfactors) in B
            a2b3::Int128 = a2 * b3;
            if a2b3 > limit
                break
            end
            factors::Dict{Int64, Int64} = mergewith(+, afactors, bfactors)
            # @printf "factors: %s\n" factors
            minsquare = getSmallestSquareKeepingCube(factors)
            # shouldn't be nothing
            if minsquare < a2
                # @printf "SKIPPING: %i^2 * %i^3 can be written as %i^2 * %i^3\n" sqrt(a2) cbrt(b3) sqrt(minsquare) cbrt((a2*b3)/minsquare)
                continue
            end

            total += 1;
        end
    end

    # println(nums)
    return total;
end

function problem634v13(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));

    A::Array{Tuple{Int128, Dict{Int64, Int64}}} = map(
        a -> begin 
            afactors = factor(Dict, a)
            map!(a->2a, values(afactors))
            return (a^2, afactors)
        end,
        2:alimit);
    B::Array{Tuple{Int128, Dict{Int64, Int64}}} = map(
        b -> begin 
            bfactors = factor(Dict, b)
            map!(b->3b, values(bfactors))
            return (b^3, bfactors)
        end,
        2:blimit);

    println("Calced A and B")
    total::Int128 = 0;
    for (a2, afactors) in A
        for (b3, bfactors) in B
            if a2 * b3 > limit
                break
            end
            factors::Dict{Int64, Int64} = mergewith(+, afactors, bfactors)
            if !isSquareSmaller(factors, a2)
                total += 1;
            end
        end
    end

    # println(nums)
    return total;
end

function problem634v14(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));

    main = map(a -> (a, factor(Dict, a)), 2:alimit);
    B = @view main[1:blimit];

    # A = map(x -> begin
    #     afactors = Dict();
    #     for (k, v) in x[2]
    #         afactors[k] = 2v
    #     end
    #     return (x[1]^2, afactors)
    # end, @view main[1:end]);

    # B = map(x -> begin
    #     bfactors = Dict();
    #     for (k, v) in x[2]
    #         bfactors[k] = 3v
    #     end
    #     return (x[1]^3, bfactors)
    # end, @view main[1:blimit]);

    println("Calced A and B")
    total::Int128 = 0;
    for (a, amainfactors) in main
        a2 = a^2;
        afactors = Dict();
        for (k, v) in amainfactors
            afactors[k] = 2v
        end
        for (b, bmainfactors) in B
            b3 = b^3;

            if a2 * b3 > limit
                break
            end
            bfactors = Dict();
            for (k, v) in bmainfactors
                bfactors[k] = 3v
            end
            factors::Dict{Int64, Int64} = mergewith(+, afactors, bfactors)
            if !isSquareSmaller(factors, a2)
                total += 1;
            end
        end
    end

    # println(nums)
    return total;
end

# @time @show problem634v14(9*10^18);
# @time @show problem634v11(3*10^6);

function problem634v15(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));

    A::Array{Int128} = map(a -> a^2, 2:alimit);
    B::Array{Int128} = map(b -> b^3, 2:blimit);

    nums::Set{Int128} = Set();
    for a2 in A
        for b3 in B
            a2b3::Int128 = a2*b3;
            if a2b3 > limit break end
            push!(nums, a2b3)
        end
    end
    return length(nums);
end

# @time @show problem634v15(9*10^12);


function problem634v16(limit)
    alimit::Int128 = ceil(sqrt(cld(limit, 2^3)));
    blimit::Int128 = ceil(cbrt(cld(limit, 2^2)));

    # (base, a^2, min cube, min cube all)
    A = map(a -> a^2, 2:alimit);
    B = map(b -> (b^3, getSmallestSquareKeepingCube(factor(Dict, b))), 2:blimit);

    total::Int128 = 0
    for a2 in A
        for (b3, minsquare) in B
            if minsquare !== nothing && minsquare < a2 continue end
            if a2*b3 > limit break end
            total += 1
        end
    end
    return total
end

@time @show problem634v16(3*10^6);

#=
v1
problem634(3000000) = 2014
  0.074193 seconds (24.88 k allocations: 1.115 MiB)

problem634(9 * 10 ^ 12) = 3980422
1.316921 seconds (15.77 M allocations: 466.619 MiB, 48.45% gc time)
3980422

problem634(9 * 10 ^ 13) = 12631162
5.748430 seconds (59.81 M allocations: 1.784 GiB, 47.14% gc time)

problem634(9 * 10 ^ 14) = 40037997
 13.439951 seconds (137.82 M allocations: 4.566 GiB, 33.47% gc time)

10^18 is taking longer than 6 hours...

2*10^4:
[32, 72, 108, 128, 200, 243, 256, 288, 392, 432, 500, 512, 576, 648, 675, 800, 864, 968, 972, 1024, 1125, 1152, 1323, 1352, 1372, 1568, 1600, 1728, 1800, 1944, 2000, 2048, 2187, 2304, 2312, 2592, 2700, 2888, 2916, 3087, 3125, 3136, 3200, 3267, 3456, 3528, 3872, 3888, 4000, 4096, 4232, 4500, 4563, 4608, 5000, 5184, 5292, 5324, 5400, 5408, 5488, 5832, 6075, 6125, 6272, 6400, 6561, 6728, 6912, 7200, 7688, 7744, 7776, 7803, 8000, 8192, 8575, 8712, 8748, 8788, 9000, 9216, 9248, 9747, 9800, 10125, 10368, 10584, 10800, 10816, 10952, 10976, 11552, 11664, 11907, 11979, 12168, 12348, 12500, 12544, 12800, 13068, 13448, 13500, 13824, 14112, 14283, 14400, 14792, 15125, 15488, 15552, 16000, 16200, 16384, 16807, 16875, 16928, 17496, 17672, 18000, 18225, 18252, 18432, 18496, 19208, 19652, 19683, 19773, 20000]

v5

bs calculated
problem634v5(9 * 10 ^ 12) = 3980422
  5.759389 seconds (79.11 M allocations: 2.894 GiB, 40.77% gc time, 1.44% compilation time)
3980422

bs calculated
problem634v5(9 * 10 ^ 13) = 12631162
 15.386795 seconds (260.40 M allocations: 9.483 GiB, 31.43% gc time, 0.56% compilation time)
12631162

bs calculated
problem634v5(9 * 10 ^ 14) = 40037997
 53.864913 seconds (773.13 M allocations: 28.191 GiB, 36.06% gc time, 0.15% compilation time)

v7 (essentially v1 but slightly more optimised)
problem634v7(9 * 10 ^ 14) = 40037997
  4.439676 seconds (144 allocations: 2.281 GiB, 0.40% gc time, 0.06% compilation time)
problem634v7(9 * 10 ^ 15) = 126815290
 18.660184 seconds (102 allocations: 8.499 GiB, 1.15% gc time)

v8 Dict{Int128, Bool}
problem634v7(9 * 10 ^ 12) = 3980422
  0.372615 seconds (135 allocations: 285.207 MiB, 3.02% gc time, 0.74% compilation time)
3980422

problem634v7(9 * 10 ^ 14) = 40037997
  4.776259 seconds (103 allocations: 2.248 GiB, 0.34% gc time)

v12 - slower but no Set
problem634v12(9 * 10 ^ 12) = 3980422
  8.807559 seconds (144.88 M allocations: 15.979 GiB, 30.62% gc time)

  actually faster now after some optimising!
problem634v12(9 * 10 ^ 12) = 3980422
  3.117332 seconds (44.38 M allocations: 5.084 GiB, 31.67% gc time)

v13 slight more optimisation
Calced A and B
problem634v13(9 * 10 ^ 12) = 3980422
  2.656408 seconds (31.95 M allocations: 4.398 GiB, 24.34% gc time)
3980422

Calced A and B
problem634v13(9 * 10 ^ 13) = 12631162
 16.959903 seconds (101.16 M allocations: 13.921 GiB, 48.92% gc time)
=#
