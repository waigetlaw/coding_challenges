#=
A positive fraction whose numerator is less than its denominator is called a proper fraction.
For any denominator, d, there will be d - 1 proper fractions; for example, with d = 12:
1/12, 2/12, 3/12, 4/12, 5/12, 6/12, 7/12, 8/12, 9/12, 10/12, 11/12.

We shall call a fraction that cannot be cancelled down a resilient fraction.
Furthermore we shall define the resilience of a denominator, R(d), to be the ratio of its proper fractions that are resilient; for example, R(12) = 4/11.
In fact, d = 12 is the smallest denominator having a resilience R(d) < 4/10.

Find the smallest denominator d, having a resilience R(d) < 15499/94744.
=#

using Printf, Primes, DataStructures

function problem243(resilience::Rational)
    R_d = 1;
    d = 1;
    smallestR_d = 1;
    smallestD = 1;
    while R_d ≥ resilience
        d += 1;
        primeDivisors = collect(factor(Set, d));
        r = 0;
        for n ∈ 1:d-1
            divisor = findfirst(div -> mod(n, div) == 0, primeDivisors)
            # @printf "For d = %i, n: %i, divisor: %s\n" d n divisor
            if divisor === nothing
                r += 1;
            end
        end
        R_d = r//(d-1)
        if R_d < smallestR_d
            smallestR_d = R_d
            smallestD = d
            @printf "New smallest R(%i) = %s\n" d R_d
        end
        # @printf "For d = %i, R(d) = %s\n" d R_d
    end

    @printf "For d = %i, R(d) = %s\n" d R_d
end

function problem243v2(resilience::Rational)
    R_d = 1;
    d = UInt128(1);
    smallestR_d = 1//1;
    smallestD = UInt128(1);
    while R_d ≥ resilience
        if mod(d, 1000000) == 0
            # @printf "For d = %i, R(d) = %s\n" d R_d
            @printf "Smallest R(d): %f at d = %i\n" smallestR_d smallestD
        end
        d += 1;
        f = collect(factor(Set, d));
        bot = d - 1;
        r = 0;
        for i ∈ eachindex(f)
            factor = f[i]
            timesFactorFits = fld(bot, factor)
            r += timesFactorFits
            for prevF ∈ f[1:i - 1]
                r -= fld(timesFactorFits, prevF)
            end
        end
        R_d = (bot - r)//bot

        # @printf "For d = %i, R(d) = %s\n" d R_d
        if R_d < smallestR_d
            smallestR_d = R_d
            smallestD = d
        end
    end

    @printf "For d = %i, R(d) = %s\n" d R_d
    # println(smallestR_d)
end

function problem243v3(resilience::Rational)
    R_d = 1;
    d = big(12);
    while R_d ≥ resilience && d < 10^20
        if mod(d, 12 * 10^6) == 0
            @printf "For d = %i, R(d) = %f\n" d R_d
            println(d)
        end
        d += 12;
        f = collect(factor(Set, d));
        bot = d - 1;
        r = 0;
        for i ∈ eachindex(f)
            factor = f[i]
            timesFactorFits = fld(bot, factor)
            r += timesFactorFits
            for prevF ∈ f[1:i - 1]
                r -= fld(timesFactorFits, prevF)
            end
        end
        R_d = (bot - r)//bot

        # @printf "For d = %i, R(d) = %s, %f\n" d R_d R_d
        # if R_d < smallestR_d
        #     smallestR_d = R_d
        # end
    end

    @printf "For d = %i, R(d) = %s\n" d R_d
    # println(smallestR_d)
end

function R(d)
    R_d = 1;

    f = collect(factor(Set, d));
    bot = d - 1;
    r = 0;
    for i ∈ eachindex(f)
        factor = f[i]
        timesFactorFits = fld(bot, factor)
        r += timesFactorFits
        for prevF ∈ f[1:i - 1]
            r -= fld(timesFactorFits, prevF)
        end
    end
    R_d = (bot - r)//bot

    # @printf "For d = %i, R(d) = %s, %f\n" d R_d R_d
    @printf "For d = %i, f = %s, top = %i, R(d) = %s" d f bot-r R_d
end

function Rv1(d)
    primeDivisors = collect(factor(Set, d));
    r = 0;
    for n ∈ 1:d-1
        divisor = findfirst(div -> mod(n, div) == 0, primeDivisors)
        if divisor === nothing
            r += 1;
        end
    end
    R_d = r//(d-1)
    @printf "For d = %i, R(d) = %s\n" d R_d
end

function Rv2(d)
    f = collect(factor(Set, d));
    bot = d - 1;
    r = 0;
    for i ∈ eachindex(f)
        factor = f[i]
        timesFactorFits = fld(bot, factor)
        r += timesFactorFits
        for prevF ∈ f[1:i - 1]
            r -= fld(timesFactorFits, prevF)
        end
    end
    R_d = (bot - r)//bot
    @printf "For d = %i, R(d) = %s\n" d R_d
end

# @time Rv1(210)
# @time Rv2(210)

function problem243v4(resilience::Rational)
    p = primes(100)
    R_d = 1;
    d = 1;
    index = 1;
    smallestR_d = 1;
    smallestD = 1;
    println(length(p))
    while R_d ≥ resilience && index < length(p)
        d *= p[index];
        index += 1
        primeDivisors = collect(factor(Set, d));
        r = 0;
        for n ∈ 1:d-1
            divisor = findfirst(div -> mod(n, div) == 0, primeDivisors)
            # @printf "For d = %i, n: %i, divisor: %s\n" d n divisor
            if divisor === nothing
                r += 1;
            end
        end
        R_d = r//(d-1)
        if R_d < smallestR_d
            smallestR_d = R_d
            smallestD = d
            @printf "New smallest R(%i) = %s, %f\n" d R_d R_d
        end
        @printf "For d = %i, R(d) = %s\n" d R_d
    end

    @printf "For d = %i, R(d) = %s\n" d R_d
end


function problem243v5(resilience::Rational)
    R_d = 1;
    d = 863272410;
    smallestR_d = 1;
    smallestD = 1;
    while R_d ≥ resilience
        d += 9699690
        primeDivisors = collect(factor(Set, d));
        r = 0;
        for n ∈ 1:d-1
            divisor = findfirst(div -> mod(n, div) == 0, primeDivisors)
            # @printf "For d = %i, n: %i, divisor: %s\n" d n divisor
            if divisor === nothing
                r += 1;
            end
        end
        R_d = r//(d-1)
        if R_d < smallestR_d
            smallestR_d = R_d
            smallestD = d
            @printf "New smallest R(%i) = %s, %f\n" d R_d R_d
        end
        # @printf "For d = %i, R(d) = %s\n" d R_d
    end

    @printf "For d = %i, R(d) = %s\n" d R_d
end

# @time problem243v4(15499//94744)

function Rv3(d)
    sieve = ones(d - 1, 1);
    factors = factor(Set, d);
    for factor in factors
        for idx in factor:factor:length(sieve)
            # println(idx)
            sieve[idx] = 0;
        end
    end
    top = count(i -> i > 0, sieve);
    # for idx in eachindex(sieve)
    #     @printf "%i/%i is resilient: %s\n" idx d sieve[idx] == 1 ? "Yes" : "No"
    # end
    R_d = top//(d-1)
    println(R_d)
    return R_d
end



# @time Rv1(669278610);
# @time Rv3(669278610);

# @time problem243v5(15499//94744)
# @time problem243v2(15499//94744)

include("./utils/totient.jl")

function problem243v6(resilience)
    R_d = 1;
    d = 30030;
    smallestR_d = 1;
    smallestD = 1;
    while R_d ≥ resilience
        d += 30030
        R_d = totient(d)//(d-1)
        if R_d < smallestR_d
            smallestR_d = R_d
            smallestD = d
            @printf "New smallest R(%i) = %s, %f\n" d R_d R_d
        end
        # @printf "For d = %i, R(d) = %s\n" d R_d
    end

    @printf "For d = %i, R(d) = %s\n" d R_d
end

@time problem243v6(15499//94744)

# function list()
#     p = primes(150)
#     d = UInt128(1)
#     for prime ∈ p
#         d *= prime
#         @time(R(d))
#     end
# end

# function tau(n)
#     factors = factor(DataStructures.SortedDict, n)

#     println(factors)

# end

# tau(12)

# R(30*7)
#=
for d = 12:

2, 3, 4, 6, 8, 9, 10 are simplifiable because they divide by 2 or 3.

how many 2's fit into 11?

5 do.
2, 4, 6, 8, 10

how many 3's fit into 11?
3 do.
3, 6, 9

now 6 overlaps... so we get 2, 3, 4, 6, 8, 9, 10 that are simplifiable.

how do we ignore the overlap?

we have to minus every 2nd number, so we - fld(sols, 2)

but what if there was another number like 5?

2 x 3 x 5 = 30

2, 3, 4, 5, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28,

21 total.

2 fits into 29, 14 times.
3 fits into 29 9 times.
5 fits into 29 5 times.

How about trying to count the non cancel?

1, 7, 11, 13, 17, 19, 23, 29

So it's just simply all the primes from 1 to d, minus the factors.

for d = 5

1, 2, 3, 4 don't fit...

primes are 2, 3

d factor = 5.

so by prev logic, only resilience are primes up to 5, not including 5 = 2, 3. and +1 for 1. but we are missing 4...

how can we count overlaps? Seems to be better that way... going back:
2 x 3 x 5 = 30

2, 3, 4, 5, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28,

21 total.

2 fits into 29, 14 times.
3 fits into 29 9 times.
5 fits into 29 5 times.

can we say from 3, we need to remove 9/2 = 4
and from 5 we need to remove 5/2 and 5/3 = 2 + 1 = 3

so we get 14 + 9 - 4 + 5 - 3 = 14 + 5 + 2 = 21

then we get 8 as resilience by doing 30 - 21 - 1 = 8

how do we minimise the resilience?

v1
For d = 2, R(d) = 1//1
For d = 3, R(d) = 1//1
For d = 4, R(d) = 2//3
For d = 5, R(d) = 1//1
For d = 6, R(d) = 2//5
For d = 7, R(d) = 1//1
For d = 8, R(d) = 4//7
For d = 9, R(d) = 3//4
For d = 10, R(d) = 4//9
For d = 11, R(d) = 1//1
For d = 12, R(d) = 4//11
For d = 12, R(d) = 4//11
  0.000477 seconds (294 allocations: 16.781 KiB)

  tau function...
     for 12, factors are 2, 2, 3

    ignoring 1 and 12, therefore:

    2: 2
    3: 3
    4: 2 x 2
    6: 2 x 3
    8: ??
    9: ??
    10: ??

    divisors only go to 6... how do we get the rest?

    for each prime...

    set 2 + set 3 - set 6

    6 + 4 - 2 = 8

    what about 30?

    set 2 + set 3 + set 5 = 15 + 10 + 6 = 31

    minus

    set 6, set, 10, set 15 = 5, 3, 2 = 10 -> 21.

    set 2 + set 3 + set 5 + set 7 = 105 + 70 + 42 + 30 = 247
    minus
    set 6 = 35
    set 10 = 21
    set 14 = 15
    set 15 = 14
    set 21 = 10
    set 35 = 6

    = 101

    set 30 = 7
    set 42 = 5
    set 70 = 3
    set 105 = 2

    = 17 = 118

    129 so 210 - 129 = 81 wrong.

    for 30:

    2x3x5, therefore we can split this into groups of 6:

    1/6, 2/6, 3/6, 4/6, 5/6

    yes, no, no, no, yes so every 6, has 2.


    therefore 12?

    but this would imply 10/12 is not divisible, which is wrong.

    can we do with a sieve?

    1 to 30

    remove 2's
    remove 3's
    remove 5's

    1, 7, 11, 13, 17, 19, 23, 29

    I think we can sieve to get this...


    Update, bug was found in later versions... going back to v1 should solve it!

    from v4, doing d = *= primes
    we know the answer is between d = 223092870 and d = 6469693230

    now from 669278610

New smallest R(223603380) = 39813120//223603379, 0.178052
New smallest R(226666440) = 39813120//226666439, 0.175646
New smallest R(232792560) = 39813120//232792559, 0.171024
New smallest R(242492250) = 41472000//242492249, 0.171024
New smallest R(252191940) = 43130880//252191939, 0.171024
New smallest R(261891630) = 44789760//261891629, 0.171024
New smallest R(271591320) = 46448640//271591319, 0.171024
New smallest R(281291010) = 46448640//281291009, 0.165127
New smallest R(446185740) = 72990720//446185739, 0.163588
New smallest R(669278610) = 109486080//669278609, 0.163588

For d = 892371480, R(d) = 145981440//892371479
168.095982 seconds (138.87 k allocations: 9.471 MiB, 0.04% compilation time)
=#
