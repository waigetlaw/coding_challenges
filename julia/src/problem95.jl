#=
The proper divisors of a number are all the divisors excluding the number itself. For example, the proper divisors of 28 are 1, 2, 4, 7, and 14. As the sum of these divisors is equal to 28, we call it a perfect number.

Interestingly the sum of the proper divisors of 220 is 284 and the sum of the proper divisors of 284 is 220, forming a chain of two numbers. For this reason, 220 and 284 are called an amicable pair.

Perhaps less well known are longer chains. For example, starting with 12496, we form a chain of five numbers:

12496 -> 14288 -> 15472 -> 14536 -> 14264 (-> 12496 -> ...)

Since this chain returns to its starting point, it is called an amicable chain.

Find the smallest member of the longest amicable chain with no element exceeding one million.
=#

using Primes, Combinatorics, Printf

function sumDivisors(n)
    factors = factor(Vector, n)
    return 1 + sum(map(f -> prod(f), unique(powerset(factors, 1, length(factors) - 1))))
end

function problem95()
    limit = 10^6;

    nextChain = [0]
    for n ∈ 2:limit
        push!(nextChain, sumDivisors(n))
    end

    chainLengths = []
    for (n, next) ∈ enumerate(nextChain)
        if n < 2 || n > limit
            push!(chainLengths, 0);
            continue
        end
        chainLength = 1;
        isChain = true;
        # assume no chain bigger than 1000
        while next !== n && chainLength < 1000
            if next < 2 || next > limit
                isChain = false;
                break;
            end
            # infinite loop, will never go back to n but stuck
            if nextChain[next] === next
                isChain = false;
                break;
            end
            
            chainLength += 1
            next = nextChain[next]
        end
        if isChain && chainLength < 1000
            push!(chainLengths, chainLength);
        else
            push!(chainLengths, 0);
        end
    end

    max = maximum(chainLengths)
    n = findfirst(x -> x === max, chainLengths)

    @printf "The number %i is an amicable chain of %i.\n" n max
    return n
end

@time @show problem95();

#=
The number 14316 is an amicable chain of 28.
problem95() = 14316
  5.094942 seconds (128.22 M allocations: 8.820 GiB, 14.26% gc time)
=#
