#=
Let ϕ be Euler's totient function, i.e. for a natural number n, ϕ(n) is the number of k, 1 ≤ k ≤ n, for which gcd(k, n) = 1.

By iterating ϕ, each positive integer generates a decreasing chain of numbers ending in 1.
E.g. if we start with 5 the sequence 5, 4, 2, 1 is generated.
Here is a listing of all chains with length 4:

5, 4, 2, 1
7, 6, 2, 1
8, 4, 2, 1
9, 6, 2, 1
10, 4, 2, 1
12, 4, 2, 1
14, 6, 2, 1
18, 6, 2, 1
 
Only two of these chains start with a prime, their sum is 12.

What is the sum of all primes less than 40 000 000 which generate a chain of length 25?
=#
using Printf, FastPrimeSieve

include("./utils/totient.jl")

function problem241(chain, limit)
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(limit)));
    primeChains = [0];

    for p ∈ primes
        chainCount = 2;
        ϕₙ = totient(p)
        # @printf "For p %i, ϕₙ = %i.\n" p ϕₙ
        while ϕₙ > 1 && chainCount ≤ chain
            ϕₙ = totient(ϕₙ)
            chainCount += 1
            # @printf "chain: %i, ϕₙ = %i.\n" chainCount ϕₙ
        end
        if ϕₙ == 1 && chainCount == chain
            push!(primeChains, p)
        end
    end

    @printf "The sum of all primes less than %s which generate a chain of length %s is %i.\n" limit chain sum(primeChains)
end

@time problem241(25, 40000000);

#=
The sum of all primes less than 40000000 which generate a chain of length 25 is 1677366278943.
 27.944575 seconds (257.88 M allocations: 35.096 GiB, 8.41% gc time, 0.28% compilation time)
=#
