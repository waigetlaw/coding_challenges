#=
There are some prime values, p, for which there exists a positive integer, n, such that the expression n³ + n²p is a perfect cube.

For example, when p = 19, 8³ + 8² × 19 = 12³.

What is perhaps most surprising is that for each prime with this property the value of n is unique, and there are only four such primes below one-hundred.

How many primes below one million have this remarkable property?
=#

using Printf, Primes

function problem131(limit)
    Π = primes(limit)
    possiblePs = [];
    for p ∈ Π
        n = 1;
        while true && n < 100000
            exp = n^3 + p*n^2
            if isinteger(cbrt(exp))
                push!(possiblePs, (p, n, cbrt(exp), n//Int64(cbrt(exp))))
                break;
            end
            n += 1;
        end
    end
    @printf "Ps = %s\n" possiblePs
end


function problem131v2(limit)
    idx = 1;
    total = 0;
    while true
        n = idx^3;
        a = n*(idx + 1)/idx;
        p = Int128(round(a^3/n^2) - n);
        if p >= limit
            break
        end
        if isprime(p)
            total += 1
        end
        idx += 1
    end
    return total;
end


@time @show problem131v2(10^6);

#=
v1:

clear pattern found from some brute force method:
Ps = Any[(7, 1, 2.0, 1//2), (19, 8, 12.0, 2//3), (37, 27, 36.0, 3//4), (61, 64, 80.0, 4//5), (127, 216, 252.0, 6//7), (271, 729, 810.0, 9//10), (331, 1000, 1100.0, 10//11), (397, 1331, 1452.0, 11//12)]

consider n^3 + p * n^2 = a^3, clearly a > n

n sequence are cube numbers: 1, 8, 27, 64, 216, 729 (some are missing?)
a = n * x/(x-1)

v2:

so try solve for p going through the n/a combos and if p is prime, then we know it is a solution.
    
problem131v2(10 ^ 6) = 173
0.000360 seconds (13 allocations: 408 bytes)
=#
