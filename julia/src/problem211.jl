using Printf

include("./utils/getdivisors.jl")

function σ₂(n)
    return sum(map(x -> x^2, collect(getdivisors(n))))
end

function problem211(limit)
    total = 0;
    for n ∈ 1:limit - 1
        if isinteger(sqrt(σ₂(n)))
            total += n
        end
    end
    return total
end

@time @show problem211(64*10^6)
