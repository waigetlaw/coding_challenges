# https://projecteuler.net/problem=358

using Printf, Formatting, Primes

include("./utils/fractiondigits.jl")

function problem358()
    cyclic = 0.00000000137
    # cyclic = 0.142857
    # cyclic = 0.05882352941
    println(10^9/cyclic)
    n = round(1/cyclic)

    # d = fractiondigitsv3(1, n, n + 10)["cycle"]
    r = 1;
    d::Array{Int8} = [];
    while length(d) < n && join(map(x -> string(x), d[((length(d) - 12) > 0 ? (length(d) - 12) : 1):end])) != "5678900000000"
      q, r = divrem(r, n)
      r *= 10
      # println("here", join(map(x -> string(x), d[((length(d) - 4) > 0 ? (length(d) - 4) : 1):end])))
      push!(d, q)
    end

    @printf "%s\n" join(d[length(d) - 20:end])
    @printf "length: %i\n" length(d)
    @printf "sum = %i\n" sum(d)
    return sum(d)
    # @printf "%i\n" total
    # return total
end

function problem358v2()
  cyclic = 0.00000000137
  n = round(1/cyclic)
  lowerbound = Int64(floor(1/0.00000000138))
  upperbound = Int64(n)
  println(upperbound - lowerbound)
  ps = primes(Int64(ceil(1/0.00000000138)), Int64(n))
  for p in ps
    if p <= 726509891 continue end
    if nthdigit(1, p, length(p) - 1) == 9 && nthdigit(1, p, length(p) - 2) == 8 && nthdigit(1, p, length(p) - 3) == 7 && nthdigit(1, p, length(p) - 4) == 6 && nthdigit(1, p, length(p) - 5) == 5 && nthdigit(1, p, length(p)) == 0 && nthdigit(1, p, length(p) + 1) == 0 && nthdigit(1, p, length(p) + 2) == 0

      r = 1;
      total = 0;
      d = 0;
      while d < p + 6
        q::Int8, r = divrem(r, p)
        r *= 10
        total += q
        d += 1
        # if d > 1 && d < 100
        #   @printf "q = %i, nthdigit = %i\n" q nthdigit(1, p, d - 1)
        # end
        # if d == 101
        #   println()
        # end
        # if d > p - 10
        #   @printf "q = %i, nthdigit = %i\n" q nthdigit(1, p, d - 1)

        # end
      end
      @printf "total is: %i\n" total
      return total
    end
  end
  # println(length(p))
end

@time @show problem358v2();

# fracDigits(1, 729927007, 100)

#=
  000000001370000000561700000230297000094421770038712925715872299543507642812838133553263634756838090

  000000000137000000056170000023029700009442177003871292571587229954350764281283813355326363475683809

  between 729927007.299 - 724637681.159

  3284144505
=#
