#=
In the following equation x, y, and n are positive integers.

1/x + 1/y = 1/n

It can be verified that when n = 1260 there are 113 distinct solutions and this is the least value of n for which the total number of distinct solutions exceeds one hundred.

What is the least value of n for which the number of distinct solutions exceeds four million?

NOTE: This problem is a much more difficult version of Problem 108 and as it is well beyond the limitations of a brute force approach it requires a clever implementation.

Thoughts, lets just reimplement p108 in julia first...

v2: The number of unit fraction sums that equal 1/n is the number of divisors of n^2.
=#

using Printf, Primes, Combinatorics

include("./utils/countdivisors.jl")

function problem108(sols)
    distinctSols = 0;
    maxSoFar = [0, 0];
    n = 3;
    while distinctSols <= sols
        n += 1;
        distinctSols = 0
        for x in n+1:2n
            top = n*x
            bot = x - n
            if mod(top, bot) === 0
                distinctSols += 1
            end
        end

        if distinctSols > maxSoFar[2]
            maxSoFar = [n, distinctSols]
        end
    end
    @printf "For n = %i, there are %i distinct solutions.\n" maxSoFar[1] maxSoFar[2]
    return maxSoFar[1]
end

function problem108v2(sols)
    distinctSols = 0;
    maxSoFar = [0, 0];
    base::BigInt = prod(primes(40));
    n::BigInt = base;
    while distinctSols <= sols
        n += base;
        distinctSols = 0
        divisors = countdivisors(n^2)
        distinctSols::Int64 = (divisors + 1)/2;

        if distinctSols > maxSoFar[2]
            maxSoFar = [n, distinctSols]
        end
    end
    @printf "For n = %i, there are %i distinct solutions.\n" maxSoFar[1] maxSoFar[2]
    return maxSoFar[1]
end

@time @show problem108v2(4000000);

#=
For n = 180180, there are 1013 distinct solutions.
problem108(1000) = 180180
 19.285971 seconds (47 allocations: 2.742 KiB)

This is the same implementation as p108 in JS except in JS this took 9 minutes!!
Julia is definitely faster... but still too inefficient to calculate for four million.

v2
Using countdivisors trick and "guessing" a good base of prime products:
For n = 9350130049860600, there are 4018613 distinct solutions.
problem108v2(4000000) = 9350130049860600
  0.049756 seconds (1.32 M allocations: 23.827 MiB, 24.07% gc time)
=#
