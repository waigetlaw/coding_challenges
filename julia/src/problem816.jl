#=
We create an array of points Pₙ in a two dimensional plane using the following random number generator:

s₀ = 290797
s_{n+1} = sₙ² mod 50515093
Pₙ = (s_{2n}, s_{2n+1})

Let d(k) be the shortest distance of any two (distinct) points among P₀, …, P_{k-1}.
E.g. d(14) = 546446.466846479.

Find d(2000000). Give your answer rounded to 9 places after the decimal point.
=#

using Combinatorics, Printf

function d(k)
    s::Int64 = 290797;

    function next_s()
        s = powermod(s, 2, 50515093)
        return s;
    end

    P = [(s, next_s())]
    for n ∈ 1:k-1
        push!(P, (next_s(), next_s()))
    end

    P = unique(P)
    pairs = combinations(P, 2)
    min_distance::Float64 = 2^62;
    for (p1, p2) in pairs
        diffX = abs(p1[1] - p2[1])
        diffY = abs(p1[2] - p2[2])
        distance = sqrt(diffX^2 + diffY^2)
        # @printf "P1: %s, P2: %s, distance: %1.9f\n" p1 p2 distance
        if distance < min_distance
            min_distance = distance
            # @printf "P1: %s, P2: %s, distance: %1.9f\n" p1 p2 distance
        end
    end
    @printf "d(%i) = %1.9f\n" k min_distance
    return min_distance
end

function dv2(k)
    s::Int64 = 290797;

    function next_s()
        s = powermod(s, 2, 50515093)
        return s;
    end

    P = [(s, next_s())]
    for n ∈ 1:k-1
        push!(P, (next_s(), next_s()))
    end

    P = unique(P)
    min_distance::Float64 = 2^62;
    for p_index in eachindex(P)
        if mod(p_index, 1000) == 0
            @printf "%i of %i, %s percent done.\n" p_index length(P) (p_index / length(P)) * 100
        end

        for idx2 in p_index + 1:length(P)
            (x2, y2) = P[idx2]
            (x1, y1) = P[p_index]
            dist_x = abs(x1 - x2)
            if dist_x > min_distance
                continue
            end
            dist_y = abs(y1 - y2)
            if dist_y > min_distance
                continue
            end
            distance = sqrt((dist_x)^2 + (dist_y)^2)
            # @printf "P1: %s, P2: %s, distance: %1.9f\n" p1 p2 distance
            if distance < min_distance
                min_distance = distance
                # @printf "P1: %s, P2: %s, distance: %1.9f\n" p1 p2 distance
            end
        end
    end
    @printf "d(%i) = %1.9f\n" k min_distance
    return min_distance
end

# sorting by x and moving along only checking points with their x < min_distance
function dv3(k)
    s::Int64 = 290797;

    function next_s()
        s = powermod(s, 2, 50515093)
        return s;
    end

    P = [(s, next_s())]
    for n ∈ 1:k-1
        push!(P, (next_s(), next_s()))
    end

    P = sort!(P)

    min_distance::Float64 = sqrt((P[1][1] - P[2][1])^2 + (P[1][2] - P[2][2])^2);
    for p_index in eachindex(P)[2:end]
        for idx2 in p_index-1:-1:1
            (x2, y2) = P[idx2]
            (x1, y1) = P[p_index]
            dist_x = abs(x1 - x2)
            if dist_x > min_distance
                break
            end
            dist_y = abs(y1 - y2)
            distance = sqrt((dist_x)^2 + (dist_y)^2)
            if distance < min_distance
                min_distance = distance
            end
        end
    end
    @printf "d(%i) = %1.9f\n" k min_distance
    return min_distance
end
# @time d(10000);
# @time dv2(80000);
@time dv3(2000000);

#=
v1 bruteforce:

d(20000) = 644.131197816
 14.968278 seconds (400.04 M allocations: 23.844 GiB, 16.81% gc time)

v2 try to find the closest point for each point instead of calculating all combinations (might be the same though)
d(20000) = 644.131197816
  0.664592 seconds (59.02 k allocations: 2.984 GiB, 22.40% gc time)

d(200000) = 214.848784032
 62.993090 seconds (599.05 k allocations: 298.064 GiB, 19.87% gc time)

d(2000000) = 20.880613017821101
d(2000000) = 20.880613018
7531.732790 seconds (6.04 M allocations: 29.104 TiB, 12.28% gc time)

v3
d(2000000) = 20.880613018
0.365448 seconds (2.04 M allocations: 101.786 MiB, 3.29% gc time)
=#
