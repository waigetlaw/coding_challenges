#=
For any positive integer k, a finite sequence aᵢ of fractions xᵢ/yᵢ is defined by:
a₁ = 1/k and
aᵢ = (x_{i - 1} + 1)/(y_{i-1} - 1) reduced to lowest terms for i > i.
When aᵢ reaches some integer n, the sequence stops. (That is, when yᵢ = 1.)
Define f(k) = n.
For example, for k = 20:

1/20 ➡ 2/19 ➡ 3/18 = 1/6 ➡ 2/5 ➡ 3/4 ➡ 4/3 ➡ 5/2 ➡ 6/1 = 6

So f(20) = 6.

Also f(1) = 1, f(2) = 2, f(3) = 1 and Σf(k³) = 118937 for 1 ≤ k ≤ 100.

Find Σf(k³) for 1 ≤ k ≤ 2 × 10⁶.

Seems like everytime k is not prime, we know it reduces to:

k = ab, where b > a
then reduces to 1/(b - 1)

and if k is prime, then it reduces all the way down to k.
=#

using Primes, Printf, DataStructures, FastPrimeSieve

function f(k)
    while !isprime(k + 1)
        # @printf "k = %i\n" k
        if iseven(k + 1)
            div = Int128((k + 1)/2)
            k = div - 1
        else
            div::Int128 = 3
            while mod(k + 1, div) != 0
                div += 2
            end
            k::Int128 = Int128(Int128(round((k + 1)/div)) - 1);
        end
    end
    # println(k)
    return k
end

function problem343(limit)
    total::Int128 = 0;
    for k::Int128 ∈ limit:-1:1
        if mod(k, 10^4) == 0 @printf "Progress %1.2f\n" 100*k/limit end
        # total += f(k^3)
        if iseven(k)
            # div = k^2 - k + 1
            # div = k + 1
            # total += f(k^2  - k)
            total += f(k^3)
        else
            # div = 2
            # k_halfed::Int128 = ((k^3 + 1)/2) - 1
            # total += f(k_halfed)
            total += f(k^3)
        end
    end
    return total;
end

function log()
    for k in 3:10
    # k = 82
        if iseven(k)
            # @printf "k = %i, k^3 = %i, f(k^3) = %i, f(k^2 - k) = %i, equal? %s\n" k k^3 f(k^3) f(k^2 - k) f(k^3) == f(k^2 - k) 
        else
            @printf "k^3 = %i, f(k^3) = %i, f(k^3/2 - 1) = %i\n" k^3 f(k^3) f(Int128((k^3 + 1)/2) - 1)
        end
    end
end

# function problem343v2(limit)
#     total::Int128 = 0;
#     for k::Int128 ∈ 1:limit
#         if mod(k, 10^4) == 0 @printf "Progress %1.2f\n" 100*k/limit end
#         # total += f(k^3)
#         if iseven(k)
#             # div = k^2 - k + 1
#             # div = k + 1
#             total += f(k^2  - k)
#         else
#             # div = 2
#             # k_halfed::Int128 = ((k^3 + 1)/2) - 1
#             # total += f(k_halfed)
#             total += f(k^3)
#         end
#     end
#     return total;
# end

function fv2(k)
    # while !isprime(k + 1)
    while true
        # @printf "k = %i\n" k
        if iseven(k + 1)
            div = Int128((k + 1)/2)
            k = div - 1
        else
            if isprime(k + 1)
                return k
            end
            div::Int128 = 3
            while mod(k + 1, div) != 0
                div += 2
            end
            k::Int128 = Int128(Int128((k + 1)/div) - 1);
        end
    end
    # println(k)
    # return k
end

function problem343v7(limit)
    total::Int128 = 1;
    for k::Int128 ∈ 2:limit
        if mod(k, 10^4) == 0 @printf "Progress %1.2f\n" 100*k/limit end
        # total += f(k^3)
        if iseven(k)
            remaining = k^2 - k + 1
            lowestdiv = k + 1
            for div in 3:2:k+1
                if mod(remaining, div) == 0
                    lowestdiv = div;
                    break;
                end
            end
            # @printf "k = %i, k^3 = %i, lowestdiv = %i\n" k k^3 lowestdiv
            total += f(Int128(round((k^3 + 1)/lowestdiv) - 1))
        else
            # div = 2
            # k_halfed::Int128 = ((k^3 + 1)/2) - 1
            # total += f(k_halfed)
            total += f(Int128(round((k^3 + 1) / 2) - 1))
        end
    end
    return total;
end

function problem343v8(limit)
    total::Int128 = 1;
    for k::Int128 ∈ limit:-1:2
        if mod(k, 10^4) == 0 @printf "Progress %1.2f\n" 100*k/limit end
        # total += f(k^3)
        if iseven(k)
            remaining = k^2 - k + 1
            lowestdiv = k + 1
            div = 3
            limitdiv = ceil(sqrt(remaining))
            while div < k + 1 && div <= limitdiv
            # for div in 3:2:k+1
                if mod(remaining, div) == 0
                    lowestdiv = div;
                    break;
                end
                div += 2
            end
            # @printf "k = %i, k^3 = %i, lowestdiv = %i\n" k k^3 lowestdiv
            total += fv2(Int128(round((k^3 + 1)/lowestdiv) - 1))
        else
            # div = 2
            # k_halfed::Int128 = ((k^3 + 1)/2) - 1
            # total += f(k_halfed)
            total += fv2(Int128(round((k^3 + 1) / 2) - 1))
        end
    end
    return total;
end

function fv3(k)
    while true
        if iseven(k + 1)
            div::Int128 = (k + 1) ÷ 2
            k = div - 1
        else
            factors = sort(collect(factor(Vector, k + 1)))
            if length(factors) == 1
                return k
            end
            k::Int128 = ((k + 1) ÷ factors[1]) - 1;
        end
    end
end

# println(factor(1))
# fv3(1)

function problem343v9(limit)
    total::Int128 = 1;
    for k::Int128 ∈ limit:-1:2
        if mod(k, 10^4) == 0 @printf "Progress %1.2f\n" 100*k/limit end
        # total += f(k^3)
        if iseven(k)
            remaining = k^2 - k + 1
            lowestdiv = k + 1
            div = 3
            limitdiv = ceil(sqrt(remaining))
            while div < k + 1 && div <= limitdiv
            # for div in 3:2:k+1
                if mod(remaining, div) == 0
                    lowestdiv = div;
                    break;
                end
                div += 2
            end
            # @printf "k = %i, k^3 = %i, lowestdiv = %i\n" k k^3 lowestdiv
            total += fv2(Int128(round((k^3 + 1)/lowestdiv) - 1))
        else
            # div = 2
            # k_halfed::Int128 = ((k^3 + 1)/2) - 1
            # total += f(k_halfed)
            total += fv2(Int128(round((k^3 + 1) / 2) - 1))
        end
    end
    return total;
end

function problem343v10(limit)
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(ceil(Int128, sqrt((limit + 1)^3)))));
    total::Int128 = 1;

    function maxfactor(n)
        idx = 1;
        div = primes[1]
        maxf = 1;
        while n > 1 && div <= ceil(sqrt(n))
            while mod(n, div) == 0
                n = n ÷ div
                maxf = div;
            end
            idx += 1;
            div = primes[idx];
        end
        return max(maxf, n)
    end

    for k::Int128 ∈ 2:limit
        if mod(k, 10^4) == 0 @printf "Progress %1.2f\n" 100*k/limit end
        maxprimefactor = maxfactor(k + 1)
        maxf = maxfactor(k^2 - k + 1)
        total += max(maxprimefactor, maxf) - 1
    end
    return total;
end

function fv4test(k)
    if k == 1
        return 1
    end
    x = 2
    k -= 1
    while k > 1
        if mod(k, x) == 0
            # @printf "%i\\%i -> %i\n" x k k ÷ x
            return fv4test(k ÷ x)
        end
        x += 1
        k -= 1
    end
    return x
end

# @time @show problem343(2*(10^6));

# @time @show problem343v8(10^5);
# @time @show problem343v7(2*(10^6));

# @time @show problem343v10(10^5);
@time @show problem343v10(2*(10^6));
# f(20)

function comparef()

    # @time for k in 99:100
    #     v1 = f(k^3)
    #     v4 = fv4test(k^3)

    #     if v1 != v4
    #         @printf "Not equal at k = %i, v1 = %i, v4 = %i\n" k v1 v4
    #     end
    # end

    total = 0
    @time for k in 1:100

        v3 = fv4test(k^3)
        total += v3
    end
    println(total)
end

# comparef()

# log()

#=
v1

using factor and set

problem343(10 ^ 2) = 118937
  0.001338 seconds (29.99 k allocations: 739.820 KiB)

problem343(10 ^ 3) = 72421603
  0.001633 seconds (14 allocations: 448 bytes)

problem343(10 ^ 4) = 55202166687
12.942518 seconds (334.49 M allocations: 5.367 GiB, 20.92% gc time)

v2 using while loop to get min div
problem343(10 ^ 4) = 55202166687
3.486504 seconds (85.41 M allocations: 1.459 GiB, 22.88% gc time)

v3 maximum factor
problem343(10 ^ 4) = 55202166687
  3.539507 seconds (90.42 M allocations: 1.448 GiB, 21.19% gc time)

v4 iseven or while
problem343(10 ^ 4) = 55202166687
  1.758247 seconds (43.31 M allocations: 759.121 MiB, 23.62% gc time)

v5 using Int128 as that is enough to fit (2 × 10⁶)³
problem343(10 ^ 4) = 55202166687
  0.059833 seconds (14 allocations: 448 bytes)
55202166687

problem343(10 ^ 5) = 42661285291615
  3.421603 seconds (14 allocations: 448 bytes)

v6 if k = odd, then k^3 + 1 is even, and therefore divisible by 2.
    However, if k = even, then k^3 + 1 is odd, but it will be dibisible by k + 1

slightly off for some reason at 10^3 or higher...

problem is, although k+1 is a divider of k^3 + 1, it isn't necessarily the smallest or biggest one, which is what we need.
However, it can help us narrow down our calculations:

for k = 82

551369 = 7 x 13 x 73 x 83

We know k + 1 (83) is a divider, and the result is k^2 - k + 1 = 6643

so we can just calculate the factors of 6643 to get 7 x 13 x 73 and then compare the smallest factor as the div.

We could even loop from only 2 to k + 1 as a divider of 6643 as any higher will not be required. And we already know it won't divide by 2
so we can loop from 3 to k + 1 in steps of 2

v7 (fixed v6)
problem343v7(10 ^ 4) = 55202166687
  0.089577 seconds (26.95 k allocations: 1.786 MiB, 25.66% compilation time)

problem343v7(10 ^ 5) = 42661285291615
  4.115694 seconds (65 allocations: 5.578 KiB)

problem343v7(2 * 10 ^ 5) = 321220272186925
 14.877257 seconds (115 allocations: 10.422 KiB)

v1 with round
problem343(2 * 10 ^ 6) = 473049310220814856419
7525.077560 seconds (1.11 k allocations: 105.688 KiB)

but wrong

v10
problem343v10(10 ^ 5) = 42661285291615
1.300970 seconds (27.00 k allocations: 32.580 MiB, 0.42% gc time)

problem343v10(2 * 10 ^ 6) = 269533451410884183
257.517941 seconds (2.43 k allocations: 2.123 GiB, 0.00% gc time)
=#
