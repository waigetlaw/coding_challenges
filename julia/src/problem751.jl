#=
A non-decreasing sequence of integers a_n can be generated from any positive real value θ by the following procedure:
 
b_1 = θ
b_n = ⌊b_{n-1}⌋(b_{n-1} - ⌊b_{n-1}⌋ + 1) ∀ n ≥ 2
a_n = ⌊b_n⌋
 
Where ⌊⋅⌋ is the floor function.

For example, θ = 2.956938891377988... generates the Fibonacci sequence: 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

The concatenation of a sequence of positive integers a_n is a real value denoted τ constructed by concatenating the elements of the sequence after the decimal point, starting at a₁:a₁.a₂a₃a₄...

For example, the Fibonacci sequence constructed from θ = 2.956938891377988... yields the concatenation τ = 2.3581321345589... Clearly, τ ≠ θ for this value of θ.

Find the only value of θ for which the generated sequence starts at a₁ = 2 and the concatenation of the generated sequence equals the original value: τ = θ. Give your answer rounded to 24 places after the decimal point.

Thoughts: I'm assuming I can use some kind of recursion and get a better guess each time by using the error.
=#

using Printf

function generateSequence(θ, precision=50)
    bₙ = [θ]
    for n ∈ 1:precision - 1
        next_b = floor(bₙ[n])*(bₙ[n] - floor(bₙ[n]) + 1)
        push!(bₙ, next_b)
    end
    aₙ::Array{Int64} = []
    for b ∈ bₙ
        push!(aₙ, floor(b))
    end
    stringaₙ = join(aₙ)
    return parse(BigFloat, stringaₙ[1] * '.' * stringaₙ[2:end])
end

function problem751(θ = 2.956938891377988)
    error = 1;
    while error > 0
        aₙ = generateSequence(θ)
        error = θ - aₙ;
        @printf "θ = %1.24f\naₙ = %1.24f\nerror = %1.24f.\n--------------------------------------------\n" θ aₙ error
        θ -= error
    end
    @printf "θ = %1.24f\n" θ
    return θ
end

@time @show problem751();

#=
θ = 2.223561019313554106173177
problem751() = 2.223561019313554106173177195280486808140723213605541386631172112588199923016144
  0.097208 seconds (168.60 k allocations: 10.429 MiB, 6.54% gc time, 98.16% compilation time)
=#
