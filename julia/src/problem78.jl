#=
Let p(n) represent the number of different ways in which n coins can be separated into piles. For example, five coins can be separated into piles in exactly seven different ways, so p(5) = 7.

OOOOO
OOOO   O
OOO   OO
OOO   O   O
OO   OO   O
OO   O   O   O
O   O   O   O   O
Find the least value of n for which p(n) is divisible by one million.

Thoughts: I thought this problem translated to the coin problem - number of ways to sum to n using coins from 1 to n but it is WAY too slow...

From wiki:

𝑝(𝑛)=𝑝(𝑛−1)+𝑝(𝑛−2)−𝑝(𝑛−5)−𝑝(𝑛−7)+𝑝(𝑛−12)+𝑝(𝑛−15)+…

𝑘(3𝑘−1)/2 are the pentagonal numbers
=#

include("./utils/getCombinationCount.jl")
include("./utils/intPart.jl")

using Printf, Combinatorics, BigCombinatorics

function problem78(div)

    n = 1;
    # groups::Int128 = length(partitions(n));

    groups = IntPartitions(n);
    while mod(groups, div) != 0 && n < 100
        @printf "For n = %i, the number of groups is %i.\n" n groups
        n += 1;
        groups = IntPartitions(n)

    end

end

function problem78v2(div)
    n = 1;
    # groups::Int128 = length(partitions(n));
    pentagonal = [];
    parts = Array{BigInt}([0, 1])
    for k ∈ 1: 200
        push!(pentagonal, Dict{String, BigInt}("p" => k*(3*k - 1)/2, "mult" => mod(k, 2) == 0 ? -1 : 1))
        push!(pentagonal, Dict{String, BigInt}("p" => (-k)*(3*(-k) - 1)/2, "mult" => mod(k, 2) == 0 ? -1 : 1))
    end


    # println(pentagonal)

    
    for n ∈ 2:last(pentagonal)["p"]
        part_n = 0;
        for pent ∈ pentagonal
            # println(pent)
            part_index = n - pent["p"] + 1
            # @printf "checking n: %i, index: %i\n" n part_index
            if part_index < 1
                break
            end
            if part_index > length(parts)
                continue
            end
            # @printf "p_%i += p[%i] %i\n" n part_index parts[part_index] * pent["mult"]
            part_n += (parts[part_index] * pent["mult"])
        end
        push!(parts, part_n)
    end
    for p ∈ eachindex(parts)
        if p < 3
            continue
        end
        # if (p - 2 < 1700)
        if (mod(parts[p], div) == 0)
            @printf "For n = %i, the number of groups is %i.\n" p - 2 parts[p]
            break;
        end
    end

    # groups = IntPartitions(n);
    # while mod(groups, div) != 0
    #     @printf "For n = %i, the number of groups is %i.\n" n groups
    #     n += 1;
    #     groups = IntPartitions(n)

    # end
    return n
end

function problem78v3(div)
    integer_partitions = intPart(55500);

    n = findfirst(n -> mod(integer_partitions[n], div) == 0, eachindex(integer_partitions));

    @printf "The first p(n) that divides by %i is n = %i.\n" div n
end

# @time problem78(10^6)
# @time problem78v2(10^6)
@time problem78v3(10^6)

#=
For n = 1, the number of groups is 1.
For n = 2, the number of groups is 2.
For n = 3, the number of groups is 3.
For n = 4, the number of groups is 5.
For n = 5, the number of groups is 7.
For n = 6, the number of groups is 11.
For n = 7, the number of groups is 15.
For n = 8, the number of groups is 22.
For n = 9, the number of groups is 30.
For n = 10, the number of groups is 42.
For n = 11, the number of groups is 56.
For n = 12, the number of groups is 77.
For n = 13, the number of groups is 101.
For n = 14, the number of groups is 135.
For n = 15, the number of groups is 176.
For n = 16, the number of groups is 231.
For n = 17, the number of groups is 297.
For n = 18, the number of groups is 385.
For n = 19, the number of groups is 490.
For n = 20, the number of groups is 627.
For n = 21, the number of groups is 792.
For n = 22, the number of groups is 1002.
For n = 23, the number of groups is 1255.
For n = 24, the number of groups is 1575.
For n = 25, the number of groups is 1958.
For n = 26, the number of groups is 2436.
For n = 27, the number of groups is 3010.
For n = 28, the number of groups is 3718.
For n = 29, the number of groups is 4565.
For n = 30, the number of groups is 5604.
For n = 31, the number of groups is 6842.
For n = 32, the number of groups is 8349.
For n = 33, the number of groups is 10143.
For n = 34, the number of groups is 12310.
For n = 35, the number of groups is 14883.
For n = 36, the number of groups is 17977.
For n = 37, the number of groups is 21637.
For n = 38, the number of groups is 26015.
For n = 39, the number of groups is 31185.
For n = 40, the number of groups is 37338.
For n = 41, the number of groups is 44583.
For n = 42, the number of groups is 53174.
For n = 43, the number of groups is 63261.
For n = 44, the number of groups is 75175.
For n = 45, the number of groups is 89134.
For n = 46, the number of groups is 105558.
For n = 47, the number of groups is 124754.
For n = 48, the number of groups is 147273.
For n = 49, the number of groups is 173525.
For n = 50, the number of groups is 204226.
For n = 51, the number of groups is 239943.
For n = 52, the number of groups is 281589.
For n = 53, the number of groups is 329931.
For n = 54, the number of groups is 386155.
For n = 55, the number of groups is 451276.
For n = 56, the number of groups is 526823.
For n = 57, the number of groups is 614154.
For n = 58, the number of groups is 715220.
For n = 59, the number of groups is 831820.
For n = 60, the number of groups is 966467.
For n = 61, the number of groups is 1121505.
For n = 62, the number of groups is 1300156.
For n = 63, the number of groups is 1505499.
For n = 64, the number of groups is 1741630.
For n = 65, the number of groups is 2012558.
For n = 66, the number of groups is 2323520.
For n = 67, the number of groups is 2679689.
For n = 68, the number of groups is 3087735.
For n = 69, the number of groups is 3554345.
For n = 70, the number of groups is 4087968.
For n = 71, the number of groups is 4697205.
For n = 72, the number of groups is 5392783.
For n = 73, the number of groups is 6185689.
For n = 74, the number of groups is 7089500.
For n = 75, the number of groups is 8118264.
For n = 76, the number of groups is 9289091.
For n = 77, the number of groups is 10619863.
For n = 78, the number of groups is 12132164.
For n = 79, the number of groups is 13848650.
For n = 80, the number of groups is 15796476.
For n = 81, the number of groups is 18004327.
For n = 82, the number of groups is 20506255.
For n = 83, the number of groups is 23338469.
For n = 84, the number of groups is 26543660.
For n = 85, the number of groups is 30167357.
For n = 86, the number of groups is 34262962.
For n = 87, the number of groups is 38887673.
For n = 88, the number of groups is 44108109.
For n = 89, the number of groups is 49995925.
For n = 90, the number of groups is 56634173.
For n = 91, the number of groups is 64112359.
For n = 92, the number of groups is 72533807.
For n = 93, the number of groups is 82010177.
For n = 94, the number of groups is 92669720.
For n = 95, the number of groups is 104651419.
For n = 96, the number of groups is 118114304.
For n = 97, the number of groups is 133230930.
For n = 98, the number of groups is 150198136.
For n = 99, the number of groups is 169229875.

For n = 1686, the number of groups is 467794246437739506976775111608393022209053.
For n = 1687, the number of groups is 482349752052240657962887540925835136720740.
For n = 1688, the number of groups is 497353725307958208396664918548576500570384.
For n = 1689, the number of groups is 512819848828887897371554062220903289550130.
For n = 1690, the number of groups is 528762218615331555088826226879544901167527.
For n = 1691, the number of groups is 545195356410872371074704272735369048924689.
For n = 1692, the number of groups is 562134222435726415975597022642148002675881.
For n = 1693, the number of groups is 579594228497218762288102882601473336765100.
For n = 1694, the number of groups is 597591251488444805746508999799665944566660.
For n = 1695, the number of groups is 616141647286498628873307956507246249662412.
For n = 1696, the number of groups is 635262265061980727342758633558885467930686.
For n = 1697, the number of groups is 654970462011837401470060834112028353314761.
For n = 1698, the number of groups is 675284118527933869908522234215965152162520.
For n = 1699, the number of groups is 696221653814122968723573796976021441661750.
For n = 1700, the number of groups is 717802041964941442478681516751205185010007.
For n = 1701, the number of groups is 740044828519446608929091853958115568986164.
For n = 1702, the number of groups is 762970147504097887787893822256219849371554.
For n = 1703, the number of groups is 786598738978990637725956554797278124357808.
For n = 1704, the number of groups is 810951967102164263980984405643613443347625.
For n = 1705, the number of groups is 836051838727132970358751925465426223753244.
For n = 1706, the number of groups is 861921022549226171951777077723669881527186.
For n = 1707, the number of groups is 888582868816776806015468170319304987709289.
For n = 1708, the number of groups is 916061429623659935353293704664261165680563.

For n = 980, the number of groups is 10863611303931504965592652844878.
For n = 981, the number of groups is 11306358488849924787366667765407.
For n = 982, the number of groups is 11766916372239763961801564990016.
For n = 983, the number of groups is 12245992177539511607834487453052.
For n = 984, the number of groups is 12744320848028628464246059627690.
For n = 985, the number of groups is 13262666119314202551196742822008.
For n = 986, the number of groups is 13801821632778520931079437719552.
For n = 987, the number of groups is 14362612091531863067120268402228.
For n = 988, the number of groups is 14945894460472306341153073892017.
For n = 989, the number of groups is 15552559212113915719970799358900.
For n = 990, the number of groups is 16183531619906475296861224625027.
For n = 991, the number of groups is 16839773100833956878604913215477.
For n = 992, the number of groups is 17522282609145324707635966077022.
For n = 993, the number of groups is 18232098083140097717852712346115.
For n = 994, the number of groups is 18970297947002453464660671155990.
For n = 995, the number of groups is 19738002669751617842096992232436.
For n = 996, the number of groups is 20536376383452971700767593594021.
For n = 997, the number of groups is 21366628562913781584556907794729.
For n = 998, the number of groups is 22230015769169865076825741905555.
For n = 999, the number of groups is 23127843459154899464880444632250.

For n = 55374, the number of groups is 36325300925435785930832331577396761646715836173633893227071086460709268608053489541731404543537668438991170680745272159154493740615385823202158167635276250554555342115855424598920159035413044811245082197335097953570911884252410730174907784762924663654000000.
 14.846204 seconds (176.90 M allocations: 5.194 GiB, 13.23% gc time)
=#
