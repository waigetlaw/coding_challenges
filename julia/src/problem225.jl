function problem225()
    trifibs::Array{BigInt} = [big(1), big(1), big(1)]
    prevprev = 1
    prev = 1
    current = 1
    while length(trifibs) < 100000
        next = big(prevprev + prev + current)
        push!(trifibs, next)
        prevprev = prev
        prev = current
        current = next
    end

    cantdiv = [];
    for odd in 1:2:10001
        candiv = findfirst(x -> mod(x, odd) == 0, trifibs)
        if candiv === nothing
            push!(cantdiv, odd)
        end
    end
    # println(length(cantdiv))
    # println(trifibs)
    if length(cantdiv) >= 124
        return cantdiv[124]
    else
        return nothing
    end
end


@time @show problem225()
