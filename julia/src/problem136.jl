# The positive integers, x, y, and z, are consecutive terms of an arithmetic progression. Given that n is a positive integer, the equation, x^2 - y^2 - z^2 = n, has exactly one solution when n = 20:

# 13^2 - 10^2 - 7^2 = 20

# In fact there are twenty-five values of n below one hundred for which the equation has a unique solution.

# How many values of n less than fifty million have exactly one solution?

# Thoughts, similar to p135 but I wonder if there is a way to rethink to speed up and 'skip' if we already found more than one solution for a given n.

# v1:
# since we need integer values, and we know (2a - N)(2a + N) = n,

# we can simply get factors of n, and try solve for a or N and if they are both integers and fit a > 0, N >= 0 then it's a valid answer.

# Note that although we want only 1 solution, it's not necessarily only when N = 0. if N > a, then remember:

# x1 = a - N, x2 = a + N

# if N > 0, as long as N >= a, then it will still be one solution only as x1 would become 0 or negative.

# So for each n, keep searching for factors, and then try solve for a, N.

# If we ever find more than 1 solution, we skip to next n.

using Printf

function problem136(limit::Int)
    uniqueSols = 0

    for n=1:1:limit-1
        nSols = 0
        for f1=1:1:sqrt(n)
            if nSols > 1 break end
            if mod(n, f1) == 0
                f2 = n/f1
                a = (f1 + f2)/4
                N = 2*a - f1
                # a and N both must be integers
                if !(isinteger(a) && isinteger(N)) continue end
                x1 = a + N
                x2 = a - N
                nSols += x2 !== x1 && x2 > 0 ? 2 : 1
                # @printf "%i = %i x %i, a: %i, N: %i, solutions: %i, %i\n" n f1 n/f1 a N x1 x2
            end
        end
        if nSols === 1
            uniqueSols += 1
        end
    end
    println(uniqueSols)
end

@time problem136(50000000)

#= 
should have: [3, 4, 7, 11, 12, 16, 19, 20, 23, 28, 31, 43, 44, 47, 48, 52, 59, 67, 68, 71, 76, 79, 80, 83, 92] 
we have :    [7, 11, 19, 20, 23, 28, 31, 43, 44, 47, 52, 59, 67, 68, 71, 76, 79, 80, 83, 92]

missing: [3, 4, 12, 16, 48]

2544559
1337.696575 seconds (11 allocations: 248 bytes)
=#
