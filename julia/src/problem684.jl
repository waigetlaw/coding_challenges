#=
Define s(n) to be the smallest number that has a digit sum of n. For example s(10) = 19.
Let S(k) = Σs(n), 1 ≤ n ≤ k. You are given S(20) = 1074.

Further let fᵢ be the Fibonacci sequence defined by f₀ = 0, f₁ = 1 and fᵢ = fᵢ-2 + fᵢ-1 for all i ≥ 2.

Find ΣS(fᵢ), 2 ≤ i ≤ 90. Give your answer modulo 1 000 000 007.
=#

using Printf

include("./utils/fibonacci.jl")

function s(n)
    nines = fld(n, 9)
    r = mod(n, 9)

    return parse(BigInt, string(r) * '9' ^ nines)
end

function S(k)
    total::BigInt = 0
    for n ∈ 1:k
        total += s(n)
    end
    return total
end

function Sv2(k)
    if k < 10
        total = 0
        for n in 1:k
            total += s(n)
        end
        return total;
    end

    total::BigInt = 45
    nines = fld(k, 9);
    for x in 1:nines-1
        total += (BigInt(10)^x * 45) + parse(BigInt, '1' ^ x) * 81
    end
    remainder = mod(k, 9);

    for r in k - remainder + 1:k
        total += s(r)
    end
    return total
end

function Sv3(k)
    if k < 10
        total = 0
        for n in 1:k
            total += s(n)
        end
        return total;
    end

    total::BigInt = 45
    nines = fld(k, 9);
    for x in 1:nines-1
        total += (BigInt(10)^x * 54) - 9
    end
    remainder = mod(k, 9);

    for r in k - remainder + 1:k
        total += s(r)
    end
    return total
end

function Sv4(k)
    total::BigInt = 0
    nines = fld(k, 9);
    if nines > 0
        total += parse(BigInt, '1' ^ nines) * 54
        total -= 9 * nines
    end

    remainder = mod(k, 9);

    for r in k - remainder + 1:k
        total += s(r)
    end
    return total
end

remainderMap = [2,5,9,14,20,27,35,44]

function Sv5(k)
    total::BigInt = 0
    nines = fld(k, 9);
    if nines > 0
        total += parse(BigInt, '1' ^ nines) * 54
        total -= 9 * nines
    end

    remainder = mod(k, 9);
    if remainder > 0
        total += remainderMap[remainder] * BigInt(10)^nines - remainder
    end
    return total
end

function Sv6(k)
    # to get repeating 1's, we divide by 9 to get 0.111111..., and we can multiply by 10^x to get x digits of 1!
    total::BigInt = 0
    (nines, remainder) = divrem(k, 9);
    if nines > 0
        oneRepeats::BigInt = fld(BigInt(10)^nines, 9);
        total += (oneRepeats * 54) - (9 * nines)
    end
    if remainder > 0
        total += remainderMap[remainder] * BigInt(10)^nines - remainder
    end
    return total
end

function Sv7(k)
    # to get repeating 1's, we divide by 9 to get 0.111111..., and we can multiply by 10^x to get x digits of 1!
    total::BigInt = 0
    (nines, remainder) = divrem(k, 9);
    big10 = BigInt(10)^nines
    if nines > 0
        oneRepeats::BigInt = fld(big10, 9);
        total += (oneRepeats * 54) - (9 * nines)
    end
    if remainder > 0
        total += remainderMap[remainder] * big10 - remainder
    end
    return total
end

function Sv8(k)
    # to get repeating 1's, we divide by 9 to get 0.111111..., and we can multiply by 10^x to get x digits of 1!
    total::BigInt = 0
    (nines, remainder) = divrem(k, 9);
    # big10 = mod(BigInt(10)^nines, 1000000007)
    big10::BigInt = BigInt(10)^nines
    if nines > 0
        total += (big10 * 6) - 6 - (9 * nines)
    end
    if remainder > 0
        total += remainderMap[remainder] * big10 - remainder
    end
    return total
end

function Sv8modded(k)
    # to get repeating 1's, we divide by 9 to get 0.111111..., and we can multiply by 10^x to get x digits of 1!
    total::BigInt = 0
    (nines, remainder) = divrem(k, 9);
    big10 = mod(BigInt(10)^nines, 1000000007)
    if nines > 0
        # oneRepeats::BigInt = fld(big10, 9);
        total += (big10 * 6) - 6 - (9 * nines)
    end
    if remainder > 0
        total += remainderMap[remainder] * big10 - remainder
    end
    return total
end

function Sv9(k)
    (nines, remainder) = divrem(k, 9);
    coeff::BigInt = 6 + (remainder > 0 ? remainderMap[remainder] : 0)

    # println(BigInt(10)^nines)
    return coeff * BigInt(10)^nines - BigInt(6 + (9 * nines) + remainder)
end

function Sv10modded(k)
    (nines, remainder) = divrem(k, 9);
    coeff::BigInt = 6 + (remainder > 0 ? remainderMap[remainder] : 0)
    return coeff * powermod(BigInt(10), nines, 1000000007) - BigInt(6 + (9 * nines) + remainder)
end

function problem684(f)
    total::BigInt = 0;
    for fib ∈ fibonacci(f)[3:end]
        S_n = S(fib);
        total += S_n
        # @printf "S(%i) = %i\n" fib S_n
    end
    @printf "ΣS(%i) modulo 1000000007 = %i.\n" f mod(total, 1000000007)
end

function problem684v2(f)
    total::BigInt = 0;
    for fib ∈ fibonacci(f)[3:end]
        S_n = Sv2(fib);
        # @printf "%i, %i\n" S(fib) Sv2(fib)
        total += S_n
        # @printf "S(%i) = %i\n" fib S_n
    end
    @printf "ΣS(%i) modulo 1000000007 = %i.\n" f mod(total, 1000000007)
end

function problem684v3(f)
    total::BigInt = 0;
    n = 2;
    for fib ∈ fibonacci(f + 1)[3:end]
        S_n = Sv10modded(fib);
        # @printf "%i, %i\n" S(fib) Sv4(fib)
        total += S_n
        @printf "%i: S(%i) = %i\n" n fib S_n
        n += 1
    end
    @printf "ΣS(%i) modulo 1000000007 = %i.\n" f mod(big(total), big(1000000007))
end

# @time problem684(27)
# @time problem684v2(28)
@time problem684v3(90)

# benchmark = 1000000
# @time v2 = Sv2(benchmark)
# @time v3 = Sv3(benchmark)
# @time v4 = Sv4(benchmark)
# @time v5 = Sv5(benchmark)
# @time v6 = Sv6(benchmark)
# @time v7 = Sv7(benchmark)
# @time v8modded = Sv8modded(benchmark)
# @time v8 = Sv8(benchmark)
# @time v9 = Sv9(benchmark)
# @time v10modded = Sv10modded(benchmark)

# println(v3)
# println(v4)
# println(v5)
# println(v6)
# println(v7)
# println(v8)
# @printf "Equals? %s" v8modded == v10modded ? "Yes" : "No"
# @printf "hello"

#=
S(1) = 1
S(2) = 3
S(3) = 6
S(5) = 15
S(8) = 36
S(13) = 181
S(21) = 1473
S(34) = 40960
S(55) = 7999939
S(89) = 49999999905
S(144) = 59999999999999850
S(233) = 499999999999999999999999761
S(377) = 4999999999999999999999999999999999999999617
ΣS(15) modulo 1000000007 = 93041145.
  0.005836 seconds (12.81 k allocations: 448.123 KiB, 83.57% compilation time)

v2 (attempt to calculate value of S(k))
ΣS(27) modulo 1000000007 = 503409380.
  5.177178 seconds (3.28 M allocations: 4.873 GiB, 2.49% gc time, 0.10% compilation time)
ΣS(27) modulo 1000000007 = 503409380.
  0.699466 seconds (717.56 k allocations: 650.223 MiB, 2.43% gc time)

it's faster but not enough... we are only dividing by 10 and fib90 is still way too big to loop through.

90th fib:
2880067194370816120

500000000 takes 3.37 seconds atm...

k = 500,000,000
Sv4 18.302614 seconds (549.02 k allocations: 8.780 GiB, 0.45% gc time, 0.05% compilation time)
Sv5  3.379663 seconds (106.22 k allocations: 1.676 GiB, 0.44% gc time, 0.25% compilation time)
v4 equals v5? Yes

v6 is even faster by mathematically calculating the repeating 1's
  3.417704 seconds (106.22 k allocations: 1.676 GiB, 0.31% gc time, 0.22% compilation time)
  0.629876 seconds (23.84 k allocations: 472.382 MiB, 0.48% gc time, 1.25% compilation time)
v5 equals v6? Yes

v4 v5 v6
ΣS(40) modulo 1000000007 = 642184997.
  4.093259 seconds (62.00 k allocations: 2.218 GiB, 0.56% gc time, 0.11% compilation time)

ΣS(40) modulo 1000000007 = 642184997.
  0.616695 seconds (13.46 k allocations: 370.086 MiB, 0.98% gc time, 0.72% compilation time)

ΣS(40) modulo 1000000007 = 642184997.
  0.166120 seconds (8.32 k allocations: 159.358 MiB, 2.63% gc time, 2.58% compilation time)

  v6
  ΣS(50) modulo 1000000007 = 983825219.
 31.758287 seconds (1.15 M allocations: 21.737 GiB, 0.28% gc time, 0.01% compilation time)

 v6modded keeping big10
 ΣS(45) modulo 1000000007 = 562446383.
  1.523573 seconds (96.04 k allocations: 985.863 MiB, 1.00% gc time, 0.35% compilation time)

  ΣS(50) modulo 1000000007 = 983825219.
 19.003440 seconds (655.90 k allocations: 10.979 GiB, 0.28% gc time, 0.02% compilation time)

 Sv7 just keeping big10 no mod

 ΣS(50) modulo 1000000007 = 983825219.
 19.840909 seconds (655.52 k allocations: 15.274 GiB, 0.56% gc time, 0.03% compilation time)

 so modding does minimal diff

 v8modded
 ΣS(50) modulo 1000000007 = 983825219.
 17.489311 seconds (704.15 k allocations: 9.439 GiB, 0.26% gc time, 0.37% compilation time)

 v10modded using powermod
 ΣS(50) modulo 1000000007 = 983825219.
  0.005093 seconds (3.43 k allocations: 192.100 KiB, 96.51% compilation time)

ans complete!

88: S(1100087778366101931) = -1100087775495599049
89: S(1779979416004714189) = -1779979407201309615
90: S(2880067194370816120) = -2880067191918568790
ΣS(90) modulo 1000000007 = 922058210.
  0.008576 seconds (17.02 k allocations: 475.131 KiB, 54.33% compilation time)
=#
