#=
The smallest positive integer n for which the numbers n² + 1, n² + 3, n² + 7, n² + 9, n² + 13, and n² + 27 are consecutive primes is 10. The sum of all such integers n below one-million is 1242490.

What is the sum of all such integers n below 150 million?

Thoughts: key is consecutive primes.
=#

using Primes, Printf, FastPrimeSieve

function problem146(limit)
    primes = vcat([2,3,5],collect(FastPrimeSieve.SmallSieve(limit^2 + 27)));
    sumOfValidN = 0;

    for n ∈ 1:limit-1
        n² = n^2
        numbers = [n² + 1, n² + 3, n² + 7, n² + 9, n² + 13, n² + 27]
        idx = findfirst(==(numbers[1]), primes);
        
        if idx === nothing
            continue
        end

        areConsecutive = true;

        for i ∈ 1:5
            if numbers[i + 1] !== primes[idx + i]
                areConsecutive = false;
                break;
            end
        end

        if areConsecutive
            @printf "For n = %i, the numbers: %s are all consecutive primes.\n" n numbers
            sumOfValidN += n
        end
    end

    @printf "The sum of all such integers n below %i is %i.\n" limit sumOfValidN
end

function problem146v2(limit)
    sumOfValidN = 0;

    for n ∈ 1:limit-1
        n² = n^2
        numbers = [n² + 1, n² + 3, n² + 7, n² + 9, n² + 13, n² + 27]

        if !isprime(numbers[1])
            continue
        end

        areConsecutive = true;

        for i ∈ 1:5
            if numbers[i + 1] !== nextprime(numbers[i] + 1)
                areConsecutive = false;
                break;
            end
        end

        if areConsecutive
            @printf "For n = %i, the numbers: %s are all consecutive primes.\n" n numbers
            sumOfValidN += n
        end
    end

    @printf "The sum of all such integers n below %i is %i.\n" limit sumOfValidN
end

@time problem146v2(150*10^6);

#=
For v1:
when limit = 16^3

For n = 10, the numbers: [101, 103, 107, 109, 113, 127] are all consecutive primes.
The sum of all such integers n below 4096 is 10.
  1.968148 seconds (4.14 k allocations: 17.420 MiB, 0.19% gc time)

For v2:
when limit = 16^3
For n = 10, the numbers: [101, 103, 107, 109, 113, 127] are all consecutive primes.
The sum of all such integers n below 4096 is 10.
  0.001292 seconds (4.14 k allocations: 450.031 KiB)

Much faster as we use nextprime rather than calculate all primes first.

for limit = 10^6:
For n = 10, the numbers: [101, 103, 107, 109, 113, 127] are all consecutive primes.
For n = 315410, the numbers: [99483468101, 99483468103, 99483468107, 99483468109, 99483468113, 99483468127] are all consecutive primes.
For n = 927070, the numbers: [859458784901, 859458784903, 859458784907, 859458784909, 859458784913, 859458784927] are all consecutive primes.
The sum of all such integers n below 1000000 is 1242490.

For limit = 150 * 10^6
For n = 10, the numbers: [101, 103, 107, 109, 113, 127] are all consecutive primes.
For n = 315410, the numbers: [99483468101, 99483468103, 99483468107, 99483468109, 99483468113, 99483468127] are all consecutive primes.
For n = 927070, the numbers: [859458784901, 859458784903, 859458784907, 859458784909, 859458784913, 859458784927] are all consecutive primes.
For n = 2525870, the numbers: [6380019256901, 6380019256903, 6380019256907, 6380019256909, 6380019256913, 6380019256927] are all consecutive primes.
For n = 8146100, the numbers: [66358945210001, 66358945210003, 66358945210007, 66358945210009, 66358945210013, 66358945210027] are all consecutive primes.
For n = 16755190, the numbers: [280736391936101, 280736391936103, 280736391936107, 280736391936109, 280736391936113, 280736391936127] are all consecutive primes.
For n = 39313460, the numbers: [1545548137171601, 1545548137171603, 1545548137171607, 1545548137171609, 1545548137171613, 1545548137171627] are all consecutive primes.
For n = 97387280, the numbers: [9484282305798401, 9484282305798403, 9484282305798407, 9484282305798409, 9484282305798413, 9484282305798427] are all consecutive primes.
For n = 119571820, the numbers: [14297420138112401, 14297420138112403, 14297420138112407, 14297420138112409, 14297420138112413, 14297420138112427] are all consecutive primes.
For n = 121288430, the numbers: [14710883251864901, 14710883251864903, 14710883251864907, 14710883251864909, 14710883251864913, 14710883251864927] are all consecutive primes.
For n = 130116970, the numbers: [16930425881980901, 16930425881980903, 16930425881980907, 16930425881980909, 16930425881980913, 16930425881980927] are all consecutive primes.
For n = 139985660, the numbers: [19595985005635601, 19595985005635603, 19595985005635607, 19595985005635609, 19595985005635613, 19595985005635627] are all consecutive primes.
The sum of all such integers n below 150000000 is 676333270.
137.774691 seconds (150.00 M allocations: 15.646 GiB, 0.39% gc time)
=#
