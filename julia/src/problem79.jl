#=
A common security method used for online banking is to ask the user for three random characters from a passcode. For example, if the passcode was 531278, they may ask for the 2nd, 3rd, and 5th characters; the expected reply would be: 317.

The text file, keylog.txt, contains fifty successful login attempts.

Given that the three characters are always asked for in order, analyse the file so as to determine the shortest possible secret passcode of unknown length.
=#

function problem79()
    logs = readlines("julia/src/files/0079_keylog.txt")

    allKeySatisfied = false
    n = 99;
    while !allKeySatisfied && n < 10^8
        allKeySatisfied = true;
        n += 1;
        code = string(n);
        for log in logs
            currentIndex = 0;
            for digit in log
                index = findfirst(digit, code)
                if index === nothing || index < currentIndex
                    allKeySatisfied = false;
                    break;
                else
                    currentIndex = index
                end
            end
            if !allKeySatisfied
                break
            end
        end
    end
    @printf "Possible code: %s, code length is %i.\n" n length(string(n))
end

@time problem79()

#=
Possible code: 73162890, code length is 8.
  3.224063 seconds (146.33 M allocations: 6.467 GiB, 15.46% gc time)
=#
