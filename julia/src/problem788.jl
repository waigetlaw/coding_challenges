#=
A dominating number is a positive integer that has more than half of its digits equal.

For example, 2022 is a dominating number because three of its four digits are equal to 2. But 2021 is not a dominating number.

Let D(N) be how many dominating numbers are less than 10ᴺ. For example, D(4) = 603 and D(10) = 21893256.

Find D(2022). Give your answer modulo 1 000 000 007.

Thoughts, for every N digits, we need >0.5N digits to be dominating. Whatever is left, x10 for [0-9] is the amount of dominating numbers for that many digits. But we need to check for each number of digits > 0.5N.

E.g. a 7 digit number is dominating when 4 or more digits are same.

for 4:
    3 is left, therefore with 4 0's, we have from 0-999 potentials. then * 10 for [0-9] (although it can't have a leading 0 :thinking).
for 5:
    2 is left so 0-99 * 10 (but discount leading 0's)
for 6:
    1 left so 0-9 remaining number, but discount leading 0'S
for 7:
    0 left, so only x10 for repeating 0-9 (but all 0's is 0 so discount)

For 3 digits:

110, 101, 011 not allowed.

100, 010, 001 not allowed 2.

For the 0 case... 
5 digits example

3: 00011, 00101, 01001, 10001, 10010, 10100, 11000, only 4 is valid x 99
4: 10000 only 1 is valid.

for 0 case, remove 1 of the numbers from the leftover as it must be at the start:


=#
using Printf

function problem788(N)
    DN = big(0);
    for digits ∈ 1:N
        dominating = big(fld(digits, 2) + 1);
        
        for dom ∈ dominating:digits
            dominatingNumbers = binomial(big(digits), big(dom)) * 9 ^ (digits - dom + 1)
            # @printf "digits: %i, dom: %i, dominatingNumbers: %i, power: %i\n" digits dom dominatingNumbers digits - dom + 1
            DN += mod(dominatingNumbers, 1000000007)
            DN = mod(DN, 1000000007)
        end
    end
    println(DN)
end

@time problem788(2022);

#=
471745499
  9.251168 seconds (122.76 M allocations: 3.632 GiB, 4.42% gc time)
=#
