function problem92(limit)
    return count(n -> begin
        while n != 1 && n != 89
            n = sum(map(d -> d^2, digits(n)))
        end
        return n === 89
    end, 2:limit-1)
end

@time @show problem92(10^7)

# problem92(10 ^ 7) = 8581146
# 4.204951 seconds (139.57 M allocations: 10.905 GiB, 9.82% gc time)
