#=
2⁷ = 128 is the first power of two whose leading digits are "12".
The next power of two whose leading digits are "12" is 2^80.

Define p(L, n) to be the nth-smallest value of j such that the base 10 representation of 2ʲ begins with the digits of L.
So p(12, 1) = 7 and p(12, 2) = 80.

You are also given that p(123, 45) = 12710.

Find p(123, 678910).
=#

using Printf

function problem686(L, n)
    ans = [];
    j = 0;
    prefix = string(L)
    currentPow::BigInt = 1;
    while length(ans) < n
        j += 1;
        currentPow *= 2;
        # if currentPow > 100000000000000000000
        #     currentPow = parse(BigInt, string(currentPow)[1:20])
        # end
        if startswith(string(currentPow), prefix)
            push!(ans, j)
        end
    end
    println(ans[end])
end

function problem686v2(L, n)
    ans = 0;
    j = 0;
    prefix = string(L)
    # currentPow::BigInt = 1;
    while ans < n
        j += 1;
        currentPow = BigInt(2) ^ j;
        if startswith(string(currentPow), prefix)
            ans += 1
        end
    end
    println(j)
end

function problem686v3(L, n)
    ans = 0;
    j::BigInt = 0;

    currentL::BigInt = L;
    while ans < n
        j = ceil(log2(currentL));
        # @printf "Trying j = %i\n" j
        power2::BigInt = BigInt(2)^j;
        if startswith(string(power2), string(L))
            ans += 1
            # @printf "Solution: 2^%i = %i\n" j power2
        end
        currentL *= 10;
    end
    println(j)
end

function problem686v4(L, n)
    ans = 0;
    j::BigInt = 0;
    Js = []
    
    currentL::BigInt = L;
    while ans < n
        j = ceil(log2(currentL));
        # @printf "Trying j = %i\n" j
        power2::BigInt = BigInt(2)^j;
        if startswith(string(power2), string(L))
            ans += 1
            # @printf "Solution: 2^%i = %i\n" j power2
            # @printf "Sol at j = %i\n" j
            push!(Js, j)
            currentL *= 1000; # try * 1000 because it's close to 1024 so it might work, there is no need to try x10 or x100 after a sol.
        else
            currentL *= 10;
        end
    end
    println(j)

    # diffs = [];

    # for index in eachindex(Js[1:end])
    #     if index == 1 continue end
    #     push!(diffs, Js[index] - Js[index - 1])
    # end

    # println(unique(diffs))
    # println(diffs)
end

function startsWithL(j, L)
    power2::BigInt = BigInt(2)^j;
    return startswith(string(power2), string(L))
end

function problem686v5(L, n)
    ans = 1;
    j::BigInt = 90;
    
    while ans < n
        if startsWithL(j + 196, L)
            ans += 1
            j += 196
        elseif startsWithL(j + 289, L)
            ans += 1
            j += 289
        elseif startsWithL(j + 485, L)
            ans += 1
            j += 485
        else
            j += 1
        end
    end
    println(j)

end

function problem686v6(L, n)
    ans = 0;
    
    Lfloor::BigInt = L;
    Lceil::BigInt = L + 1;
    j::BigInt = 0;
    # Js = []
    
    while ans < n
        j1 = ceil(log2(Lfloor));
        j2 = floor(log2(Lceil));
        if j1 == j2
            ans += 1
            j = j1
            # @printf "Solution: 2^%i = %i\n" j power2
            # @printf "Sol at j = %i\n" j
            # push!(Js, j)
            skip = BigInt(10)^59; # need 58 or 59 zeros for first next possibility of 2^196.
            Lfloor *= skip;
            Lceil *= skip;

            if ans > 0 && mod(ans, 10000) == 0
                @printf "Current progress: ans = %i, j = %i.\n" ans j
            end
        else
            Lfloor *= 10;
            Lceil *= 10;
        end
    end
    println(j)

end

function containsJ(lf, lc, zeroes)
    Lfloor::BigInt = BigInt(lf) * zeroes
    Lceil::BigInt = BigInt(lc) * zeroes
    j1 = ceil(log2(Lfloor));
    j2 = floor(log2(Lceil));
    if j1 == j2
        return j1
    else
        return nothing
    end
end

function problem686v7(L, n)
    ans = 1;
    j::Union{BigInt, Nothing} = 90;
    Lfloor::BigInt = BigInt(L) * BigInt(10)^25;
    Lceil::BigInt = BigInt(L + 1) * BigInt(10)^25;

    big59::BigInt = BigInt(10)^59
    big60::BigInt = BigInt(10)^60
    big86::BigInt = BigInt(10)^86
    big87::BigInt = BigInt(10)^87
    big145::BigInt = BigInt(10)^145
    big146::BigInt = BigInt(10)^146

    while ans < n
        if mod(ans, 10000) === 0
            println(ans, " ", j)
        end
        j = containsJ(Lfloor, Lceil, big59)
        if j !== nothing
            ans += 1
            Lfloor *= big59
            Lceil *= big59
            continue
        end

        j = containsJ(Lfloor, Lceil, big60)
        if j !== nothing
            ans += 1
            Lfloor *= big60
            Lceil *= big60
            continue
        end

        j = containsJ(Lfloor, Lceil, big86)
        if j !== nothing
            ans += 1
            Lfloor *= big86
            Lceil *= big86
            continue
        end

        j = containsJ(Lfloor, Lceil, big87)
        if j !== nothing
            ans += 1
            Lfloor *= big87
            Lceil *= big87
            continue
        end

        j = containsJ(Lfloor, Lceil, big145)
        if j !== nothing
            ans += 1
            Lfloor *= big145
            Lceil *= big145
            continue
        end

        j = containsJ(Lfloor, Lceil, big146)
        if j !== nothing
            ans += 1
            Lfloor *= big146
            Lceil *= big146
            continue
        end
        Lfloor *= 10
        Lceil *= 10
    end
    println(j)
end

function problem686v8(L, n)
    # using laws of logs, logA + logB = logAB
    # so to multiply by 10... we can just add log10 to the prev answer!

    ans = 0;
    j::BigInt = 0;
    logL::BigFloat = log2(L);
    logLPlusOne::BigFloat = log2(L + 1);

    logOf10::BigFloat = log2(10);
    while ans < n
        logL += logOf10;
        logLPlusOne += logOf10;

        j1 = ceil(logL)
        j2 = floor(logLPlusOne)

        if j1 == j2
            j = j1
            ans += 1
            # @printf "Solution at j = %i.\n" j
        end
    end
    println(j)
end

# @time problem686(123, 300)
# @time problem686v2(123, 300)
# @time problem686v3(123, 10)
# @time problem686v4(123, 1)
# @time problem686v5(123, 10000)
# @time problem686v6(123, 10000)
# @time problem686v7(123, 678910)
@time problem686v8(123, 678910)


#=
v1 123, 100

28343
  0.773031 seconds (237.59 k allocations: 319.482 MiB, 1.75% gc time)

v3 is significantly faster by concatenating the leading number with zeros and trying an educated j guess

85623
 11.314441 seconds (724.97 k allocations: 2.580 GiB, 0.37% gc time)
85623
 11.343344 seconds (895.91 k allocations: 2.585 GiB, 0.43% gc time)
85623
  3.442510 seconds (721.81 k allocations: 903.217 MiB, 0.48% gc time)

for L = 123, the jump is always 289, 196 or 485. Maybe some cycle...

[289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 196, 289, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485, 196, 289, 196, 289, 196, 485, 485]

Sol at j = 90
Sol at j = 379
Sol at j = 575
Sol at j = 864
Sol at j = 1060
Sol at j = 1545
Sol at j = 1741
Sol at j = 2030
Sol at j = 2226
Sol at j = 2515
Sol at j = 2711
Sol at j = 3000
Sol at j = 3196
Sol at j = 3681

v5: n = 1000

284168
  1.368175 seconds (62.00 k allocations: 251.869 MiB, 1.00% gc time, 1.00% compilation time)

v6: n = 1000

we don't actually have to calculate 2^j, we just need to check the log2 of 123xxx and 124xxx contains an integer solution, then we know a 2^x exists between them.

284168
  0.408378 seconds (704.01 k allocations: 995.572 MiB, 2.64% gc time)

n = 10000

2844275
 16.770880 seconds (7.05 M allocations: 95.047 GiB, 4.39% gc time)

 27, 112, 171, 257, 317, 462, 522, 608, 668, 754

 v7 n = 10000

 2844275
  6.449769 seconds (832.53 k allocations: 13.270 GiB, 1.78% gc time)

10000 2844275
20000 5687005
30000 8531386
40000 11373827
50000 14218693
60000 17062104
70000 19905804
80000 22749504
90000 25592430
100000 28436615
110000 31280026
120000 34124603

v9 using laws of logs
193060223
 10.853478 seconds (468.33 M allocations: 22.557 GiB, 19.19% gc time)
=#
