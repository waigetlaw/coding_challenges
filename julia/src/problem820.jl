#=
Let dₙ(x) be the nth decimal digit of the fractional part of x, or 0 if the fractional part has fewer than n digits.

For example:

d₇(1) = d₇(1/2) = d₇(1/4) = d₇(1/5) = 0
d₇(1/3) = 3 since 1/3 = 0.3333333...
d₇(3
d₇(1/7) = 1 since 1/7 = 0.1428571...
 
Let S(n) = Σdₙ(1/k), where 1 ≤ k ≤ n

You are given:

S(7) = 0 + 0 + 3 + 0 + 0 + 6 + 1 = 10
S(100) = 418

Find S(10⁷).
=#

using Printf

include("./utils/fractiondigits.jl")

function getNthFractionalDigit(n, k, l = 50)
  d::Array{Int8} = [];
  q::Int8, r::Int128 = divrem(n, k);
  idx::Int128 = 1;
  if r == 0
      return 0;
  end
  r *= 10
  remainders = Dict();
  while length(d) < l && r != 0
      # if mod(idx, 10^6) == 0
      #     @printf "Percentage done: %1.2f\n" 100*length(d)/l
      # end
      q, r_new::Int128 = divrem(Int128(r), k)
      # r_new::Int128 = Int128(r_new * 10)

      cyclestartindex = get(remainders, r, 0)
      if cyclestartindex > 0
          cycle = @view d[cyclestartindex:end]
          remaining = l - (cyclestartindex - 1)
          cycleidx = mod(remaining, length(cycle))
          if cycleidx == 0 return cycle[end] end
          return cycle[cycleidx]
      else
          remainders[r] = idx
          push!(d, q)
          idx += 1
      end
      r = r_new * 10
  end
  return idx >= l ? d[end] : 0
end

function d(n, x)
    digs= fractiondigitsv3(1, x, n)
    # println(digs)
    # a = Dict{String, AbstractVector{Int8}}("cycle" => [0, 1, 0, 7, 5, 2, 6, 8, 8, 1, 7, 2, 0, 4, 3], "initial" => [])
    # println(digs["cycle"])
    # return 1
    initial = digs["initial"]
    cycle = digs["cycle"]
    # @printf "x: %i, n: %i, digs: %s\n" x n digs
    if length(initial) >= n
      return initial[n]
    end
    if length(cycle) == 0 return 0 end
    remaining = n - length(initial)
    # @printf "remaining %i, cyclelength: %i, modidx: %i, ans: %i\n" remaining length(cycle) mod(remaining, length(cycle)) cycle[mod(remaining, length(cycle)) + 1]
    cycleidx = mod(remaining, length(cycle))

    if cycleidx == 0
      return cycle[end]
    end
    
    return cycle[cycleidx]
end

function dv2(n, x)
  return getNthFractionalDigit(1, x, n)
end

function dv3(n, x)
  return nthdigit(1, x, n)
end

function S(n)
    total::Int128 = 0;
    for k ∈ 1:n
      # if mod(k, 10^4) == 0 @printf "progress %1.2f\n" 100*k/n end
      # @printf "n: %i, k: %i = %i\n" n k dv2(n, k)
        total += dv3(n, k)
    end
    return total;
end

@time @show S(10^7)
# @time @show d(3, 3)

#=
S(100) = 418
  0.000232 seconds (583 allocations: 170.508 KiB)
418

S(10 ^ 4) = 43742
  0.949493 seconds (109.69 k allocations: 3.100 GiB, 17.11% gc time)
43742

S(10 ^ 5) = 445676
 79.693470 seconds (1.30 M allocations: 178.355 GiB, 8.30% gc time)

 with fractiondigitsv3

 S(100) = 418
  0.000588 seconds (7.11 k allocations: 495.633 KiB)
418

S(10 ^ 4) = 43742
  0.861162 seconds (25.67 M allocations: 1.255 GiB, 9.36% gc time)
43742

S(10 ^ 5) = 445676
 75.110371 seconds (2.20 G allocations: 104.948 GiB, 10.62% gc time)

removing log checking ifs etc.
S(10 ^ 5) = 445676
 70.368931 seconds (2.20 G allocations: 104.942 GiB, 10.99% gc time)

 using v3 nth digit trick
S(10 ^ 7) = 44967734
  6.405190 seconds (14 allocations: 448 bytes)
=#
