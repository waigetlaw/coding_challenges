#=
The binomial coefficient (10 C 3) = 120.
120 = 2^3 * 3 * 5 = 2 x 2 x 2 x 3 x 5, and 2 + 2 + 2 + 3 + 5 = 14.

So the sum of the terms in the prime factorisation of (10 C 3) is 14.

Find the sum of the terms in the prime factorisation of (20 000 000 C 15 000 000).

Thoughts, we only need to know what the numbers required to multiple are, and not actually do the multiplication (factorials).

(n choose k) = n! / (k! * (n - k)!)

If we can factorise that to all the numbers that multiply to the answer, then sum it instead, we would have our answer.

E.g.

(10 choose 3) = 10! / (3! * (10 - 3)!)
              = 10! / (3! * 7!)
              = 1 x 2 x 3 x 4 x 5 x 6 x 7 x 8 x 9 x 10 / (1 x 2 x 3 x 1 x 2 x 3 x 4 x 5 x 6 x 7)
              = 8 x 9 x 10 / 1 x 2 x 3
              = 2 x 2 x 2 x 3 x 3 x 2 x 5 / 1 x 2 x 3
              = 2 x 2 x 3 x 2 x 5
              = 2 x 2 x 2 x 3 x 5

We need to break down the numbers to prime factors too.

It can always start simplified as:

(n - k) x (n - k + 1) x ... x n / k!

and then we just sum all prime factors on the top minus all prime factors on the bottom:

so 10 choose 3 would have been...

top = 8 x 9 x 10 = 2 x 2 x 2 x 3 x 3 x 2 x 5 = sum -> 19
bot = 1 x 2 x 3 = 2 x 3 = sum -> 5

ans = 19 - 5 = 14
=#

using Primes

function problem231(n, k)
    top = range(n - k + 1, n)
    bot = range(2, k)

    total = 0
    for n in top
        total += sum(factor(Vector, n))
    end

    for n in bot
        total -= sum(factor(Vector, n))
    end

    print(total)
end

@time problem231(20000000, 15000000)

# 7526965179680 22.486760 seconds (306.37 M allocations: 24.305 GiB, 6.98% gc time)
