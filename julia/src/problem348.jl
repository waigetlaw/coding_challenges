#=
Many numbers can be expressed as the sum of a square and a cube. Some of them in more than one way.

Consider the palindromic numbers that can be expressed as the sum of a square and a cube, both greater than 1, in exactly 4 different ways.
For example, 5229225 is a palindromic number and it can be expressed in exactly 4 different ways:

2285² + 20³
2223² + 66³
1810² + 125³
1197² + 156³

Find the sum of the five smallest such palindromic numbers.

Thoughts:

We should iterate from 1 to x and create palindromic numbers by:

n + reverse(n)
and also: n + [0-9] + reverse(n)

That should capture all palindromic numbers and avoids the problem with leading 0's

Then we would likely go through all numbers from 1 to m and subtract the cube and see if the remaining part can be perfectly square rooted.
If we ever get more than 4 answers we break as we only want exactly 4.
=#

using Printf

function canExpressInExactlyFourWays(n)
    ways = 0;
    cube = 2;
    while cube^3 < n && ways <= 4
        remaining = n - cube^3;
        sqrtremaining = sqrt(remaining);
        if isinteger(sqrtremaining) && sqrtremaining > 1
            # @printf "%i ^ 2 + %i ^ 3 = %i\n" sqrtremaining cube n
            ways += 1;
        end
        cube += 1
    end
    return ways === 4;
end

function problem348(limit)
    palindromicNumbers = [];
    for n in 522:limit
        d =  digits(n)
        palindrome = parse(Int128, join(reverse(d)) * join(d))

        if canExpressInExactlyFourWays(palindrome)
            push!(palindromicNumbers, palindrome)
        end
        for middle in 0:9
            pmid = parse(Int128, join(reverse(d)) * string(middle) * join(d))
            if canExpressInExactlyFourWays(pmid)
                push!(palindromicNumbers, pmid)
            end
        end
    end
    sort!(palindromicNumbers)

    println(palindromicNumbers)
    println(sum(palindromicNumbers[1:5]))
    return sum(palindromicNumbers[1:5])
end

@time @show problem348(10^5);

#=
problem348(10 ^ 5) = 1004195061
 11.765660 seconds (32.30 M allocations: 1.427 GiB, 0.54% gc time, 0.10% compilation time)
=#
