#=
For any N, let f(N) be the last five digits before the trailing zeroes in N!.
For example,

9! = 362880 so f(9) = 36288
10! = 3628800 so f(10) = 36288
20! = 2432902008176640000 so f(20) = 17664

Find f(1 000 000 000 000).

Thoughts: you can just keep multiplying but ignoring all the numbers higher up and 0's.
=#

function problem160(N)
    trimmedFactorial = 1;

    for n = 2:N
        if (mod(n, 10^10) == 0) println(n) end
        # next = last(rstrip(string(trimmedFactorial * n), ['0']), 5);
        # trimmedFactorial = parse(Int64, next, base=10);

        next = trimmedFactorial * n;

        while (mod(next, 10) == 0)
            next /= 10
        end

        trimmedFactorial = mod(next, 100000)
    end

    println(trimmedFactorial)
end

@time problem160(10^7);

#=
v1 using strings to remove 0 and keep last 5 digits

51552
  1.472040 seconds (30.00 M allocations: 1.490 GiB, 5.92% gc time)

v2 mathematically removing the 0's and keeping last 5
51552.0
  0.271821 seconds (7 allocations: 528 bytes)

v2.1 if n mod 10 == 0, skip

10^9
64704.0
 27.181494 seconds (47 allocations: 3.312 KiB)
=#
