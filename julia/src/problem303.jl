#=
For a positive integer n, define f(n) as the least positive multiple of n that, written in base 10, uses only digits ≤ 2.

Thus f(2) = 2, f(3) = 12, f(7) = 21, f(42) = 210, f(89) = 1121222.

Also, ∑ f(n)/n (where n=1, 100) = 11363107 .

Find ∑f(n)/n n=1 to 10000.
=#

using Printf

function problem303(limit);

    Σfₙ= 0

    for n in 1:limit
        # if mod(n, 10) == 0 println(n); end;
        mult::Int128 = 1;
        while true
            if match(r"^[012]+$", string(n * mult)) ≠ nothing
                break
            end
            mult+=1
        end
        @printf "n: %i, mult: %i, result: %i.\n" n mult n * mult
        Σfₙ += mult # going to divide by n anyway so we do not multiply by n
    end
    # println(fₙ)
    # fₙ_div_n = map(/,fₙ,1:length(fₙ))
    # println(fₙ_div_n)
    # @printf "Σ f(n)/n = %i\n" sum(fₙ_div_n)
    @printf "Σfₙ = %i\n" Σfₙ
end

function problem303_v2(limit);

    Σfₙ= big(0)

    for n::UInt128 in 1:limit
        if mod(n, 100) == 0 println(n); end;
        stringBase::UInt128 = 1;
        string012::UInt128 = 0;
        while true

            base3Digits = digits(UInt128, stringBase, base=3);
            string012 = foldr((rest, msd) -> rest + 10*msd, base3Digits);
            # @printf "stringBase %i, base3Digits %s, string012 %i\n" stringBase base3Digits string012
            # string012 = parse(Int64, join(digits(stringBase, base=3)))
            if mod(string012, n) == 0
                break
            end
            stringBase+=1
        end
        mult = big(string012 / n);
        @printf "n: %i, mult: %i, result: %i.\n" n mult n * mult
        Σfₙ += mult # going to divide by n anyway so we do not multiply by n
    end
    # println(fₙ)
    # fₙ_div_n = map(/,fₙ,1:length(fₙ))
    # println(fₙ_div_n)
    # @printf "Σ f(n)/n = %i\n" sum(fₙ_div_n)
    @printf "Σfₙ = %i\n" Σfₙ
end

# @time problem303(100);

@time problem303_v2(10000);

#=
v1
n: 1, mult: 1, result: 1.
n: 2, mult: 1, result: 2.
n: 3, mult: 4, result: 12.
n: 4, mult: 3, result: 12.
n: 5, mult: 2, result: 10.
n: 6, mult: 2, result: 12.
n: 7, mult: 3, result: 21.
n: 8, mult: 14, result: 112.
n: 9, mult: 1358, result: 12222.

Σfₙ = 11363107
  1.325881 seconds (22.73 M allocations: 1.015 GiB, 7.17% gc time)

v2
Σfₙ = 59522298
  0.053101 seconds (956.99 k allocations: 110.562 MiB, 15.26% gc time)

  limit = 1000
  Σfₙ = 111427232491
  0.692142 seconds (8.79 M allocations: 1.348 GiB, 21.17% gc time)

  limit = 2000
Σfₙ = 167168345283
  1.399022 seconds (18.99 M allocations: 2.860 GiB, 18.17% gc time)

  with big:
  Σfₙ = 111427232491
 33.328896 seconds (895.07 M allocations: 15.480 GiB, 21.91% gc time, 0.10% compilation time)

Swapping everything to UInt128 with big mult (technically a v3)
Σfₙ = 1111981904675169
598.558346 seconds (1.96 G allocations: 665.502 GiB, 12.07% gc time, 0.00% compilation time)
=#
