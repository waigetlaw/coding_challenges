#=
Consider numbers t(n) of the form t(n) = 2n² - 1 with  n > 1.
The first such numbers are 7, 17, 31, 49, 71, 97, 127 and 161.
It turns out that only 49 = 7⋅7 and 161 = 7⋅23 are not prime.
For n ≤ 10000 there are 2202 numbers t(n) that are prime.

How many numbers t(n) are prime for n ≤ 50 000 000?
=#

using Printf, Primes

function t(n)
    return 2n^2 - 1
end

function problem216(limit)
    total = 0;
    for n ∈ 2:limit
        if isprime(t(n))
            total += 1
        end
    end
    @printf "The total numbers of t(n) that are prime for n ≤ %i is: %i.\n" limit total
    return total;
end

@time @show problem216(50000000);

#=
The total numbers of t(n) that are prime for n ≤ 50000000 is: 5437849.
problem216(50000000) = 5437849
 54.316377 seconds (20 allocations: 672 bytes)
=#
