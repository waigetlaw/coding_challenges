#=
A spider, S, sits in one corner of a cuboid room, measuring 6 by 5 by 3, and a fly, F, sits in the opposite corner. By travelling on the surfaces of the room the shortest "straight line" distance from S to F is 10 and the path is shown on the diagram.

However, there are up to three "shortest" path candidates for any given cuboid and the shortest route doesn't always have integer length.

It can be shown that there are exactly 2060 distinct cuboids, ignoring rotations, with integer dimensions, up to a maximum size of M by M by M, for which the shortest route has integer length when M = 100. This is the least value of M for which the number of solutions first exceeds two thousand; the number of solutions when M = 99 is 1975.

Find the least value of M such that the number of solutions first exceeds one million.

Thoughts, if we have A x B x C cuboid, the three potential shortest paths are the hypotenuse of the triangles:

base A, height B + C
base B, height A + C
base C, height A + B

I assume the shortest path is when the base is closest to the height.

So... do we just loop through all potential cuboids and calculate their shortest paths?
=#

using Printf

function problem86(limit)
    
    
    intShortPaths = 0;
    M = 0;
    while (intShortPaths <= limit)
        intShortPaths = 0;
        M += 1;
        for A in 1:M, B in A:M, C in B:M
            # shortest path is always when base is C because C is the largest side. Naturally abs(C - (A + B)) will be minimised.
            height = A + B
            shortest_path = sqrt(C^2 + height^2);

            if isinteger(shortest_path)
                intShortPaths += 1
            end
        end

        @printf "For M = %i, number of integer shortest paths is %i.\n" M intShortPaths
    end
    println(M)
end

@time problem86(10^6)

#=
For M = 1818, number of integer shortest paths is 1000457.
1818
=#
