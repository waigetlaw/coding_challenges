#=
NOTE: This problem is a more challenging version of Problem 81.

The minimal path sum in the 5 by 5 matrix below, by starting in any cell in the left column and finishing in any cell in the right column, and only moving up, down, and right, is indicated in red and bold; the sum is equal to 994.

[
    [131, 673, 234, 103, 18],
    [201, 96, 342, 965, 150],
    [630, 803, 746, 422, 111],
    [537, 699, 497, 121, 956],
    [805, 732, 524, 37, 331]
]

Find the minimal path sum from the left column to the right column in matrix.txt (right click and "Save Link/Target As..."), a 31K text file containing an 80 by 80 matrix.

Thoughts: step through each col at a time and calculate the min path to reach that by summing up either the direct left item, or going up/down first and left.

notes:
firstCol = matrix[:,1]
firstCol = matrix[1,:]
=#

using Printf

function problem82(matrix::Matrix{Int64} = [
    131 673 234 103 18;
    201 96 342 965 150;
    630 803 746 422 111;
    537 699 497 121 956;
    805 732 524 37 331
])

    # display(matrix)

    for (i, col) in enumerate(eachcol(matrix)[2:end])
        prevCol = matrix[:,i]

        updatedCol = []
        for (n_index, n) in enumerate(col)
            potentialPaths = []
            # println()
            for (m_index, m) in enumerate(prevCol)
                if n_index == m_index
                    # @printf "Equals, n_index: %i, m_index: %i, n: %i, m: %i, min path: %i\n" n_index m_index n m m
                    push!(potentialPaths, m)
                elseif n_index < m_index
                    # @printf "n<m, n_index: %i, m_index: %i, n: %i, m: %i, min path: %i\n" n_index m_index n m m + sum(col[n_index + 1:m_index])
                    push!(potentialPaths, m + sum(col[n_index + 1:m_index]))
                else
                    # @printf "n>m, n_index: %i, m_index: %i, n: %i, m: %i, min path: %i\n" n_index m_index n m m + sum(col[m_index:n_index - 1])
                    push!(potentialPaths, m + sum(col[m_index:n_index - 1]))
                end
            end
            push!(updatedCol, n + minimum(potentialPaths))
        end

        matrix[:,i + 1] = updatedCol
        # display(matrix)
    end

    return minimum(matrix[:,end])
end

rows = readlines("julia/src/files/0082_matrix.txt")

M = Matrix{Int64}(undef, 0, length(rows))
for row in rows
    r = map(s -> parse(Int64, s), split(row, ","))
    global M = [M;r']
end

# display(M)
@time @show problem82(M);

#=
problem82(M) = 260324
  0.058592 seconds (1.56 M allocations: 161.444 MiB, 28.21% gc time)
=#
