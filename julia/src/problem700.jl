#=
Leonhard Euler was born on 15 April 1707.

Consider the sequence 1504170715041707n mod 4503599627370517.

An element of this sequence is defined to be an Eulercoin if it is strictly smaller than all previously found Eulercoins.

For example, the first term is 1504170715041707 which is the first Eulercoin. The second term is 3008341430083414 which is greater than 1504170715041707 so is not an Eulercoin. However, the third term is 8912517754604 which is small enough to be a new Eulercoin.

The sum of the first 2 Eulercoins is therefore 1513083232796311.

Find the sum of all Eulercoins.
=#

using Printf

function problem700()
    num = 1504170715041707;
    m = 4503599627370517;
    eulercoins = [num];
    n = num;
    i = 0;
    while true && length(eulercoins) < 40
        i+=1;
        n += num;
        remainder = mod(n, m);

        if remainder < eulercoins[end]
            push!(eulercoins, remainder)
            @printf "New eulercoin: %i.\n" remainder
        end

        n = remainder
    end



    println(eulercoins)
    println(sum(eulercoins))
end

# using v1 to calculate first 40 eulercoins, then we make use of the modulo multiplicative inverse
function problem700v2()
    num::Int128 = 1504170715041707;
    m = 4503599627370517;
    eulercoins = [1504170715041707, 8912517754604, 2044785486369, 1311409677241, 578033868113, 422691927098, 267349986083, 112008045068, 68674149121, 25340253174, 7346610401, 4046188430, 745766459, 428410324, 111054189, 15806432, 15397267, 14988102, 14578937, 14169772, 13760607, 13351442, 12942277, 12533112, 12123947, 11714782, 11305617, 10896452, 10487287, 10078122, 9668957, 9259792, 8850627, 8441462, 8032297, 7623132, 7213967, 6804802, 6395637, 5986472];
    
    inv::Int128 = invmod(num, m);

    n = 6854147137

    while eulercoins[end] != 1
        potential_n = []
        for remainder ∈ 1:eulercoins[end] - 1
            mult = mod(inv * remainder, m)
            if mult > n
                push!(potential_n, mult)
            end

        end
        n = minimum(potential_n)
        push!(eulercoins, mod(num * n, m));
    end

    println(eulercoins)
    println(sum(eulercoins))
end

@time problem700v2()

#=
[1504170715041707, 8912517754604, 2044785486369, 1311409677241, 578033868113, 422691927098, 267349986083, 112008045068, 68674149121, 25340253174, 7346610401, 4046188430, 745766459, 428410324, 111054189, 15806432, 15397267, 14988102, 14578937, 14169772, 13760607, 13351442, 12942277, 12533112, 12123947, 11714782, 11305617, 10896452, 10487287, 10078122, 9668957, 9259792, 8850627, 8441462, 8032297, 7623132, 7213967, 6804802, 6395637, 5986472, 5577307, 5168142, 4758977, 4349812, 3940647, 3531482, 3122317, 2713152, 2303987, 1894822, 1485657, 1076492, 667327, 258162, 107159, 63315, 19471, 14569, 9667, 4765, 4628, 4491, 4354, 4217, 4080, 3943, 3806, 3669, 3532, 3395, 3258, 3121, 2984, 2847, 2710, 2573, 2436, 2299, 2162, 2025, 1888, 1751, 1614, 1477, 1340, 1203, 1066, 929, 792, 655, 518, 381, 244, 107, 77, 47, 17, 4, 3, 2, 1]
1517926517777556
  4.396363 seconds (141.41 M allocations: 4.731 GiB, 36.84% gc time, 0.08% compilation time)
=#
