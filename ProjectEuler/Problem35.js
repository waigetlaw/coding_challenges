/**
 * The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
 */

// using gen prime, we can check all combinations of rotations easily - although it should stop
// generating the next one as soon as one isn't prime.
// for this problem though, only below 1,000,000 is very easy and does not need to be very efficient

// First, find all primes below 1,000,000 then filter list for those that are all circular.

// update: not permutations, but rotations...

const isPrime = require('../JSUtils/isPrime');
const genRotations = require('../JSUtils/genRotations');

const primes = [2];
for (let i = 3; i < 1000000; i += 2) {
    if (isPrime(i)) primes.push(i);
}

const divisibleEndings = ['0', '2', '4', '5', '6', '8']; // speed up by skipping rotations
const circPrimes = primes.reduce((circ, p) => {
    if (divisibleEndings.some((n) => String(p).includes(n))) return circ;
    if (circ.includes(p)) return circ;
    const allRot = genRotations(String(p)).map(s => +s);
    if (allRot.every(p => isPrime(p))) return [...circ, ...allRot];
    return circ;
}, [2, 5]); // due to skipping rotations, 2 and 5 must be manually added.

console.log(circPrimes.length); // 55
