/*
In the following equation x, y, and n are positive integers.

1/x + 1/y = 1/n

For n = 4 there are exactly three distinct solutions:

1/5 + 1/20 = 1/4
1/6 + 1/12 = 1/4
1/8 + 1/8 = 1/4

What is the least value of n for which the number of distinct solutions exceeds one-thousand?

NOTE: This problem is an easier version of Problem 110; it is strongly advised that you solve this one first.

Thoughts, by rearranging the equation we get:
y = (nx)/(x - n)
plotting this on a graph, we see asymptotes at n.
We also always see a 'middle' at 2n.

Therefore, for each n, we only need to check from n+1 to 2n if the equation y = nx / (x - n) is an integer or not.
*/

const problem108 = () => {
    console.time();
    let distinctSolves = 0;
    let maxSoFar = { n: 0, distinctSolves: 0 };
    let n = 4;
    while (distinctSolves <= 1000) {
        if (n % 10000 === 0) console.log(n, maxSoFar)
        distinctSolves = 0;
        for (let x = n + 1; x <= 2 * n; x++) {
            const top = (n * x);
            const bot = (x - n);
            if (top % bot === 0) {
                // console.log(`1/${x} + 1/${top/bot} = 1/${n}`)
                distinctSolves++;
            }
        }
        if (distinctSolves > maxSoFar.distinctSolves) maxSoFar = { n, distinctSolves };
        // console.log(`Total solves for n = ${n} is ${distinctSolves}`);
        n++;
    }
    console.log(--n);
    console.timeEnd();
};

problem108();

/*

10000 { n: 9240, distinctSolves: 284 }
20000 { n: 18480, distinctSolves: 365 }
30000 { n: 27720, distinctSolves: 473 }
40000 { n: 27720, distinctSolves: 473 }
50000 { n: 27720, distinctSolves: 473 }
60000 { n: 55440, distinctSolves: 608 }
70000 { n: 55440, distinctSolves: 608 }
80000 { n: 55440, distinctSolves: 608 }
90000 { n: 83160, distinctSolves: 662 }
100000 { n: 83160, distinctSolves: 662 }
110000 { n: 83160, distinctSolves: 662 }
120000 { n: 110880, distinctSolves: 743 }
130000 { n: 120120, distinctSolves: 851 }
140000 { n: 120120, distinctSolves: 851 }
150000 { n: 120120, distinctSolves: 851 }
160000 { n: 120120, distinctSolves: 851 }
170000 { n: 120120, distinctSolves: 851 }
180000 { n: 120120, distinctSolves: 851 }
180181
default: 8:59.574 (m:ss.mmm)
wrong? it was 180180, I did an unecessary n++;
*/
