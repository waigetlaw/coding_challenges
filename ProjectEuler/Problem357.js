/**
 * Consider the divisors of 30: 1,2,3,5,6,10,15,30.
It can be seen that for every divisor d of 30, d+30/d is prime.

Find the sum of all positive integers n not exceeding 100 000 000
such that for every divisor d of n, d+n/d is prime.
 */

// seems bruteforceable although there may be some way to determine if certain divisors wont work (with odd even combs)
// apart from 1, every other odd number will fail as (1 + (odd/1)) == even number and even wont be prime
// so we skip all odds whilst including 1
// furthermore, since all numbers are even, 2 is always a divisor.
// if a number divided by 2 is even, it wont work as 2 + (X/2: even) makes even, which cant be prime
// therefore we can skip in multiples of 4
// and further, if the number + 1 is not prime, we dont need to check other divisors. (+1 is from where d = number, so d + i/d) is d+1
// and since we already jump in multiples of 4, we know that for d = 2 it must be prime, but we still calc this for getting i/2

// update: what d + i/d does is actually just sum up the product pairs, so for 30, 2, 15 is just 2 + 15 = 17 is prime or not
// rather than 2 checks of 2 + 30/15 = 17 and 15 + 30/15 = 17

const isPrime = require('../JSUtils/isPrime');

let sum = 1;    // include 1
console.time();
for (let i = 2; i < 100000000; i+=4) {
    let allDivisorsPrime = true;
    if (i % 1000002 === 0) console.log(i); // for progress
    const limit = Math.sqrt(i) | 0;
    for (let div = 1; div <= limit; div++) { // there can be repeats for div equal limit but I think performance is fine
        if (i % div === 0) {
            if (!isPrime(div + (i/div))) {
                allDivisorsPrime = false;
                break;
            }
        }
    }

    if (allDivisorsPrime) {
        sum += i;
    }
}
console.timeEnd();
console.log(sum);   // 1739023853137
