/**
 * For a prime p let S(p) = (∑ (p-k)!) mod(p) for 1 ≤ k ≤ 5.

For example, if p=7,
(7-1)! + (7-2)! + (7-3)! + (7-4)! + (7-5)! = 6! + 5! + 4! + 3! + 2! = 720+120+24+6+2 = 872.
As 872 mod(7) = 4, S(7) = 4.

It can be verified that ∑ S(p) = 480 for 5 ≤ p < 100.

Find ∑ S(p) for 5 ≤ p < 108.
 */

/* Thoughts
(7-1)! + (7-2)! + (7-3)! + (7-4)! + (7-5)! = 6! + 5! + 4! + 3! + 2! = 720+120+24+6+2 = 872.

872 mod 7 = 4

we should be able to mod each one as we go

720 becomes 6
120 becomes 1
24 becomes 3
6 is 6
2is 2

total is 18
where 18 mod 4 = 4

also I believe it is possible to mod during multiplication for factorial:

6! =
6 mod 7 = 6
6 * 5 = 30 mod 7 = 2
2 * 4 = 8 mod 7 = 1
1 * 3 = 3 mod 7 = 3
3 * 2 = 6 mod 7 = 6
6 * 1 = 6
correct.

So we just need to mod as we go along to keep the numbers small - however for p up to 10^8 - is this enough?
*/

// firstly we need a way to generate the 10^8 primes quicker...
// we need a new util that either does the sieve method or goes through only primes as divisors.

const calcFactWithMod = (n, mod) => {
    if (n <= 1) return 1;
    let ans = 1;
    while (n > 1) {
        ans = (n * ans) % mod;
        n--;
    }
    return ans
}

/*
{ n_start: 6, n_end: 110, divCount: 8, div: 10, mod: 13 }

so 6 + 3X mod 10 = 0
3x mod 10 = -6 || 4
10 + 4 = 3x // not divisible!!
10 + 4 + 10 = 3x divisible
x = 8
*/

const calcDivCount = (start, div, mod) => {
    const diff = mod - div;
    modRequired = -start + div;
    while (modRequired % diff !== 0) {
        modRequired += div;
    }
    return modRequired / diff;
}

const divideFact = (n, div, mod) => {
    // if (div === n) return div;
    // const n_start = n;
    let divCountNew = calcDivCount(n, div, mod);
    let divCount = 0;
    // while (n % div !== 0) {
    //     divCount++;
    //     n += mod
    // }
    // console.log({ n_start, n_end: n, n_end_new: (n_start + (mod * divCountNew)) / div, divCount, divCountNew, div, mod})
    // return (n / div) % mod
    return ((n + (mod * divCountNew)) / div) % mod
}

const genPrimes = require('../JSUtils/genPrimes');

console.time()
const primes = genPrimes(100000000);

// to start at p = 5
primes.shift();
primes.shift();

let sum = 0;
let log = {}

for (const prime of primes) {
    // console.log(prime)
    let Sp = prime - 1;

    // let maxK = Math.min(10, prime - 1);
    // let Sp = calcFactWithMod(prime - maxK, prime);

    let fact = Sp;
    let facts = [Sp];

    // log[prime].push( { f: prime - maxK, fact})
    // log[prime] = []
    // log[prime].push( { f: prime, fact})

    // for (let k = maxK - 1; k > 0; k--) {
    //     fact = fact * ( prime - k) % prime
    //     Sp += fact;
    //     facts.push(fact);
    //     log[prime].push( { f: prime - k, fact})
    // }

    for (let k = 1; k < 5; k++) {
        fact = divideFact(fact, prime - k, prime)
        Sp += fact;
        facts.push(fact);
            // log[prime].push( { f: prime - k, fact})
    }
    Sp = Sp % prime;
    sum += Sp;
    // log.push({ prime, facts: JSON.stringify(facts), Sp })
};

// console.log(log)
console.log(sum)

console.timeEnd()

/*
 Original Primes up to 10^8:
 5761455
default: 82148.133ms

With a 'naive; sieve:
$ node JSUtils/genPrimes.js
5761455
default: 13381.417ms

with idx starting from p^2:
5761455
default: 13331.127ms    // strange that there isn't much improvement

as optimised as can be to my knowledge:
5761455
default: 13121.062ms


for prime p, facts (p-5)!, (p-4)!, (p-3)!, (p-2)!, (p-1)!

[a, b, c, d, e]

e = p - 1
c = (p-1)/2
d = 1

why?

p = 7
(7-1)! = 1 * 2 * 3 * 4 * 5 * 6 will always be 6
(7-2)! = 1 * 2 * 3 * 4 * 5 will always be 1
(7-3)! = 1 * 2 * 3 * 4 will always be half 6 = 3
(7-4)! = 1 * 2 * 3 will always be ?
(7-5)! = 1 * 2 will always be ?

┌─────────┬───────┬───────────────────────┬─────┐
│ (index) │ prime │         facts         │ Sp  │
├─────────┼───────┼───────────────────────┼─────┤
│    0    │   5   │     '[1,1,2,1,4]'     │  4  │
│    1    │   7   │     '[2,6,3,1,6]'     │  4  │
│    2    │  11   │    '[5,2,5,1,10]'     │  1  │
│    3    │  13   │    '[7,11,6,1,12]'    │ 11  │
│    4    │  17   │    '[12,3,8,1,16]'    │  6  │
│    5    │  19   │   '[15,16,9,1,18]'    │  2  │
│    6    │  23   │   '[22,4,11,1,22]'    │ 14  │
│    7    │  29   │    '[6,5,14,1,28]'    │ 25  │
│    8    │  31   │   '[9,26,15,1,30]'    │ 19  │
│    9    │  37   │   '[20,31,18,1,36]'   │ 32  │
│   10    │  41   │   '[29,7,20,1,40]'    │ 15  │

1000
38140
default: 8.479ms with divFact calc

38140
default: 4.649ms
with calcFactWithMod - this is faster because it takes too much effort figuring out the prev mods with div compared to the next mod with mult

38140
default: 3.684ms

with better calcDivFact that tries to calculate the divCount

1000000
18773749932
default: 55.931ms

calc div better...


{ n_start: 2, n_end: 35, divCount: 3, div: 7, mod: 11 }

we want to know if we start with 2, how many 11 to add, to make it divisible by 7.

Well, each time we add 11, it's adding an extra mod 4.

so:

2
6
10
14 <- here is divisible by 7

(x = 3 is ans)

so (2 + 4X) mod 7 = 0, find X

4X mod 7 = -2 (or 5)

4X mod 7 = 5

7 + 5 = 12 / 4 = 3

is there a scenario where 7 + 5 is not divisible by 4?


ex 2:

{ n_start: 6, n_end: 110, divCount: 8, div: 10, mod: 13 }

so 6 + 3X mod 10 = 0
3x mod 10 = -6 || 4
10 + 4 = 3x // not divisible!!
10 + 4 + 10 = 3x divisible
x = 8

Either way, even though sometimes it's not divisble, this method would keep with smaller numbers I think.

{ n_start: 4, n_end: 9, divCount: 1, div: 3, mod: 5 }
{ n_start: 3, n_end: 8, divCount: 1, div: 2, mod: 5 }
{ n_start: 4, n_end: 4, divCount: 0, div: 1, mod: 5 }
{ n_start: 6, n_end: 20, divCount: 2, div: 5, mod: 7 }
{ n_start: 4, n_end: 18, divCount: 2, div: 3, mod: 7 }
{ n_start: 10, n_end: 54, divCount: 4, div: 9, mod: 11 }
{ n_start: 6, n_end: 72, divCount: 6, div: 8, mod: 11 }
{ n_start: 9, n_end: 42, divCount: 3, div: 7, mod: 11 }
{ n_start: 12, n_end: 77, divCount: 5, div: 11, mod: 13 }
{ n_start: 7, n_end: 20, divCount: 1, div: 10, mod: 13 }
{ n_start: 2, n_end: 54, divCount: 4, div: 9, mod: 13 }
{ n_start: 16, n_end: 135, divCount: 7, div: 15, mod: 17 }
{ n_start: 9, n_end: 196, divCount: 11, div: 14, mod: 17 }
{ n_start: 14, n_end: 65, divCount: 3, div: 13, mod: 17 }
{ n_start: 18, n_end: 170, divCount: 8, div: 17, mod: 19 }
{ n_start: 10, n_end: 48, divCount: 2, div: 16, mod: 19 }
{ n_start: 3, n_end: 60, divCount: 3, div: 15, mod: 19 }
41
*/
