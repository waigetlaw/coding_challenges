/*
It turns out that 12cm is the smallest length of wire that can be bent to form an integer sided right angle triangle in exactly one way, but there are many more examples.

12cm: (3, 4, 5)
24cm: (6, 8, 10)
30cm: (5, 12, 13)
36cm: (9, 12, 15)
40cm: (8, 15, 17)
48cm: (12, 16, 20)

In contrast, some lengths of wire, like 20cm, cannot be bent to form an integer sided right angle triangle, and other lengths allow more than one solution to be found; for example, using 120cm it is possible to form exactly three different integer sided right angle triangles.

120cm: (30, 40, 50), (20, 48, 52), (24, 45, 51)

Given that L is the length of the wire, for how many values of L <= 1500000 can exactly one integer sided right angle triangle be formed?

Thoughts we can shift x-y through all possibilities where:
start x = 1
y = Math.ceil(L/2) // probably not possible because hypotenuse needs to be biggest

then find some kind of function as y changes, so does x, and thus so does hypotenuse.
we need to check for all y from start -> 1 if x and hypotenuse are integers.

Or actually consider this as from x = 1 to x => max

With some algebra from x + y + z = L
x^2 + y^2 = z^2

z = (p^2 + x^2 - 2px + x^2)/(2p - 2x)

v2 thoughts:

can we limit y > x ? and this should remove the need for the Set too to speed things up.
 */

const problem75 = () => {
    console.time();

    let uniqueL = 0;
    for (let L = 12; L <= 1500000; L++) {
        if (L % 100000 === 0) console.log(L);
        let solutions = 0;
        for (let x = 1; x < L; x++) {
            const z_top = L ** 2 + (2 * x ** 2) - 2 * L * x;
            const z_bot = 2 * L - 2 * x;
            if (z_bot <= 0) break;
            z = z_top/z_bot;
            const y = L - z - x;
            if (y <= 0) break;
            if (x > y) break;

            if (Number.isInteger(y) && Number.isInteger(z)) {
                // console.log({L, x, y, z})
                solutions++;
            }
            if (solutions === 2) break;
        }
        if (solutions === 1) uniqueL++;
    }
    console.log(uniqueL);
    console.timeEnd();
};

problem75();

/*
161667
default: 25:12.128 (m:ss.mmm)
*/
