/**
 * There are exactly ten ways of selecting three from five, 12345:

123, 124, 125, 134, 135, 145, 234, 235, 245, and 345

In combinatorics, we use the notation, (53)=10.

In general, (nr)=n!r!(n−r)!, where r≤n, n!=n×(n−1)×...×3×2×1, and 0!=1.

It is not until n=23, that a value exceeds one-million: (2310)=1144066.

How many, not necessarily distinct, values of (nr) for 1≤n≤100, are greater than one-million?
 */

/*
We will only need to work out the first value of (n r) that breaches 1000000 then we simply do n - 2*r for how many
combinations there exceeds 1 million. The reason is that the choose function is like a bellcurve, it increases, with its
max value at the middle then decreases symmetrically. So if we find the limit where (nr) > 1000000, we also know at what
point (nr) on 'the other end' will dip back below 1000000 and we just have to count whats in between.

we just need exceeding 1 mill, not the actual value, so we will not need to calc 100!

we just need to start with the denominator, and then slowly multiply this by 1, 2, 3, 4... until either it
exceeds 1 million, or reaches n.
*/

const factorial = require('../JSUtils/factorial');

const limit = 100;
let valuesAboveOneMillion = 0;

for (let n = 1; n <= limit; n++) {
    let aboveOneMillion = false;
    for (let r = 1; r < Math.ceil(n/2); r++) {
        const rf = factorial(r);

        let value = 1/rf;
        for (let i = (n - r) + 1; i <= n; i++) {
            value *= i;
            if (value > 1000000) {
                aboveOneMillion = true;
                break;
            }
        }
        if (aboveOneMillion) {
            const exceedsInN = (n - r) - r + 1; // n - r is when (nr) dips back below 1mil
            valuesAboveOneMillion += exceedsInN;
            break;
        }
    }
}

console.log(valuesAboveOneMillion); // 4075
