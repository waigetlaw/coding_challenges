/*
In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:

High Card: Highest value card.
One Pair: Two cards of the same value.
Two Pairs: Two different pairs.
Three of a Kind: Three cards of the same value.
Straight: All cards are consecutive values.
Flush: All cards of the same suit.
Full House: Three of a kind and a pair.
Four of a Kind: Four cards of the same value.
Straight Flush: All cards are consecutive values of same suit.
Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
The cards are valued in the order:
2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.

If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives (see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared (see example 4 below); if the highest cards tie then the next highest cards are compared, and so on.
*/
const readFile = require('../JSUtils/readFile');
const path = require('path')

const order = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'];

const HAND_TYPE = {
    STRAIGHT_FLUSH: 8,
    FOUR: 7,
    FULLHOUSE: 6,
    FLUSH: 5,
    STRAIGHT: 4,
    THREE: 3,
    TWO_PAIR: 2,
    PAIR: 1,
    HIGHCARD: 0
}

const getCardRank = card => order.indexOf(card[0]);

const sortCards = cards => cards.sort((a, b) => getCardRank(b) - getCardRank(a))
const countCards = cards => cards.reduce((count, card) => count[card[0]] ? { ...count, [card[0]]: count[card[0]] + 1 } : { ...count, [card[0]]: 1 }, {});
const getXCard = x => count => Object.entries(count).find(([, val]) => val === x)?.[0]

const isFlush = cards => cards.every(card => card[1] === cards[0][1]) && { type: HAND_TYPE.FLUSH, highCards: cards };
const isStraight = cards => cards.every((card, i) => i === 0 || getCardRank(cards[i - 1]) - getCardRank(card) === 1) && { type: HAND_TYPE.STRAIGHT, highCard: cards[0] };
const isStraightFlush = cards => isFlush(cards) && isStraight(cards) && { type: HAND_TYPE.STRAIGHT_FLUSH, highCard: cards[0] };
const isFour = cards => {
    const count = countCards(cards);
    const quad = getXCard(4)(count);
    if (!quad) return false;
    const highCard = getXCard(1)(count);
    return { type: HAND_TYPE.FOUR, quad, highCard }
}
const isFullHouse = cards => {
    const count = countCards(cards);
    const triple = getXCard(3)(count);
    const pair = getXCard(2)(count);
    if (triple && pair) return { type: HAND_TYPE.FULLHOUSE, triple, pair };
    return false;
}
const isThree = cards => {
    const count = countCards(cards);
    const triple = getXCard(3)(count);
    if (!triple || Object.keys(count).length !== 3) return false;
    const highCards = cards.filter(card => card[0] !== triple);
    return { type: HAND_TYPE.THREE, triple, highCards };
}
const isTwoPair = cards => {
    const count = countCards(cards);
    const pairs = Object.entries(count).filter(([, val]) => val === 2).map(([key]) => key).sort((a, b) => getCardRank(b) - getCardRank(a))
    const highCard = getXCard(1)(count);
    if (Object.keys(count).length !== 3 || !pairs.length === 2) return false;
    return { type: HAND_TYPE.TWO_PAIR, pairs, highCard }
}
const isOnePair = cards => {
    const count = countCards(cards);
    const pair = getXCard(2)(count);
    if (Object.keys(count).length !== 4) return false;
    const highCards = cards.filter(card => card[0] !== pair);
    return { type: HAND_TYPE.PAIR, pair, highCards }
}
const highCards = cards => ({ type: HAND_TYPE.HIGHCARD, highCards: cards });

const checkHand = cards => {
    const handChecks = [isStraightFlush, isFour, isFullHouse, isFlush, isStraight, isThree, isTwoPair, isOnePair, highCards];
    for (const handCheck of handChecks) {
        const hand = handCheck(cards);
        if (hand) return hand;
    }
}

const compareHighCards = (cards1, cards2) => {
    for (let i = 0; i < cards1.length; i++) {
        if (getCardRank(cards1[i]) === getCardRank(cards2[i])) continue;
        return getCardRank(cards1[i]) > getCardRank(cards2[i]) ? 'p1' : 'p2';
    }
}

const compareHands = ({ p1, p2 }) => {
    const hand1 = checkHand(p1);
    const hand2 = checkHand(p2);

    if (hand1.type > hand2.type) return 'p1';
    if (hand1.type < hand2.type) return 'p2';
    switch (hand1.type) {
        case 8:
        case 4:
            return getCardRank(hand1.highCard) > getCardRank(hand2.highCard) ? 'p1' : 'p2';
        case 7:
            return getCardRank(hand1.quad) > getCardRank(hand2.quad) ? 'p1' : 'p2';
        case 6:
        case 3:
            return getCardRank(hand1.triple) > getCardRank(hand2.triple) ? 'p1' : 'p2';
        case 2:
            const winner = compareHighCards(hand1.pairs, hand2.pairs);
            if (winner) return winner;
            return getCardRank(hand1.highCard) > getCardRank(hand2.highCard) ? 'p1' : 'p2';
        case 1:
            if (getCardRank(hand1.pair) > getCardRank(hand2.pair)) return 'p1';
            if (getCardRank(hand1.pair) < getCardRank(hand2.pair)) return 'p2';
            return compareHighCards(hand1.highCards, hand2.highCards);
        default: // 5 and 0
            return compareHighCards(hand1.highCards, hand2.highCards);

    }
}

const countPlayerOneWins = games => games.reduce((wins, game) => compareHands(game) === 'p1' ? wins + 1 : wins, 0);

const problem54 = async () => {
    console.time()
    const filePath = path.join(__dirname, './files/0054_poker.txt');
    const content = await readFile(filePath);
    const games = content.split('\n').filter(Boolean).reduce((hands, line) => {
        const cards = line.split(' ');
        hands.push({ p1: sortCards(cards.slice(0, 5)), p2: sortCards(cards.slice(5)) });
        return hands;
    },[]);

    console.log(countPlayerOneWins(games));
    console.timeEnd();
}

problem54();

/*
376
default: 34.143ms
*/
