/**
 * Consider all integer combinations of ab for 2 ≤ a ≤ 5 and 2 ≤ b ≤ 5:

22=4, 23=8, 24=16, 25=32
32=9, 33=27, 34=81, 35=243
42=16, 43=64, 44=256, 45=1024
52=25, 53=125, 54=625, 55=3125
If they are then placed in numerical order, with any repeats removed, we get the following sequence of 15 distinct terms:

4, 8, 9, 16, 25, 27, 32, 64, 81, 125, 243, 256, 625, 1024, 3125

How many distinct terms are in the sequence generated by ab for 2 ≤ a ≤ 100 and 2 ≤ b ≤ 100?
 */

// Every unique number should have a unique way of being reduced to prime factors.
// Therefore if we reduce them all to the prime factors and powers, we should be able to filter
// to distinct numbers without knowing the actual number.
//
// e.g. 4^2 is actually just (2^2)^2 which makes 2^4
// By reducing everything to the min possible, we can calculate all the distinct terms.
// we only need to reduce a, not b.
// e.g. 15^5 would become (3*5)^5 =>  3^5 * 5^5
// and 12^4 would become (2 * 2 * 3)^4 => (2^2)^4 * (3^4) => 2^8 * 3^4
// essentially we get from a^b into => (a1^b1 * a2^b2 * ... * an^bn)^b
// which reduces to a1^(b1*b) * a2^(b2*b) * ... * an^(bn*b)
//
// we shall put this into a string format:
// 2^b, 3^b, 5^b,..., n^b etc.
// then once they are all in their reduced form, we just need to find the distinct terms.

const ab = new Set();

for (let a = 2; a <= 100; a++) {
    for (let b = 2; b <= 100; b++) {
        let factors = [];
        let n = a;  // only need to reduce a
        if (n % 2 === 0) {
            let times = 0;
            while (n > 1 && n % 2 === 0) {
                n /= 2;
                times++;
            }
            factors.push({ base: 2, times });
        }

        let div = 3;
        while (n > 1) {
            if (n % div === 0) {
                let times = 0;
                while (n > 1 && n % div === 0) {
                    n /= div;
                    times++;
                }
                factors.push({ base: div, times });
            }
            div += 2;
        }
        ab.add(
            factors.reduce((primeIdentity, f) =>
                [...primeIdentity, `${f.base}^${f.times * b}`], []).join()
        );
    }
}

console.log(ab.size);   // 9183
