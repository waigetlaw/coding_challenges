/*
A composite is a number containing at least two prime factors. For example, 15 = 3 x 5; 9 = 3 x 3; 12 = 2 x 2 x 3.

There are ten composites below thirty containing precisely two, not necessarily distinct, prime factors: 
4, 6, 9, 10, 14, 15, 21, 22, 25, 26.

How many composite integers, n < 10^8, have precisely two, not necessarily distinct, prime factors?

Thoughts: First these are obviously the non-primes. Likely we can just try to divide primes and if more than 2 divide, we skip.

edit: misunderstood question - I thought 12 was a semiprime but it's not because there are 3 prime factors total.
Since it's just two, we can easily brute force this...
*/

const genPrimesV2 = require("../JSUtils/genPrimesV2");

const problem187 = () => {
    console.time();

    const limit = 10 ** 8;
    const primes = genPrimesV2(limit);
    let semiPrimes = 0;

    for (let i = 0; i < primes.length; i++) {
        for (let j = i; j < primes.length; j++) {
            const n = primes[i];
            const m = primes[j];
            if (n * m < limit) {
                semiPrimes++;
            } else {
                break;
            }
        }
    }

    console.log(semiPrimes);
    console.timeEnd();
};

problem187();

/*
17427258
default: 1.021s
*/
