/*
A Hamming number is a positive number which has no prime factor larger than 5.
So the first few Hamming numbers are 1,2,3,4,5,6,8,9,10,12,15.
There are 1105 Hamming numbers not exceeding 10^8.

We will call a positive number a generalised Hamming number of type n, if it has no prime factor larger than n.
Hence the Hamming numbers are the generalised Hamming numbers of type 5.

How many generalised Hamming numbers of type 100 are there which don't exceed 10^9?

Thoughts:
I feel like we can apply a modified version of the Sieve of Eratosthenes to a 10^9 array but skip the first primes up to 100.
We should also remove the prime number itself.

Should write the code such that we can change the type and limit to confirm 1105 for type 5 <= 10^8.
*/

const genPrimesV2 = require("../JSUtils/genPrimesV2");

const problem204 = () => {
    console.time();
    const hammingType = 100;
    const limit = 10 ** 9;
    const primes = genPrimesV2(limit);
    const sieve = new Uint8Array(limit);

    for (const prime of primes) {
        if (prime <= hammingType) continue;
        let idx = prime;
        while (idx <= limit) {
            // kind of flipping it so I don't need to fill original array but true means NOT hamming type number
            // also minus one to use the zero index
            sieve[idx - 1] = true; 
            idx += prime;
        };
    }
    let total = 0;
    for (const n of sieve) {
        if (!n) {
            total++;
        }
    }
    console.log(total);
    console.timeEnd();
};

problem204();

/*
2944730
default: 33.175s
*/
