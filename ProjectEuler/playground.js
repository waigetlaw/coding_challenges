// const genPrimesV2 = require("../JSUtils/genPrimesV2");
const Uint1Array = require("../JSUtils/Uint1Array");
const genPrimes = require("../JSUtils/genPrimes");

const calcPrimeFactors = (number) => {
    const primes = genPrimes(Math.ceil(Math.sqrt(number)));

    const primeFactors = [];
    let n = number; // copy to not modify original number
    for (const prime of primes) {
        if (n === 1) break;
        while (n % prime === 0) {
            n /= prime;
            primeFactors.push(prime)
        }
    }
    if (n !== 1) primeFactors.push(n);
    return primeFactors;
}

// console.log(
//     calcPrimeFactors(123),

//     calcPrimeFactors(1200),

//     calcPrimeFactors(31)
// )

const problem668 = limit => {
    console.time();
    const primes = genPrimesV2(Math.ceil(Math.sqrt(limit)));
    let totalSquareRootSmoothNumbers = 1;

    const calcPrimeFactors = n => {
        let largestPrimeFactor = 0;
        const sqrtN = Math.sqrt(n);
        for (const prime of primes) {
            if (n < sqrtN || prime > sqrtN) break;
            while (n % prime === 0) {
                n /= prime;
                largestPrimeFactor = prime
            }
        }
        if (n > largestPrimeFactor) {
            largestPrimeFactor = n;
        }
        return largestPrimeFactor;
    }

    for (let n = 2; n <= limit; n++) {
        const largestPrimeFactor = calcPrimeFactors(n);
        if (largestPrimeFactor < Math.sqrt(n)) {
            totalSquareRootSmoothNumbers++;
        }
    
    }
    console.log(totalSquareRootSmoothNumbers)
    console.timeEnd();
}

const problem668_v3 = limit => {
    console.time();
    const sqrtLimit = Math.sqrt(limit);
    const primes = genPrimes(limit);
    let nonSmooth = 0;

    for (const p of primes) {
        if (p <= sqrtLimit) {
            nonSmooth += p
        } else {
        nonSmooth += Math.floor(limit/p)
        }
    }
    
    const smoothNumbers = limit - nonSmooth;
    console.log(smoothNumbers)
    console.timeEnd();
}

// problem668(100);
// problem668(1000000);
// problem668(10000000);

// problem668_v3(50);
// problem668_v3(100);
// problem668_v3(1000000);
// problem668_v3(10 ** 9);

// const Dp = p => {
//     const setOfDisqualifiedNumbers = [];
//     for (let n = 1; n <= p; n++) {
//         setOfDisqualifiedNumbers.push(p * n);
//     }
//     return setOfDisqualifiedNumbers;
// }

const Dp = p => Array.from(Array(p)).map((_, i) => (i + 1) * p)

// console.log(Dp(5))

const genPrimesV1 = limit => {
    console.time();
    const primes = [];
    for (let n = 2; n <= limit; n++) {
        let isPrime = true;
        for (let d = 2; d <= Math.sqrt(n); d++) {
            if (n % d === 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) primes.push(n);
    }
    console.timeEnd();
    return primes;
}

// genPrimesV1(1000)
// genPrimesV1(10 ** 6)
// genPrimesV1(10 ** 7)
// genPrimesV1(10 ** 8)
// console.log('Dp(2)', Dp(2))
// console.log('Dp(3)', Dp(3))
// console.log('Dp(5)', Dp(5))

const genPrimesV2 = limit => {
    console.time();
    const sieve = [...Array(limit - 1)].map((_, i) => i + 2);
    for (const n of sieve) {
        if (!n) continue; 
        for (let cancel = (n ** 2) - 2; cancel < sieve.length; cancel += n) {
            sieve[cancel] = null;
        }
    }

    const primes = sieve.filter(Boolean);
    console.timeEnd();
    return primes;
}

// genPrimesV2(1000)
// genPrimesV2(10 ** 6)
// genPrimesV2(10 ** 7)
// genPrimesV2(10 ** 8)


const genPrimesV3 = limit => {
    console.time();
    const sieve = [...Array(limit - 1)].fill(true);
    for (let index = 0; index < sieve.length; index++) {
        const sieveValue = sieve[index];
        const prime = index + 2;
        if (!sieveValue) continue;
        for (let cancel = (prime ** 2) - 2; cancel < sieve.length; cancel += prime) {
            sieve[cancel] = false;
        }
    }
    
    const primes = sieve.reduce((primes, val, i) => {
        if (val) {
            primes.push(i + 2);
        }
        return primes;
    }, [])
    console.timeEnd();
    return primes;
}

// genPrimesV3(1000)
// genPrimesV3(10 ** 6)
// genPrimesV3(10 ** 7)
// genPrimesV3(10 ** 9)
// genPrimesV2(1000)
// genPrimesV2(10 ** 6)
// genPrimesV2(10 ** 7)
// genPrimesV2(10 ** 8)

const genPrimesV4 = limit => {
    console.time();
    const sieve = new Uint8Array(limit - 1);
    for (let index = 0; index < sieve.length; index++) {
        const sieveValue = sieve[index];
        const prime = index + 2;
        if (sieveValue) continue;
        for (let cancel = (prime ** 2) - 2; cancel < sieve.length; cancel += prime) {
            sieve[cancel] = 1;
        }
    }
    
    const primes = sieve.reduce((primes, val, i) => {
        if (!val) {
            primes.push(i + 2);
        }
        return primes;
    }, [])
    console.timeEnd();
    return primes;
}

// genPrimesV4(1000)
// genPrimesV4(10 ** 6)
// genPrimesV4(10 ** 7)
// genPrimesV4(10 ** 8)
// genPrimesV4(10 ** 9)


const genPrimesV5 = limit => {
    console.time();
    const sieve = new Uint1Array(limit - 1);
    for (let index = 0; index < limit - 1; index++) {
        const sieveValue = sieve.get(index);
        const prime = index + 2;
        if (sieveValue) continue;
        for (let cancel = (prime ** 2) - 2; cancel < limit - 1; cancel += prime) {
            sieve.set(cancel, 1);
        }
    }

    const primes = [];
    for (let i = 0; i < limit - 1; i++) {
        const sieveValue = sieve.get(i);
        if (!sieveValue) {
            primes.push(i + 2);
        }
    }
    console.timeEnd();
    return primes;
}

// genPrimesV5(1000)
// genPrimesV5(10 ** 6)
// genPrimesV5(10 ** 7)
// genPrimesV5(10 ** 8)
// genPrimesV5(10 ** 9)
// console.log(genPrimesV5(1000))

// const problem668_v4 = limit => {
//     console.time();
//     const sqrtLimit = Math.sqrt(limit);
//     const primes = genPrimesV5(limit);
//     let nonSmooth = 0;

//     for (const p of primes) {
//         if (p <= sqrtLimit) {
//             nonSmooth += p
//         } else {
//         nonSmooth += Math.floor(limit/p)
//         }
//     }
    
//     const smoothNumbers = limit - nonSmooth;
//     console.log(smoothNumbers)
//     console.timeEnd();
// }

// problem668_v4(1000);
// problem668_v4(10**6);
// problem668_v4(10**7);
// problem668_v4(10**8);
// problem668_v4(10**9);
// genPrimesV5(10**10);

// console.log(Number.parseInt('10101011', 2))


const problem668_v4 = limit => {
    console.time();
    const sqrt = Math.sqrt(limit);
    const n = limit;
    const lastNumber = n % 2 === 0 ? n - 1 : n;
    const arrSize = Math.ceil(n / 2) - 1;
    let sieve = new Uint1Array(n)
    for (let i = 0; i < arrSize; i++) {
        sieve.set(i * 2 + 3, true); // mark all odd numbers from 3 as potential primes
    }

    let idx = 0;
    let current = idx * 2 + 3;
    while (current ** 2 <= lastNumber) {
        const idxSquare = idx + (current * (current - 1)) / 2;
        for (let i = idxSquare; i < arrSize; i += current) {
            sieve.set(i * 2 + 3, false); // marking every nth as not prime starting from p^2
        }
        for (let i = idx + 1; i < arrSize; i++) { // get next unmarked in sieve
            const next = sieve.get(i * 2 + 3);
            if (next) {
                current = i * 2 + 3;
                idx = i;
                break;
            }
        }
    }
    console.log('sieve calculated!');

    let nonSmooth = 2; // sieve does not contain 2

    for (let i = 1; i <= limit; i++) {
        const sieveElement = sieve.get(i);
        if (sieveElement) {
            if (i <= sqrt) {
                nonSmooth += i
            } else {
            nonSmooth += Math.floor(limit/i)
            }
        }
    }

    const smoothNumbers = limit - nonSmooth;
    console.log(smoothNumbers)
    console.timeEnd();
}
const problem668_v5 = limit => {
    console.time();
    const sqrt = Math.sqrt(limit);
    let nonSmooth = 0;
    const sieve = new Uint1Array(limit - 1);
    for (let index = 0; index < limit - 1; index++) {
        const sieveValue = sieve.get(index);
        if (sieveValue) continue;

        const prime = index + 2;
        nonSmooth += prime <= sqrt ? prime : Math.floor(limit/prime)
 
        for (let cancel = (prime ** 2) - 2; cancel < limit - 1; cancel += prime) {
            sieve.set(cancel, 1);
        }
    }
    console.log('sieve calculated!');

    const smoothNumbers = limit - nonSmooth;
    console.log(smoothNumbers)
    console.timeEnd();
}

const problem668_v6 = limit => {
    console.time();
    const sqrt = Math.sqrt(limit);
    let nonSmooth = 2; // start with 2 to skip all even number checks
    const arrSize = Math.ceil(limit / 2) - 1;
    let sieve = new Uint1Array(limit)
    for (let i = 0; i < arrSize; i++) {
        sieve.set(i * 2 + 3, true); // mark all odd numbers from 3 as potential primes
    }

    for (let i = 0; i < arrSize; i++) {
        const sieveValue = sieve.get(i * 2 + 3);
        if (!sieveValue) continue;
        const prime = i * 2 + 3;
        nonSmooth += prime <= sqrt ? prime : Math.floor(limit/prime);
        const idxSquare = i + (prime * (prime - 1)) / 2;
        for (let j = idxSquare; j < arrSize; j += prime) {
            sieve.set(j * 2 + 3, false); // marking every nth as not prime starting from p^2
        }
    }

    const smoothNumbers = limit - nonSmooth;
    console.log(smoothNumbers)
    console.timeEnd();
}

// problem668_v6(100)
// problem668_v4(10**6)
// problem668_v5(10**6)
// problem668_v6(10**6)

// problem668_v4(10**7)
// problem668_v6(10**7)

// problem668_v4(10**8)
// problem668_v6(10**8)

// problem668_v6(10 ** 10)


const genPrimesV3_1 = limit => {
    console.time();
    const sieve = Array(limit - 1).fill(true);
    for (let index = 0; index < sieve.length; index++) {
        const sieveValue = sieve[index];
        const p = index + 2;
        if (!sieveValue) continue;
        for (let cancel = (p ** 2) - 2; cancel < sieve.length; cancel += p) {
            sieve[cancel] = false;
        }
    }
    
    const primes = sieve.reduce((primes, val, i) => {
        if (val) {
            primes.push(i + 2);
        }
        return primes;
    }, [])
    console.timeEnd();
    return primes;
}

// console.log(genPrimesV3_1(1000))   // default: 0.122ms

console.log(['Ê','硩'].reduce((s, c) => `${s+c.charCodeAt()}`, ''))
console.log(parseInt("accged", "abcdefghijklmnopqr".length));
let x=1;for(c of ['䧭','Э'])x*=c.charCodeAt();console.log(x);
let p=0;m=n=>'䧭Э'.charCodeAt(p++);console.log(m()*m());
console.log(parseInt("shejj", Math.clz32(++Math.PI)));
console.log(parseInt("accged",` ${Math.E}`.length));
h=s=>+[]+s;console.log(h('xfacebfeaae')/h('xcffe'));
console.log(parseInt("dgtxk", (''+Number).length));
console.log(parseInt("shejj", +`${+''}x${+!''}d`));
console.log('Ê'.charCodeAt()+''+'硩'.charCodeAt());
console.log(parseInt("accged",`   ${{}}`.length));
console.log(parseInt("dgtxk",'#'.charCodeAt()));
console.log('䧭'.charCodeAt()*'Э'.charCodeAt());
r=c=>c.charCodeAt();console.log(r('䧭')*r('Э'));
console.log(`${+''}xfacebfeaae`/`${+''}xcffe`);
console.log((+[]+'xfacebfeaae')/(+[]+'xcffe'));
console.log((+''+'xfacebfeaae')/(+''+'xcffe'));
console.log('END')


h=s=>+[]+s;console.log(h('xfacebfeaae')/h('xcffe'));

h=s=>`${+[]}x${s}`;console.log(h('facebfeaae')/h('cffe'));
// let k=+'';while(!'')console.log(k++); // in theory will log 20230825!

console.log(parseInt("accged", `   ${{}}`.length));
console.log(['䧭','Э'].reduce((t, c) => t*c.charCodeAt(),1))

r = c => c.charCodeAt();console.log(r('䧭')*r('Э'));




// `${+''}x${+!''}d`
// `   ${{}}`.length
// ['x','d'].reduce((s,d,i) => s+i+d,'')
// (Math.SQRT2 + '').length

console.log(
    // true << true,
    // 5 << 3,
    // Math.round((++Math.PI << Math.PI) - Math.PI),
    // 3 << 3,
    // Math.acos(0),
    // true << true,
    // [{}].toString().length,
    // Math.toString(),
    // Number.toString().length,
    // `${Number}`.length,
    // (''+Number).length,
    // `${Array}`.length,
    // (20230825).toString(16),
    // parseInt(0x134b2a9),
    // `   ${{}}`.length,
    // ('abc'+{}).length,
    // String({}).length,
    // `${{}}`.repeat(2).length,
    // `${Date}`.length,
    // `${Error}`,
    // 5 * 757,
    5 * 1069,
    String.fromCharCode(5 * 757 * 5),
    String.fromCharCode(1069),
    ['䧭','Э'].reduce((t, c) => t*c.charCodeAt(),1),
    (Number).length,
    Number.EPSILON,
    Error.name,
    `${BigInt}`,
    5>>'x',
//     5<<'x',
//     parseInt(`${+''}x${+!''}d`),
//     +''+'x'+!''+'d',
//     0x1d,
//     Number.MAX_VALUE,
//     Infinity * 1,
//     `${function*(){}}`.length,
//     32-18,
//     +'0x1d',
//     +'1d',
//     ['x','d'].reduce((s,d,i) => s+i+d,''),
//     +`${+''}x${+!''}d`,
//     true+true,
//     `${Boolean}`,
//     Number('abc'),
//     1_000_000,
//     new Date('ddds'),
//     `${Object}`,
//     Object.keys(Object),
//     "😄".split("").join(),
//     "😄".charCodeAt(),
//     Math.cbrt(20230825),
//     4497*4497,
//     (''+Number.EPSILON).length,
//     +Object.entries(['x','d']).join().replaceAll(',',''),

//     +(+''+`x${+!''}d`),
//    Math.log1p(32),
//    `${Symbol}`.length,
//    Math.E << Math.PI,
//    Math.PI<<Math.PI,
//    (Math.SQRT2 + '').length,
//    `   ${{}}`.length,
//    ('abc'+{}).length,
//    `${Set}`,
   "😄".normalize(),
   "😄".split('').join().length,
   Array('fegera'),
   Date().length,
   Date.length,
   Object.keys(Number),
    +[]+'x',
    'x'.padStart(2,0),
    'x'>>>1,

)

let xfefw = [18, 29, 35].forEach(n => {
    console.log(n.toString(16))
})

// 20230825 = 5 x 5 x 757 x 1069



// console.log(parseInt("kfveji", Math.clz32())/34)

const s = 20230825;
for (let r = 2; r < 37;r++) {
   if(/^[a-z]*$/.test(s.toString(r))) console.log(s.toString(r), r);
}

console.log((20230825).toString(16))

console.log(+"0xbdfcccbebe"/40334)


// 134b2a9

// "dgtxk",  35 
// "shejj",  29
// "accged", 18

//100011
// 11101
// 10010
// console.log('#'.charCodeAt())

// 20230825




// console.log(20230825/45737)

// console.log((35).toString(2))
// console.log((29).toString(2))
// console.log((18).toString(2))
// console.log("abcdefghijklmnopqr".length)



// how i got my answer

for (let i = 2; i < 1000000; i++) {
    const hex = (i * 20230825).toString(16);
    // console.log(hex, i * 20230825, i)
    if ([...hex].every(c => /[a-z]/.test(c)) 
    && [...i.toString(16)].every(c => /[a-z]/.test(c))
) {
        console.log(hex, i, i.toString(16))
    }
}

// for (let i = 2; i < 1000000; i++) {
//     const hex = (20230825/i).toString(16);
//     // console.log(hex, i * 20230825, i)
//     if ([...hex].every(c => /[a-z]/.test(c))) {
//         console.log(hex, i, i.toString(16))
//     }
// }

// console.log((20230825/5).toString(16))
// console.log((20230825/25).toString(16))
// console.log((20230825/(5*757)).toString(16))
// console.log((20230825/1069).toString(16))
// console.log((20230825/(1069*5)).toString(16))

console.log(Math.sqrt(20230825))

const want = 20230825
// for (let div = 2; div <= Math.sqrt(want); div++) {
//     if (want % div === 0) {
//         const hex = (want/div).toString(16);
//         // console.log(hex, div)
//         // console.log(hex, i * 20230825, i)
//         // if ([...hex].every(c => /[a-z]/.test(c))) {
//             console.log(hex, div, div.toString(16))
//         // }
//     }
// }

// for (let minus = 2; minus <= want; minus++) {
//     // if (want % div === 0) {
//         const hex = (want-minus).toString(16);
//         // console.log(hex, minus)
//         // console.log(hex, i * 20230825, i)
//         if ([...hex].every(c => /[a-z]/.test(c))) {
//             console.log(hex, minus, minus.toString(16))
//         }
//     // }
// }

for (let pow = 2; pow < 10; pow++) {
    console.log(Math.log(20230825)/Math.log(pow))
}

for (let pow = 2; pow < 5; pow++) {
    const hex = (want ** pow).toString(16)
    console.log(hex)
}
// // console.log(+'\uaaa')
// console.log("😄".split(''))
// console.log('\udddd'*'\udddd')
// console.log(['x'].toString())
