/*
By counting carefully it can be seen that a rectangular grid measuring 3 by 2 contains eighteen rectangles:

■□□    ■■□    ■■■
□□□    □□□    □□□
6       4      2

■□□    ■■□    ■■■
■□□    ■■□    ■■■
 3      2      1

Although there exists no rectangular grid that contains exactly two million rectangles, find the area of the grid with the nearest solution.

thoughts

for each extra nth row, we add an extra n * col:
e.g. 3rd row to the above means extra 9 + 6 + 3 to make 18 more rectangles.
so 3x3 would be 36 rectangles

for each extra mth col, we add:
m + 2m + 3m ... nm
so in the example, it would be 4 + 8 which is 12 more

so 4x2 would be 30 rectangles.
*/

const problem85 = () => {
    console.time();
    const target = 2000000;
    let ans = { cols: 0, rows: 0, rects: 0, diff: target };

    for (let cols = 1; cols < 100; cols++) {
        for (let rows = 1; rows < 100; rows++) {
            const baseColRects = [...Array(cols).keys()].map(v => v + 1);
            const totalBaseRects = baseColRects.reduce((total, r) => total + r, 0);
            let total = totalBaseRects;
        
            for (let i = 1; i < rows; i++) {
                total += (i + 1) * totalBaseRects;
                if (total > (target + ans.diff)) break;
            }
            if (total > (target + ans.diff)) break;
        
            const diff = Math.abs(target - total);
            if (diff < ans.diff) {
                ans = { cols, rows, rects: total, diff };
            }
        }
    }

    console.log(ans, ans.cols * ans.rows)
    console.timeEnd();
};

problem85();

/*
{ cols: 36, rows: 77, rects: 1999998, diff: 2 } 2772
default: 20.303ms
*/
