/*
Find the number of integers 1 < n < 10^7, for which n and n + 1 have the same number of positive divisors. For example, 14 has the positive divisors 1, 2, 7, 14 while 15 has 1, 3, 5, 15.

Thoughts: seems brute forcable to me.
*/

const problem179 = () => {
    console.time();

    const limit = 10 ** 7;
    let prev;
    let total = 0;
    for (let n = 2; n < limit; n++) {
        const sqrt = Math.sqrt(n);
        let divisors = Number.isInteger(sqrt) ? 3 : 2; // to include 1 and n and sqrt if it's square.
       
        for ( let divisor = 2; divisor < sqrt; divisor++) {
            if (n % divisor === 0) {
                divisors += 2;
            }
        }
        if (prev === divisors) {
            total++;
        }
        prev = divisors;
    }
    console.log(total)
    console.timeEnd();
};

problem179();

/*
986262
default: 24.766s
*/
