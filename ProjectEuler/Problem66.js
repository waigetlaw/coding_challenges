/*
<p>Consider quadratic Diophantine equations of the form:
x^2 - Dy^2 = 1
<p>For example, when $D=13$, the minimal solution in $x$ is 649^2 - 13 \times 180^2 = 1.</p>
<p>It can be assumed that there are no solutions in positive integers when $D$ is square.</p>
<p>By finding minimal solutions in $x$ for $D = \{2, 3, 5, 6, 7\}$, we obtain the following:</p>
\begin{align}
3^2 - 2 \times 2^2 &= 1\\
2^2 - 3 \times 1^2 &= 1\\
{\color{red}{\mathbf 9}}^2 - 5 \times 4^2 &= 1\\
5^2 - 6 \times 2^2 &= 1\\
8^2 - 7 \times 3^2 &= 1
\end{align}
<p>Hence, by considering minimal solutions in $x$ for $D \le 7$, the largest $x$ is obtained when $D=5$.</p>
<p>Find the value of $D \le 1000$ in minimal solutions of $x$ for which the largest value of $x$ is obtained.</p>

*/

const Big = require('big-js');

const getDivisors = require('../JSUtils/getDivisors');

const getPrimeDivisors = require('../JSUtils/getPrimeDivisors');
const genContinuedFracs = require('../JSUtils/genContinuedFracs');

function sqrt(value) {
    if (value < 0n) {
        throw 'square root of negative numbers is not supported'
    }

    if (value < 2n) {
        return value;
    }

    if (value === 4n) return 2n;

    function newtonIteration(n, x0) {
        const x1 = ((n / x0) + x0) >> 1n;
        if (x0 === x1 || x0 === (x1 - 1n)) {
            return x0;
        }
        return newtonIteration(n, x1);
    }

    return newtonIteration(value, 1n);
}

function getMax(a, b) {
    if (!a && !b) return;
    if (a) {
        if (b) {
            return a > b ? a : b;
        }
    }
    return b;
}

// unsolved Ds that are filtered with each run:
let Ds = ["61","109","149","151","157","166","181","193","199","211","214","233","241","244","253","261","268","271","277","281","298","301","309","313","317","331","334","337","353","358","367","379","382","394","397","409","412","419","421","433","436","449","454","457","461","463","466","477","478","481","487","489","491","493","501","502","508","509","511","517","521","523","524","526","537","538","541","547","549","553","554","556","559","562","565","569","571","581","586","589","593","596","597","599","601","604","607","610","613","614","617","619","622","628","631","633","634","637","641","643","649","652","653","655","661","662","664","669","673","679","681","685","686","691","694","701","709","716","718","719","721","724","733","737","739","742","746","749","751","753","754","757","758","763","764","766","769","771","772","773","778","787","789","790","794","796","797","801","802","805","809","811","814","821","823","826","829","834","835","838","844","845","849","853","856","857","859","861","862","863","865","869","871","877","881","883","886","889","893","907","911","913","917","919","921","922","926","928","929","931","932","937","941","946","947","949","951","953","955","956","958","964","965","967","970","971","972","974","976","977","981","988","989","991","997","998"]

Ds = Ds.map(n => BigInt(n));

// v6 with wikipedia help
const problem66 = () => {
    console.time();
    let limit = 1000;
    Ds = Array.from({ length: limit }).map((_, i) => i + 1).filter(x => Math.sqrt(x) !== Math.floor(Math.sqrt(x)))
    let solutions = [];

    for (const D of Ds) {
        console.log(D)
        let ansFound = false;
        // try up to 100 fracs
        for (let i = 1; i < 100; i++) {
            const { whole, continuedFracs } = genContinuedFracs(Big(D).sqrt(), i)
            // console.log(continuedFracs)
            const fracs = continuedFracs.map(f => BigInt(f)).reduceRight((frac, f) => {
                if (!frac.length) return [1n, f]; // first frac
                const leftWhole = f * frac[1];
                return [frac[1], leftWhole + frac[0]]
            }, [])
    
            const finalWhole = BigInt(whole) * fracs[1];
            // console.log(`final frac: ${finalWhole + fracs[0]}/${fracs[1]}`)
            const x = finalWhole + fracs[0];
            const y = fracs[1];

            // test
            if ((x ** 2n) - 1n === BigInt(D) * (y ** 2n)) {
                solutions.push({ D, x, y });
                ansFound = true;
                break;
            }
        }
        if (!ansFound) solutions.push({ D, x: 'n/a', y: 'n/a' })
    }
  
    console.table(solutions)
    const unsolvedD = solutions.filter(({x}) => x === 'n/a').map(sol => sol.D);
    console.log('unsolvedD', JSON.stringify(unsolvedD.map(x => +x.toString())))
    console.log('unsolved amount', unsolvedD.length)
    const solvedD = solutions.filter(({ x }) => x !== 'n/a');
    console.log(solvedD.sort((a, b) => a.x < b.x ? 1 : -1)[0])
    console.timeEnd();
}

// v4.1 no bigint
// const problem66 = () => {
//     console.time()

//     const limit = 1000;
//     const fromX = 175199949 - 3;
//     // const fromX = 0
//     const limitXSquared = 2500000;
//     const xSquareds = Array.from({ length: limitXSquared }).map((_, i) => BigInt(i + fromX) ** 2n);
//     // Ds = Array.from({ length: limit }).map((_, i) => i + 1).filter(x => Math.sqrt(x) !== Math.floor(Math.sqrt(x))).map(x => BigInt(x))
// // Ds = [43n, 46n]
//     let solved = []; // not needed but might be good for debugging
//     let DLength = Ds.length;
//     let i = 0;
//     let DsSolved = []

//     // const possYs = xSquareds.slice(0, fromX);
//     while(Ds.length > 1 && i < xSquareds.length) {
//         if (DsSolved.length) {
//             console.log(Ds, DsSolved, sqrt(xSquareds[i]));
//             DLength = Ds.length;
//         }

//         // x^2 - Dy^2 = 1 means (x^2 - 1) / D = y^2 and y^2 is simply a number lower than x^2
//         const xSquared = xSquareds[i];
//         DsSolved = []; // for current x^2, find all Ds that are solved and remove them from the list;
        
//         for (const D of Ds) {

//             // if (xSquared === 2500n) console.log({xSquared, D, xSquaredMinusOne: xSquared - 1n, left: (xSquared - 1n) / D})
//             if (D > xSquared) break; // no more Ds would be solvable
//             if (xSquared % D !== 1n) continue; // means (x^2 - 1) / D would not be integer
//             const leftSide = (xSquared - 1n) / D;
//             // if (possYs.includes(leftSide)) {
//             const sqrtLeftSide = sqrt(leftSide);
//             if (sqrtLeftSide * sqrtLeftSide === leftSide) {
//                 solved.push({ D, xSquared, ySquared: leftSide })
//                 DsSolved.push(D);
//             }
//         }
//         // console.log(`Ds solved for x^2 = ${xSquared}: ${DsSolved}`)
//         Ds = Ds.filter(d => !DsSolved.includes(d));
//         // possYs.push(xSquared)
//         i++;
//     }
//     // solved.sort((a, b) => a.xSquared > b.xSquared ? 1 : 0);
//     solved = solved.sort((a, b) => a.D < b.D ? 1 : 0)
//     solved = solved.map(s => ({ ...s, x: sqrt(s.xSquared)}))
//     const maxInSolved = solved.reduce((m, s) => s.xSquared > m.xSquared ? s : m, { xSquared : 0n })
//     console.log(solved, Ds, Ds.length, maxInSolved)

//     console.log(JSON.stringify(Ds.map(x => x.toString())))
//     console.log(fromX + limitXSquared)
//     console.timeEnd();
// }

//v5 - bigint for prep


let multLimit;
const problem66_v5 = () => {
    console.time();
    const limit = 50;
    let Ds = Array.from({ length: limit }).map((_, i) => i + 1).filter(x => Math.sqrt(x) !== Math.floor(Math.sqrt(x))).map(x => BigInt(x));

    // let Ds = [5n]
    const sols = [];
    for (const D of Ds) {
        console.log(D)
        const divisors = getPrimeDivisors(+D.toString()).map(x => BigInt(x));
        const pairs = [...divisors.reduce((all, divisor) => {
            all.add([divisor, divisor + 2n])
            all.add([divisor, divisor - 2n])
            return all;
        }, new Set())].filter(([_, y]) => y > 0n);
        const ySolutions = [];
        // try initial divisor pairs
        for (const [d, ySquared] of pairs) {
            // check possible to 'mutate' to the correct D:
            const mult = D/d;
            if (ySquared % mult !== 0n) continue;
            const potentialY = ySquared/mult;
            const sqrtY = sqrt(potentialY);
            if (sqrtY * sqrtY === potentialY) {
                ySolutions.push(potentialY);
                break;
            }
        }

        // for logging only
        if (ySolutions.length) {
            const x = sqrt((D * ySolutions[0]) + 1n);
            sols.push({ D, x, y: sqrt(ySolutions[0]), divisors, eqn: `${x}^2 - 1 = ${D} * ${sqrt(ySolutions[0])} ^ 2` });
        }
        // else {
        //     console.log('No easy sol for D: ', D)
        // }

        // else go for the harder sols:
        // let mult = 200000n - 1n;
        // multLimit = mult + 1000000n;
        let mult = 2n;
        while (!ySolutions.length) {
            // try generate this based on D divisors:

            // console.log({ mult})

            const potentialYs = divisors.reduce((possYs, d) => {

                const dFitsTimes = D/d;
                // if (D / dWithMult === 0n) return possYs;
                const dWithMult = d * mult;
                const y1 = (dWithMult - 2n) * mult;
                const y2 = (dWithMult + 2n) * mult;
                // console.log(y1, y2, D, dWithMult, D / dWithMult, div)
                if (y1 % dFitsTimes === 0n) {
                    possYs.push(y1/dFitsTimes);
                }
                if (y2 % dFitsTimes === 0n) {
                    possYs.push(y2/dFitsTimes);
                }
                return possYs;
            }, [])

            // if (potentialYs.length)  console.log({ D, mult, potentialYs})

            // const potentialYs = [((D* mult) - 2n) * mult,((D* mult) + 2n) * mult];
            // const pairs = [
            //     [D, (D - 2n) * mult * mult],
            //     [D, (D + 2n) * mult * mult]
            // ]
            // const x = sqrt(D * potentialYs[1] + 1n);
            // if (x > 1000) break;
            // console.log(mult, pairs, x)
            for (const potentialY of potentialYs) {
                const sqrtY = sqrt(potentialY);
                if (sqrtY * sqrtY === potentialY) {
                    ySolutions.push(potentialY); // just needed to exit loop.
                    const x = sqrt(D * potentialY + 1n);
                    sols.push({ D, x, y: sqrtY, divisors, eqn: `${x}^2 - 1 = ${D} * ${sqrtY} ^ 2` })

                    console.log('solved!', { D, x})
                    break;
                }
            }
            mult += 1n;
            
            // const x = sqrt((D * ySolutions[0]) + 1n);
            // sols.push({ D, x });
        }
        if (!ySolutions.length) {
            sols.push({D, x: 'n/a'})
        }
    }
    console.table(sols);
    const unsolvedD = sols.filter(({x}) => x === 'n/a').map(sol => sol.D);
    console.log('unsolvedD', JSON.stringify(unsolvedD.map(x => +x.toString())))
    console.log('unsolved amount', unsolvedD.length)
    const solvedD = sols.filter(({ x }) => x !== 'n/a');
    // console.log({ multLimit })


    solvedD.sort((a, b) => b.x > a.x ? 1 : -1)
    // console.log(solvedD)
    console.log(solvedD[0])
    console.timeEnd();
}


// const problem66_normalInt = () => {
//     console.time();
//     const limit = 50;
//     let Ds = Array.from({ length: limit }).map((_, i) => i + 1).filter(x => Math.sqrt(x) !== Math.floor(Math.sqrt(x)))
//     const sols = [];
//     for (const D of Ds) {
//         divisors = getDivisors(D);
//         const pairs = [...divisors.reduce((all, divisor) => {
//             all.add([divisor, divisor + 2])
//             all.add([divisor, divisor - 2])
            
//             return all;
//         }, new Set())].filter(([_, y]) => y > 0);
//         const ySolutions = [];
//         // try initial divisor pairs
//         for (const [d, ySquared] of pairs) {
//             // check possible to 'mutate' to the correct D:
//             const mult = D/d;
//             if (ySquared % mult !== 0) continue;
//             const potentialY = ySquared/mult;
//             if (Number.isInteger(Math.sqrt(potentialY))) {
//                 ySolutions.push(potentialY);
//                 break;
//             }
//         }

//         // for logging only
//         if (ySolutions.length) {
//             const x = Math.sqrt((D * ySolutions) + 1);
//             sols.push({ D, x });
//         } else {
//             console.log('No easy sol for D: ', D)
//         }
//     }
//     console.table(sols);
//     sols.sort((a, b) => b.x - a.x)
//     console.log(sols[0])
//     console.timeEnd();
// }

// const problem66 = () => {
//     console.time();
//     const limit = 61;
//     const Ds = Array.from({ length: limit }).map((_, i) => i + 1).filter(x => Math.sqrt(x) !== Math.floor(Math.sqrt(x))).map(x => BigInt(x))
//     let maxX = 0n;
//     const log = [];
//     // const Ds = [5n]
//     for (const D of Ds) {
//         // let x = 1n;
//         // let ansFound = false;

//         //v3
//         // note: x = -2dc +- [2 * sqrt(dc^2 + 1 - d)] all over (2-2d)
//         let C = 0n;
//         while(true) {
//             C += 1n;
//             const firstPart = -2n * D * C;
//             const insideSqrt = (D * C * C) + 1n - D;
//             const sqrtTheInside = sqrt(insideSqrt);
//             if (sqrtTheInside * sqrtTheInside !== insideSqrt) continue; // means could not square root properly
//             const bottom = 2n - (2n * D);
//             const twoTimesSqrtPart = 2n * sqrtTheInside;
//             const topPlus = firstPart + twoTimesSqrtPart;
//             const topMinus = firstPart - twoTimesSqrtPart;
//             let potentialAnsPlus, potentialAnsMinus;
//             // console.log({ D, C, firstPart, insideSqrt, bottom, topPlus, topMinus, modPlus: topPlus % bottom, modMinus: topMinus % bottom})
//             if (topPlus % bottom === 0n) {
//                 potentialAnsPlus = topPlus / bottom;
//             }
//             if (topMinus % bottom === 0n) {
//                 potentialAnsMinus = topMinus / bottom;
//             }
            
//             // console.log({ D, C, firstPart, insideSqrt, bottom, topPlus, topMinus, potentialAnsMinus, potentialAnsPlus})
//             const potentialAns = getMax(potentialAnsPlus, potentialAnsMinus);
//             if (potentialAns) {
//                 log.push({D, X: potentialAns, Y: potentialAns - C})
//                 if (potentialAns > maxX) maxX = potentialAns;
//                 break;
//             }
//         }
//         //v2
//         // while (true) {
//         //     console.log(D, x)
//         //     x += 1n;
//         //     const xSquared = x * x;
//         //     const xSquared_minus_one = xSquared - 1n;

//         //     // console.log({ D, x, mod: xSquared_minus_one % D, check: xSquared_minus_one/D, int: Number.isInteger(Math.sqrt(xSquared_minus_one / D)) })
//         //     if (xSquared_minus_one % D !== 0n) continue;
//         //     const xSquaredOverD = xSquared_minus_one/D;
//         //     const tryRoot = sqrt(xSquaredOverD);
//         //     if (tryRoot * tryRoot === xSquaredOverD) break;
//         // }
//         // log.push({ D, x })
//         // if (x > maxX) maxX = x;

//         // v1
//         // while (!ansFound) {
//         //     let y = 1;
//         //     let ySquared = 1;
//         //     let xSquared = x * x;
//         //     while ((D * ySquared) < xSquared) {
//         //         if (xSquared - (D * ySquared) === 1) {
//         //             ansFound = true;
//         //             break;
//         //         }
//         //         y++;
//         //         ySquared = y * y;
//         //     }
//         //     if (ansFound) {
//         //         console.log(`${x}^2 - ${D} * ${y} ^ 2 = 1`)
//         //         if (x > maxX) maxX = x;
//         //         break;
//         //     }
//         //     x++;
//         // }
//     }

//     console.table(log)
//     console.log(maxX)
//     console.timeEnd();
// }

problem66();

/*
for limit = 60;
66249
default: 438.76ms
brute force gets stuck at 61 though. It could be due to numbers becoming large and requiring bitInt.

with BigInt:
66249n
default: 38.913s

NEVER USE BIG INT

with string add/multiply number
DNF
for just limit = 30:
9801n
default: 49.127s

x ^ 2 - D y ^ 2 = 1
rearrange for y:

x^2 - 1 = D y ^ 2
(x^2 - 1)/D = y ^2

If we say Y^2, then X^2 - 1/D is always a square

so X^2 - 1 = D of a square number.
to minimise X, Y also needs to be minimised.

for example x = 9, y = 4

9^2 - 5 * 4^2 = 1
81 - 5 * 16 = 1
81 - 80 = 1

81 - 5 * 16 = 1

81 - 1 = 5 * 16
80 / 5 = 16

OR

81 - 1 = D y^2

so 80 / D = square number

so x^2 -1 mod D === 0 and is a square number would minimise X

e.g. for D = 5;

x: 2 => (4 - 1) mod 5 not 0
x: 3 => (9 - 1) mod 5 not 0
x: 4 => (16 - 1) mod 5 IS 0, but 15 is not a square number
x: 5 => 25 - 1 mod 5 
x: 6 => 36 - 1 but 35 not square
x: 7 48 mod 5
x: 8 63 mod 5
x 9 80 mod 5 = 16 SQUARE

v2
using this method of finding an X where X^2 - 1 mod D === 0 && (x^2 - 1) / D === square number:
66249
default: 23.012ms

it's FAST but I think numbers get too big as it still stalls at 61

v3 with bigint
66249n
default: 212.679ms // super fast for big int...

v5 with limit 50
{ D: 46n, x: 24335n }
default: 16.118ms

v5 with limit 60 - super fast!!
{ D: 53n, x: 66249n }
default: 19.297ms

v5 with prime divisors only
{ D: 53n, x: 66249n }
default: 19.915ms

v6 with wiki on new strat of calculating the continued fractions of sqrt(D):
for limit 60 - how will above 60 go?
default: 6.242ms

no solution for 61 even past 6million...

using new v5 it actually calculated answer for 61! and few other tough ones. But it will take forever to get all...

realised if we only looped through prime divisors, that should be enough! because with the mult, we should eventually go through every possible pair.

I need an integer X such that X^2 - 1 mod D === 0 (which is doable)
^ or X^2 mod D === 1;

then also (X^2 - 1) / D is a square number.

so X > Y and both are squares

therefore X can be the same as Y + C

(Y + C)^2 - 1 / D === Y

(Y + C)^2 - 1 = YD

(Y+C)^2 = YD + 1
Y^2 + 2YC + C^2 = YD + 1

Y^2 + 2YC - YD + C^2 - 1 = 0

Y^2 + Y(2C - D) + C^2 - 1 = 0

hmm maybe I should have done Y = X - C to solve for X as that's what we want...

(X^2 - 1) / D = X - C

(X^2 - 1)  = D(X - C)

X^2 - 1 = DX - DC

X^2 - DX + DC - 1 = 0

solve quadratic eqn where a = 1, b = -D, c = DC - 1;

x = -b -+ sqrt(b^2 - 4ac) all over 2a

x = D -+ sqrt(D^2 - 4(DC - 1)) all over 2

x = D -+ sqrt(D^2 - 4DC + 4) all over 2

test so far...

for D = 5, x = 9 y = 4 therefore C = 5



firstly
X^2 - DX + DC - 1 = 0
81 - 45 + 25 - 1 = 0, wrong.

from x^2 - Dy^2 = 1

x^2 - D(x - C)^2 - 1 = 0

x^2 - D(x^2 - 2xC + C^2) - 1 = 0 -> 81 - 5(81 - 90 + 25) - 1 = 0 -> 81 - 5(16) - 1 = 0 yes

x^2 - Dx^2 + 2xDC - DC^2 -1 = 0 -> 81 - 405 + 450 - 125 - 1 = 0 yes

(1 - D)x^2 + 2DCx - DC^2 - 1 = 0 -> (-4)81 + 450 - 125 - 1 = 0 -> -324 + 450 - 125 - 1 = 0 yes

quadratic eqn here: a = 1-d, b = 2dc, c = -(DC^2 + 1)

solve for some C that x will be int...

x = -2dc +- sqrt ((2dc^2) + 4(1-d)(DC^2 + 1)) all over 2(1-d)

x = -2dc +- sqrt ((4d^2c^2) + 4(dc^2 + 1 -d^2c^2 - d)) all over (2 - 2d)

x = -2dc +- sqrt((4d^2c^2) + 4dc^2 -4d^2c^2 + 4 - 4d) all over (2 - 2d)

x = -2dc +- sqrt(4dc^2 + 4 - 4d) all over (2-2d)

test so far... d = 5, c = 5

x = -50 +- sqrt (4*5*25 + 4 - 20) all over (-8)

x = -50 +- sqrt (484) / -8

x = -50 +- 22 / -8

x = -72 / -8 OR -28/-8 = 9 OR 3.5 wow it worked!

so for some C, x needs to be a positive integer

IF there is a way to simplify the sqrt part, it would be amazing but still, lets try implement this....

lets see if I can simplify a tiny bit more on this part: sqrt(4dc^2 + 4 - 4d)

sqrt(4dc^2 + 4 - 4d)

sqrt(4(dc^2 + 1 - d))

2 * sqrt(dc^2 + 1 - d)


V4
Actually we only need the D that produces the highest X so we might not even need to solve for it!

We can perhaps just try X = 1... X = n and solve for all Ds 1-1000 at once and score them out as we get an answer.
Last one remaining is highest D

and just have squares solved first and just check includes.

limit 60 with v4.1 (no bigint)
] [ 53 ] { D: 46, xSquared: 592192225, ySquared: 12873744, x: 24335 }
default: 151.586ms


// v4.2
we can reduce the D array each time something has already been solved - it might speed it up because we don't need to solve for Ds that we know isn't an answer.

breakthrough?? it seems whenever we solve for 1, x4 and x8 is also a solve:
[ 106n, 424n, 954n ] 32080052n

why is this not working with mult of 4?
] [ 195n ] 10935n

why is this mult of 9 working? and no mult 8 - oh its 9 
so 4, 9, 16 these are square numbers.

it seems if we solve for D, we also solved for 4D, 9D, 16D... that's nice. but it depends on size of X!!

 [ 2n, 8n ] 4n

[ 2n, 8n, 50n, 98n, 200n, 392n ] 100n

[
    2n,   8n,  18n,
   32n,  72n, 128n,
  288n, 578n
] 578n
] [ 2n, 8n, 18n, 32n, 72n, 288n ] 18n
[ 2n, 8n, 50n, 98n, 200n, 392n ] 100n
] [ 2n, 8n ] 3364n // why would this be only 2??

] [
    2n,   8n,  18n,  32n,
   50n,  72n,  98n, 162n,
  200n, 242n, 288n, 392n,
  450n, 648n, 800n, 882n,
  968n
] 19602n

there is some kind of correlation with the square numbers but I cannot understand what other rule there is as 3364 only passes for 2 and 8.

x^2 - Dy^2 = 1 means that:

D * y * y + 1 = square number, and we want to minimise y (or x)

The way to solve this is likely D is a prime. The diff of 2 squares: x^2 - 1 = Dy^2

(x - 1)(x + 1) = D(y^2)

we can consider that D = x + 1, and y^2 = x - 1. (or vice versa)

then we either multiply or divide until the y^2 part is a square

e.g.
D = 8 (it's actually probably easier to start with the lowest prime factor that fits to ensure we start at the lowest point?)

if 8 was x + 1, then try 8 * 6. but 6 is not a square
therefore try 8 * 10 but 10 is not a square

now we have to multiply by the same number to find a square number?

TODO

// v5
ans is supposed to be: x = 3, D = 8, y = 1:
3^2 - 1 = 8 * 1 ^ 1

it looks like this is from  D = 4, y = 2 and we can mult D by 2 and divide y by 2 and then y = 1 is a square number, and D = 8.

So whenever D = x^2 - 1, y = 1 is always a solution.

lets see D = 10, x should be 19:

mults are: 1, 2, 5, 10:

1, -1 doesn't make sense,
1, 3
2, 0 is not right
2, 4
5, 3
5, 7
10, 8
10, 12

are all solutions to x^2 - 1

out of them, 2, 4, 4 is a square.

then we just need to multiply by 5  to get back to the answer:

3^2 - 1 = 2 * (2^2)
(3 - 1)(3 + 1) = 2 * 2^2

hmm I feel like we can't just transform a 2(2^2) into another answer... if we want the 2 to a 10, it needs to mult by 5 but then the left side is also mult by 5 which is no longer a diff of 2 squares
however, if we mult by a square number, then left is kept:

5^2(3^2 - 1) = 5 * 2 * 5 * 2^2

15^2 - 25 = 10 * 5 ( 2^2) but now 20 for the Y part is no longer square...

so we can't just transform them, we can only play with the numbers and keep the left same.

so 2, 4 could only work if we could divide the Y part by 5, to multiply the 2 to a 10.

none of them fit the criteria, so next we need to look further...
e.g. 20, 18
20, 22

and we can half it and double the other to find squares.

10, 36
10, 44

10,36 is a solution!

so D = 10, y = 6, means x is...

x ^2 - 1 = 10 * 36
x^2 = 361 => x = 19

I think the only way is to check if any of the divisor pairs work first, then check the max pairs and double/half third etc.


another example D = 17, x = 33

17, 15
17, 19

34, 30 -> 17, 60 this should have been 34 32? then 17, 64. I see....
34, 38 -> 17 76

try *3^2

17, 15 * 9 = 135
17, 171

try 4^2

17, 240
17, 304

should have been y = 8...???

33^2 - 1 = 17 * 8^2, how?

(32)(34)

34 is a mult of D, so 64, 17 is a pair.



So firstly, any D that is 2 minus a square is a solution. E.g. for 9, D = 7 is a solution.

7 * 9 = 63 = 8^2 - 1

lets try D = 18, x should be 17:

1, 3 -> to get 1 to 18, we need to divide Y by 18, not possible
2, 4 -> to get to 18, need divide Y by 9 which is not possible
3, 1 same as above
3, 5 same as above
6, 4 same as above
6, 8 same as above
9, 7 same as above
9, 11 same as above
18, 16 -> is square!
18, 20

therefore x^2 - 1 = 18 * 4^2
x^2 = 289 = 17

lets test higher, D = 13, x should be 649

13, 11
13, 15
26, 24 -> 13, 48
26, 28 -> 13 56
39, 37
39, 41
52, 50 -> 13 200
52, 54 -> 13 216


lets just try to code this...

code debug time!

for D = 13, ans should be y = 180, x = 649

at 13, 15
13, 11

try mult 5

65, 63 => 315 
65, 67 => 335

try mult 7

91, 89 -> 623
91, 93 -> 651
why is 13, 180 a solution?
why is 13, 32400 a sol? how can we know?

13, 32400

1300, 324

650, 648 is the origin because 648 * 50 = 32400

so with my pattern, it should have found 650, 32400 and notice 32400 is a square number, but we never arrived at 650 with mults, lets debug:
D = 13,
mult at 50

from 650, 648
divide 650 / 13 = mult 50, 648 * 50 = 32400

50n [ [ 13n, 27500n ], [ 13n, 37500n ] ] 698n



mult 14:

182, 180 -> 2520???


182, 184
13 * 14 = 182

182 180

now 13 is debugged, but 44 is wrong...

 44n │ 79201n │ when it should be x = 199n

 D = 44, y = 30, x = 199

 199^2 - 1 = 44 * (30^2)

 why are we missing this???

 44, 900 this seems simple too...
 maybe I need to include all divisor pairs???

 such as 11, 9 and 11, 13 but this seems messy. after multiplying, need to make sure it can divide by 4 after to get the D back up...

 if x is 199

 pairs should be 198, 200

 44, 900 with mult 4.5:
44 * 4.5 = 198
198, 200
198, 196

then y part *4.5 should be int:
198, 900
198, 882

so both are still ints, how do we find all mults that can do this??
 198, 200 omg this is it. but we can't do a mult of 4.5... so how do we catch this???
It seems like something to do with 22 being a divisor of 44, then mult 9 here.
 Do we simply fracs? it prob stems from the fact 22, 20 can be a value, and y-part is divisible by 2. need to include these as parts of the pairs to use
 with mults.

 22, 20

 * 9
 198, 200
 198, 196

 I feel like I need to check all multiples for all pairs...

 d = 44, divisors include 4:

4, 2 or 4,6 was a sol

with a mult of 2:

8, 6 or 8, 10 is a sol, but need to divide by 44/8 

8 -> 44 = 5.5 mult

so divide 6 and 10 by 5.5 (can't do in big int...) need to do this in two stages

original 4 fits in 11 times, so 8 + 11, then divide by 2 (mult)


11 * 18 = 198
11, 13 -> 234


] [ 37n, 148n, 333n, 592n ] 10658n

[ 35n, 140n, 315n, 560n ] 10082n

v5 was promising with prime divisors etc calculating the first 60 super fast but it seems to not be able to reduce our list any further...

109n is reallly stuck even on v5 fully focused on solving it...

is there anyway to improve??


need a way to test refactoring such that D * Y * Y can be refactored to some M * N where M +- 2 = N

┌─────────┬─────┬────────┐
│ (index) │  D  │   x    │
├─────────┼─────┼────────┤
│    0    │ 2n  │   3n   │
│    1    │ 3n  │   2n   │
│    2    │ 5n  │   9n   │
│    3    │ 6n  │   5n   │
│    4    │ 7n  │   8n   │
│    5    │ 8n  │   3n   │
│    6    │ 10n │  19n   │
│    7    │ 11n │  10n   │
│    8    │ 12n │   7n   │
│    9    │ 13n │  649n  │
│   10    │ 14n │  15n   │
│   11    │ 15n │   4n   │
│   12    │ 17n │  33n   │
│   13    │ 18n │  17n   │
│   14    │ 19n │  170n  │
│   15    │ 20n │   9n   │
│   16    │ 21n │  55n   │
│   17    │ 22n │  197n  │
│   18    │ 23n │  24n   │
│   19    │ 24n │   5n   │
│   20    │ 26n │  51n   │
│   21    │ 27n │  26n   │
│   22    │ 28n │  127n  │
│   23    │ 29n │ 9801n  │
│   24    │ 30n │  11n   │
│   25    │ 31n │ 1520n  │
│   26    │ 32n │  17n   │
│   27    │ 33n │  23n   │
│   28    │ 34n │  35n   │
│   29    │ 35n │   6n   │
│   30    │ 37n │  73n   │
│   31    │ 38n │  37n   │
│   32    │ 39n │  25n   │
│   33    │ 40n │  19n   │
│   34    │ 41n │ 2049n  │
│   35    │ 42n │  13n   │
│   36    │ 43n │ 3482n  │
│   37    │ 44n │  199n  │
│   38    │ 45n │  161n  │
│   39    │ 46n │ 24335n │
│   40    │ 47n │  48n   │
│   41    │ 48n │   7n   │
│   42    │ 50n │  99n   │
│   43    │ 51n │  50n   │
│   44    │ 52n │  649n  │
│   45    │ 53n │ 66249n │
│   46    │ 54n │  485n  │
│   47    │ 55n │  89n   │
│   48    │ 56n │  15n   │
│   49    │ 57n │  151n  │
│   50    │ 58n │ 19603n │
│   51    │ 59n │  530n  │
│   52    │ 60n │  31n   │
└─────────┴─────┴────────┘

gotta find a better pattern...
┌─────────┬─────┬────────┬───────┬────────────────┬───────────────────────────────┐
│ (index) │  D  │   x    │   y   │    divisors    │              eqn              │
├─────────┼─────┼────────┼───────┼────────────────┼───────────────────────────────┤
│    0    │ 2n  │   3n   │  2n   │     [ 2n ]     │     '3^2 - 1 = 2 * 2 ^ 2'     │
│    1    │ 3n  │   2n   │  1n   │     [ 3n ]     │     '2^2 - 1 = 3 * 1 ^ 2'     │
│    2    │ 5n  │   9n   │  4n   │     [ 5n ]     │     '9^2 - 1 = 5 * 4 ^ 2'     │
│    3    │ 6n  │   5n   │  2n   │   [ 2n, 3n ]   │     '5^2 - 1 = 6 * 2 ^ 2'     │
│    4    │ 7n  │   8n   │  3n   │     [ 7n ]     │     '8^2 - 1 = 7 * 3 ^ 2'     │
│    5    │ 8n  │   3n   │  1n   │     [ 2n ]     │     '3^2 - 1 = 8 * 1 ^ 2'     │
│    6    │ 10n │  19n   │  6n   │   [ 2n, 5n ]   │    '19^2 - 1 = 10 * 6 ^ 2'    │
│    7    │ 11n │  10n   │  3n   │    [ 11n ]     │    '10^2 - 1 = 11 * 3 ^ 2'    │
│    8    │ 12n │   7n   │  2n   │   [ 2n, 3n ]   │    '7^2 - 1 = 12 * 2 ^ 2'     │
│    9    │ 13n │  649n  │ 180n  │    [ 13n ]     │  '649^2 - 1 = 13 * 180 ^ 2'   │
│   10    │ 14n │  15n   │  4n   │   [ 2n, 7n ]   │    '15^2 - 1 = 14 * 4 ^ 2'    │
│   11    │ 15n │   4n   │  1n   │   [ 3n, 5n ]   │    '4^2 - 1 = 15 * 1 ^ 2'     │
│   12    │ 17n │  33n   │  8n   │    [ 17n ]     │    '33^2 - 1 = 17 * 8 ^ 2'    │
│   13    │ 18n │  17n   │  4n   │   [ 2n, 3n ]   │    '17^2 - 1 = 18 * 4 ^ 2'    │
│   14    │ 19n │  170n  │  39n  │    [ 19n ]     │   '170^2 - 1 = 19 * 39 ^ 2'   │
│   15    │ 20n │   9n   │  2n   │   [ 2n, 5n ]   │    '9^2 - 1 = 20 * 2 ^ 2'     │
│   16    │ 21n │  55n   │  12n  │   [ 3n, 7n ]   │   '55^2 - 1 = 21 * 12 ^ 2'    │
│   17    │ 22n │  197n  │  42n  │  [ 2n, 11n ]   │   '197^2 - 1 = 22 * 42 ^ 2'   │
│   18    │ 23n │  24n   │  5n   │    [ 23n ]     │    '24^2 - 1 = 23 * 5 ^ 2'    │
│   19    │ 24n │   5n   │  1n   │   [ 2n, 3n ]   │    '5^2 - 1 = 24 * 1 ^ 2'     │
│   20    │ 26n │  51n   │  10n  │  [ 2n, 13n ]   │   '51^2 - 1 = 26 * 10 ^ 2'    │
│   21    │ 27n │  26n   │  5n   │     [ 3n ]     │    '26^2 - 1 = 27 * 5 ^ 2'    │
│   22    │ 28n │  127n  │  24n  │   [ 2n, 7n ]   │   '127^2 - 1 = 28 * 24 ^ 2'   │
│   23    │ 29n │ 9801n  │ 1820n │    [ 29n ]     │ '9801^2 - 1 = 29 * 1820 ^ 2'  │
│   24    │ 30n │  11n   │  2n   │ [ 2n, 3n, 5n ] │    '11^2 - 1 = 30 * 2 ^ 2'    │
│   25    │ 31n │ 1520n  │ 273n  │    [ 31n ]     │  '1520^2 - 1 = 31 * 273 ^ 2'  │
│   26    │ 32n │  17n   │  3n   │     [ 2n ]     │    '17^2 - 1 = 32 * 3 ^ 2'    │
│   27    │ 33n │  23n   │  4n   │  [ 3n, 11n ]   │    '23^2 - 1 = 33 * 4 ^ 2'    │
│   28    │ 34n │  35n   │  6n   │  [ 2n, 17n ]   │    '35^2 - 1 = 34 * 6 ^ 2'    │
│   29    │ 35n │   6n   │  1n   │   [ 5n, 7n ]   │    '6^2 - 1 = 35 * 1 ^ 2'     │
│   30    │ 37n │  73n   │  12n  │    [ 37n ]     │   '73^2 - 1 = 37 * 12 ^ 2'    │
│   31    │ 38n │  37n   │  6n   │  [ 2n, 19n ]   │    '37^2 - 1 = 38 * 6 ^ 2'    │
│   32    │ 39n │  25n   │  4n   │  [ 3n, 13n ]   │    '25^2 - 1 = 39 * 4 ^ 2'    │
│   33    │ 40n │  19n   │  3n   │   [ 2n, 5n ]   │    '19^2 - 1 = 40 * 3 ^ 2'    │
│   34    │ 41n │ 2049n  │ 320n  │    [ 41n ]     │  '2049^2 - 1 = 41 * 320 ^ 2'  │
│   35    │ 42n │  13n   │  2n   │ [ 2n, 3n, 7n ] │    '13^2 - 1 = 42 * 2 ^ 2'    │
│   36    │ 43n │ 3482n  │ 531n  │    [ 43n ]     │  '3482^2 - 1 = 43 * 531 ^ 2'  │
│   37    │ 44n │  199n  │  30n  │  [ 2n, 11n ]   │   '199^2 - 1 = 44 * 30 ^ 2'   │
│   38    │ 45n │  161n  │  24n  │   [ 3n, 5n ]   │   '161^2 - 1 = 45 * 24 ^ 2'   │
│   39    │ 46n │ 24335n │ 3588n │  [ 2n, 23n ]   │ '24335^2 - 1 = 46 * 3588 ^ 2' │
│   40    │ 47n │  48n   │  7n   │    [ 47n ]     │    '48^2 - 1 = 47 * 7 ^ 2'    │
│   41    │ 48n │   7n   │  1n   │   [ 2n, 3n ]   │    '7^2 - 1 = 48 * 1 ^ 2'     │
│   42    │ 50n │  99n   │  14n  │   [ 2n, 5n ]   │   '99^2 - 1 = 50 * 14 ^ 2'    │
└─────────┴─────┴────────┴───────┴────────────────┴───────────────────────────────┘

biggest solved:
{ D: 89, xSquared: 250001000001, ySquared: 2809000000, x: 500001 }
 { D: 364, xSquared: 24551539412401, ySquared: 67449284100, x: 4954951 }
 255 {
  D: 172,
  xSquared: 587996881330609,
  ySquared: 3418586519364,
  x: 24248647
}
 253 {
  D: 550n,
  xSquared: 935191505971801n,
  ySquared: 1700348192676n,
  x: 30580901n
}

] 218 {
  D: 431n,
  xSquared: 22970651846918400n,
  ySquared: 53296175978929n,
  x: 151560720n
}

] 216 {
  D: 683n,
  xSquared: 28923016460853124n,
  ySquared: 42347022636681n,
  x: 170067682n
}

we are going to hit the limit of normal ints soon...

solved! { D: 61n, x: 1766319049n } y = 226,153,980

unsolved amount 127
{ D: 797, x: 1221759532448649n, y: 43276943002540n }

unsolved:
[ 61, 73, 94, 97 ]
[61,97,106,109,139,149,151,157,163,166,181,193,199,211,214,233,241,244,250,253,261,262,265,268,271,277,281,283,298,301,307,309,313,317,331,334,337,343,349,353,358,367,373,379,382,388,393,394,397,406,409,412,417,419,421,424,431,433,436,445,446,449,451,454,457,461,463,466,477,478,481,487,489,491,493,501,502,508,509,511,517,521,523,524,526,533,537,538,541,547,549,550,553,554,556,559,562,565,566,569,571,581,586,589,593,596,597,599,601,604,606,607,610,613,614,617,619,622,628,629,631,633,634,637,641,643,647,649,652,653,655,661,662,664,666,667,669,673,679,681,683,685,686,691,694,698,701,709,716,718,719,721,724,733,737,739,742,746,749,751,753,754,757,758,763,764,766,769,771,772,773,778,781,787,789,790,794,796,797,801,802,805,807,809,811,814,821,823,826,829,834,835,838,844,845,849,853,856,857,859,861,862,863,865,869,871,873,877,879,881,883,886,889,893,907,911,913,914,917,919,921,922,926,928,929,931,932,937,941,946,947,949,951,953,954,955,956,958,964,965,967,970,971,972,974,976,977,981,988,989,991,997,998,999,1000]
*/

/*
24551539412401
9007199254740991
*/

/*
Pell's equation is what this is...

need to calculate continued convergent fractions of root (D). How do we do this?

example of 7 goes to 8,3:

h/k (convergent)	h2 − 7k2 (Pell-type approximation)
2/1	                −3
3/1	                +2
5/2	                −3
8/3	                +1

root 7 = 2.64575131106

therefore 2/1 is the first answer.
Next, is 0.645 -> 1/0.645 => 1.5485 not seeing how this becomes 3/1 i


next 1/0.5485 = 1.82287 => 1

it would be 2 + 1/(3 + 1/ 1 + ...)


i think it's just simplifying it.

so starts off as just 2 so fraction is 2/1

then 2 + 1/3 = 7/3

lets try root(3)

fracs should be:

2/1
5/3
7/4
19/11
26/15
71/41
97/56

so root 3 is

1.732
first frac is 1 + ...

next 1/0.732 = 1.366

1 + 1/1 = 2 (step 1?)

next 1/0.366

2.732

1 + 1 /(1 + 1/2) = 1 + 2/3 = 5/3

then 1/.732 = 1.366 again

1 + 1/ (1 + 1/ (2 + 1/1))

= 1 + 1/ (1 + 1/3)
= 1 + 3/4
= 7/4

ok i see the frac pairs now, but computing this seems like a nightmare...

v6 is ridiculously good because the continued fracs gets to the answer very quickly - however
precision is a nightmare. fracs for D = 61 is wrong - it should be a repeat of:

1, 4, 3, 1, 2, 2, 1, 3, 4, 1, 14

but actually it's showing as...

[
     1, 4, 3,  1, 2, 2, 1,
     3, 4, 1, 14, 1, 4, 3,
     1, 2, 2,  1, 4, 5, 1,
  6900, 1, 3
]
*/

/*
finally with 100 continued fracs and Big.js for precision...

unsolvedD []
unsolved amount 0
{
  D: 661,
  x: 16421658242965910275055840472270471049n,
  y: 638728478116949861246791167518480580n
}
default: 50.571s
*/
