/**
 * Starting with the number 1 and moving to the right in a clockwise direction a
 * 5 by 5 spiral is formed as follows:
 *
 * 21 22 23 24 25
 * 20  7  8  9 10
 * 19  6  1  2 11
 * 18  5  4  3 12
 * 17 16 15 14 13
 *
 * It can be verified that the sum of the numbers on the diagonals is 101.
 *
 * What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
 *
 */

// top right is always the n * n value
// top left is always the (n * n) - (n - 1) value
// bottom left is always the (n * n) - 2 * (n - 1) value
// bottom right is then the (n * n) - 3 * ( n - 1) value
// so skipping the trivial 1 in the center, the next n jumps in 2, from 1x1 to 3x3 to 5x5.
// And the sum at each stage is therefore:
// 4 * (n * n) - 6 * (n - 1)
// e.g. for 3x3 is 7 + 9 + 5 + 3 = 24
// 4 * (3 * 3) - 6 * (3 - 1) = 4 * 9 - 6 * 2 = 36 - 12 = 24
// so to get sum of all diagonals, we just go through each layer one at a time and add.

const sums = [1]; // starting sum is 1 for the center

for (let n = 3; n <= 1001; n += 2) {
    sums.push((4 * (n * n)) - (6 * (n - 1)));
}

const total = sums.reduce((sum, n) => sum + n, 0);
console.log(total); // 669171001
