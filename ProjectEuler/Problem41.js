/**
 * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?
 */

// stealing code from problem 24 and altering to loop through all combos of pandigital from 1-n where n <= 9

const factorial = require('../JSUtils/factorial');
const isPrime = require('../JSUtils/isPrime');

let loop = 0; // starting 0
let digits = 9;
const pandigital = ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
let found = false;

const pandigitalPrimes = [];

while (!found) {
    let lexPos = loop;
    let nums = pandigital.slice(0, digits);
    const ending = nums.reverse().join('');
    nums.reverse();

    let i = nums.length - 1;
    const vals = [];

    while (lexPos > 0) {
        const f = factorial(i);
        const times = Math.floor(lexPos / f);
        vals.push(times);
        lexPos = lexPos % f;
        i--;
    }

    const arr = nums.map((n, i) => {
        return vals[i] || 0;
    });

    let ans = '';

    arr.map((n) => {
        ans += nums[n];
        nums.splice(n, 1);
    });

    if (isPrime(+ans)) {
        pandigitalPrimes.push(ans);
    }

    if (ans === ending) { // went through all combinations but no primes, therefore we reduce down the digits by 1 and restart
        loop = 0;
        digits--;
    }
    if (digits === 1) break;
    loop++;
}

console.log(pandigitalPrimes.join());
console.log("\nMax value is: ", pandigitalPrimes.reduce((max, p) => p > max ? p : max, pandigitalPrimes[0])); // 7652413
