/**
 * The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
*/

const isPrime = require('../JSUtils/isPrime');

const isTruncatable = (n) => {
    if (!isPrime(+n)) return false;
    for (let i = 1; i < n.length; i++) {
        const rightT = n.substr(i);
        const leftT = n.substr(0, n.length - i);
        if (!isPrime(+rightT) || !isPrime(+leftT)) return false;
    }
    return true;
};

const truncatable = [];

let i = 11;
while (truncatable.length < 11) {
    const test = String(i);
    i += 2; // pretty bruteforce but the numbers are "small"
    if (test.includes('4') || test.includes('6') || test.includes('8') || test.includes('0')) continue;
    // because it will either truncate down to it, or end with it, it cannot end with these else it would divide by 2, and they are not prime themselves
    // so we can't have something like 8xyz for example, as it will truncate to 8 eventually.
    // technically could have been smarter here and only generate strings with the numbers 1, 2, 3, 5, 7, 9 as we only add 2 each time
    // so when we are for example, in 8000's we skip in increments of 2 until we reach 9000 which is inefficient.
    if (isTruncatable(test)) {
        truncatable.push(+test);
    }
}

console.log(truncatable, truncatable.length); // [ 23, 37, 53, 73, 313, 317, 373, 797, 3137, 3797, 739397 ] 11
console.log(truncatable.reduce((sum, n) => sum + n, 0)); // 748317
