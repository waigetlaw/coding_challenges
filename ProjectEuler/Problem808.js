/*
Both 169 and 961 are the square of a prime. 
169 is the reverse of 961.

We call a number a reversible prime square if:

1. It is not a palindrome, and
2. It is the square of a prime, and
3. Its reverse is also the square of a prime.

169 and 961 are not palindromes, so both are reversible prime squares.

Find the sum of the first 50 reversible prime squares.
*/

const genPrimes = require('../JSUtils/genPrimes');
const addNumber = require('../JSUtils/addNumber');
const multiplyNumber = require('../JSUtils/multiplyNumber');
const isPalindrome = require('../JSUtils/isPalindrome');

function reverseString(str) {
    return str.split("").reverse().join("");
}

const problem808 = () => {
    const reversiblePrimeSquares = [];
    console.time()
    const primes = genPrimes(100000000)
    const primesSquared = primes
        .map(p => String(p))
        .filter(p => /^[1349]/.test(p)) // only numbers beginning with 1, 3, 4, or 9 can square to a number starting with 1 or 9.
        .map(p => multiplyNumber(p, p))
        .filter(p => (p[0] === '1' || p[0] === '9') && p.length % 2 === 1 && !isPalindrome(p));
    
    console.log(primesSquared.length, primesSquared[primesSquared.length - 1])
    console.timeEnd()
    console.time();

    while (primesSquared.length) {

        function remove(num) {
            const index = primesSquared.indexOf(num);
            primesSquared.splice(index, 1);
        }
    
        function includes(num) {
            let j = 1;
            while ((j + 1) < primesSquared.length && primesSquared[j] <= num) {
                if (num === primesSquared[j++]) return true
            }
            return false
        }

        const pSquared = primesSquared[0];
        let rev = reverseString(pSquared);
        if (includes(rev)) {
            reversiblePrimeSquares.push(...[pSquared, rev]);
            remove(rev);
            console.log(reversiblePrimeSquares.length)
            if (reversiblePrimeSquares.length === 50) break;
        } 
        remove(pSquared);
    }


    // for (const pSquared of copy) {
    //     let rev = reverseString(pSquared);
    //     if (rev !== pSquared && primesSquared.includes(rev)) {
    //         reversiblePrimeSquares.push(pSquared)
    //         if (reversiblePrimeSquares.length === 50) break;
    //     } else {
    //         const index = primesSquared.indexOf(pSquared);
    //         primesSquared.splice(index, 1);
    //     }
    // }
    console.log(reversiblePrimeSquares, reversiblePrimeSquares.length, reversiblePrimeSquares.reduce((sum, n) => addNumber(sum, n), '0'))
    console.timeEnd();
}

problem808();

// 16 53009475688
// default: 548.137ms with remove after used.
// default: 324.398ms with length check on includes and string from start with multiplyNumber instead of ** 

// 32 34740767840240
// default: 46.452s
// default: 37.789s with remove.

/*
First solve - but I noticed the length of the answers are always odd... try to filter...
[
    '169',             '961',             '12769',
    '96721',           '1042441',         '1442401',
    '1062961',         '1692601',         '1216609',
    '9066121',         '121066009',       '900660121',
    '12148668841',     '14886684121',     '12367886521',
    '12568876321',     '1000422044521',   '1254402240001',
    '1002007006009',   '9006007002001',   '1020506060401',
    '1040606050201',   '1210684296721',   '1276924860121',
    '1212427816609',   '9066187242121',   '1212665666521',
    '1256665662121',   '1214648656321',   '1236568464121',
    '1234367662441',   '1442667634321',   '100042424498641',
    '146894424240001', '100222143232201', '102232341222001',
    '100240164024001', '100420461042001', '100402824854641',
    '146458428204001', '102012282612769', '967216282210201',
    '102014060240401', '104042060410201', '121002486012769',
    '967210684200121', '121264386264121', '121462683462121',
    '123212686214641', '146412686212321'
  ] 50 3807504276997394
  default: 7:24.711 (m:ss.mmm)
  
*/

// with filter odd length!!
// ] 50 3807504276997394
// default: 1:37.514 (m:ss.mmm)

/*
[
  '169',         '961',
  '12769',       '96721',
  '1042441',     '1442401',
  '1062961',     '1692601',
  '1216609',     '9066121',
  '121066009',   '900660121',
  '12148668841', '14886684121',
  '12367886521', '12568876321'
]
*/

/*
[
  '169',           '961',           '12769',
  '96721',         '1042441',       '1442401',
  '1062961',       '1692601',       '1216609',
  '9066121',       '121066009',     '900660121',
  '12148668841',   '14886684121',   '12367886521',
  '12568876321',   '1000422044521', '1254402240001',
  '1002007006009', '9006007002001', '1020506060401',
  '1040606050201', '1210684296721', '1276924860121',
  '1212427816609', '9066187242121', '1212665666521',
  '1256665662121', '1214648656321', '1236568464121',
  '1234367662441', '1442667634321'
]
*/
