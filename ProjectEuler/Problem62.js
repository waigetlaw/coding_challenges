/*
The cube, 41063625 (345^3), can be permuted to produce two other cubes: 56623104 (384^3) and 66430125 (405^3). In fact, 41063625 is the smallest cube which has exactly three permutations of its digits which are also cube.

Find the smallest cube for which exactly five permutations of its digits are cube.

Thoughts:

I think we can keep cubing numbers and then adding their answers sorted to an object and whenever any value's length is 5, we know that is likely the answer, unless it's actually got more than 5 permutations.

e.g. the above would be placed in an object as:
{
    '01234566': [345, 384, 405]
}

If we want to ensure exactly 5, we can keep going until the length goes above the key length as all cubes thereafter will be bigger. So we could modify it to be:

{
    '01234566': { cubes: [345, 384, 405], complete: true }
}

But I assume the first to reach 5 would be exactly 5 as it would likely need more digits to reach 6+
*/

const problem62 = () => {
    console.time();

    const limit = 5;
    const track = {};

    let n = 345;
    while (!Object.values(track).some(cubes => cubes.length === limit) && n < 100000) {
        const cube = n ** 3;
        const key = `${cube}`.split('').sort().join('');
        if (!track[key]) {
            track[key] = [n];
        } else {
            track[key].push(n);
        }
        n++;
    }

    const answer = Object.values(track).find(cubes => cubes.length >= limit)[0] ** 3;
    console.log(answer)
    console.timeEnd();
}

problem62();

/*
127035954683
default: 5.869s
*/
