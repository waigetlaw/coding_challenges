/*
A square is drawn around a circle as shown in the diagram below on the left.
We shall call the blue shaded region the L-section.
A line is drawn from the bottom left of the square to the top right as shown in the diagram on the right.
We shall call the orange shaded region a concave triangle.

0587_concave_triangle_1.png
It should be clear that the concave triangle occupies exactly half of the L-section.

Two circles are placed next to each other horizontally, a rectangle is drawn around both circles, and a line is drawn from the bottom left to the top right as shown in the diagram below.

0587_concave_triangle_2.png
This time the concave triangle occupies approximately 36.46% of the L-section.

If n  circles are placed next to each other horizontally, a rectangle is drawn around the n circles, and a line is drawn from the bottom left to the top right, then it can be shown that the least value of n for which the concave triangle occupies less than 10% of the L-section is 15.

What is the least value of n for which the concave triangle occupies less than 0.1% of the L-section?
*/

const calculateSegmentArea = (intersections, r) => {
    // we now need a function for calculating the area of the 'chopped' circle given two intersections
    // we know two sides of the triangle will be r length, and third side is distance between the two points
    // we should have enough to work out the angle between the radius's and therefore work out the area using
    // area of circle * angle/360 minus the triangle area created by r, r, intersection line.
    // area of triangle is: half base * height, where height is r ^2 - 0.5 intersection ^2 = h ^2
    // consider width as the distance of the intersectionss

    const width = Math.sqrt((intersections[1].x - intersections[0].x) ** 2 + (intersections[1].y - intersections[0].y) ** 2);
    const height = Math.sqrt(r ** 2 - (0.5 * width) ** 2);
    const angle = (Math.acos((2 * r * r - width ** 2) / (2 * r * r)) / (2 * Math.PI)) * 360;

    const sectorArea = Math.PI * r * r * (angle / 360);
    const triangleArea = 0.5 * width * height;
    return sectorArea - triangleArea;
};

const calcQuadratic = (a, b, c) => {
    const discriminant = b ** 2 - 4 * a * c;
    const x1 = (-b + Math.sqrt(discriminant)) / (2 * a);
    const x2 = (-b - Math.sqrt(discriminant)) / (2 * a);
    return [x1, x2];
}

const problem587 = () => {
    console.time();

    const nLimit = 10000;
    const percentageGoal = 0.001;
    const log = [];
    const scale = 10; // essentially diameter of the circles for coordinates.

    for (let n = 1; n <= nLimit; n++) {
        if (n === 1) {
            log.push({ n, percentage: 0.5 });
            continue;
        }

        // circle equation is (x - a)^2 + (y - b)^2 = r^2, where a and b are also radius
        const r = scale / 2;

        // line equation is y = mx + c, where c is 0 since we chose bottom left corner as origin
        const m = 1/n;

        // need intersection points of line and first circle
        // with substitution of y = mx into the circle equation we get:
        // (1 + m^2)x^2 - (2r + 2mr)x + r^2 = 0
        // so the quadratic equation using:
        // a = 1 + m^2, b = -2r - 2mr, c = r^2

        const a = 1 + m ** 2;
        const b = -2 * r - 2 * m * r;
        const c = r ** 2;

        const [x1, x2] = calcQuadratic(a, b, c);
        const y1 = m * x1;
        const y2 = m * x2;

        const intersections = [ { x: x1, y: y1 }, { x: x2, y: y2 }].sort((p1, p2) => p1.x - p2.x);

        const segmentArea = calculateSegmentArea(intersections, r);
        
        // next if n > 2, we need to calculate what has been chopped off the bottom right corner
        // if n = 2, it should just be 0 but no harm in calculating
        const widthRect = (r * 2) - intersections[1].x;
        // side intersections of the circle will be when x = intersections[1].x
        const va = 1;
        const vb = -2 * r;
        const vc = intersections[1].x ** 2 - 2 * intersections[1].x * r + r * r;

        const [vy1, vy2] = calcQuadratic(va, vb, vc);
        const sideIntersections = [{ x: intersections[1].x, y: vy1 }, { x: intersections[1].x, y: vy2 }];
        const sideSegmentArea = calculateSegmentArea(sideIntersections, r);

        // so the corner chopped off due to intersecting earlier is
        const choppedArea = (((r * 2) * widthRect) - sideSegmentArea) / 2;
    
        // original corner area is simply the square minus circle divided by 4
        const originalCornerArea = (scale * scale - Math.PI * r * r) / 4;
        const remainingCornerArea = originalCornerArea - choppedArea;

        // finally, the area of shaded area is:
        // the triangle from origin, (intersection[1].x, 0), intersection[1]
        // minus remaining corner area minus the first segment of the circle
        
        const triangleArea = 0.5 * intersections[1].x * intersections[1].y;
        const shadedArea = triangleArea - remainingCornerArea - segmentArea;

        // finally the percentage is shaded area / original corner area
        const percentage = shadedArea/originalCornerArea;
        log.push({n, percentage });
        if (percentage < percentageGoal) break;
    }
    console.table(log);
    console.timeEnd();
}

problem587();

/*
┌─────────┬──────┬───────────────────────┐
│ (index) │ n    │      percentage       │
├─────────┼──────┼───────────────────────┤
│    0    │ 1    │          0.5          │
│    1    │ 2    │ 0.36462616929172686   │
│    2    │ 3    │ 0.29256827702091365   │
│  ...    │ ...  │ ...                   │
│  2237   │ 2238 │ 0.0010004854854472522 │
│  2238   │ 2239 │ 0.0010000474962124544 │
│  2239   │ 2240 │ 0.000999609892228585  │
└─────────┴──────┴───────────────────────┘
default: 43.197ms
*/
