const isPalindrome = require('../JSUtils/isPalindrome');

const limit = 100000000;

// form array of squares to use
const squareArr = [];

for (let n = 1; n <= Math.ceil(limit ** 0.5); n++) {
    squareArr.push(n ** 2);
}

// make every combination of sums of consec squares
const palindromes = [];

for (let j = squareArr.length; j > 1; j--) {
    for (let k = 0; k <= squareArr.length - j; k++) {
        const arr = squareArr.slice(k, (j + k));
        const sum = arr.reduce((sum, p) => sum + p, 0);
        if (sum < limit && isPalindrome(String(sum))) {
            palindromes.push({ arr, sum });
        }
        if (sum > limit) break; // further sums will only result in a bigger total
    }
}
const sums = new Set(palindromes.map((p) => p.sum)); // Some numbers can be written as a consecutive sum in more than one way!
console.log([...sums].reduce((sum, p) => sum + p, 0)); // 2906969179
