/**
 * Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0,
 * where each “_” is a single digit.
 */

// just find the limits of the answer and loop until the square is of that form:

const multiplyNumber = require('../JSUtils/multiplyNumber');

// only a number ending with 0 can square to a number ending in 0 so... ending must be 00, we can remove and solve for n/10:
const lower = Math.floor(Math.sqrt(10203040506070809));
const higher = Math.ceil(Math.sqrt(19293949596979899));

console.time()
for (let i = lower; i <= higher; i = increment(i)) {
    const ans = multiplyNumber(('' + i), ('' + i));
    if (/^1\d2\d3\d4\d5\d6\d7\d8\d9$/.test(ans)) {
        console.log(i * 10);
        break;
    }
}

// this is the only way to make it end with 9_0, the number must end in 30 or 70
function increment(i) {
    const r = i % 10;
    if (r === 3) return i + 4;
    if (r === 7) return i + 6;
    return 3 + Math.floor(i / 10) * 10; // anything else, start it at 3 to start the looping of 3/7
}

console.timeEnd();

// 1389019170
// default: 94609.097ms

// ok, most naive way is too slow - especially with string multiplys
// try remove regex of /^1\d2\d3\d4\d5\d6\d7\d8\d9\d0$/

// with regex: default: 14263.000ms
// string to array check: default: 15254.668ms

// regex is best with all testing done...

// as soon as the square 'overflows' such as 102030 becomes 102040, i know i can skip until it becomes 10213000
// if I implement this 'skipping' it would be a lot faster.

// we can loop through for pattersn 1-9, without the 0 because it must end with 00
// the answer would then be n/10 - which would probably speed up the string mult part.