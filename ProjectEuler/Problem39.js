// If p is the perimeter of a right angle triangle with integral length sides, {a,b,c},
// there are exactly three solutions for p = 120.
//
// {20,48,52}, {24,45,51}, {30,40,50}
//
// For which value of p ≤ 1000, is the number of solutions maximised?

//345 triangle is minimum, therefore starting p = 12
// min lengths are 1 so for every p, we check for (p-2), 1, 1 as min case:

let maxSols = { p: 12, sols: 1 };

for (let p = 12; p <= 1000; p++) {
    let possibleSolutions = [];
    for (let c = 1; c < (p - 2); c++) {
        for (let b = 1; b <= c; b++) {
            for (let a = 1; a <= b; a++) {
                if (a + b + c === p)
                    possibleSolutions.push({ a, b, c });
            }
        }
    }

    const sols = possibleSolutions.filter(({ a, b, c }) => {
        return a ** 2 + b ** 2 === c ** 2;
    }).length;

    if (sols > maxSols.sols) {
        maxSols = { p, sols };
    }
}

console.log(maxSols); // { p: 840, sols: 8 }
