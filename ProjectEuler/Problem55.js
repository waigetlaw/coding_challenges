const addNumber = require('../JSUtils/addNumber');
const isPalindrome = require('../JSUtils/isPalindrome');

const lychrel = [];
for (let i = 1; i < 10000; i++) {
    let count = 0;
    let sum = i;
    do {
        count++;
        sum = addNumber(String(sum), String(sum).split('').reverse().join(''));
    } while (count < 50 && !isPalindrome(sum))
    if (count === 50) {
        lychrel.push(i);
    }
}

console.log(lychrel.length); // 249
