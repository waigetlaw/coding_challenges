/**
 * In the United Kingdom the currency is made up of pound (£) and pence (p). There are eight coins in general circulation:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p), and £2 (200p).
It is possible to make £2 in the following way:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
How many different ways can £2 be made using any number of coins?
 */

// This seems solvable with recursion
// we should have a fn that takes a total, the next division and do a for loop of
// how many times the division can fit in the total, and call itself with the
// remainder and next division.
// once we get to the lowest division, we should simply +1 to the total.

// doing this as pennies to avoid decimals.

/* for 10:
 1111111111
 211111111
 22111111
 2221111
 222211
 22222
 511111
 52111
 5221
 55

 total: 10
*/

const getCombinationCount = require('../JSUtils/getCombinationCount');

const totalCombinations = getCombinationCount(200, [200, 100, 50, 20, 10, 5, 2, 1]);

console.log(totalCombinations);  // 73682
