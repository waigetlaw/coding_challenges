/*
The smallest number expressible as the sum of a prime square, prime cube, and prime fourth power is 28. In fact, there are exactly four numbers below fifty that can be expressed in such a way:

28 = 2^2 + 2^3 + 2^4
33 = 3^2 + 2^3 + 2^4
49 = 5^2 + 2^3 + 2^4
47 = 2^2 + 3^3 + 2^4
 
How many numbers below fifty million can be expressed as the sum of a prime square, prime cube, and prime fourth power?
*/

const genPrimesV2 = require("../JSUtils/genPrimesV2");

const problem87 = () => {
    console.time();

    const limit = 50000000;
    const primes = genPrimesV2(Math.ceil(Math.sqrt(limit)));

    const squares = [];
    const cubes = [];
    const fourths = [];

    const numbers = new Set();

    for (const prime of primes) {
        primeSquared = prime ** 2;
        primeCubed = prime ** 3;
        primeFourth = prime ** 4;
        if (primeSquared >= limit) break;
        if (primeSquared < limit) squares.push(primeSquared);
        if (primeCubed < limit) cubes.push(primeCubed);
        if (primeFourth < limit) fourths.push(primeFourth);
    }

    for (const square of squares) {
        for (const cube of cubes) {
            if (square + cube >= limit) break;
            for (const fourth of fourths) {
                const sum = square + cube + fourth;
                if (square + cube + fourth >= limit) break;
                numbers.add(sum);
            }
        }
    }
    console.log(numbers.size)
    console.timeEnd();
};

problem87();

/*
1097343
default: 144.114ms
*/
