/**
 * Comparing two numbers written in index form like 211 and 37 is not difficult, as any calculator would confirm that 211 = 2048 < 37 = 2187.

However, confirming that 632382518061 > 519432525806 would be much more difficult, as both numbers contain over three million digits.

Using base_exp.txt (right click and 'Save Link/Target As...'), a 22K text file containing one thousand lines with a base/exponent pair on each line, determine which line number has the greatest numerical value.

NOTE: The first two lines in the file represent the numbers in the example given above.
 */

// We can easily compare by changing the 'base' of one number to be the same as the other and just compare the power component
// e.g. 2^11 vs 3^7, we can change 3^7 to 2^x
// and x would be: (7 log 3)/(log 2)
// so we compare 11 with x, which essentially compares (3^7 === 2^x) > 2^11 ?
// lets convert all numbers to a common base of 10

const readFile = require('../JSUtils/readFile');
const path = require('path');

const problem99 = async () => {

    const filePath = path.join(__dirname, './files/p099_base_exp.txt');
    const lines = (await readFile(filePath)).split('\n');
    const newExp = [];
    for (const line of lines) {
        const [base, exp] = line.split(',');
        const base10exp = (exp * Math.log10(base));
        newExp.push(base10exp);
    }

    const max = newExp.reduce(
        (max, exp, i) => exp > max.exp ? { idx: i + 1, exp } : max,
        { i: 1, exp: newExp[0] }
    );

    console.log(max);   // 709
}

problem99();