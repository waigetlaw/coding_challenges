/**
 * Define s(n) to be the smallest number that has a digit sum of n. For example s(10)=19.
Let S(k)=∑n=1ks(n). You are given S(20)=1074.

Further let fi be the Fibonacci sequence defined by f0=0,f1=1 and fi=fi−2+fi−1 for all i≥2.

Find ∑i=290S(fi). Give your answer modulo 1000000007.
 */

/*
So for finding the smallest digit, we need the least amount of digits and then make the sum using the smallest digits possible
i.e. favoring [1,9] over [5,5] for 10

but this can also be re-worded as using as big a number as possible i.e. choosing to have 9, rather than 5,5

I'm not sure how to find S(k) in an efficient manner without actually doing s(n) from 1 -> k...
furthermore, doing mod of 1,000,000,007 with string concatenations of many 9's is difficult

There must be a shortcut from S(k) to the answer without iterating through all 1 -> k...
*/

const s = (n) => {
    const digits = n / 9 | 0;
    return +((n % 9) + '9'.repeat(digits));
}

const S = (k) => {
    let sum = 0;
    for (let i = 1; i <= k; i++) {
        sum += s(i);
    }
    return sum;
}

let fi_minus_1 = 1;
let fi_minus_2 = 0;
let sum = 0;

for (let i = 2; i <= 90; i++) {
    console.log(i)
    fi = fi_minus_1 + fi_minus_2;
    fi_minus_2 = fi_minus_1;
    fi_minus_1 = fi;
    console.log(fi);
    // sum += +s(fi);
    // sum %= 1000000007;
}

// console.log(S(2880067194370816000))
// console.log(sum)

let results = [];
for (let i = 1; i <= 200; i++) {
    // const s = '' + 1 + '9'.repeat(i);
    // console.log(+s % 1000000007);
    results.push({ index: i, 'S(k)': S(i), 'S(k) modulo': S(i) % 1000000007 })
}


console.table(results)