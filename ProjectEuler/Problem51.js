/*
By replacing the 1st digit of the 2-digit number *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.

Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
*/

// This should be brute forceable... I think.
// smallest basically means with the least amount of digits so I'm guessing it's not huge. Last number cannot be replaced as there will be even numbers
// start by replacing 1, then 2 then 3 etc.

// start from *[1-9 += 2 !== 5]
// then X*[1-9 +=2 !== 5]
// then **[1-9 +=2 !== 5]
// then *X[1-9 +=2 !== 5]
// so it always ends with [1-9 +=2] so from now on we ignore that...

/*
*
X*
*X
**
***
**X
*X*
X**
*XX
X*X
XX*

what is this type of permutations??
its just all permutations of length: [* X], [* *] then replace X from 1-9 
so for 3, its all perms of [ * * *], [ * * X], [ * X X] and so on

then always doing it 4 times, for each diff ending of 1, 3, 7, 9
*/

// Unsure this method is fast enough for bruteforce...

// Update: Upon completion with bruteforce, a smarter way to generate the 'masks' is to iterate through base11 and use A as the mask.

const genPermutation = require('../JSUtils/genPermutation');
const isPrime = require('../JSUtils/isPrime');

const primesReq = 9;
const allAns = [];
let digits = 1;

while (allAns.length < 1) { // while no answers pushed
    const template = Array(digits).fill('*');
    const templates = template.reduce((arr, _, i) => {
        if (i === 0) return arr;
        template[i] = 'N';
        return [...arr, [...template]]
    }, [[...template]])

    // for each template: we will fill in N with actual numbers, then try sub all the *'s from 0 to 1 and check if prime
    // and do this for each unique permutation of them.
    for (const template of templates) {
        const numberOfStars = template.filter((c) => c === '*').length;
        if (primesReq > 7 && numberOfStars % 3 !== 0) continue;  // added after reading forum on completion of problem
        // if numberOfStars is not a multiple of 3, then the sum of the digits would add up to a number divisible by 3 too often
        // that not enough solutions would be left for primesReq
        const numberOfNs = template.filter((c) => c === 'N').length;
        const perms = [... new Set(genPermutation(template))];
        for (const permTemp of perms) {   // for each unique perm
            for (let j = 0; j < 10 ** numberOfNs; j++) {    // sub N's with all possible different combinations of numbers from 0-9
                const sub = String(j).padStart(numberOfNs, '0');
                let perm = permTemp;
                for (let k = 0; k < numberOfNs; k++) {
                    perm = perm.replace('N', sub[k]);
                }
                if (perm.startsWith('0')) continue; // if starts with 0, the length will be shrunk and they rule out 03 being *3
                for (endDigit of ['1', '3', '7', '9']) {
                    const ans = [];
                    const testNumTemplate = perm + endDigit;
                    for (const l of ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']) {  // only *'s remain to be substituted
                        if (primesReq - ans.length > 10 - +l) break;
                        if (l === '0' && testNumTemplate.startsWith('*')) continue;    // apparently they rule out things like 03 being a prime of *3
                        const testNum = +testNumTemplate.replace(/\*/g, l);
                        if (isPrime(testNum)) ans.push(testNum);
                    }
                    if (ans.length >= primesReq) {
                        // because we just substitute numbers, it is not ordered, and therefore the first answer we come across
                        // may not be the smallest, so we cannot just break out of the loops and end here
                        allAns.push({ template: testNumTemplate, primes: [...ans] });
                    }
                    ans.length = 0;
                }
            }
        }
    }
    digits++;
}

console.log(allAns);
/*
    [{
        template: '*2*3*3',
        primes:
            [121313, 222323, 323333, 424343, 525353, 626363, 828383, 929393]
    }]
*/

/**
 * Interestingly enough, solutions exist for 9/10 primes too!
 *
 * [
 *    {
 *        template: '38*0*2*1',
 *        primes: [
 *                38000201,
 *                38101211,
 *                38202221,
 *                38303231,
 *                38505251,
 *                38606261,
 *                38707271,
 *                38808281,
 *                38909291
 *            ]
 *    },
 *    {
 *        template: '42*0*1*7',
 *        primes: [
 *                42000107,
 *                42101117,
 *                42303137,
 *                42404147,
 *                42505157,
 *                42606167,
 *                42707177,
 *                42808187,
 *                42909197
 *            ]
 *    }
 * ]
 * default: 235244.809ms
 *
 */
