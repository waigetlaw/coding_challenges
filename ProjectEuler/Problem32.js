/**
 * We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.

HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
 */

// Brute force method here can be going from n * m = x where n > 1 as it is impossible with 1
// and m is 2 -> infinity until when the digits of n, m and x concats to a length > 9
// then we move n++ and repeat until a certain limit
// the max is probably around 2 digit x 3 digit = 4 digit, therefore max of 999 for n is enough
// we also must store the value of x to do a filter later.

const limit = 999;
const ans = [];

for (let n = 2; n < limit; n++) {
    let m = 2;
    let conc = '';
    while (conc.length < 10) {
        const x = n * m;
        conc = '' + n + m + x;
        if (conc.split('').sort().join('') === '123456789') {
            ans.push(x);
        }
        m++;
    }
}

console.log([...new Set(ans)].reduce((sum, n) => sum + n, 0));  // 45228
