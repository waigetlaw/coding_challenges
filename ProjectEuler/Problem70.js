const phi = require('../JSUtils/phi');
const genPrimes = require('../JSUtils/genPrimes');

console.time();
const limit = 10000000;
const primes = genPrimes(limit);

// answer won't be a n of prime because the totient will be prime - 1, which cant be a permutation of the number.
const notPrimes = [];
let p_idx = 2;
for (let i = 4; i < limit; i++) {
    if (i === primes[p_idx]) {
        p_idx++;
        continue;
    }
    notPrimes.push(i);
}

let min = { n: 1, n_phi: Number.MAX_VALUE };
for (n of notPrimes) {
    if (n % 100000 === 0) console.log(`${n / 100000}% done...`);
    const ph = phi(n, primes);
    if ([...('' + ph)].sort().join() === [...('' + n)].sort().join()) {
        if ((n / ph) < min.n_phi) {
            min = { n, n_phi: (n / ph) };
        }
    }
}

console.log(min);
console.timeEnd();

// { n: 8319823, n_phi: 1.0007090511248113 }
// default: 25963.217ms

/*
to 250,000 using n = 2 start  [...String(ph)].every(c => [...String(n)].includes(c)) // this doesnt work, things like 22 gets matched with 23 as 23 has 2
{ n: 249989, n_phi: 1.0000040001920092 }
default: 8005.056ms

using [...String(ph)].sort().join() === [...String(n)].sort().join()
{ n: 176569, n_phi: 1.0049688097623168 }
default: 7905.472ms

using only not primes
{ n: 176569, n_phi: 1.0049688097623168 }
default: 4157.691ms

for 1mill:
{ n: 783169, n_phi: 1.0022690159662961 }
default: 52626.977ms

removing string checks (just to see if it is comp intensive or not)
{ n: 994009, n_phi: 1.001004016064257 }
default: 47859.492ms

barely a diff so we need to make phi more computationally better.

with primeArr passed to phi() and permutation check back in:

{ n: 783169, n_phi: 1.0022690159662961 }
default: 13271.920ms

using string concat rather than String():
{ n: 783169, n_phi: 1.0022690159662961 }
default: 11595.235ms

with better phi:
{ n: 783169, n_phi: 1.0022690159662961 }
default: 2212.001ms

final result was 22seconds

without prime Arr:
{ n: 8319823, n_phi: 1.0007090511248113 }
default: 30007.412ms

with primeArr:
// { n: 8319823, n_phi: 1.0007090511248113 }
// default: 25963.217ms
*/
