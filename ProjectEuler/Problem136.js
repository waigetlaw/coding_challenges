/*
The positive integers, x, y, and z, are consecutive terms of an arithmetic progression. Given that n is a positive integer, the equation, x^2 - y^2 - z^2 = n, has exactly one solution when n = 20:

13^2 - 10^2 - 7^2 = 20

In fact there are twenty-five values of n below one hundred for which the equation has a unique solution.

How many values of n less than fifty million have exactly one solution?

Thoughts, similar to p135 but I wonder if there is a way to rethink to speed up and 'skip' if we already found more than one solution for a given n.

v1:
since we need integer values, and we know (2a - N)(2a + N) = n,

we can simply get factors of n, and try solve for a or N and if they are both integers and fit a > 0, N >= 0 then it's a valid answer.

Note that although we want only 1 solution, it's not necessarily only when N = 0. if N > a, then remember:

x1 = a - N, x2 = a + N

if N > 0, as long as N >= a, then it will still be one solution only as x1 would become 0 or negative.

So for each n, keep searching for factors, and then try solve for a, N.

If we ever find more than 1 solution, we skip to next n.
 */

const genPrimesV2 = require("../JSUtils/genPrimesV2");

const problem136 = () => {
    console.time();

    const limit = 100;

    const n_track = {};
    for (let a = 1; a <= 1 + limit/2; a++) {
        if (a % 10000 === 0) console.log(a);
        for (let N = 0; N < 2 * a; N++) {
            const n = (2 * a - N) * (2 * a + N);
            if (n > limit) continue;
            // const x_1 = a - N;
            // const x_2 = a + N;
            // if (x_1 > 0) console.log(`${x_1 + 2*a}^2 - ${x_1 + a}^2 - ${x_1}^2 = ${n}`)
            // console.log(`${x_2 + 2*a}^2 - ${x_2 + a}^2 - ${x_2}^2 = ${n}`)
            if (!n_track[n]) {
                n_track[n] = N > 0 && a > N ? 2 : 1;
            } else {
                n_track[n] += N > 0 && a > N ? 2 : 1;
            }
        }
    }
    // console.log(n_track)
    console.log(Object.entries(n_track).filter(([n, freq]) => freq === 1))
    console.log(Object.entries(n_track).filter(([n, freq]) => freq === 1).length)
    console.timeEnd();
};

// problem136();
