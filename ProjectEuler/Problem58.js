/**
 * Starting with 1 and spiralling anticlockwise in the following way, a square spiral with side length 7 is formed.

37 36 35 34 33 32 31
38 17 16 15 14 13 30
39 18  5  4  3 12 29
40 19  6  1  2 11 28
41 20  7  8  9 10 27
42 21 22 23 24 25 26
43 44 45 46 47 48 49

It is interesting to note that the odd squares lie along the bottom right diagonal, but what is more interesting is that 8 out of the 13 numbers lying along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.

If one complete new layer is wrapped around the spiral above, a square spiral with side length 9 will be formed. If this process is continued, what is the side length of the square spiral for which the ratio of primes along both diagonals first falls below 10%?
*/

// bottom right is always the n * n value
// bottom left is always the (n * n) - (n - 1) value
// top left is always the (n * n) - 2 * (n - 1) value
// top right is then the (n * n) - 3 * ( n - 1) value
// number of numbers in diagonals is 1 + (n - 1) * 2

const isPrime = require('../JSUtils/isPrime');

const primesAtDia = [];
let ratio = 1;
let n = 1;

while (ratio >= 0.1) {
    n+=2;
    const br = n * n; // can never be prime
    const bl = br - (n - 1);
    const tl = br - 2 * (n - 1);
    const tr = br - 3 * (n - 1);

    if (isPrime(bl)) primesAtDia.push(bl);
    if (isPrime(tl)) primesAtDia.push(tl);
    if (isPrime(tr)) primesAtDia.push(tr);

    const diags = 1 + (n - 1) * 2;
    ratio = primesAtDia.length/diags;
}

console.log(n); // 26241
