/**
 * The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle numbers are:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.

Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how many are triangle words?
 */

// the file is in ProjectEuler/files/p042_words.txt

const readFile = require('../JSUtils/readFile');
const path = require('path')

const problem42 = async () => {

    const filePath = path.join(__dirname, './files/p042_words.txt');

    const content = await readFile(filePath);

    const words = content.replace(/\"/g, '').split(',');

    let triangleWords = 0;
    await Promise.all(words.map(async (word) => {
        const sum = [...word].map((c) => c.charCodeAt(0) - 64).reduce((sum, v) => sum + v, 0);
        const triangleTerm = (-1 + Math.sqrt(1 + (8 * sum))) / 2; // assume is triangle, this is nth term
        if (Math.floor(triangleTerm) === triangleTerm) { // essentially, if n is integer, then it is a triangle number
            triangleWords++
        }
    }));

    console.log(triangleWords); // 162
}

problem42();
