/*
The 5-digit number, 16807 = 7^5, is also a fifth power. Similarly, the 9-digit number, 134217728 = 8^9, is a ninth power.

How many n-digit positive integers exist which are also an nth power?
*/

const problem63 = () => {
    console.time();
    let x = 0;
    let total = 0;
    let n = 1;
    while (true && x++ < 300) {
        if (`${2 ** n}`.length > n) break;
        let i = 0;
        while (true) {
            const numberToPowerN = i ** n;
            // const digits = `${numberToPowerN}`.length;
            let digits = 0;
            while (numberToPowerN >= 10 ** digits++) {}
            digits--;
            // console.log(numberToPowerN, digits)
            if (digits === n) {
                total++;
                console.log(`${i} ^ ${n} = ${numberToPowerN}`)
            }
            if (digits > n) break;
            i++;
        }
        n++;
    }
    console.log(total);
    console.timeEnd();
};

problem63();

/*
1 ^ 1 = 1
2 ^ 1 = 2
3 ^ 1 = 3
4 ^ 1 = 4
5 ^ 1 = 5
6 ^ 1 = 6
7 ^ 1 = 7
8 ^ 1 = 8
9 ^ 1 = 9
4 ^ 2 = 16
5 ^ 2 = 25
6 ^ 2 = 36
7 ^ 2 = 49
8 ^ 2 = 64
9 ^ 2 = 81
5 ^ 3 = 125
6 ^ 3 = 216
7 ^ 3 = 343
8 ^ 3 = 512
9 ^ 3 = 729
6 ^ 4 = 1296
7 ^ 4 = 2401
8 ^ 4 = 4096
9 ^ 4 = 6561
7 ^ 5 = 16807
8 ^ 5 = 32768
9 ^ 5 = 59049
7 ^ 6 = 117649
8 ^ 6 = 262144
9 ^ 6 = 531441
8 ^ 7 = 2097152
9 ^ 7 = 4782969
8 ^ 8 = 16777216
9 ^ 8 = 43046721
8 ^ 9 = 134217728
9 ^ 9 = 387420489
8 ^ 10 = 1073741824
9 ^ 10 = 3486784401
9 ^ 11 = 31381059609
9 ^ 12 = 282429536481
9 ^ 13 = 2541865828329
9 ^ 14 = 22876792454961
9 ^ 15 = 205891132094649
9 ^ 16 = 1853020188851841
9 ^ 17 = 16677181699666568
9 ^ 18 = 150094635296999140
9 ^ 19 = 1350851717672992000
9 ^ 20 = 12157665459056929000
9 ^ 21 = 109418989131512350000
49
default: 18.387ms
*/
