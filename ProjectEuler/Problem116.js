/*
A row of five grey square tiles is to have a number of its tiles replaced with coloured oblong tiles chosen from red (length two), green (length three), or blue (length four).

If red tiles are chosen there are exactly seven ways this can be done.

png116_1.png
If green tiles are chosen there are three ways.

png116_2.png
And if blue tiles are chosen there are two ways.

png116_3.png
Assuming that colours cannot be mixed there are 
 ways of replacing the grey tiles in a row measuring five units in length.

How many different ways can the grey tiles in a row measuring fifty units in length be replaced if colours cannot be mixed and at least one coloured tile must be used?

NOTE: This is related to Problem 117.

Thoughts: should be super similar to problem 117 but just do three different 'coin sums' of [1, 2], [1, 3], [1, 4] and also filter (or just delete?) the all 1's array
*/

const getCombinations = require("../JSUtils/getCombinations");
const getPermutationCount = require("../JSUtils/getPermutationCount");

const problem116 = () => {
    console.time();
    const units = 50;

    const combinations = [...getCombinations(units, [4,1]), ...getCombinations(units, [3,1]), ...getCombinations(units, [2,1])].filter(combination => !combination.every(n => n === 1));
    const total = combinations.reduce((permutations, combination) => permutations + getPermutationCount(combination), 0)

    console.log(total)
    console.timeEnd();
}

problem116();

/*
20492570929
default: 5.335ms
*/
