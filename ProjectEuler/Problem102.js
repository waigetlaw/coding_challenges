/*
Three distinct points are plotted at random on a Cartesian plane, for which -1000 <= x, y <= 10000, such that a triangle is formed.

Consider the following two triangles:

 A (-340, 495), B(-153, -910), C(835, -947)
 X(-175, 41), Y(-421, -714), Z(574, 0645)

It can be verified that triangle ABC contains the origin, whereas triangle XYZ does not.

Using triangles.txt (right click and 'Save Link/Target As...'), a 27K text file containing the co-ordinates of one thousand "random" triangles, find the number of triangles for which the interior contains the origin.

NOTE: The first two examples in the file represent the triangles in the example given above.

Idea: for each pair of dots, get the line Equation then check which side the 3rd dot is at (plug in X and the Y will either be above or below the expected).
Then also plug in the origin and it must be within the same side of the line. Repeat for all 3 pairs of dots and if the origin is on the same side on all
three pairs, then the origin must be inside the triangle.

line eq = (y1 - y2)/(x1 - x2) + C?

y = mx + c


*/

const readFile = require('../JSUtils/readFile');
const path = require('path')

const getLineEq = (p1, p2) => {
    const m = (p1.y - p2.y) / (p1.x - p2.x);
    const c = p1.y - (m * p1.x);
    return ({ m, c });
}

const checkSide = (A, B, C) => {
    const line = getLineEq(A, B);
    const y = line.m * C.x + line.c;

    // console.log({ A, B, C, line: `y = ${line.m}x + ${line.c}`, yShould: y, sideC: C.y > y, sideOrigin: 0 > line.c})
    return (C.y > y) !== (0 > line.c)
}

const problem102 = async () => {
    const filePath = path.join(__dirname, './files/0102_triangles.txt');
    const content = await readFile(filePath);

    const triangles = content.split('\n').filter(Boolean).map(text => text.split(',').map(n => Number(n)));
    let count = 0;

    for (const triangle of triangles) {
        const A = { x: triangle[0], y: triangle[1]};
        const B = { x: triangle[2], y: triangle[3]};
        const C = { x: triangle[4], y: triangle[5]};
    
        if (checkSide(A, B, C) || checkSide(B, C, A) || checkSide(C, A, B)) continue;
        count++;
    }
    console.log(count);
}

problem102();
