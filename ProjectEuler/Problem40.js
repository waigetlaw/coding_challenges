/**
 * An irrational decimal fraction is created by concatenating the positive integers:

0.123456789101112131415161718192021...

It can be seen that the 12th digit of the fractional part is 1.

If dn represents the nth digit of the fractional part, find the value of the following expression.

d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000
*/

// only up to 1,000,000th digit... this seems bruteforcable?

let i = 1;
let s = '.'; // the dot is purely to get the index aligned with the nth term.

while (s.length < 500000) {
    s += i++;
}

let s2 = '';
while (s2.length < 500000) {    // breaking string in 2 to stay within string limits - quick hack for this small problem.
    s2 += i++;
}

console.log(s[1] * s[10] * s[100] * s[1000] * s[10000] * s[100000] * s2[1000000 - s.length]);   // 210
