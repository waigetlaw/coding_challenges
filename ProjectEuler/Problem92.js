count89 = 0;
count1 = 0;
for (let i = 1; i < 10000000; i++) {
    if (i % 100000 === 0) console.log("Progress: ", i, "Count: ", count89);
    let x = i;
    do {
        x = [...String(x)].reduce((sum, num) => {
            return sum + Math.pow(+num, 2);
        }, 0);
        if (x === 89) count89++;
        if (x === 1) count1++;
    }
    while ((x !== 1) && (x !== 89));
}

console.log(count89, count1, count1 + count89);
