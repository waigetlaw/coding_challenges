/*
Using a combination of grey square tiles and oblong tiles chosen from: red tiles (measuring two units), green tiles (measuring three units), and blue tiles (measuring four units), it is possible to tile a row measuring five units in length in exactly fifteen different ways.

png117.png
How many ways can a row measuring fifty units in length be tiled?

NOTE: This is related to Problem 116.
*/

const getCombinations = require("../JSUtils/getCombinations");
const getPermutationCount = require("../JSUtils/getPermutationCount");

const problem117 = () => {
    console.time();

    const units = 50;

    const combinations = getCombinations(units, [4,3,2,1]);
    const total = combinations.reduce((permutations, combination) => permutations + getPermutationCount(combination), 0)

    console.log(total)

    console.timeEnd();
};

problem117();

/*
 '4321',
  '4312',
  '4231',
  '4213',
  '4132',
  '4123',
  '3421',
  '3412',
  '3241',
  '3214',
  '3142',
  '3124',
  '2431',
  '2413',
  '2341',
  '2314',
  '2143',
  '2134',
  '1432',
  '1423',
  '1342',
  '1324',
  '1243',
  '1234',


  getPermCount

[
  1, 1, 1, 1, 1,
  1, 1, 1, 1, 1
] 1
[
  2, 1, 1, 1, 1,
  1, 1, 1, 1
] 9
[
  2, 2, 1, 1,
  1, 1, 1, 1
] 28
[
  2, 2, 2, 1,
  1, 1, 1
] 35
[ 2, 2, 2, 2, 1, 1 ] 60
[ 2, 2, 2, 2, 2 ] 1
[
  3, 1, 1, 1,
  1, 1, 1, 1
] 8
[
  3, 2, 1, 1,
  1, 1, 1
] 42
[ 3, 2, 2, 1, 1, 1 ] 60
[ 3, 2, 2, 2, 1 ] 20
[ 3, 3, 1, 1, 1, 1 ] 15
[ 3, 3, 2, 1, 1 ] 30
[ 3, 3, 2, 2 ] 6
[ 3, 3, 3, 1 ] 4
[
  4, 1, 1, 1,
  1, 1, 1
] 7
[ 4, 2, 1, 1, 1, 1 ] 30
[ 4, 2, 2, 1, 1 ] 30
[ 4, 2, 2, 2 ] 4
[ 4, 3, 1, 1, 1 ] 20
[ 4, 3, 2, 1 ] 24
[ 4, 3, 3 ] 3
[ 4, 4, 1, 1 ] 6
[ 4, 4, 2 ] 3
446


[ 2, 2, 2, 2, 1, 1 ] 15
[ 2, 2, 2, 2, 2 ] 1
[
  3, 1, 1, 1,
  1, 1, 1, 1
] 8
[
  3, 2, 1, 1,
  1, 1, 1
] 42
[ 3, 2, 2, 1, 1, 1 ] 60
[ 3, 2, 2, 2, 1 ] 20
[ 3, 3, 1, 1, 1, 1 ] 15
[ 3, 3, 2, 1, 1 ] 30
[ 3, 3, 2, 2 ] 6
[ 3, 3, 3, 1 ] 4
[
  4, 1, 1, 1,
  1, 1, 1
] 7
[ 4, 2, 1, 1, 1, 1 ] 30
[ 4, 2, 2, 1, 1 ] 30
[ 4, 2, 2, 2 ] 4
[ 4, 3, 1, 1, 1 ] 20
[ 4, 3, 2, 1 ] 24
[ 4, 3, 3 ] 3
[ 4, 4, 1, 1 ] 6
[ 4, 4, 2 ] 3
401

[ 2, 2, 2, 2, 1, 1 ] 60 vs 15 ??? // fixed bug where I didn't divide some kFactorials as mod was not 0

100808458960497
default: 26.348ms
*/

