/**
 * A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

1/2	= 	0.5
1/3	= 	0.(3)
1/4	= 	0.25
1/5	= 	0.2
1/6	= 	0.1(6)
1/7	= 	0.(142857)
1/8	= 	0.125
1/9	= 	0.(1)
1/10	= 	0.1
Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
 */

// we need a way to find the decimals in string format as it will be long
// then we simply take the first digit, find the next time it appears, and see if it repeats
// if not, try finding the 2nd instance it appears, and try again, etc. until a reasonable number
// if not good, then we try starting from 2nd digit, ignoring the first e.g. like 0.166666 we need to
// ignore the 1
// recurring means "going on forever" so any fraction which has an end will not be an answer
// therefore we can filter any answers whos length is less than the desired precision

const d = 1000;

// Calculate the length of the recurring digits
const maxCycleCheck = 1000; // unconfirmed but I believe max cycle can only go as high as d itself

const recurringLength = (s) => {
    for (let i = 0; i <= maxCycleCheck + 1; i++) { // try from 1st digit to get recurring and continue
        // naive way of solving it first;
        let found = 0;
        if (!s.substr(i + 1).includes(s[i])) continue; // cannot find the next digit ever again
        for (let j = 1; j < maxCycleCheck; j++) {
            const sub = s.substr(i, j);
            if (!s.substr(i + j + 1).includes(sub)) break; // cannot find a repeat anymore
            const r = sub.repeat(Math.floor(s.length / j) - 1); // repeat until length of s~
            if (s.includes(r)) {
                found = j;
                break;
            }
        }
        if (found) return found;
    }
    return -1;
}

const decimals = 5000;
const fracs = [];

for (let i = 2; i < d; i++) {
    const digits = String(i).length;
    let start = 10 ** digits;
    let dec = '';
    for (let j = 0; j < decimals; j++) {
        dec += Math.floor(start / i);
        const q = start % i;
        if (q === 0) break;
        start = 10 * q;
    }

    if (dec.length === decimals) {
        fracs.push({ i, dec });
    }
}

// find the length of recurring
const max = fracs.reduce((max, frac) => {
    const recLength = recurringLength(frac.dec);
    return recLength > max.cycle ? { ...frac, cycle: recLength } : max;
}, { cycle: 0 });

console.log("d:", max.i, "cycle length:", max.cycle);   // d: 983 cycle length: 982
