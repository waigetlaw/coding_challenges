/**
 * A googol (10100) is a massive number: one followed by one-hundred zeros; 100100 is almost unimaginably large:
 * one followed by two-hundred zeros. Despite their size, the sum of the digits in each number is only 1.
 *
 * Considering natural numbers of the form, ab, where a, b < 100, what is the maximum digital sum?
 */

const multiplyNumber = require('../JSUtils/multiplyNumber');

let maxDigitalSum = 0;
for (let a = 1; a < 100; a++) {
    for (let b = 1; b < 100; b++) {
        let ans = String(a);
        let orig = String(a);
        for (let power = 1; power < b; power++) {
            ans = multiplyNumber(ans, orig);
        }
        const digitalSum = [...ans].reduce((sum, n) => sum + +n, 0);
        maxDigitalSum = Math.max(digitalSum, maxDigitalSum);
    }
}

console.log(maxDigitalSum); // 972
