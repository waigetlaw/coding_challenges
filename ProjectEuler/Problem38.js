/**
 * Take the number 192 and multiply it by each of 1, 2, and 3:

192 × 1 = 192
192 × 2 = 384
192 × 3 = 576
By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?
 */

// we should be able to brute force this... ?
// it must be concatenated else 987654321 x 1 would be the answer.
// therefore the form must be at least n x 1 + n x 2
// and the largest it can be is ofc 987654321,
// so if like n x 1 = 98765, and n x 2 = 4321 (ofc this is not possible - just setting limits)
// therefore we only need to test this for n up to a max of 98765.

const pand = [];

for (let n = 1; n < 98765; n++) {
    let conc = '';
    let mult = 1;
    while (conc.length < 9) {
        conc += n * mult++;
    }
    if (conc.split('').sort().join('') === '123456789') {
        pand.push(conc);
    }
}

console.log(Math.max(...pand.map((p) => +p)));  // 932718654
