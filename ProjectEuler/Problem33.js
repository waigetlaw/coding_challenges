const fractions = [];
for (let cancel = 1; cancel <= 9; cancel++) {
    for (let i = 1; i <= 9; i++) {
        let top1 = String(i) + String(cancel);
        let top2 = String(cancel) + String(i);
        let bot1 = "";
        let bot2 = "";
        for (let j = 1; j <= 9; j++) {
            if (i / j >= 1) continue;
            bot1 = String(j) + String(cancel);
            bot2 = String(cancel) + String(j);

            let simple = i / j;
            let f1 = top1 / bot1;
            let f2 = top1 / bot2;
            let f3 = top2 / bot1;
            let f4 = top2 / bot2;

            if (f1 === simple || f2 === simple || f3 === simple || f4 === simple) {
                fractions.push({ top: i, bot: j });
            }
        }
    }
}
const prod = fractions.reduce((prod, f) => prod * (f.top / f.bot), 1);
console.log(fractions, prod)

/**
 * [ { top: 1, bot: 4 },
  { top: 2, bot: 5 },
  { top: 1, bot: 5 },
  { top: 4, bot: 8 } ] 0.010000000000000002

  therefore, 0.01, which is 1/100
 */