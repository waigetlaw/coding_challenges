/**
 * The number 145 is well known for the property that the sum of the factorial of its digits is equal to 145:

1! + 4! + 5! = 1 + 24 + 120 = 145

Perhaps less well known is 169, in that it produces the longest chain of numbers that link back to 169; it turns out that there are only three such loops that exist:

169 → 363601 → 1454 → 169
871 → 45361 → 871
872 → 45362 → 872

It is not difficult to prove that EVERY starting number will eventually get stuck in a loop. For example,

69 → 363600 → 1454 → 169 → 363601 (→ 1454)
78 → 45360 → 871 → 45361 (→ 871)
540 → 145 (→ 145)

Starting with 69 produces a chain of five non-repeating terms, but the longest non-repeating chain with a starting number below one million is sixty terms.

How many chains, with a starting number below one million, contain exactly sixty non-repeating terms?
 */

// Seems like just a simple string split with factorial sum... might need to use string add but doesn't seem like a problem.

// Afterthoughts:
// It is a bit slow, but not terrible - we could speed up by knowing that only 3 numbers ever loop back to start - which means
// Every 60 chain must end by going back to a number within the chain i.e. not the original.
// what this means is if abcde is a 60 chain, then all permutations of abcde is also a 60 chain, as the sum of factorials will be the same
// and we know for a fact the chain did not terminate by going back to the original abcde, but some other number within the chain.
// this means we only need to check for the chain length for all unique combination of numbers up to 1,000,000 and if any is 60
// multiply that by the number of permutations that combination can produce.

const factorial = require('../JSUtils/factorial');
const addNumber = require('../JSUtils/addNumber');

const chains60 = [];

for (let num = 1; num < 1000000; num++) {
    if (num % 10000 === 0) console.log(num) // See progress
    const n = String(num);
    const steps = [n];
    let next;
    let i = 0;
    while (true) {
        next = [...steps[i++]].reduce((sum, d) => {
            return addNumber(sum, String(factorial(+d)))
        }, '0');
        if (steps.includes(next)) {
            break;
        }
        steps.push(next);
    }
    if (steps.length === 60) chains60.push(num);
}

console.log(chains60.join());
console.log(chains60.length);   // 402
