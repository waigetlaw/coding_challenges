/*
Some positive integers n have the property that the sum [n + reverse(n)] consists entirely of odd (decimal) digits. For instance, 
36 + 63 = 99 and 409 + 904 = 1313. We will call such numbers reversible; so 36, 63, 409, and 904 are reversible. Leading zeroes are not allowed in either 
n or reverse(n).

There are 120 reversible numbers below one-thousand.

How many reversible numbers are there below one-billion (10^9)?
*/

// thoughts
// no need to actually reverse the number. There should be a mathematical way to quickly add it's own reverse without reversing.
// 36 + 63 is 36 + 60 + 3.
// go through each digit and * 10 more when summing.

let limit = 1000000000;
let counter = 0;

console.time()

function getReverseN(n) {
    let rev = 0;
    // let length = 1;
    let length = Math.log10(n);
    // let copy = n;
    // while (copy >= 10) {
    //     copy = ~~(copy/10);
    //     length++;
    // }

    while(n >= 10) {
        let unit = n % 10;
        n = ~~(n/10);
        rev += unit * (10 ** --length)
    }
    rev += n
    return rev;
}
for (let n = 1; n < limit; n++) {
    if (n % 1000000 === 0) console.log(n)
    if (n % 10 === 0) continue;

    let reverseN = getReverseN(n);
    // console.log(n, reverseN)
    // let reverseN = [...String(n)].reduce((s, n, i) => {
    //    return s + +n * (10 ** i)
    // }, 0)
    // let reverseNOld = +[...String(n)].reverse().join('')
    let sum = n + reverseN;
    // console.log(n, reverseN)


    if (!/[02468]+/.test(''+sum)) {
        counter++;
    }
}
console.log(counter)
console.timeEnd()
// 1000000 default: 211.069ms brute force 18720 answer
