/**
 * A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
*/

// We need to home into the millionth by finding one digit at a time. The first digit is how many 9 factorials is in a million.
// 2nd is how many 8 factorials can fit in the remainder etc.
// Logic being if you went from 0 -> 1, then it must have went through all permutations that began with a 0.
// That is, for 0abcd, if the first digit went to 1, it must have went through all permutations of abcd starting with 0 to reach 1
// which is 4! combinations. So the first digit is always n! times until lexicographic position where n is digits remaining - 1.

/*
Testing for 4:
1   0123        13  2013
2   0132        14  2031
3   0213        15  2103
4   0231        16  2130
5   0312        17  2301
6   0321        18  2310
7   1023        19  3012
8   1032        20  3021
9   1203        21  3102
10  1230        22  3120
11  1302        23  3201
12  1320        24  3210
*/

const factorial = require('../JSUtils/factorial');

let lexPos = 999999; // starting 0
let nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

let i = nums.length - 1;
const vals = [];

while (lexPos > 0) {
    const f = factorial(i);
    const times = Math.floor(lexPos / f);
    vals.push(times);
    lexPos = lexPos % f;
    i--;
}

const arr = nums.map((n, i) => {
    return vals[i] || 0;
});

let ans = '';

arr.map((n) => {
    ans += nums[n];
    nums.splice(n, 1);
});

console.log(arr, ans); // 2783915460
