/*
Let p(n) represent the number of different ways in which n coins can be separated into piles. For example, five coins can be separated into piles in exactly seven different ways, so p(5) = 7.

OOOOO
OOOO   O
OOO   OO
OOO   O   O
OO   OO   O
OO   O   O   O
O   O   O   O   O
Find the least value of n for which p(n) is divisible by one million.

Thoughts:

This is actually same as all combinations to sum to n using all numbers from 1 to n.

e.g., for n = 5, we can sum to 5 in 7 different ways:
5
4 + 1
3 + 2
3 + 1 + 1
2 + 2 + 1
2 + 1 + 1 + 1
1 + 1 + 1 + 1 + 1

so if we can find a formula for calculating all these combinations, that would solve this problem.

There are 5 different ways to choose the first number:

5, 4, 3, 2, 1
However, after choosing 5, we end.
After choosing 4, we have 1 more way to continue
After choosing 3, we have 2 ways to continue, 2, 1
After choosing 2 we have 3 ways to continue, 3, 2, 1
After choosing 1 we have 4 ways to continue, 4, 3, 2, 1

however, there would be duplication here created via 3, 2 and 2, 3 so why don't we make this strictly equal or less than only:

5 -> end
4 -> 1 -> end
3 -> 2, 1
2 -> 2, 1
1 -> 1

next, 3 -> 1 would have 1 more choice.
2 -> 2 has 1 more
2 -> 1, strictly equal or reducing so once we reach 1, it's effectively over.
1 -> 1 same as above.

so it's about how many ways we can reduce this down until 1...

so start with all numbers 1 to n:
1, 2, 3, 4, 5
for each number, find the next biggest number that fits that is equal to or smaller

5 -> test 5,4,3,2,1
4 -> test 4,3,2,1
3 -> test 3,2,1
2 -> test 2,1
1 -> we know it's 1

once we find the answer, find what's left

5 needs 0
4 needs 1 -> 1 sol
3 needs 2, therefore 2, 1
2 needs 3, both 2 and 1 fits, so its 2. If it needed more, then it's possible to go 2, 2, 2, ... 1
*/

const getCombinationCount = require("../JSUtils/getCombinationCount");
const getCombinations = require("../JSUtils/getCombinations");

const problem78 = () => {
    for (let n = 1; n < 10; n++) {
        const combos = getCombinations(n, [...Array(n).keys()].map(x => x + 1));

        const results = combos.reduce((results, combo) => {
            if (!results[combo.length]) {
                results[combo.length] = 1
            } else {
                results[combo.length]++;
            }
            return results;
        }, {});

        console.log(n, combos.length, results)
        if (combos % 1000000 === 0) {
            console.log('sol', n, combos);
            break;
        }
    }
}

problem78();


/*
5 7
6 11
7 15
8 22
9 30
10 42
11 56
12 77
13 101
14 135
15 176
16 231
17 297
18 385
19 490
20 627
21 792
22 1002
23 1255

7, 11, 15, 22, 30, 42, 56, 77, 101, 135, 176, 231, 297, 385, 490, 627, 792, 1002, 1255

diffs

4, 4, 7, 8, 12, 14, 21, 24, 34, 41, 55, 66, 88, 105, 137, 165, 210, 253
no clear pattern here...
*/
