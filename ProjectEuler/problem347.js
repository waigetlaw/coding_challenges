/*
The largest integer <= 100 that is only divisible by both the primes 2 and 3 is 96, as 96 = 32 * 3 = 2^5 * 3. For two distinct primes p and q let M(p, q, N) be the largest positive integer <= N only divisible by both p and q and M(p, q, N) = 0 if such a positive integer does not exist.

E.g. M(2, 3, 100) = 96.
M(3, 5, 100) = 75 and not 90 because 90 is divisible by 2, 3 and 5, 
Also M(2, 73, 100) = 0 because there does not exist a positive integer <= 100 that is divisible by both 2 and 73.

Let S(N) be the sum of all distinct M(p, q, N), S(100) = 2262. 

Find S(10 000 000).

thoughts
v1
first find all primes up to 10,000,000 and loop through all possible p, q combos.

for getting the actual value of each individual one, we want to just start with p * q and work out the best combo of p * q that gets closest to N.

e.g. 2, 3, 100:

start with 2 * 3 = 6

then figure out that

2 * 2 * 2 * 2 * 2 * 3 = 96
2 * 3 * 3 * 3 = 54

we can try to tweak the 54
2 * 2 * 3 * 3 = 36

so basically, I think we start with max p, * 1 q, and shift downwards:

p^max * q <= N
then p^max - 1 * q ^ max <= N
and keep going until p ^ 1.
get the max number.

if p * q > N, stop from the start as it's 0.
*/

const genPrimesV2 = require("../JSUtils/genPrimesV2");

const problem347 = () => {
    console.time();

    const N = 10 ** 7;
    const primes = genPrimesV2(N);
    
    let total = 0;

    for (let i = 0; i < primes.length; i++) {
        for (let j = i + 1; j < primes.length; j++) {
            const p = primes[i];
            const q = primes[j];
            const base = p * q;
            if (base > N) {

                // console.log(`M(${p}, ${q}, ${N}) = 0`);
                break; // base is already bigger than N, no solutions
            }
            const freq = { p: 0, q: 0 };
            while (base * (p ** (freq.p + 1)) <= N) {
                freq.p++;
            }
            let maxValue = { ...freq, value: base * (p ** (freq.p)) }
            // try going down by 1 p multiple each time, and see if we can fit some q's in
            while (freq.p > 0) {
                freq.p--;
                while ((base * (p ** (freq.p)) * (q ** (freq.q + 1)) <= N)) {
                    freq.q++;
                }
                if (base * (p ** (freq.p)) * (q ** (freq.q)) > maxValue.value) {
                    maxValue = { ...freq, value: base * (p ** (freq.p)) * (q ** (freq.q)) };
                }
            }
            // console.log(`M(${p}, ${q}, ${N}) = ${maxValue.value}`);
            total += maxValue.value;
        }
    }

    console.log(`S(${N}) = ${total}`)
    console.timeEnd();
};

problem347();

/*
S(10000000) = 11109800204052
default: 764.77ms
*/
