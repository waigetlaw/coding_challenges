/*
A positive integer is called square root smooth if all of its prime factors are strictly less than its square root.
Including the number 1, there are 29 square root smooth numbers not exceeding 100.

How many square root smooth numbers are there not exceeding 10 000 000 000?
*/

/*
v1 was to divide by all primes under sqrt, and if the number was reduced to 1, that meant there would be no more prime factors above.
v2 Seems too slow to calc all prime factors below, so might try to divide by all primes =< sqrt and if any divides, it's not smooth
*/

// const genPrimesV2 = require('../JSUtils/genPrimesV2');
const isPrime = require('../JSUtils/isPrime');
const Uint1Array = require('../JSUtils/Uint1Array');

// v5
// calculating as we sieve
/**
 * Using Sieve of Eratosthenes Method
 * https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 */

// we are going to use the sieve directly so we don't run out of memory
const problem668 = () => {
    console.time();
    const limit = 10000000000;
    const sqrt = Math.sqrt(limit);
    const n = limit;
    const lastNumber = n % 2 === 0 ? n - 1 : n;
    const arrSize = Math.ceil(n / 2) - 1;
    let sieve = new Uint1Array(n)
    // let sieve = new Uint8Array(n)
    for (let i = 0; i < arrSize; i++) {
        sieve.set(i * 2 + 3, true);
    }

    let idx = 0;
    let current = idx * 2 + 3;
    while (current ** 2 <= lastNumber) {
        const idxSquare = idx + (current * (current - 1)) / 2;
        for (let i = idxSquare; i < arrSize; i += current) {
            sieve.set(i * 2 + 3, false);    // marking every nth as not prime starting from p^2
        }
        for (let i = idx + 1; i < arrSize; i++) {   // get next unmarked in sieve
            const next = sieve.get(i * 2 + 3);
            if (next) {
                current = i * 2 + 3;
                idx = i;
                break;
            }
        }
    }
    console.log('sieve calculated!');

    let nonSmooth = 2; // sieve does not contain 2

    for (let i = 1; i <= limit; i++) {
        const sieveElement = sieve.get(i);
        if (sieveElement) {
            if (i <= sqrt) {
                nonSmooth += i
            } else {
            nonSmooth += Math.floor(limit/i)
            }
        }
    }

    const smoothNumbers = limit - nonSmooth;
    console.log(smoothNumbers)
    console.timeEnd();
}





// v4

const problem668_v4 = () => {
    console.time();
    console.time('genPrime')
    const limit = 10000000000;
    const sqrt = Math.sqrt(limit);
    const primes = genPrimesV2(limit);
    let nonSmooth = 0; // take account of prime 2 so only odd checks remain
    
    console.log(primes.length)
    console.log(primes.slice(primes.length - 50))

    console.timeEnd('genPrime')
    for (const p of primes) {
        if (p <= sqrt) {
            nonSmooth += p
        } else {
        nonSmooth += Math.floor(limit/p)
        }
    }
    
    const smoothNumbers = limit - nonSmooth;
    console.log(smoothNumbers)
    console.timeEnd();
}

const problem668_old = () =>  {
    console.time();
    const primes = genPrimes(1100000);
    // const limit = 100;
    // const limit = 10000000;
    const limit = 50
    // const squareRootSmoothNumbers = [1];
    const log = [];
    let total = 1;

    const getPrimeRange = (from, to) => {
        const startingIndex = primes.findIndex(p => p >= from);
        const endingIndex = primes.findIndex(p => p >= to);
        return primes.slice(startingIndex, endingIndex + 1);
    }

    const isSqrtPrime = (sqrt, lastIndex) => {
        for (let i = lastIndex; i < primes.length; i++) {
            if (primes[i] > sqrt) return false;
            if (primes[i] === sqrt) return i + 1; // so it can't be 0 and falsey
        }
    }

    let lastSqrtIdx = 0;

    for (let i = 2; i <= limit; i++) {
        const primeFactors = [];
        if (i % 10000000 === 0) console.log(i / 10000000)
        const sqrt = Math.sqrt(i);
        if (Number.isInteger(sqrt)) {
            const issqrPrime = isSqrtPrime(sqrt, lastSqrtIdx);
            if (issqrPrime) {
                lastSqrtIdx = issqrPrime - 1
                continue;
            }
        }
        // const primeRange = getPrimeRange(sqrt, i);
        // if (primeRange.every(p => i % p !== 0)) squareRootSmoothNumbers.push(i)
        let n = i; // clone to not mess with loop
        let primeDivIndex = 0;
        // if (primes[startingPrimeDivIndex + 1] <= sqrt) startingPrimeDivIndex++;
        // let primeDivIndex = startingPrimeDivIndex;

        let primeDiv = primes[primeDivIndex];
        while (n >= sqrt && primeDiv < sqrt) {
            // console.log(n, primeDiv)
            while (n % primeDiv === 0) {
                // primeFactors.push(primeDiv)
                n /= primeDiv;
            }
            primeDiv = primes[++primeDivIndex];
        }
        log.push({i, n, primeFactors: primeFactors.toString(), sqrt, smooth: n < sqrt ? 'Yes' : ''})
        if (n < sqrt) total++;

    }

    console.table(log.filter(x => x.smooth))
    console.log(total)
    console.timeEnd();
    console.log(primes)
}

problem668();

/*
v1 limit = 10000000
] 2719288
default: 5.529s

v2
] 9919288
default: 59.110s
why so different answers?

at const limit = 1000000; they are different.
problem was the genPrimes was not high enough. For v2 we need much bigger primes, whereas v1 we only need primes up to the square root.

v3 lets modify v1, if the n is already reduced to a number less than sqrt, then all prime factors remaining will be smaller than the sqrt

[
   1,   8, 12, 16, 18, 24, 27, 30, 32,
  36,  40, 45, 48, 50, 54, 56, 60, 63,
  64,  70, 72, 75, 80, 81, 84, 90, 96,
  98, 100
] 29

amazingly, v3 is no improvement basically.

benchmark:
limit = 1000000
268172
default: 247.408ms

trying to check any primes from n - sqrt and break is so bad because there's so many primes after sqrt
268172
default: 55.518s


going backwards
2719288
default: 25.088s

nothing beats going forwards reducing n by prime divs until n < sqrt
2719288
default: 5.272s

2719288
default: 5.261s

not really improvement by checking if sqrt is square of a prime

v4
I may have found a way to count the number of smooth numbers.

Take prime number 7, it can only break a smooth number if 7 is > the sqrt of a number. So even if any number above 49 is divisible by 7, it does not break the rule to be a smooth number. So we only have to look at the numbers from 1 to 49.

We know exactly 8 numbers are from 1 - 49 that will be a multiple of 7 (basically p + 1):
1, 7, 14, 21, 28, 35, 42, 49
We can ignore the 1 because it's already a known smooth number
So for prime 7, it breaks 7 numbers.
Also note that 14 is not double counted by 2, because 2 already stops at 2. 14 is bigger than 2 squared so it being divisible by 2 does not break it.

This is always true because we search for p_n, we go to p_n^2, so when we check for p_n+1, where p_n+1 > p_n, p_n * p_n+1 is the first time that the number will be divisible by p_n+1, which is outside of the range p_n^2 as p_n * p_n+1 > p_n^2

so we simply calculate by adding the prime p's within the range, + 1 to account for 1:

for limit = 100;
primes are: 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97

2 + 3 + 5 + 7 = 17 + 11 + 1 = 29

why do we include 11 though?

for limit = 50:
 1, 8, 12, 16, 18, 24, 27, 30, 32, 36, 40, 45, 48, 50

 prime 2 breaks: 2, 4
 prime 3 breaks: 3, 6, 9
 prime 5 breaks: 5, 10, 15, 20, 25
 prime 7 breaks: 7, 14, 21, 28, 35, 42, 49
 prime 11 breaks: 11, 22, 33, 44
 prime 13 breaks: 13, 26, 39
 prime 17 breaks: 17, 34
 prime 19 breaks: 19, 38
 prime 23 breaks: 23, 46
 prime 29 breaks: 29
 prime 31 breaks: 31
 prime 37 breaks: 37
 prime 41 breaks: 41
 prime 43 breaks: 43
 prime 47 breaks: 47

 so the total broken is: 2 + 3 + 5 + 7 + 4 + 3 + 2 + 2 + 2 + 1 + 1 + 1 + 1 + 1 + 1 = 36

 so smooth numbers are the unbroken ones = 50 - 36 = 14

 this is likely much faster but we would need to calculate all primes up to the limit which will be difficult.

 However, worth finishing off this algorithm.

 Alorithm should simply be:

 let nonSmooth = 0
 if (p^2 <= limit) {
    nonSmooth += p
 } else {
    nonSmooth += Math.floor(limit/p)
 }

 v4 benchmarks:
 268172
default: 22.934ms // 10x faster!

2719288
default: 132.542ms like 38x faster!!

sadly due to how I generate primes, I can only generate up to:

const limit = 100000000;
27531694
default: 6.976s

we need a different way to calculate all the primes?
naive way - lets just loop through all odd numbers and just calculate if isPrime

268172
default: 46.043ms

not much slower, but prime checking will be painful as numbers get higher

27531694
default: 26.035s // same as max limit with genprimes, 4x slower but not terrible...!

with Uint8Array:
27531694
default: 1.031s

one zero away but this is Uint8Array's limit... using Uint1Array library is a lot slower.
278418003
default: 11.110s

using Uint1Array
278418003
default: 4:52.950 (m:ss.mmm)

it's wrong?
3541131650
default: 3:17:54.293 (h:mm:ss.mmm)

v5

lets find primes up to sqrt and only check it divides by primes where it's mult would be bigger than limit

e.g.
for limit = 50:
 1, 8, 12, 16, 18, 24, 27, 30, 32, 36, 40, 45, 48, 50

 the primes to check: 2, 3, 5, 7
for the number 30
 3 < sqrt(30), so we dont check
 5 < sqrt(30) so we don't check
 7 > sqrt(30) we check from here.

 31 is prime, how does this work?

 70462980
[
  1410064367, 1410064373, 1410064399, 1410064429,
  1410064433, 1410064483, 1410064493, 1410064517,
  1410064529, 1410064547, 1410064549, 1410064553,
  1410064559, 1410064583, 1410064631, 1410064633,
  1410064637, 1410064723, 1410064793, 1410064801,
  1410064807, 1410064811, 1410064823, 1410064951,
  1410064961, 1410064969, 1410064991, 1410065011,
  1410065023, 1410065047, 1410065087, 1410065143,
  1410065147, 1410065149, 1410065179, 1410065191,
  1410065197, 1410065201, 1410065219, 1410065257,
  1410065263, 1410065281, 1410065291, 1410065339,
  1410065353, 1410065381, 1410065387, 1410065389,
  1410065401, 1410065407
]
genPrime: 3:10:44.105 (h:mm:ss.mmm)
3541131650

problem is the sieve is cut short.

27531694
default: 953.426ms
with sieve in file so we do not need to push results to an array

sieve calculated!
3541131650
default: 3:03:05.432 (h:mm:ss.mmm)
even using Uint1Array, the index is wrong...

need to create our own Uint1Array by using a Uint8Array and calculating the number to bits.

sieve calculated!
27531694
default: 8.993s
with my own uint1Array, it's considerably slower, but it can at least calculate the answer (hopefully)

sieve calculated!
2811077773
default: 34:44.109 (m:ss.mmm)
*/
