let count = 0;
for (let year = 1901; year <= 2000; year++) {
    for (let month = 10; month <= 21; month++) {
        let m = 1 + month % 12;
        let yy = +String(year).substring(2);
        let c = +String(year).substring(0, 2);
        let d = m > 10 ? yy - 1 : yy;
        let k = 1;
        let c1 = Math.floor(((13 * m) - 1) / 5);
        let f = k + c1 + d + Math.floor(d / 4) + Math.floor(c / 4) - (2 * c);
        let day = f % 7;
        day = day < 0 ? day + 7 : day;

        if (day === 0) count++;
    }
}

console.log(count); // 171
