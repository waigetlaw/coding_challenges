/*
For a positive integer, n, define g(n) to be the maximum perfect square that divides n.
For example, g(18) = 9, g(19) = 1.

Also define

S(N) = n = 1 -> N Sigma g(n)

For example, S(10) = 24 and S(100) = 767. 

Find S(10^14). Give your answer modulo 1000000007.
*/


// need to go backwards - start with 10^14 as a square and add those - any in between = 1.

/*
 for 20:
perfect squares 1, 4, 9, 16, 25

only 16 < 20:

so loop 4,9,16:
4, 8, 12, 16 = 4
then overwrite:
9, 18 = 9
then overwrite:
16 = 16
fill rest with 1's and sum.
*/

const problem745 = () => {
    console.time()

    const limit = 100000000000000n
    let powers = [];
    let x = 2n;
    let xSquared = 4n;
    console.time('initial')
    while (xSquared <= limit) {
        powers.push({ index: x - 2n, original: x, squared: xSquared, freq: limit / xSquared });
        x++;
        xSquared = x * x;
    }

    console.timeEnd('initial')
    console.log(powers.length)

    for (let i = powers.length - 1; i >= 0; i--) {
        const power = powers[i];
        // console.log(power)
        let currentIndex = power.index + power.original;
        while (currentIndex < powers.length) {
            power.freq -= powers[currentIndex].freq
            currentIndex += power.original;
        }
    }

    const tally = powers.reduce((acc, { freq, squared }) => {
        return { sum: acc.sum + (squared * freq), count: acc.count + freq}
    }, { sum: 0n, count: 0n })

    const totalSum = tally.sum + (limit - tally.count)
    console.log({totalSum})
    console.log('ans with mod: ', totalSum % 1000000007n);

    console.timeEnd();
}

problem745();

/* 100

767n
default: 5.227ms
*/

/*
1000000n

725086120n
default: 125.204ms

725086120n
default: 37.421ms with counting style


{ totalSum: 725086120n }
ans with mod:  725086120n
default: 12.271ms with counting style jumping by original

{ totalSum: 725086120 }
ans with mod:  725086120
default: 5.029ms

without bigint and no more freq object
*/



/* 
for 1000
learn to count them
[
  { val: 4n, count: 153 }, { val: 9n, count: 70 },
  { val: 16n, count: 39 }, { val: 25n, count: 26 },
  { val: 36n, count: 17 }, { val: 49n, count: 13 },
  { val: 64n, count: 11 }, { val: 81n, count: 8 },
  { val: 100n, count: 7 }, { val: 121n, count: 6 },
  { val: 144n, count: 5 }, { val: 169n, count: 4 },
  { val: 196n, count: 4 }, { val: 225n, count: 3 },
  { val: 256n, count: 3 }, { val: 289n, count: 3 },
  { val: 324n, count: 3 }, { val: 361n, count: 2 },
  { val: 400n, count: 2 }, { val: 441n, count: 2 },
  { val: 484n, count: 2 }, { val: 529n, count: 1 },
  { val: 576n, count: 1 }, { val: 625n, count: 1 },
  { val: 676n, count: 1 }, { val: 729n, count: 1 },
  { val: 784n, count: 1 }, { val: 841n, count: 1 },
  { val: 900n, count: 1 }, { val: 961n, count: 1 }
]

22606n
default: 8.932ms

*/

/*
100000000n
{ totalSum: 724475280152n }
ans with mod:  475275084n
default: 2.168s

{ totalSum: 724475280152n }
ans with mod:  475275084n
default: 38.953ms with jumping skip

{ totalSum: 724418074781021074448n }
ans with mod:  94586478n
default: 53.938s
*/
