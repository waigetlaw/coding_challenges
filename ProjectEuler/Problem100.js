/*
If a box contains twenty-one coloured discs, composed of fifteen blue discs and six red discs, and two discs were taken at random, it can be seen that the probability of taking two blue discs, P(BB) = (15/21) x (14/20) = 1/2.

The next such arrangement, for which there is exactly 50% chance of taking two blue discs at random, is a box containing eighty-five blue discs and thirty-five red discs.

By finding the first arrangement to contain over 10^12 = 1 000 000 000 000 discs in total, determine the number of blue discs that the box would contain.

By starting with:
B/(B+R) x (B-1)/(B+R+1) = 0.5

we can get a quadratic to solve B where:
a = 1, b = -1 - 2R, c = R(1 - R)

Solve such that B is integer and B+R > 10^12

discriminant = 8r^2 + 1 (must be square number)

v2. rounding is a problem for testing integer...
*/

const Big = require('big-js');
Big.DP = 50;
const problem100 = () => {
    console.time();

    const limit = 10**12;
    const L = Big(limit);
    const a = Big(2);
    const b = Big(2).minus(L.times(4));
    const c = L.pow(2).minus(L);

    console.log(L.toString(), a.toString(), b.toString(), c.toString())

    const discriminant = b.pow(2).minus(Big(4).times(a).times(c));
    const sqrt = discriminant.sqrt();

    // calculate a good starting point for R such that B + R >= limit
    let R = Number((b.times(-1).minus(sqrt)).div(Big(2).times(a)).round(0, Big.roundDown));
    let B;

    while(true) {
        const b = -1 - 2*R;
        const discriminant = 8 * R * R + 1
        const sqrt = Math.sqrt(discriminant);
        if (Math.round(sqrt) ** 2 !== discriminant) {
            R++;
            continue;
        }
        if ((Math.round(sqrt) - b) % 2 !== 0) {
            R++;
            continue;
        }
        B = BigInt((Math.round(sqrt) - b)/2);
        const top = B * (B - 1n);
        const bot = (B + BigInt(R)) * (B + BigInt(R) - 1n);
        if(top * 2n === bot) {
            if (B + BigInt(R) > limit) break;
        }
        R++;
    }
    console.log(`${B} blue discs, ${R} red discs: (${B}/${B + BigInt(R)}) * (${B - 1n}/${B + BigInt(R) - 1n}) = 1/2`);
    console.timeEnd();
};

problem100();

/*
756872327473 blue discs, 313506783024 red discs: (756872327473/1070379110497) * (756872327472/1070379110496) = 1/2
default: 2:51.926 (m:ss.mmm)
*/
