/*
5-smooth numbers are numbers whose largest prime factor doesn't exceed 5.
5-smooth numbers are also called Hamming numbers.

Let O(a) be the count of prime factors of a (counted with multiplicity).
Let s(a) be the sum of the prime factors of a (with multiplicity).
For example, O(300) = 5 (2 * 2 * 3 * 5 * 5) and s(300) = 2 + 2 + 3 + 5 + 5 = 17.

Let f(n) be the number of pairs, (p, q), of Hamming numbers such that O(p) = O(q) and s(p) + s(q) = n.
You are given f(10) = 4 (the pairs are ((4, 9), (5, 5), (6, 6), (9, 4)) and f(10^2) = 3629.

Find f(10^7) mod 1 000 000 007.

Thoughts: although problem 204 finds hamming numbers quickly, we need to know the actual factors in this puzzle.
Can try to be naive and just divide by 2, 3, 5 and if !== 1, we know it's not 5 smooth.

If this is slow, we can try using problem 204's method of finding 5-smooth numbers first before calculating their factors.

Actually we will just go with that from the start...

then we categorise them based on O(a) as the key.
*/

const genPermutation = require("../JSUtils/genPermutation");
const genPrimesV2 = require("../JSUtils/genPrimesV2");
const getCombinationCount = require("../JSUtils/getCombinationCount");
const getCombinationCountFromList = require("../JSUtils/getCombinationCountFromList");
const getCombinations = require("../JSUtils/getCombinations");
const getCombinationsFromList = require("../JSUtils/getCombinationsFromList");

const tri = n => {
    return n * (n + 1) / 2;
}

const problem682 = () => {
    console.time('total');
    const hammingType = 5;
    const limit = 10 ** 2;
    const primes = genPrimesV2(hammingType).sort((a, b) => a - b);

    // let waysv2 = getCombinations(limit, [5, 3, 2]) // need one that shows all permutations, not combinations
    // const evenLength =  waysv2.filter(way => way.length % 2 === 0).length
    // console.log(waysv2, waysv2.length, evenLength, waysv2.length - evenLength)
    // return;
    console.time('getCombinations')
    let ways = getCombinations(limit, primes)
    let total = 0;
    console.timeEnd('getCombinations')

    console.log('number of ways:', ways.length)
    console.log('number of even ways:', ways.filter(way => way.length % 2 === 0).length);

    console.time('calcCombinationTime')
    for (const way of ways) {
        if (way.length % 2 !== 0) continue; // must be splittable into two equallly sized groups.
        const length = way.length / 2;
        const combinations = getCombinationsFromList(way, length);
        console.log(way, combinations)
        const combinationCount = combinations.length;
        // const combinationCount = getCombinationCountFromList(way, length);
        total += combinationCount;
    }
    console.timeEnd('calcCombinationTime')

    console.log(total)
    console.timeEnd('total');
};

problem682();

/*
definitely need to be smarter about this...

we basically want to say that for a same lengthed number of factors:

2 x 2 x 2 | 5 x 5 x 5 for example,
the 2 + 2 + 2 + 5 + 5 + 5 = n.
No need to actually calculate the smooth numbers. It's more about the combinations with 2, 3 and 5's

V1.

we need to 'just' calculate all possible ways to add up to n using 2, 3, 5's then find how many ways we can split these possibilities into
two exact halves (quantity of numbers) so it must be even number of 2, 3, 5s

e.g.

f(10):

2 + 2 + 2 + 2 + 2
2 + 3 + 2 + 3
2 + 3 + 5
5 + 5

only 2, 3, 2, 3 and 5, 5 are even, so we can ignore the rest.

we can split them as:
[2, 3] [2, 3] 
[2, 2], [3, 3]
[3, 3], [2, 2]
[5], [5]

Note that there is no point swapping [2, 3] and [3, 2] because they would multiply to the same number, 6
and we can't swap them round because both sides are [2, 3] (identical)

so it's for each unique split where left is different to right.
since we never need to swap 2, 3 and 3, 2 we can sort them too.

However, if we have X numbered list, how can we know how many ways there are to split it in half?

e.g.

[2,2,2,3,3,5]

there is:

222 335
223 235
225 233
233 225
235 223
335 222

seems count is just length of array, but we need to be careful for identical ones as 2, 3, 2, 3 had.

So we need to account for how many identical possibilities there are:

extreme case:
2 2 2 2 2 2

222 222 is the only one.

2 2 2 2 2 5
always need a 5 somewhere, so no identical

2 2 2 2 5 5

222 255
225 225
255 222
why only 3 here?
it's some kind of 6 choose 3 that's identical...

so max uniqueness would be if we had enough 3's and 5's for everything but we need to remember order doesn't matter i.e. 223 === 322

222
223
225
233
235
255
333
335
355
555

so if we just keep it always ascending we get all possibilities. 10 for 6
because

22 3 choices
23 2 choices
25 1 choice
33 2 choices
35 1 choice
55 1 choice

3 + 2 + 1 + 2 + 1 + 1 = 10

so 4 should be:
4 + 3 + 2 + 1 + 3 + 2 + 1 + 2 + 1 + 1 = 20

however, how do we remove dupes??

must be a way to mathematically calculate this because the numbers will get insanely large.

222335:
222 335
223 235
225 233
233 225
235 223
335 222

max:
222
223
225
233
235
255
333
335
355
555

there are 3 2's, 2 3's, 1 5
we need 3 of each to get all combinations, any less is a reduction.

1 reduction in 3 means...? 1 less
2 reductions in 5 means... 3 less too. why only 3?

I think 1 less only removes when all is same. so 1 less 3 removes 333 only.
but 2 less means any 33x is removed too. and x could be 2, 3, 5 so 3 choices but we already know 3 is missing so only 2.? or dont count 1 and count this as 3?

imagine 4 length
2222
2223
2225
2233
2235
2255
2333
2335
2355
2555
3333
3335
3355
3555
5555

15 choices. why 15?

should be triangle numbers... 5 * 6 / 2 = 15. so it's length + 1'ths triangle number.

when missing 1 5, we only lose 1.
when missing 2 5's we lose 1 + 2 = 3
when missing 3 5's we can't do 55 => 1 + 2 + 3

so the number missing is also triangle number...
missing => missing * (missing + 1) / 2;

lets see the first example..

2 2 2 2 5 5

222 255
225 225
255 222
why only 3 here?

length = 3 so possibilities is: 4 * 5 / 2 = 10;

missing 3 3's = 1 + 2 + 3 = 6 gone.
missing 1 5 = 1 gone.

10 - 7 = 3. !!!

So now only problem is how do we calculate all the arrays of possible sums to n.

We can potentially use the coin sum problem but it seems like it would be too difficult for 10^7...


2 3 2 3

22
23
25
33
35
55


// this is too ridiculous though it seems...
already 16834 ways to sum to 1000
166683334 ways to sum to 100,000

10 4
100 184
1000 16834
10000 1668334
100000 166683334

looks like we can know the number of ways to sum to 10 ** n.
but now how do we reduce it?

following pattern...
1000000 16666833334
10000000 1666668333334

2, 2, 2, 2, 2, 2

222

223
225
233
235
255
333
335
355
555

no 3's meant missing 3 3s meaning 3 + 2 + 1... true but we are double counting with the 5's!
for the 5's we need to remove 3

2 2 2 2 2 5

222
225

3's eliminate 6
but 5's eliminate 3 - 1 because 355 is already accounted for.

i dont know how to account for doubles...

on paper I kind of get it but still too hard to come up with an efficient algorithm...

2222
2223
2225
2233
2235
2255
2333
2335
2355
2555
3333
3335
3355
3555
5555
15 choices

if missing 1 2:
2223
2225
2233
2235
2255
2333
2335
2355
2555
3333
3335
3355
3555
5555
14 choices

if missing 2 2:
2233
2235
2255
2333
2335
2355
2555
3333
3335
3355
3555
5555

12 choices

if missing 3 2
2333
2335
2355
2555
3333
3335
3355
3555
5555
9 choices

if missing all 2's
3333
3335
3355
3555
5555

5 choices

pattern is -1, -2, -3, -4

if missing only 1 2, or 3, or 5, it doesn't affect the rest, because it's just the all 2's all 3's all 5's affected.

it starts from 2 missing:

if 2 2's missing, then it affects the 3's results by:

missing 2 | less 3 | less 5 too
1 | 0
2 | 1
3 | 3
4 | 6

however, now how do we combine the less 3 and 2??

e.g.

2 missing 2, 2 missing 3

-1 from 2
-1 from 3

it doesn't clash because even if 2 2's are missing, we can't then have 3 3's missing because length is 4.

but if we go 
-3 2's and -2 3's
2222 NO 2
2223 NO 2
2225 NO 2
2233 NO 2
2235 NO 2
2255 NO 2
2333 NO 3
2335
2355
2555
3333 NO 3
3335 NO 3
3355
3555
5555

6 REMAIN

-1 5 from 3's, -3 5's from 2 as per the table... ?

lets go more extreme:
-4 2, -3 3:
2222 NO 2
2223 NO 2
2225 NO 2
2233 NO 2 | 3
2235 NO 2
2255 NO 2
2333 NO 2 | 3
2335 NO 2 | 3
2355 NO 2
2555 NO 2
3333 NO 3
3335 NO 3
3355 NO 3
3555
5555

only 2 remain, 3555 and 5555.
2 normally cancels out 10 total, 6 3's
3's cancels out 6 normally but 3 overlap. how do we know only 3 overlapped?

well with the 2's, we know each number affects more 2's
with 4 2's:

it affects 0 of 4 3's
 1 of 3 3's
 2 of 2 3's
 3 of 1 3's
 4 of 0 3's

 for the 3 parts, because our 3 is up to 3, it affects 6.

 but 3 of them are single 3's. which we have.

 there are 3 single 3's, 2 2 3's, 1 3 3s.

 so max overlap is

 -4 2, -4 3, which means overlap is 6.
 -4 2, -3 3, max overlap is 3
 -4 2, -2 3, means overlap is 2,
 -4 2, -1 3, means overlap is 1.

then i dont even want to think about overlaps for 5...

breakthrough...?
Finally coded to get all combinations from a list
however, the answer is still too much - but I think I understand the overlap.

If we had a combination:
333333333332222222222222
for example, then 2222222222222 is a solution.

however, we can swap 5 3's for 3 5's, which would make the string 2 smaller....
5553333333333222222222222222

AND substitute 4 3's for 6 2's to gain 2 again... this makes the length same as before - but with different numbers
5553333333332222222222222222
in this case, 22222222222222 is still a solution, which overlaps with the original.

let's use a set for now... after all it's only v1.

Now with a set it's way too small... what am I missing?!?

v1:
3629
default: 11.422ms

25808429
default: 2:05.083 (m:ss.mmm)

v2:
using only combination count rather than all combinations as an array of strings
3629
default: 10ms
25808429
default: 38.563s

10 = 4
100 = 3629
1000 = 25808429

10
number of ways: 4
number of even ways: 2

100
number of ways: 184
number of even ways: 94

1000
number of ways: 16834
number of even ways: 8434

20 === 20
30 === 63

might be some way to shortcut the even + find all possible ways to half by starting the 'sum problem' with sums of all numbers
ie.
2 + 2 = 4
2 + 3 = 5
2 + 5 = 7
3 + 3 = 6
3 + 5 = 8
5 + 5 = 10
[4, 5, 6, 7, 8, 10]

any combination of these that sum to 10 would already be divisible by 2 to split the number to two omegas.

[ 5, 5, 5, 5 ] [ '55' ]
[ 3, 3, 3, 3, 3, 5 ] [ '333', '335' ]
[ 2, 2, 3, 3, 5, 5 ] [
  '223', '225',
  '233', '235',
  '255', '335',
  '355'
]
[
  2, 2, 2, 2,
  3, 3, 3, 3
] [ '2222', '2223', '2233', '2333', '3333' ]
[
  2, 2, 2, 2,
  2, 2, 3, 5
] [ '2222', '2223', '2225', '2235' ]
[
  2, 2, 2, 2, 2,
  2, 2, 2, 2, 2
] [ '22222' ]

[55, 55]            -> [10, 10]
[333, 335]          -> [6, 6, 8]
[335, 333]          -> [6, 8, 6] // repeated
[223, 355]          -> [4, 6, 10]
[225, 335]          -> [4, 8, 8]
[233, 255]          -> [5, 5, 10]
[235, 235]          -> [5, 7, 8]
[255, 233]          -> [7, 7, 6]
[335, 225]          -> [6, 7, 7] //677 repeated
[355, 223]          -> [8, 7, 5]
[2222, 3333]        -> [4, 4, 6, 6]
[2223, 2333]        -> [4, 5, 5, 6]
[2233, 2233]        -> [4, 6, 4, 6] // 4466 repeated
[2333, 2223]        -> [5, 6, 4, 5] // 4556 repeated
[3333, 2222]        -> [6, 6, 4, 4] // 4466 repeated
[2222, 2235]        -> [4, 4, 4, 8]
[2223, 2225]        -> [4, 5, 4, 7]
[2225, 2223]        -> [4, 7, 4, 5] // 4457 repeated
[2235, 2222]        -> [4, 8, 4, 4] //4448 repeated
[22222, 22222]      -> [4, 4, 4, 4, 4]

// using 4, 5, 6, 7, 8, 10
[
  [ 10, 10 ],        [ 6, 7, 7 ],
  [ 6, 6, 8 ],       [ 5, 7, 8 ],
  [ 5, 5, 10 ],      [ 5, 5, 5, 5 ],
  [ 4, 8, 8 ],       [ 4, 6, 10 ],
  [ 4, 5, 5, 6 ],    [ 4, 4, 6, 6 ],
  [ 4, 4, 5, 7 ],    [ 4, 4, 4, 8 ],
  [ 4, 4, 4, 4, 4 ]
] 13 6 7

why only:
668 x2
667 x2
4466 x3
4556 x2
4457 x2
4448 x2

for each repeated number, add one more??
why is 488 not repeated?
488 becomes 4810 when swapping

[223, 355]          -> [4, 6, 10]
[225, 335]          -> [4, 8, 8]

repeats
-----------------------------------------------------
668
[333, 335]          -> [6, 6, 8]
[335, 333]          -> [6, 8, 6] // repeated

667
[255, 233]          -> [7, 7, 6]
[335, 225]          -> [6, 7, 7] //677 repeated

4466
[2222, 3333]        -> [4, 4, 6, 6]
[2233, 2233]        -> [4, 6, 4, 6] // 4466 repeated
[3333, 2222]        -> [6, 6, 4, 4] // 4466 repeated

4556
[2223, 2333]        -> [4, 5, 5, 6]
[2333, 2223]        -> [5, 6, 4, 5] // 4556 repeated

4457
[2223, 2225]        -> [4, 5, 4, 7]
[2225, 2223]        -> [4, 7, 4, 5] // 4457 repeated

4448
[2222, 2235]        -> [4, 4, 4, 8]
[2235, 2222]        -> [4, 8, 4, 4] //4448 repeated
*/
