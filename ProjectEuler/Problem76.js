/**
 * It is possible to write five as a sum in exactly six different ways:

4 + 1
3 + 2
3 + 1 + 1
2 + 2 + 1
2 + 1 + 1 + 1
1 + 1 + 1 + 1 + 1

How many different ways can one hundred be written as a sum of at least two positive integers?
 */

// We can use the code used to count all the different ways to sum to a total from problem 31 which is moved to util code:
// at least 2 positive integers means 100 itself cannot be included, so we need an arr of 1-99
// Update, although the answer was correct - it took a long time so there should be a more efficient way...

const getCombinationCount = require('../JSUtils/getCombinationCount');

const arr = [];
for (let i = 1; i < 100; i++) {
    arr.push(i);
}

const totalCombinations = getCombinationCount(100, arr);

console.log(totalCombinations);
