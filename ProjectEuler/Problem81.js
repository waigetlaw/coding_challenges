/*
In the 5 by 5 matrix below, the minimal path sum from the top left to the bottom right, by only moving to the right and down, is indicated in bold red and is equal to 2427.

\begin{pmatrix}
\color{red}{131} & 673 & 234 & 103 & 18\\
\color{red}{201} & \color{red}{96} & \color{red}{342} & 965 & 150\\
630 & 803 & \color{red}{746} & \color{red}{422} & 111\\
537 & 699 & 497 & \color{red}{121} & 956\\
805 & 732 & 524 & \color{red}{37} & \color{red}{331}
\end{pmatrix}
 
Find the minimal path sum from the top left to the bottom right by only moving right and down in matrix.txt (right click and "Save Link/Target As..."), a 31K text file containing an 80 by 80 matrix.
*/

/*
Proposal, split the matrix into two triangles and solve min path to the diagonal just like problem 67 and then solve 2nd triangle with diagonal replaced with the min path from sol 1 of the first triangle.

actually this won't work because I need the min paths from if I took each of the different diags.
but it's still similar to problem67, but maybe the algorithm can be modified so that instead of a triangle, it's a diamond.
*/

/*
 I think it's basically there but the algorithm to convert to diamond breaks if the matrix is even sized. Need a small tweak.

 [
    [1, 2],
    [3, 4]
 ]

 should become:

 [
    [1],
    [3, 2]
    [4]
 ]

 actually problem was a hardcoded number that should have depended on the matrix.length...
*/

// adapted from problem67 and changed max to min
const findMinPath = (matrix) => {
    for (let i = matrix.length - 2; i >= 0; i--) {
        for (let j = 0; j < matrix[i].length; j++) {
            const col = matrix[i].length < matrix[i + 1].length ? j : j - 1;
            const left = matrix[i + 1][col] || Number.MAX_SAFE_INTEGER;
            const right = matrix[i + 1][col + 1] || Number.MAX_SAFE_INTEGER;
            matrix[i][j] += Math.min(left, right)
        }
    }
    return matrix[0][0];
}


const exampleMatrix = [
    [131, 673, 234, 103, 18],
    [201, 96, 342, 965, 150],
    [630, 803, 746, 422, 111],
    [537, 699, 497, 121, 956],
    [805, 732, 524, 37, 331]
];

const readFile = require('../JSUtils/readFile');
const path = require('path')

const problem81 = async () => {
    console.time();
    const filePath = path.join(__dirname, './files/0081_matrix.txt');
    const content = await readFile(filePath);

    const matrix = content.split('\n').filter(Boolean).map(line => line.split(',').map(str => +str));

    const diamond = []

    for (let i = 0; i < (matrix.length * 2 - 1); i++) {
        const row = [];
        const deltaDiag = (matrix.length -  Math.abs((matrix.length - 1) - i));
        for (let j = 0; j < deltaDiag; j++) {

            if (i < matrix.length) {
                row.push(matrix[i - j][j])
            } else {
                row.push(matrix[(matrix.length - 1) - j][j + (i - (matrix.length - 1))])
            }
        }
        diamond.push(row)
    }

    const min = findMinPath(diamond);
    
    console.log(min)
    console.timeEnd();
}

problem81();

/*
427337
default: 7.451ms
*/
