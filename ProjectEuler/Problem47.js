/**
 * The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors each. What is the first of these numbers?
 */

// For debugging purposes, we should form an array of numbers which contains 4 distinct prime factors
// and keep going until we hit four consecutive integers in a row.
// to calculate the distinct prime factors, we should just continuously divide and reset until
// the original number is 1.

const consecutive = 4;
const factors = 4;
const numbers = [];
const consecutiveNumbers = [];

let i = 1;
while (true) {
    let n = ++i;
    const distinctPrimeFactors = [];

    // separating div 2 so we can increment by odd numbers after for efficiency
    if (n % 2 === 0) distinctPrimeFactors.push(2);
    while (n > 1 && n % 2 === 0) {
        n /= 2;
    }

    let div = 3;
    while (n > 1) {
        if (n % div === 0) {
            while (n % div === 0) {
                n /= div;
            }
            if (n !== 1 && distinctPrimeFactors.length === 3) break;    // means there are more factors than wanted, so we just break here and move onto the next number
            distinctPrimeFactors.push(div);
        }
        div += 2;
    }
    if (distinctPrimeFactors.length === factors) {
        consecutiveNumbers.push(i);
    } else {
        consecutiveNumbers.length = 0;
    }
    if (consecutiveNumbers.length === consecutive) break; // found answer
}

console.log(consecutiveNumbers.join()); // 134043,134044,134045,134046
