/*
It is easily proved that no equilateral triangle exists with integral length sides and integral area. However, the almost equilateral triangle 5-5-6 has an area of 12 square units.

We shall define an almost equilateral triangle to be a triangle for which two sides are equal and the third differs by no more than one unit.

Find the sum of the perimeters of all almost equilateral triangles with integral side lengths and area and whose perimeters do not exceed one billion (1000000000).

Thoughts: seems potentially brute forcible though may need to be smarter about it.
The equal sides vary from 1 to 1 billion~
and the 3rd side varies by only +/- 1.

However, might be better to check the area formula of half x base x height and figure if there is an easy way to figure what the integers would be.
*/

function sqrt(value) {
    if (value < 0n) {
        throw 'square root of negative numbers is not supported'
    }

    if (value < 2n) {
        return value;
    }

    if (value === 4n) {
        return 2n;
    }

    function newtonIteration(n, x0) {
        const x1 = ((n / x0) + x0) >> 1n;
        if (x0 === x1 || x0 === (x1 - 1n)) {
            return x0;
        }
        return newtonIteration(n, x1);
    }

    return newtonIteration(value, 1n);
}


const problem94 = () => {
    console.time();
    const limitPerimeter = 1000000n;
    const log = [];
    let sum = 0n;
    let x = 1n; // equal sides
    while (x + x + (x-1n) <= limitPerimeter) {
        let bases = [x - 1n, x + 1n];
        for (const base of bases) {
            if (x + x + base > limitPerimeter) break;
            if (base % 2n !== 0n) continue;
            if (base === 0n) continue;
            if (base >= 2n * x) continue;

            const heightSquared = (x * x) - ((base/2n) ** 2n);
            const sqrtHeight = sqrt(heightSquared);

            const area = base * sqrtHeight / 2n;
            if (sqrtHeight * sqrtHeight === heightSquared) {
                log.push(`${x}, ${base}, ${area}`)
                sum += x + x + base
            }
        }
        x++;
    }

    console.table(log);
    console.log(sum)
    console.timeEnd();
};

problem94();

/*

┌─────────┬───────────────────────────────────────────┐
│ (index) │                  Values                   │
├─────────┼───────────────────────────────────────────┤
│    0    │                 '1, 0, 0'                 │
│    1    │                 '1, 2, 0'                 │
│    2    │                '5, 6, 12'                 │
│    3    │               '17, 16, 120'               │
│    4    │              '65, 66, 1848'               │
│    5    │             '241, 240, 25080'             │
│    6    │            '901, 902, 351780'             │
│    7    │           '3361, 3360, 4890480'           │
│    8    │         '12545, 12546, 68149872'          │
│    9    │         '46817, 46816, 949077360'         │
│   10    │       '174725, 174726, 13219419708'       │
│   11    │      '652081, 652080, 184120982760'       │
│   12    │     '2433601, 2433602, 2564481115560'     │
│   13    │    '9082321, 9082320, 35718589344360'     │
│   14    │   '33895685, 33895686, 497495864091732'   │
│   15    │  '92604733, 92604734, 3713359590086717'   │
│   16    │ '126500417, 126500416, 6929223155685600'  │
│   17    │ '143448260, 143448261, 8910277040981728'  │
│   18    │ '177343944, 177343943, 13618628081437712' │
│   19    │ '185209465, 185209464, 14853438039554664' │
│   20    │ '194291787, 194291788, 16345925792225962' │
│   21    │ '202157308, 202157309, 17696180071908738' │
│   22    │ '236052992, 236052991, 24127907203106504' │
│   23    │ '253000835, 253000836, 27716893060952700' │
│   24    │ '279030998, 279030997, 33713631833989600' │
│   25    │ '286896519, 286896518, 35641107667007570' │
│   26    │ '294762040, 294762039, 37622161391907590' │
│   27    │ '303844362, 303844363, 39976337350345890' │
│   28    │ '311709883, 311709884, 42072835396811736' │
└─────────┴───────────────────────────────────────────┘
9399467731
default: 2.117s

problems at high numbers...

┌─────────┬───────────────────────────────┐
│ (index) │            Values             │
├─────────┼───────────────────────────────┤
│    0    │           '1, 0, 0'           │
│    1    │           '1, 2, 0'           │
│    2    │          '5, 6, 12'           │
│    3    │         '17, 16, 120'         │
│    4    │        '65, 66, 1848'         │
│    5    │       '241, 240, 25080'       │
│    6    │      '901, 902, 351780'       │
│    7    │     '3361, 3360, 4890480'     │
│    8    │   '12545, 12546, 68149872'    │
│    9    │   '46817, 46816, 949077360'   │
│   10    │ '174725, 174726, 13219419708' │
└─────────┴───────────────────────────────┘
716038n
default: 710.533ms

┌─────────┬──────────────────────────────────────────┐
│ (index) │                  Values                  │
├─────────┼──────────────────────────────────────────┤
│    0    │                '5, 6, 12'                │
│    1    │              '17, 16, 120'               │
│    2    │              '65, 66, 1848'              │
│    3    │            '241, 240, 25080'             │
│    4    │            '901, 902, 351780'            │
│    5    │          '3361, 3360, 4890480'           │
│    6    │         '12545, 12546, 68149872'         │
│    7    │        '46817, 46816, 949077360'         │
│    8    │      '174725, 174726, 13219419708'       │
│    9    │      '652081, 652080, 184120982760'      │
│   10    │    '2433601, 2433602, 2564481115560'     │
│   11    │    '9082321, 9082320, 35718589344360'    │
│   12    │  '33895685, 33895686, 497495864091732'   │
│   13    │ '126500417, 126500416, 6929223155685600' │
└─────────┴──────────────────────────────────────────┘
518408346n
default: 16:13.537 (m:ss.mmm)

*/
