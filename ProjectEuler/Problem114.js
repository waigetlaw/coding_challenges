/*
A row measuring seven units in length has red blocks with a minimum length of three units placed on it, such that any two red blocks (which are allowed to be different lengths) are separated by at least one grey square. There are exactly seventeen ways of doing this.

0114.png
How many ways can a row measuring fifty units in length be filled?

NOTE: Although the example above does not lend itself to the possibility, in general it is permitted to mix block sizes. For example, on a row measuring eight units in length you could use red (3), grey (1), and red (4).

Thoughts: again extremely similar to p116 and 115, but a little tricky to determine if at least 1 block in-between red ones. I think I will just filter them out after finding all combinations.

It might have tons of combinations though... for an n length, and minimum m length, I would need to have the range from [1, m, m + 1, m + 2, ..., n]

update: I thought I could filter combinations and still use getPermutationCount but actually it's trickier because if I have something like:
[3, 3, 1], I need to know the permutationCount can only be 3, 1, 3 and nothing else...

I think the trick is to remove n - 1 one's, where n is the number of red tiles (i.e. number of numbers > 1)

so in the above, there are 2 x 3's so we remove 1 one. If there are not enough ones, it's an invalid configuration because there isn't enough ones to put them in-between each red tile.
*/

const getCombinations = require("../JSUtils/getCombinations");
const getPermutationCount = require("../JSUtils/getPermutationCount");

const problem114 = () => {
    console.time();
    const units = 50;
    const minLength = 3;

    const mToN = [...Array(units - minLength + 1)].map((_, i) => i + minLength);

    const combinations = getCombinations(units, [...mToN, 1])
        .filter(combination => !(combination.length > 1 && combination.every(n => n !== 1)))
        .map(combination => {
            const numberOfRedTiles = combination.reduce((red, n) => n > 1 ? red + 1 : red, 0);
            const numberOfOnes = combination.length - numberOfRedTiles;
            const shouldRemove = numberOfRedTiles - 1;
            if (shouldRemove > numberOfOnes) return null;
            return combination.slice(0, combination.length - shouldRemove);
        }).filter(Boolean);
    const total = combinations.reduce((permutations, combination) => permutations + getPermutationCount(combination), 0)

    console.log(total)
    console.timeEnd();
};

problem114();

/*
16475640049
default: 85.645ms
*/
