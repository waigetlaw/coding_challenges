/*
Let p_n be the nth prime: 2, 3, 5, 7, 11,..., and let r be the remainder when (p_n - 1) ^ n + (p_n + 1) ^ n is divided by p_n^2.

For example, when n = 3, p_3 = 5, and 4^3 ++ 6^3 = 280 == 5 mod 25.

The least value of n for which the remainder first exceeds 10^9 is 7037.

Find the least value of n for which the remainder first exceeds 10^10.

Thoughts: this problem is extremely similar to problem 120 and instead of a it's primes. Remainder for odd n will always be 2an, even n is always 2 so we can ignore.
*/

const genPrimes = require("../JSUtils/genPrimes");

const problem123 = () => {
    console.time();
    const limit = 10000000000;
    const primes = genPrimes(1000000);

    const n = primes.findIndex((p, i) => i % 2 === 0 && 2 * p * (i + 1) > limit) + 1;
    console.log(n);
    console.timeEnd();
};

problem123();

/*
21035
default: 21.406ms
*/
