/*
The number 512 is interesting because it is equal to the sum of its digits raised to some power: 5 + 1 + 2 = 8, and 8^3 = 512. Another example of a number with this property is 614656 = 28^4.

We shall define a_n to be the nth term of this sequence and insist that a number must contain at least two digits to have a sum.

You are given that a_2 = 512 and a_10 = 614656.

Find a_30.

Thoughts: brute force is ok up to maybe the first 15 but it gets tough. We can likely do this 'backwards'.

For example, setting base of 8, we try 8^1, 8^2, 8^3, ... until 8^x 'clearly' has too many digits to add up to 8 (although technically there could be lots of 0's)
But we can loop them until a 'decent' length such as 20 digits long and keep the ones where the digit sum back to the base.
Then just sort the list and hopefully we have caught all of the numbers and haven't skipped any.

As long as we pick a number such that base ^ 2 >= 'decent length' we should have captured all.
*/

const problem119 = () => {
    console.time();

    const limitPower = 14n;

    const interestingNumbers = [];

    const limitBase = 10n;
    const limit = limitBase ** limitPower;
    for (let base = 2n; base < 1n + (limitBase ** (limitPower/2n)); base++) {
        let number = base;

        while (number < limit) {
            number *= base;
            const digitSum = `${number}`.split('').reduce((total, digit) => total + BigInt(digit), 0n);
            if (digitSum === base) {
                interestingNumbers.push(number);
            }
        }
    }
    // while (a_n.length < 30) {
    //     if (number % 1000000n === 0n) console.log(number)
    //     const digitSum = `${number}`.split('').reduce((total, digit) => total + BigInt(digit), 0n);
    //     let base = digitSum;
    //     while (base > 1 && base < number) base *= digitSum;
    //     if (base === number) {
    //         a_n.push(number);
    //         console.log(a_n)
    //     }
    //     number++;
    // }

    console.log(interestingNumbers.sort((a, b) => b < a ? 1 : -1))
    if (interestingNumbers.length >= 30) console.log(interestingNumbers[29])
    console.timeEnd();
};

problem119();

/*
[
        81n,       512n,
      2401n,      4913n,
      5832n,     17576n,
     19683n,    234256n,
    390625n,    614656n,
   1679616n,  17210368n,
  34012224n,  52521875n,
  60466176n, 205962976n
]


[
                81n,             512n,
              2401n,            4913n,
              5832n,           17576n,
             19683n,          234256n,
            390625n,          614656n,
           1679616n,        17210368n,
          34012224n,        52521875n,
          60466176n,       205962976n,
         612220032n,      8303765625n,
       10460353203n,     24794911296n,
       27512614111n,     52523350144n,
       68719476736n,    271818611107n,
     1174711139837n,   2207984167552n,
     6722988818432n,  20047612231936n,
    72301961339136n, 248155780267521n,
  3904305912313344n
]
248155780267521n
default: 17.864s
*/
