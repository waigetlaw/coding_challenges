/**
 * Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:
 *
 * 1634 = 14 + 64 + 34 + 44
 * 8208 = 84 + 24 + 04 + 84
 * 9474 = 94 + 44 + 74 + 44
 * As 1 = 14 is not a sum it is not included.
 *
 * The sum of these numbers is 1634 + 8208 + 9474 = 19316.
 *
 *Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
 */

// For this, we firstly find the limitations to check until.
// We do this by working out what 9^5 is, as this is the largest it can be for any digit.
// this is 59049, so for every digit, it can add a max of 59049 to the sum.
// Find the point where the sum becomes < the number of digits.
// at 6 digits, max sum is 59049 * 6 = 354294, so we should check up to this at most.
// going to 7 digits or higher will never work as the sum cannot even sum to 7 digits.

const possible = [];
for (let i = 2; i < 354294; i++) {
    const digits = [...String(i)].map((c) => +c);
    if (digits.reduce((sum, n) => sum + (n ** 5), 0) === i)
        possible.push(i);
}

console.log(possible); // [ 4150, 4151, 54748, 92727, 93084, 194979 ]
console.log(possible.reduce((sum, n) => sum + n, 0)); // 443839
