/**
 * The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?
*/

// start on the "middle" prime and subtract by d -> 0 -> 10000 (limit 1000, 10000) in steps of 2 and if both hit a prime, thats the answer

const isPrime = require('../JSUtils/isPrime');

const triplePrimes = [];

for (let cur = 1001; cur < 10000; cur++) {
    if (!isPrime(cur)) continue;
    let d = 2;
    let prev, next;
    do {
        prev = cur - d;
        next = cur + d;

        const strCur = String(cur).split('').sort().join('');
        const strPrev = String(prev).split('').sort().join('');
        const strNext = String(next).split('').sort().join('');

        if (strCur === strPrev && strPrev === strNext && isPrime(prev) && isPrime(next))
            triplePrimes.push({ prev, p: cur, next, d, concat: '' + prev + cur + next });
        d += 2;
    } while (prev > 1000 && next < 9999)
}

console.log(triplePrimes);

// [{
//     prev: 1487,
//     p: 4817,
//     next: 8147,
//     d: 3330,
//     concat: '148748178147'
// },
// {
//     prev: 2969,
//     p: 6299,
//     next: 9629,
//     d: 3330,
//     concat: '296962999629'
// }]
