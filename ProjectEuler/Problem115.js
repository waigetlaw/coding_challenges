/*
NOTE: This is a more difficult version of Problem 114.

A row measuring n units in length has red blocks with a minimum length of m units placed on it, such that any two red blocks (which are allowed to be different lengths) are separated by at least one black square.

Let the fill-count function, F(m, n), represent the number of ways that a row can be filled.

For example, F(3, 29) = 673135 and F(3, 30) = 1089155.

That is, for m = 3, it can be seen that n = 30 is the smallest value for which the fill-count function first exceeds one million.

In the same way, for m = 10, it can be verified that F(10, 56) = 880711 and F(10, 57) = 1148904, so n = 57 is the least value for which the fill-count function first exceeds one million.

For m = 50, find the least value of n for which the fill-count function first exceeds one million.

Thoughts: using the same algorithm for p114, we should be able to solve this.
 */

const getCombinations = require("../JSUtils/getCombinations");
const getPermutationCount = require("../JSUtils/getPermutationCount");

const problem115 = () => {
    console.time();

    const minLength = 50;
    let total = 0;
    let units = minLength;

    while (total < 1000000) {
        units++;
        const mToN = [...Array(units - minLength + 1)].map((_, i) => i + minLength);

        const combinations = getCombinations(units, [...mToN, 1])
            .filter(combination => !(combination.length > 1 && combination.every(n => n !== 1)))
            .map(combination => {
                const numberOfRedTiles = combination.reduce((red, n) => n > 1 ? red + 1 : red, 0);
                const numberOfOnes = combination.length - numberOfRedTiles;
                const shouldRemove = numberOfRedTiles - 1;
                if (shouldRemove > numberOfOnes) return null;
                return combination.slice(0, combination.length - shouldRemove);
            }).filter(Boolean);
        total = combinations.reduce((permutations, combination) => permutations + getPermutationCount(combination), 0)
    }
    console.log(units, total)
    console.timeEnd();
}

problem115();

/*
168 1053389
default: 127.837ms
*/
