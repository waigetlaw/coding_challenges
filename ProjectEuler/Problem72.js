/**
 * Consider the fraction, n/d, where n and d are positive integers. If n<d and HCF(n,d)=1, it is called a reduced proper fraction.
 *
 * If we list the set of reduced proper fractions for d ≤ 8 in ascending order of size, we get:
 *
 * 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
 *
 * It can be seen that there are 21 elements in this set.
 *
 * How many elements would be contained in the set of reduced proper fractions for d ≤ 1,000,000?
 */

// I believe we just have to loop in order and +1 if no common divisor.

// update firstly, I believe the proper fractions for any given denominator is symmetrical, so we can half the calc
// secondly, I think by looping through tops, it will be exponentially faster:

// for each top, find all bots where bot > top, and bot cannot be divided by top or any of its factors.
// as top increases, since bot > top, the number of calcs will reduce.

// WE SHOULD REIMPLEMENT WITH LOOPING TOP - OTHER METHODS TOO SLOW.

const genPrimes = require('../JSUtils/genPrimes');

const primeFacts = {}; // prime factors of number and itself
const limit = 1000000;
let total = 0; // because all 1/x will be a proper fraction
let primeDivs = genPrimes(limit);

console.time();

// calc all not primes
const notPrimes = [];
let idx = 2;
for (let i = 4; i <= limit; i++) {
    if (i === primeDivs[idx]) {
        idx++;
        primeFacts[i] = [i]; // add prime factor of this prime to the mapping.
        continue;
    }
    notPrimes.push(i);
}

// only loop through non primes to get factors - this speeds it up a lot as primes would loop until end
for (i of notPrimes) {
    let primeFactors = [];
    let idx = 0;
    while (primeDivs[idx] <= i) {
        const d = primeDivs[idx++];
        if (i % d === 0) {
            primeFactors.push(d);
            if (primeFacts[i/d]) {  // we should already have the prime factors for the composite number so we just include them and end.
                primeFacts[i/d].map((p) => {
                    if (!primeFactors.includes(p)) primeFactors.push(p);
                });
                break;
            }            
        }
    }
    primeFacts[i] = primeFactors;
}

// for every prime bot, we can add all tops from 1 up to but not including p since they cannot be simplified anymore
primeDivs.map((p) => total += p - 1);

// count all the tops for the bots that don't have the same factors
for (bot of notPrimes) {

    const facts = primeFacts[bot];  // all prime factors of bot

    // each factor removes 1/factor from the original total. but as we add factors, previous factors would have already removed it
    // e.g. for 15, factors are 3 and 5
    // for 3, it removes 5 numbers from the original 15 (15/3)
    // but for 5, it is not just 15/5 = 3, but we also have to multiply this by 2/3 because counting the previous factor of 3
    // every 3rd factor of 5 was already counted for by 3, so we should only count what wasn't already counted by 3, i.e. 2/3's of them
    // so as each factor goes on, we count 'what wasnt already counted' by multiplying by (factor - 1)/factor
    // by reducing from original by * (factor - 1)/factor, we are removing only what is left to count by the new factor.
    const numberOfTopsNotFactors = facts.reduce((no, f) => no * (f - 1)/f, bot);
    total += numberOfTopsNotFactors;
}

console.log(total); // 303963552391

console.timeEnd(); // final refactor -> default: 542.665ms

// however, too slow for my liking... default: 147627.225ms

// for (let top = 2; top < limit; top++) {
//     if (top % 2000 === 0) console.log(top);
 
//     for (let ibot = notPrimes.length - 1; ibot >= 0; ibot--) {
//         const bot = notPrimes[ibot];
//         if (bot <= top) {
//             break; // stop when bot is no longer greater than top
//         }
//         if (primeFacts[top].every((f) => bot % f !== 0)) {
//             total++;
//         }
//     }

// }

// const seive = {};

// const baseArr = new Array(limit).fill(1);
// for (p of primeDivs) {
//     seive[p] = baseArr.map((_, i) => (i + 1) % p);
// };

// console.log(seive)

// newest speed -------------
// for (let top = 2; top < limit; top++) {
//     if (top % 2000 === 0) console.log(top);
//     let countThisTop = 0;
//     let ans = [];

//     for (let bot = top + 1; bot <= limit; bot++) {
//         const topFacts = primeFacts[top];
//         if (topFacts.every((f) => bot % f !== 0)) {
//             countThisTop++;
//             total++;
//             ans.push(`${top}/${bot}`)
//         }
//     }

//     console.log(top, countThisTop, ans.join())
// }
// end newest speed ------------

// console.log([...fracs].join(), fracs.size)
// console.log(total);

/* comment out for new top loop sol
for (bot = 2; bot <= 1000000; bot++) {

    // new sol
    //=========

    if (bot % 2000 === 0) console.log(bot);
    if (isPrime(bot)) {
        total += (bot - 1);
        // let print = [];
        // for (let x = 1; x < bot; x++) {
        //     print.push(`${x}/${bot}`);
        // }
        // console.log(bot, print.join(', '))
        continue;
    }
    const botFactors = [];
    for (let d = 2; d <= Math.sqrt(bot); d++) {
        if (bot % d === 0) {
            botFactors.push(d);
            if (!botFactors.includes(bot / d)) {
                botFactors.push(bot / d);
            }
        }
    }

    const neededPF = primeFacts.slice(0, Math.floor((bot - 2) / 2));
    const count = neededPF.filter((pf) => {
        return !pf.primeFactors.some((p) => {
            return botFactors.includes(p);
        })
    });

    // const frac = [`1/${bot}`, ...count.map((c) => `${c.i}/${bot}`)];
    // console.log(bot, frac.length, frac.join(', '));
    total += (count.length + 1) * 2; // +1 as 1/bot is always a new addition


    // old sol
    // ==============================
    // for (top = 1; top < bot; top++) {
    //     // no common divisor if top is 1
    //     if (top === 1) {
    //         total++;
    //         continue;
    //     }
    //     // has common divisor?
    //     let hasCommonFactor = false;
    //     // if either prime, can't reduce fraction
    //     // need faster way to find divs
    //     for (div = 2; div <= top; div++) {
    //         if ((top % div === 0) && (bot % div === 0)) {
    //             hasCommonFactor = true;
    //             break;
    //         }
    //     }

    //     const divisorsTop = [top];
    //     let idx = 0;
    //     while (primeDivs[idx] <= Math.ceil(Math.sqrt(top))) {
    //         const div = primeDivs[idx++];
    //         if (top % div === 0) {
    //             divisorsTop.push(div);
    //             divisorsTop.push(top / div);
    //         }
    //     }
    //     // console.log(top, divisorsTop.join())
    //     for (let divisor of divisorsTop) {
    //         if (bot % divisor === 0) {
    //             hasCommonFactor = true;
    //             break;
    //         }
    //     };

    //     // if (!hasCommonFactor) ans.push(`${top}/${bot}`);
    //     if (!hasCommonFactor) total++;
    // }
}

// console.log(ans.join());
console.log(total);
*/


// to see if correct or not.

// with newest factoring method: 5000 is 7656909 // this is bugged 6/9 is included
// with slowest divs: 5000 is 7598626 ??? (this is prob more accurate)

// 100 (buggy?) is 3043
// old sol 100 = 2994

// old 25 = 187
// new 25 = 199 (will verify)

// new seems correct...

// 5000 with new seemingly correct is 7600586
// halfing seems to give wrong answer for 5000....
// 5000 with looping top is 7600914

// looping top, for 50000: takes probably about a minute
// 760015953

// same ans with new loop for 50000 of 760015953
// just loop through all facts of top and if bot doesnt divide evenly we ++
// 21486.405ms

// old method is check top facts !include botfacts
// default 42225.798ms

// bug found! I used genPrimes(1000) when should be limit
// now for 50,000 its 759924263
// default: 23175.494ms


// 5000 is showing 7600457 for new masking method with genprimes fixed...

// crude method for 5000 using a set gives 7600457 in 6045ms
// with crude f.map bot % !0, 5000 gives 7600457 in 259ms


/*

results up to 30 iterating top.

top | valid bots > top | fracs

2 14 '2/3,2/5,2/7,2/9,2/11,2/13,2/15,2/17,2/19,2/21,2/23,2/25,2/27,2/29'
3 18 '3/4,3/5,3/7,3/8,3/10,3/11,3/13,3/14,3/16,3/17,3/19,3/20,3/22,3/23,3/25,3/26,3/28,3/29'
4 13 '4/5,4/7,4/9,4/11,4/13,4/15,4/17,4/19,4/21,4/23,4/25,4/27,4/29'
5 20 '5/6,5/7,5/8,5/9,5/11,5/12,5/13,5/14,5/16,5/17,5/18,5/19,5/21,5/22,5/23,5/24,5/26,5/27,5/28,5/29'
6 8 '6/7,6/11,6/13,6/17,6/19,6/23,6/25,6/29'
7 20 '7/8,7/9,7/10,7/11,7/12,7/13,7/15,7/16,7/17,7/18,7/19,7/20,7/22,7/23,7/24,7/25,7/26,7/27,7/29,7/30'
8 11 '8/9,8/11,8/13,8/15,8/17,8/19,8/21,8/23,8/25,8/27,8/29'
9 14 '9/10,9/11,9/13,9/14,9/16,9/17,9/19,9/20,9/22,9/23,9/25,9/26,9/28,9/29'
10 8 '10/11,10/13,10/17,10/19,10/21,10/23,10/27,10/29'
11 18 '11/12,11/13,11/14,11/15,11/16,11/17,11/18,11/19,11/20,11/21,11/23,11/24,11/25,11/26,11/27,11/28,11/29,11/30'
12 6 '12/13,12/17,12/19,12/23,12/25,12/29'
13 16 '13/14,13/15,13/16,13/17,13/18,13/19,13/20,13/21,13/22,13/23,13/24,13/25,13/27,13/28,13/29,13/30'
14 7 '14/15,14/17,14/19,14/23,14/25,14/27,14/29'
15 8 '15/16,15/17,15/19,15/22,15/23,15/26,15/28,15/29'
16 7 '16/17,16/19,16/21,16/23,16/25,16/27,16/29'
17 13 '17/18,17/19,17/20,17/21,17/22,17/23,17/24,17/25,17/26,17/27,17/28,17/29,17/30'
18 4 '18/19,18/23,18/25,18/29'
19 11 '19/20,19/21,19/22,19/23,19/24,19/25,19/26,19/27,19/28,19/29,19/30'
20 4 '20/21,20/23,20/27,20/29'
21 5 '21/22,21/23,21/25,21/26,21/29'
22 4 '22/23,22/25,22/27,22/29'
23 7 '23/24,23/25,23/26,23/27,23/28,23/29,23/30'
24 2 '24/25,24/29'
25 4 '25/26,25/27,25/28,25/29'
26 2 '26/27,26/29'
27 2 '27/28,27/29'
28 1 '28/29'
29 1 '29/30'
*/

// 1/30,7/30,11/30,13/30,17/30,19/30,23/30,29/30
// removed: 2 3 4 5 6 8 9 10 12 14 15 16 18 20 21 22 24 25 26 27 28

// 2 removes 15 from 30
// 3 would have removed 10, but every 2nd is counted already so only 5
// 5 would have removed 6, but every 2nd and 3rd is already counted, so just 1 why is 5 stil 2 times??
// but every 6 is double counted so we need to add 1 back... ?? I think add 1 back for every 2 factors already counted
// 15 + 5 + 1 = 21 removed
// should be 9 remaining....

/*  removed by
1
2   2
3       3
4   2
5           5
6   2
7
8   2
9       3
10  2
11
12  2
13
14  2
15      3
16  2
17
18  2
19
20  2
21      3
22  2
23
24  2
25          5
26  2
27      3
28  2
29
30  2
*/

/*  removed by 3, 7
1
2  
3   3
4   
5   
6   3
7       7
8   
9   3
10  
11
12  3
13
14      7
15  3
16  
17
18  3
19
20  
21  3
*/

// so 3 removes 7 as expected
// 7 would have removed 3, but every 3rd is counted so count (21/7) -= (21/7)/3
// so for 30, 5 problem
// 5 would have removed 6
// but every 2nd is lost so 6 becomes 3
// then every 3rd is lost so what remains is 2/3 * 3
// so what we should do is work out for x
// then for every previous factor, mult by the "remains" which is 1/2, 2/3, 4/5 etc.

/*
for 15:

should be 8
3 removes 5
5 should remove 3
but every 3rd counted, so 3 * 2/3 = 2
so removes 7 total
15 - 7 = 8, correct.



// 5000 is showing 7600457 for new masking method with genprimes fixed...

// crude method for 5000 using a set gives 7600457 in 6045ms
// with crude f.map bot % !0, 5000 gives 7600457 in 259ms

// now 5000 is 
7600457
default: 23.601ms
with no loop top method, only count what numbers remain after calcing what the factors remove (without prime bots removed as all inc)

with primes around 18~19ms

// from before::
// now for 50,000 its 759924263
// default: 23175.494ms

now with factors removal method and non-prime bot loop
759924263
default: 603.462ms
*/

// what was slow was calculating prime factors of numbers from 1-1,000,000:

// I removed primes from calc as they would loop until end
// and for every factor, I would find the 'other' number it is multiplying by and just add its factors to the list
// e.g. for 30
// I can divide by 2 so I add 2 as prime fact, but then I check what the prime factors of 15 are (30/2)
// 15's prime factors are previously calculated (as I am onto 30 now) which are 3, 5
// so I add 3, 5 to 30's prime factors [2, 3, 5] and I am now done.
// this sped up the program tremendously as this was what was taking the longest.