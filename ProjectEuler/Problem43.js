/**
 * The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, but it also has a rather interesting sub-string divisibility property.

Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:

d2d3d4=406 is divisible by 2
d3d4d5=063 is divisible by 3
d4d5d6=635 is divisible by 5
d5d6d7=357 is divisible by 7
d6d7d8=572 is divisible by 11
d7d8d9=728 is divisible by 13
d8d9d10=289 is divisible by 17
Find the sum of all 0 to 9 pandigital numbers with this property. 
 */

// stealing code from problem 41 to find permutations of pandigital number

const factorial = require('../JSUtils/factorial');

let loop = 0; // starting 0
let found = false;

const divisiblePandigitals = [];

const isDivisible = (s) => {
    const div = [2, 3, 5, 7, 11, 13, 17];
    for (let i = 0; i < div.length; i++) {
        const d = s.substr(i + 1, 3);
        if (+d % div[i] !== 0) return false;
    }
    return true;
}

while (!found) {
    let lexPos = loop;
    nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const ending = nums.reverse().join('');
    nums.reverse();

    let i = nums.length - 1;
    const vals = [];

    while (lexPos > 0) {
        const f = factorial(i);
        const times = Math.floor(lexPos / f);
        vals.push(times);
        lexPos = lexPos % f;
        i--;
    }

    const arr = nums.map((n, i) => {
        return vals[i] || 0;
    });

    let ans = '';

    arr.map((n) => {
        ans += nums[n];
        nums.splice(n, 1);
    });

    if (isDivisible(ans)) divisiblePandigitals.push(ans);

    if (ans === ending) { // went through all combinations
        break;
    }
    loop++;
}

console.log(divisiblePandigitals.join());
console.log(divisiblePandigitals.reduce((sum, n) => sum + +n, 0)); // 16695334890
