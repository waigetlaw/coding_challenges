/**
 * It is possible to write ten as the sum of primes in exactly five different ways:

7 + 3
5 + 5
5 + 3 + 2
3 + 3 + 2 + 2
2 + 2 + 2 + 2 + 2

What is the first value which can be written as the sum of primes in over five thousand different ways?
 */

// Taking code from Problem 31 which calculates how many different ways to sum to £2, we can create an array of primes
// and work out how many different ways there are to sum a number n until there are over 5000 ways.

const isPrime = require('../JSUtils/isPrime');

const primes = [2];
for (let i = 3; i < 1000000; i += 2) {  // make primes to a certain amount we think needed
    if (isPrime(i)) primes.push(i);
}
primes.reverse();

// updated code from problem 31
let startingAmount = 0;
let possibleCombinations;
do {
    startingAmount++;
    possibleCombinations = 0;
    const coins = primes.filter((p) => p <= startingAmount);

    const reduce = (total, coins, index = 0) => {
        const coin = coins[index++]; // move index to next coin for next iteration
        if (total === 0) return possibleCombinations++; // this is a unique route that has summed to total
        if (index >= coins.length) {   // last coin left
            if (total % coin === 0) return possibleCombinations++; // if last coin can make it, we wil add one
            return;
        }
        if (coin > total) return reduce(total, coins, index); // coin is too big and needs to move on
        do {
            reduce(total, coins, index);
            total -= coin;
        } while (total >= 0)
    }

    reduce(startingAmount, coins);   // start the recursion
} while (possibleCombinations <= 5000);

console.log("Number:", startingAmount, "Possible Combinations:", possibleCombinations);  // Number: 71 Possible Combinations: 5007
