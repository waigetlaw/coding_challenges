/**
 * 145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
*/

// Find the limit it can get to, for 9! it is 362880
// For 7 digits, all 9's, the max sum it can make would be 9! * 7 = 2540160
// Any further would be impossible, as even if all digits are 9, 9! * 8 cannot become 8 digits long.

const factorial = require('../JSUtils/factorial');

const digitFactorial = [];

for (let i = 10; i <= 2540160; i++) {
    const sum = [...String(i)].map((c) => +c).reduce((sum, n) => sum + factorial(n), 0);
    if (sum === i) digitFactorial.push(i);
}

console.log(digitFactorial.reduce((sum, n) => sum + n, 0)); // 40730
