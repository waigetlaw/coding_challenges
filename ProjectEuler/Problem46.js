/**
 * It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + 2×12
15 = 7 + 2×22
21 = 3 + 2×32
25 = 7 + 2×32
27 = 19 + 2×22
33 = 31 + 2×12

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?
*/

// This is potentially bruteforcable

const isPrime = require('../JSUtils/isPrime');

for (let i = 9; i < 100000; i += 2) {
    if (isPrime(i)) {
        continue;
    }

    let possible = false;
    let n = 1;
    const limit = Math.ceil(Math.sqrt(Math.ceil(i / 2)));
    while (n <= limit) {
        const remainder = i - 2 * n * n;
        if (isPrime(remainder)) {
            possible = true;
            break;
        }
        n++;
    }
    if (!possible) {
        console.log(i); // 5777
        break;
    }
}