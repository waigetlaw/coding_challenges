const readFile = require('../JSUtils/readFile');
const path = require('path')

const problem67 = async () => {

    const filePath = path.join(__dirname, './files/0067_triangle.txt');
    const content = await readFile(filePath);

    let triangle = content.split('\n').filter(Boolean).map(row => row.split(' ').map(str => +str));

    for (let i = triangle.length - 2; i >= 0; i--) {
       for (let j = 0; j < triangle[i].length; j++) {
        triangle[i][j] += Math.max(triangle[i + 1][j], triangle[i + 1][j + 1])
       }

    }
    console.log(triangle[0][0]);
}

problem67();
