/*
Let r be the remainder when (a+1)^2 + (a-1)^2 is divided by a^2.

For example, if a = 7 and n = 3, then r = 42: 6^3 + 8^3 = 728 = 42 mod 49. And as n varies, so too will r, but for a = 7 it turns out that r_max = 42.

For 3 <= a <=1000, find Sum(r_max).

Definitely breaking brackets is binomial theorem, and the minus one cancels every 2nd term.
Each of the earlier term will be divisible by a^2 so we only need to consider the last 2 terms.

If n is even, the last term is always 2/a^2 so the mod is always 2 which is not the max.
If n is odd, the 2nd last binomial * 2 / a is always the frac. So we find binomial just before a and * a to get the mod.

e.g. a = 3, binomial means 2nd which is the first one, so 2/a is possible. therefore max mod is 6 (always 1 multiple lower than a^2)

So for any ODD a, max mod is always a^2 - a. Or a * (a - 1).

For EVEN a, it's a bit trickier. for example a = 4, max last frac cannot use 2 because 2x2 / 4 is divisible so mod would be 0.
We have to use 1/4 which makes 2/4 * a = 8.

a = 6 -> 4/6 -> 24 so that's a * (a - 2)
*/

const problem120 = () => {
    let sum_r_max = 0;
    for(let a = 3; a <= 1000; a++) {
        if (a % 2 === 0) {
            sum_r_max += a * (a - 2);
        } else {
            sum_r_max += a * (a - 1);
        }
    }
    console.log(sum_r_max)
}

problem120();
