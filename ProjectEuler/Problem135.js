/*
Given the positive integers, x, y, and z, are consecutive terms of an arithmetic progression, the least value of the positive integer, n, for which the equation, x^2 - y^2 - z^2 = n, has exactly two solutions is n = 27:

34^2 - 27^2 - 20^2 = 12^2 - 9^2 - 6^2 = 27

It turns out that n = 1155 is the least value which has exactly ten solutions.

How many values of n less than one million have exactly ten distinct solutions?

Thoughts:

consider a as the arithmetic diff:
x = x, y = x - a, z = x - 2a

x^2 - (x - a)^2 - (x - 2a)^2 = n

solves to:

x = (-6a +- sqrt(16a^2 - 4n))/-2

We need to find, for each n, all values of a that gives x an integer value.

we get to sqrt(16a^2 - 4n) needs to be int, therefore:
sqrt(4(4a^2 - n)) = int -> we know sqrt4 = 2 so...
sqrt(4a^2 - n) = int.

4a^2 - n = N^2
4a^2 - N^2 = n
(2a - N)(2a + N) = n

we can perhaps find all values a, N where a <= n < 1000000

(2a - N) would be 1 at smallest, so then (2a + N), a = limit/2 is going to reach the full range.

testing original 4a^2 - n, where n = 27:

a = 3, a = 7 should be solutions...

36 - 27 = 9 => N = 3;
196 - 27 = 169 => N = 13;

a = 3, N = 3
a = 7, N = 13

(2(7) - 13) * (2(7) + 13) = 1 * 27 = 27
(2(3) - 3) * (2(3) + 3) = 3 * 9 = 27

after we calculate a possible a/N combo, we should substitute it back to the original quadratic eqn and solve for x.
I think it's possible for the same a/N combo to produce more than 1 answer.

to get x:

N = sqrt(4a^2 - n)

x = (-2a +- 2*N)/-2

x = (2N - 2a)/-2 => a - N
x = (-2a -2N)/-2 => a + N
 */

const problem135 = () => {
    console.time();

    const limit = 10 ** 6;

    const n_track = {};
    for (let a = 1; a <= 1 + limit/2; a++) {
        if (a % 10000 === 0) console.log(a);
        for (let N = 0; N < 2 * a; N++) {
            const n = (2 * a - N) * (2 * a + N);
            if (n > limit) continue;
            // const x_1 = a - N;
            // const x_2 = a + N;
            // if (x_1 > 0) console.log(`${x_1 + 2*a}^2 - ${x_1 + a}^2 - ${x_1}^2 = ${n}`)
            // console.log(`${x_2 + 2*a}^2 - ${x_2 + a}^2 - ${x_2}^2 = ${n}`)
            if (!n_track[n]) {
                n_track[n] = N > 0 && a > N ? 2 : 1;
            } else {
                n_track[n] += N > 0 && a > N ? 2 : 1;
            }
        }
    }
    // console.log(n_track)
    console.log(Object.entries(n_track).filter(([n, freq]) => freq === 10))
    console.log(Object.entries(n_track).filter(([n, freq]) => freq === 10).length)
    console.timeEnd();
};

problem135();

/*
{
  '3': [ 1, 1 ],
  '7': [ 2, 3 ],
  '11': [ 3, 5 ],
  '12': [ 2, 2 ],
  '15': [ 2, 1, [ 4, 7 ] ],
  '19': [ 5, 9 ],
  '20': [ 3, 4 ],
  '23': [ 6, 11 ],
  '27': [ 3, 3, [ 7, 13 ] ],
  '28': [ 4, 6 ],
  '31': [ 8, 15 ],
  '32': [ 3, 2 ],
  '35': [ 3, 1, [ 9, 17 ] ],
  '36': [ 5, 8 ],
  '39': [ 4, 5, [ 10, 19 ] ],
  '43': [ 11, 21 ],
  '44': [ 6, 10 ],
  '47': [ 12, 23 ],
  '48': [ 4, 4 ]
}

4^2 - 3^2 - 2^2 = 3
5^2 - 3^2 - 1^2 = 15
7^2 - 5^2 - 3^2 = 15
8^2 - 6^2 - 4^2 = 12
9^2 - 7^2 - 5^2 = 7
12^2 - 9^2 - 6^2 = 27
13^2 - 10^2 - 7^2 = 20
14^2 - 11^2 - 8^2 = 11
18^2 - 14^2 - 10^2 = 28
19^2 - 15^2 - 11^2 = 15
24^2 - 19^2 - 14^2 = 19
29^2 - 23^2 - 17^2 = 23
34^2 - 27^2 - 20^2 = 27
{
  '3': 1,
  '7': 1,
  '11': 1,
  '12': 1,
  '15': 3,
  '19': 1,
  '20': 1,
  '23': 1,
  '27': 2,
  '28': 1
}

4994 // incorrect somehow... are numbers getting too large??
default: 6:35.423 (m:ss.mmm)

N starting from -limit, still wrong... also 1155 has disappeared
1217
default: 27:38.209 (m:ss.mmm)

N starting 0 is still wrong...
5006
default: 6:29.766 (m:ss.mmm)

N from 0 to 2a is still wrong...
5006
default: 6:36.659 (m:ss.mmm)

Realised I was double counting repeated roots, just had to add an extra check if N = 0 only add one...
4989
default: 6:37.687 (m:ss.mmm)
*/
