/*
We define the Matrix Sum of a matrix as the maximum possible sum of matrix elements such that none of the selected elements share the same row or column.

For example, the Matrix Sum of the matrix below equals 3315 ( = 863 + 383 + 343 + 959 + 767):

  7  53 183 439 863
497 383 563  79 973
287  63 343 169 583
627 343 773 959 943
767 473 103 699 303

Find the Matrix Sum of:

(see website): https://projecteuler.net/problem=345
*/

const testArr = [
    [  7,  53, 183, 439, 863],
    [497, 383, 563,  79, 973],
    [287,  63, 343, 169, 583],
    [627, 343, 773, 959, 943],
    [767, 473, 103, 699, 303]
];

const smallerArr = [
    [863,497,383,563,79,973,287,63,343,169,583],
    [943,767,473,103,699,303,957,703,583,639,913],
    [23,487,463,993,119,883,327,493,423,159,743],
    [853,407,103,983,89,463,290,516,212,462,350],
    [300,780,486,502,912,800,250,346,172,812,350],
    [593,473,915,45,989,873,823,965,425,329,803],
    [133,673,665,235,509,613,673,815,165,992,326],
    [286,255,941,541,265,323,925,281,601,95,973],
    [473,65,511,164,138,672,18,428,154,448,848],
    [798,104,566,520,302,248,694,976,430,392,198],
    [631,101,969,613,840,740,778,458,284,760,390]
];

const realArr = [
    [7,53,183,439,863,497,383,563,79,973,287,63,343,169,583],
    [627,343,773,959,943,767,473,103,699,303,957,703,583,639,913],
    [447,283,463,29,23,487,463,993,119,883,327,493,423,159,743],
    [217,623,3,399,853,407,103,983,89,463,290,516,212,462,350],
    [960,376,682,962,300,780,486,502,912,800,250,346,172,812,350],
    [870,456,192,162,593,473,915,45,989,873,823,965,425,329,803],
    [973,965,905,919,133,673,665,235,509,613,673,815,165,992,326],
    [322,148,972,962,286,255,941,541,265,323,925,281,601,95,973],
    [445,721,11,525,473,65,511,164,138,672,18,428,154,448,848],
    [414,456,310,312,798,104,566,520,302,248,694,976,430,392,198],
    [184,829,373,181,631,101,969,613,840,740,778,458,284,760,390],
    [821,461,843,513,17,901,711,993,293,157,274,94,192,156,574],
    [34,124,4,878,450,476,712,914,838,669,875,299,823,329,699],
    [815,559,813,459,522,788,168,586,966,232,308,833,251,631,107],
    [813,883,451,509,615,77,281,613,459,205,380,274,302,35,805]
]

let tracker = 0;
console.time();
const problem345 = (arr) => {
    if (arr.length === smallerArr.length - 1) console.log(tracker++);
    if (arr.length === 1) return arr[0][0];
    const f_row = arr[0];
    const lowest = f_row.reduce((min, n) => Math.min(n, min), f_row[0]);
    const lowestIndex = f_row.indexOf(lowest);
    // console.log(f_row, lowest, lowestIndex);
    let rest = arr.slice(1);
    let poss = f_row.map((n, i) => {
        
        if (i === lowestIndex) {
            const { maxInCol, index } = rest.reduce((acc, n, index) => ({ maxInCol: Math.max(acc.maxInCol, n[lowestIndex]), index}), { maxInCol: rest[0][lowestIndex], index: 0 })
            // console.log(maxInCol, index)
            const maxInMaxCol = rest[index].reduce((max, n) => n === maxInCol ? max : Math.max(max, n), 0);
            const maxPossWithMin = lowest + maxInMaxCol;


            // console.log({maxInCol, row: rest[index], maxInMaxCol, maxPossWithMin })
            for (let ii = 0; ii < rest[index].length; ii++) {
                if (ii === lowestIndex) continue;
                // console.log('in loop', { maxInCol, n: f_row[ii], sum: maxInCol + f_row[ii] })
                if (maxInCol + f_row[ii] > maxPossWithMin) {
                    // console.log('skipping', { lowest, lowestIndex, maxInCol, maxPossWithMin, ii, higherSum: maxInCol + f_row[ii]})
                    return undefined
                }
            }
        }

        return rest.map((row) => {
            return row.map((m, j) => {
                if (i === j) return;
                return j === 0 ? m + n : m
            }).filter(Boolean)
        })
    })
    // console.log('POSS', poss)
    const ans = Math.max(...poss.filter(Boolean).map(possibility => problem345(possibility)));
    return ans;
}

console.log(problem345(smallerArr));

console.timeEnd();

/*
smallerArr
9493
default: 6.068s
*/

/* With skipping if there is a better swap with smallest

9493
default: 2.137s
*/
