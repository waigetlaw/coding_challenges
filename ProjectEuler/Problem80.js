/*
It is well known that if the square root of a natural number is not an integer, then it is irrational. The decimal expansion of such square roots is infinite without any repeating pattern at all.

The square root of two is 1.41421356237309504880..., and the digital sum of the first one hundred decimal digits is 475.

For the first one hundred natural numbers, find the total of the digital sums of the first one hundred decimal digits for all the irrational square roots.
*/

const Big = require('big-js');
Big.DP = 105; // requires a slight bit higher precision than 100 to get all digits.

const problem80 = () => {
    console.time();
    let total = 0;
    for (let i = 2; i < 100; i++) {
        const sqrt = Big(i).sqrt();
        const [whole, decimal] = sqrt.toString().split('.');
        if (!decimal) continue;
        const hundredDecimals = decimal.split('').slice(0, 99);
        total += hundredDecimals.reduce((total, d) => total + +d, +whole);
    }
   
    console.log(total);
    console.timeEnd();
};

problem80();

/*
40886
default: 166.365ms
*/
