/*
The primes 
3, 7, 109, and 673, are quite remarkable. By taking any two primes and concatenating them in any order the result will always be prime. For example, taking 7 and 
109, both 7109 and 1097 are prime. The sum of these four primes, 792, represents the lowest sum for a set of four primes with this property.

Find the lowest sum for a set of five primes for which any two primes concatenate to produce another prime.

V1. Just testing brute forcing this by setting some range of primes and testing every combination.
*/

const genPrimes = require('../JSUtils/genPrimes');
const isPrime = require('../JSUtils/isPrime');

const isPairPrime = (p1, p2) => isPrime(+`${p1}${p2}`) && isPrime(+`${p2}${p1}`)

const problem60 = () => {
    console.time()

    const primes = genPrimes(10000).filter(p => (p !== 2 && p !== 5))
    let set;
    for (let p1 = 0; p1 < primes.length; p1++) {
        console.log(p1, primes.length, set)

        const prime1 = primes[p1];
        if (set && set.total < prime1) break;
        for (let p2 = p1 + 1; p2 < primes.length; p2++) {

            const prime2 = primes[p2];

            if (set && set.total < prime1 + prime2) break;
            if (!isPairPrime(prime1, prime2)) continue;
            for (let p3 = p2 + 1; p3 < primes.length; p3++) {

                const prime3 = primes[p3];

                if (set && set.total < prime1 + prime2 + prime3) break;
                if (!isPairPrime(prime1, prime3)) continue;
                if (!isPairPrime(prime2, prime3)) continue;
                for (let p4 = p3 + 1; p4 < primes.length; p4++) {

                    const prime4 = primes[p4];

                    if (set && set.total < prime1 + prime2 + prime3 + prime4) break;
                    if (!isPairPrime(prime1, prime4)) continue;
                    if (!isPairPrime(prime2, prime4)) continue;
                    if (!isPairPrime(prime3, prime4)) continue;
                    for (let p5 = p4 + 1; p5 < primes.length; p5++) {

                        const prime5 = primes[p5];
                        if (set && set.total < prime1 + prime2 + prime3 + prime4 + prime5) break;
                        if (!isPairPrime(prime1, prime5)) continue;
                        if (!isPairPrime(prime2, prime5)) continue;
                        if (!isPairPrime(prime3, prime5)) continue;
                        if (!isPairPrime(prime4, prime5)) continue;
                        const primeSet = [
                            prime1,
                            prime2,
                            prime3,
                            prime4,
                            prime5
                        ];
                        set = { set: primeSet, total: primeSet.reduce((s, p) => s + p, 0) };
                    }
                }
            }
        }
    }
    console.log(set);
    console.timeEnd();
}

problem60();

// .filter((t={}, a=> !(t[a]=a in t)));

/*
for just limit = 1000 and set of 4
[ [ 3, 7, 109, 673 ], [ 23, 311, 677, 827 ] ]
2
default: 12.878s
not looking good. But lets add a limit to the size after a solution is found.

With a break it's so fast but depends on when first solution is
{ set: [ 3, 7, 109, 673 ], total: 792 }
default: 462.666ms

lets try modifying this for set of 5 now... too slow.

v2. instead of generating all pairs and then checking prime lets change the function to allPairsArePrimes so that it can cancel early
{ set: [ 3, 7, 109, 673 ], total: 792 }
default: 119.545ms
good improvement!

v3 checking for bad pairs seems like a good idea to skip huge portions but the looping probably needs to change.

v3 with early check breaks at each inner for loop
for example, if prime1, prime2 pair already fails, we continue to next loop
also if prime1 + prime2 is already larger than the set, break
{ set: [ 3, 7, 109, 673 ], total: 792 }
default: 12.768ms

with limit = 10000
{ set: [ 13, 5197, 5701, 6733, 8389 ], total: 26033 }
default: 5.223s
*/
