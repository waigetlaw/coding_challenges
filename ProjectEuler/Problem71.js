/**
 * Consider the fraction, n/d, where n and d are positive integers. If n<d and HCF(n,d)=1, it is called a reduced proper fraction.
 *
 * If we list the set of reduced proper fractions for d ≤ 8 in ascending order of size, we get:
 *
 * 1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8
 *
 * It can be seen that 2/5 is the fraction immediately to the left of 3/7.
 *
 * By listing the set of reduced proper fractions for d ≤ 1,000,000 in ascending order of size, find the numerator of the fraction immediately to the left of 3/7.
 */

// So we need to find the fraction that is just below 3/7 and not above. brute forceable.

const limit = 3 / 7;
let closest = { val: 0 };
for (let den = 1; den <= 1000000; den++) {
    let top = Math.floor(den * closest.val);
    let val;
    while ((top / den) < limit) {
        val = top / den;
        if (val > closest.val) {
            closest = { val, top, den };
        }
        top++;
    }
}

console.log(closest); // { val: 0.42857128571385716, top: 428570, den: 999997 }
