const isPrime = require('../JSUtils/isPrime');
const primes = [2];
const pLimit = 50000;   // we should only need to check up to around 50,000 as any higher 
                        // would cause the sum to hit over 1million within 21~ terms already
                        // and we already know for < 1000, there is already a 21 term chain
const limit = 1000000;

for (let i = 3; i < pLimit; i += 2) {
    if (isPrime(i)) {
        primes.push(i);
    }
}

let maxConseecutive = 0;
let found = false;

for (let j = primes.length; j > 0; j--) {
    for (let k = 0; k <= primes.length - j; k++) {
        const arr = primes.slice(k, (j + k));
        const sum = arr.reduce((sum, p) => sum + p, 0);
        if (sum < limit && isPrime(sum)) {
            maxConseecutive = sum;
            found = true;
            break;
        }
        if (sum > limit) break; // any more slices of the array will just produce a higher sum (huge performance gain)
    }
    if (found) break;
}

console.log(maxConseecutive); // 997651
