/**
 * 
 * @param { The total that the combination must add up to } total 
 * @param { An array of numbers which can be used to attempt to add up to the total } arr 
 * @returns { An array of all possible ways to add up to the total }
 */
const getCombinations = (total, arr) => {
    let possibleCombinations = [];

    const reduce = (total, arr, currentCombination = [], index = 0) => {
        const item = arr[index++]; // move index to next item for next iteration
        if (total === 0) {
            possibleCombinations.push(currentCombination); // this is a unique route that has summed to total
            return;
        }
        if (index >= arr.length) {   // last item left
            if (total % item === 0) {
                possibleCombinations.push([...currentCombination, ...Array(total/item).fill(item)]); // if last item can make it, we wil add one
                return;
            }
            return;
        }
        if (item > total) return reduce(total, arr, currentCombination, index); // item is too big and needs to move on
        do {
            reduce(total, arr, Array.from(currentCombination), index);
            total -= item;
            currentCombination.push(item);
        } while (total >= 0)
    }

    reduce(total, arr);
    return possibleCombinations;
}

module.exports = getCombinations;
