/**
 * Generates all permutations of an array of items in lexographical order
 * according to order of items
 */

const factorial = require('./factorial');

const genPermutation = (items) => {
    let loop = 0;
    const permutations = [];

    while (true) {
        let lexPos = loop++;
        let nums = [...items];

        let i = nums.length - 1;
        const vals = [];
        while (lexPos > 0) {
            const f = factorial(i--);
            const times = (lexPos / f) >> 0;
            vals.push(times);
            lexPos %= f;
        }

        if (vals[0] >= nums.length) break;  // end of lexographical order

        let ans = '';

        for (let j = 0; j < items.length; j++) {
            const pos = vals[j] || 0;
            ans += nums[pos];
            nums = nums.filter((_, idx) => idx != pos);
        }

        permutations.push(ans);
    }
    return permutations;
}

module.exports = genPermutation;
