/**
 * Using Sieve of Eratosthenes Method
 * https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 */

/**
 * Generates an array of primes from 2 to n, including n
 * @param {range to end at, including this} n
 */
const genPrimes = (n) => {
    const lastNumber = n % 2 === 0 ? n - 1 : n;
    const arrSize = Math.ceil(n / 2) - 1;
    let sieve = Array(arrSize).fill(0);
    sieve = sieve.map((_, i) => i * 2 + 3);
    let idx = 0;
    let current = sieve[idx];
    while (current ** 2 <= lastNumber) {
        const idxSquare = idx + (current * (current - 1)) / 2;
        for (let i = idxSquare; i < arrSize; i += current) {
            sieve[i] = null;    // marking every nth as not prime starting from p^2
        }
        for (let i = idx + 1; i < arrSize; i++) {   // get next unmarked in sieve
            const next = sieve[i];
            if (next) {
                current = next;
                idx = i;
                break;
            }
        }
    }

    sieve = sieve.filter(n => n);
    sieve.unshift(2);
    return sieve;
}

module.exports = genPrimes;
