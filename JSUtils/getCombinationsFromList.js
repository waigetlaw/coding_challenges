/**
 * 
 * @param { list of numbers to generate combinations from  - can contain duplicates } arr 
 * @param { number of items to select from the list } length 
 * @returns { An array of all combinations }
 */
const getCombinationsFromList = (arr, length) => {
    if (arr.length < length) return [];
    arr.sort();
    const unique = [...new Set(arr)];
    let numberTracker = arr.reduce((tracker, n) => {
        if (tracker[n]) {
            tracker[n]++;
        } else {
            tracker[n] = 1
        }
        return tracker;
    }, {})

    const combination = arr.slice(0, length);

    const mapToString = (arr) => arr.reduce((str, n) => str + n, '');

    const allCombinations = [mapToString(combination)];

    let indexChecking = length - 1;
    while (true) {
        if (indexChecking < 0) break;
        const currentNumber = combination[indexChecking];
        let nextNumber = unique.find(n => n > currentNumber); // works because arr is sorted
        if (!nextNumber) {
            indexChecking--;
            continue;
        }

        // const currentNumberTracker = Object.entries(numberTracker).reduce((t, [k, v]) => ({ ...t, [k]: v }), {}); // clone it.
        const currentNumberTracker = { ...numberTracker };
        for (let i = 0; i < indexChecking; i++) {
            currentNumberTracker[combination[i]]--; // count everything before indexChecking
        }

        const requiredNumbers = length - indexChecking;
        const remainingNumbers = Object.entries(currentNumberTracker).reduce((total, [n, freq]) => {
            return n >= nextNumber ? total + freq : total;
        }, 0);

        // if not enough numbers to make the combination, move on.
        if (requiredNumbers > remainingNumbers) {
           indexChecking--;
           continue;
        }

        for (let i = indexChecking; i < length; i++) {
            const remainingNumbers = currentNumberTracker[nextNumber];
            if (!remainingNumbers) {
                nextNumber = unique[unique.indexOf(nextNumber) + 1];
            }

            combination[i] = nextNumber;
            currentNumberTracker[nextNumber]--;
        }
        allCombinations.push(mapToString(combination));
        indexChecking = length - 1; // reset back to end
    }

    return allCombinations;
}

module.exports = getCombinationsFromList;
