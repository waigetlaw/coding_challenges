/**
 * This function will calculate the Euler's Totient which is the number of positive numbers
 * less than or equal to n which are relatively prime to n
 * 
 * It can accept an array of primes to speed up the process or go through odd numbers instead.
 * The prime array is assumed to start from 2, i.e. index 0: 2, index 1: 3, ...
 */
const phi = (n, primes) => {
    let r = n; // this will be reduced as each factor is found.
    let total = n;

    // check 2 first so we can iterate only odd numbers after
    if (r % 2 === 0) {
        total *= 1 / 2;
        do {
            r /= 2;
        } while (r % 2 === 0)
    }

    let i = 1;
    let div = 1;
    let isPrime = true;
    let ranOutPrimes = false;
    do {
        while (!isPrime && r % div === 0) {
            r /= div;
        }
        isPrime = true;

        if (primes) {
            div = primes[i];
            if (!ranOutPrimes && !div) {
                i = (primes[i-1] - 1) / 2;
                ranOutPrimes = true;
            }
        }
        if (!primes || ranOutPrimes) {
            div = (i * 2) + 1;
        }

        if (r % div === 0) {
            isPrime = false;
            total *= (div - 1) / div;
        }
        i++;
    } while (r > 1 && div <= Math.sqrt(r));

    if (isPrime && r > 1) {
        total *= (r - 1) / r;
    }

    return total;
}

module.exports = phi;
