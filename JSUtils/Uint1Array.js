/**
 * Makes use of Uint8Array to create a Uint1Array with two methods:
 * set(index, value): sets true or false at given index
 * get(index): returns true or false at given index
 * @param {range to end at, including this} n
 */
class Uint1Array {
    constructor(length) {
        this.uint8Array = new Uint8Array(Math.ceil(length/8) + 1);
        this.length = this.uint8Array.length * 8;
    }

    set(index, value) {
        const uint8ArrayIndex = Math.floor(index / 8);
        const bitIndex = index % 8;
        const currentBitValue = !!(this.uint8Array[uint8ArrayIndex] & (2 ** bitIndex))

        // want it true but currently false
        if (value && !currentBitValue) {
            this.uint8Array[uint8ArrayIndex] += 2 ** bitIndex;
        // want it false but currently true
        } else if (!value && currentBitValue) {
            this.uint8Array[uint8ArrayIndex] -= 2 ** bitIndex;
        }
    }

    get(index) {
        const uint8ArrayIndex = Math.floor(index / 8);
        const bitIndex = index % 8;
        return !!(this.uint8Array[uint8ArrayIndex] & (2 ** bitIndex))
    }
}

module.exports = Uint1Array;
