/**
 * 
 * @param { An array of numbers to use for permutations } arr 
 * @returns { The number of permutations from the given array }
 */
const getPermutationCount = (arr) => {
    const frequency = arr.reduce((frequency, n) => {
        if (!frequency[n]) {
            frequency[n] = 1;
        } else {
            frequency[n]++;
        }
        return frequency;
    }, {});

    const nFactorialNumbers = [...Array(arr.length - 1)].map((_, i) => i + 2);
    const kFactorialsToDivide = Object.values(frequency).filter(n => n > 1).reduce((toDivide, frequency) => {
        return [...toDivide, ...[...Array(frequency - 1)].map((_, i) => i + 2)];
    }, []);

    for (const divide of kFactorialsToDivide) {
        for (let i = 0; i < nFactorialNumbers.length; i++) {
            if (nFactorialNumbers[i] % divide === 0) {
                nFactorialNumbers[i] /= divide;
                break;
            } else if (i === nFactorialNumbers.length - 1) {
                nFactorialNumbers[i] /= divide; // if last number, just divide anyway
            }
        }
    }
    return nFactorialNumbers.reduce((permutations, n) => permutations * n, 1);
}

module.exports = getPermutationCount;
