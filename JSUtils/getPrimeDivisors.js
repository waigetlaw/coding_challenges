/**
 * Generates an array of all prime divisors
 * 
 * TODO: increase efficiency
 */


const genPrimes = require('../JSUtils/genPrimes');

const getPrimeDivisors = (n) => {
    // const limit = Math.floor(Math.sqrt(n)) + 1;
    const primes = genPrimes(n);
    const divisors = [];
    for (const prime of primes) {
        if (n % prime === 0) {
            divisors.push(prime);
        }
    }
    return divisors.sort((a, b) => a - b)
}

module.exports = getPrimeDivisors;
