/**
 * Generates an array of strings for all distinct rotations of input
 */

const genRotations = (s) => {
    const rot = [];
    const ds = s + s; // by repeating the string, we can rotate by moving "into" the next cycle with little computational effort
    for (let i = 0; i < s.length; i++) {
        rot.push(ds.substr(i, s.length));
    }
    return [...new Set(rot)];
}

module.exports = genRotations;
