/**
 * 
 * @param { The total that the combination must add up to } total 
 * @param { An array of numbers which can be used to attempt to add up to the total } arr 
 * @returns { The number of different ways we can use the array of numbers to add up to the given total }
 */
const getCombinationCount = (total, arr) => {
    let possibleCombinations = 0;

    const reduce = (total, arr, index = 0) => {
        const item = arr[index++]; // move index to next item for next iteration
        if (total === 0) return possibleCombinations++; // this is a unique route that has summed to total
        if (index >= arr.length) {   // last item left
            if (total % item === 0) return possibleCombinations++; // if last item can make it, we wil add one
            return;
        }
        if (item > total) return reduce(total, arr, index); // item is too big and needs to move on
        do {
            reduce(total, arr, index);
            total -= item;
        } while (total >= 0)
    }

    reduce(total, arr);
    return possibleCombinations;
}

module.exports = getCombinationCount;
