/**
 * This function takes a string representation of two numbers
 * multiplies them together and returns the answer as a string
 * 
 * Note: only works for integer multiplication
 */

 const addNumber = require('./addNumber');

const multiplyNumber = (a, b) => {
    const parts = [];
    let units = 0;
    for (let bi = b.length - 1; bi >= 0; bi--) {
        let carry = 0;
        let tempAns = '';
        for (let ai = a.length - 1; ai >= 0; ai--) {
            const mult1 = String(+a[ai] * +b[bi] + carry).padStart(2, '0');
            carry = +mult1[0];
            tempAns = mult1[1] + tempAns;
        }
        if (carry) tempAns = carry + tempAns;
        parts.push(tempAns + '0'.repeat(units++));
    }

    return parts.reduce((sum, part) => addNumber(sum, part), '0');
}

module.exports = multiplyNumber;
