/**
 * 
 * @param { list of numbers to generate combinations from  - can contain duplicates but must be sorted in ascending order!! } arr 
 * @param { number of items to select from the list } length 
 * @returns { An array of all combinations }
 */
const getCombinationCountFromList = (arr, length) => {
    if (arr.length < length) return [];
    const unique = [...new Set(arr)];
    const mappedArr = arr.map(n => unique.indexOf(n))
    const maxNumber = unique.length - 1;
    
    let numberTracker = mappedArr.reduce((tracker, n) => {
        if (!tracker[n]) {
            tracker[n] = 1;
        } else {
            tracker[n]++;
        }
        return tracker;
    }, {})

    const combination = mappedArr.slice(0, length);
    let combinationCount = 1;

    let indexChecking = length - 1;
    while (true) {
        if (indexChecking < 0) break;
        const currentNumber = combination[indexChecking];
        let nextNumber = currentNumber + 1; // works because arr is sorted
        if (nextNumber > maxNumber) {
            indexChecking--;
            continue;
        }

        const currentNumberTracker = { ...numberTracker };
        for (let i = 0; i < indexChecking; i++) {
            currentNumberTracker[combination[i]]--; // count everything before indexChecking
        }

        const requiredNumbers = length - indexChecking;
        const remainingNumbers = Object.entries(currentNumberTracker).reduce((total, [n, freq]) => {
            return n >= nextNumber ? total + freq : total;
        }, 0);

        // if not enough numbers to make the combination, move on.
        if (requiredNumbers > remainingNumbers) {
           indexChecking--;
           continue;
        }

        for (let i = indexChecking; i < length; i++) {
            const remainingNumbers = currentNumberTracker[nextNumber];
            if (!remainingNumbers) {
                nextNumber++;
            }

            combination[i] = nextNumber;
            currentNumberTracker[nextNumber]--;
        }
        combinationCount++;
        indexChecking = length - 1; // reset back to end
    }

    return combinationCount;
}

module.exports = getCombinationCountFromList;
