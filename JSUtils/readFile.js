var fs = require('fs');

const readFile = (filePath) => {
    return new Promise((resolve) => {
        fs.readFile(filePath, { encoding: 'utf-8' }, function (err, data) {
            if (!err) {
                resolve(data);
            } else {
                throw Error(err);
            }
        });
    });
}

module.exports = readFile;
