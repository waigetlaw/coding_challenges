/**
 * Using Sieve of Eratosthenes Method
 * https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 */

/**
 * Generates an array of primes from 2 to n, including n
 * @param {range to end at, including this} n
 */
const genPrimesV2 = (limit) => {
    const n = limit + 1;
    const lastNumber = n % 2 === 0 ? n - 1 : n;
    const arrSize = Math.ceil(n / 2) - 1;
    let sieve = new Uint8Array(n)
    for (let i = 0; i < arrSize; i++) {
        sieve[i * 2 + 3] = 1;
    }

    let idx = 0;
    let current = idx * 2 + 3;
    while (current ** 2 <= lastNumber) {
        const idxSquare = idx + (current * (current - 1)) / 2;
        for (let i = idxSquare; i < arrSize; i += current) {
            sieve[i * 2 + 3] = 0;    // marking every nth as not prime starting from p^2
        }
        for (let i = idx + 1; i < arrSize; i++) {   // get next unmarked in sieve
            const next = sieve[i * 2 + 3];
            if (next) {
                current = i * 2 + 3;
                idx = i;
                break;
            }
        }
    }

    return sieve.reduce((primes, sieveElement, index) => {
        if (sieveElement) primes.push(index);
        return primes;
    }, [2]);
}

module.exports = genPrimesV2;
