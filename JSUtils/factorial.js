const factorial = (n) => {
    if (n < 0) throw Error('Cannot factorial negative');
    if (n === 1 || n === 0) return 1;
    return n * factorial(n - 1);
}

module.exports = factorial;
