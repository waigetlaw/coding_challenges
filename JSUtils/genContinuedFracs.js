/**
 * Generates continued fractions for a given number up to k limit.
 */


const Big = require('big-js');
Big.DP = 100;

const genContinuedFracs = (number, k = 100) => {
    let unit = number.round(0, 0);
    let decimals = number.minus(unit);
    const whole = unit;
    const continuedFracs = [];

    let i = 0;
    while (i < k && decimals.gt(0)) {
        number = Big(1).div(decimals);
        unit = number.round(0, 0);
        decimals = number.minus(unit);
        continuedFracs.push(Number(unit));
        i++;
    }
    return { whole, continuedFracs };
}

module.exports = genContinuedFracs;
