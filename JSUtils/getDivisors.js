/**
 * Generates an array of all divisors
 * 
 * TODO: increase efficiency
 */

const getDivisors = (n) => {
    const limit = Math.floor(Math.sqrt(n)) + 1;
    const divisors = [1];
    for (let div = 1; div < limit; div++) {
        if (n % div === 0) {
            divisors.push(div);
            divisors.push(n / div);
        }
    }
    return [...new Set(divisors)].sort((a, b) => a - b)
}

module.exports = getDivisors;
