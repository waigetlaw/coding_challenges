/**
 * Sometimes we want to add numbers that are too big
 * This function takes a string representation of the number
 * and returns the answer as a string
 * 
 * Note: only works for integer addition
 */

const addNumber = (a, b) => {
    const maxLength = Math.max(a.length, b.length);
    const aPadded = a.padStart(maxLength, '0');
    const bPadded = b.padStart(maxLength, '0');
    let ans = '';
    let carry = 0;
    for (let i = maxLength - 1; i >= 0; i--) {
        const sum = String(+aPadded[i] + +bPadded[i] + carry).padStart(2, '0');
        carry = +sum[0];
        ans = sum[1] + ans;
    }
    if (carry) ans = carry + ans;

    while (ans.startsWith('0') && ans.length > 1) {
        ans = ans.substring(1);
    }
    return ans
}

module.exports = addNumber;
