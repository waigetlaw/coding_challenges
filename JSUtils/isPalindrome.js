const isPalindrome = (s) => {
    if (s.length <= 1) return true;
    let s1 = s.substring(0, Math.floor(s.length / 2));
    let s2 = '';
    if (s.length % 2 === 0) {
        s2 = s.substring(Math.floor(s.length / 2));
    } else {
        s2 = s.substring(Math.ceil(s.length / 2));
    }
    return s1.split('').every((c, i) => c === s2.split('')[s1.length - i - 1]);
}

module.exports = isPalindrome;
