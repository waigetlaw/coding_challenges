/**
 * This function takes in takes two numbers as the numerator and denominator and returns the fraction back
 * in its simplest form as an object.
 * 
 * It also accepts an array of primes for faster calculations although not necessary.
 */

const simplifyFraction = (num, den, primes) => {
    let r = num; // this will be reduced as each factor is found.
    const primeFactors = [];

    // check 2 first so we can iterate only odd numbers after
    if (r % 2 === 0) {
        do {
            r /= 2;
            primeFactors.push(2);
        } while (r % 2 === 0)
    }

    let i = 1;
    let div = 1;
    let isPrime = true;
    let ranOutPrimes = false;
    do {
        while (!isPrime && r % div === 0) {
            r /= div;
            primeFactors.push(div);
        }
        isPrime = true;

        if (primes) {
            div = primes[i];
            if (!ranOutPrimes && !div) {
                i = (primes[i-1] - 1) / 2;
                ranOutPrimes = true;
            }
        }
        if (!primes || ranOutPrimes) {
            div = (i * 2) + 1;
        }

        if (r % div === 0) {
            isPrime = false;
        }
        i++;
    } while (r > 1 && div <= r); // can sqrt this??

    let biggestCommonFactor = 1;
    for (const primeFactor of primeFactors) {
        if (den % primeFactor === 0) {
            den /= primeFactor;
            biggestCommonFactor *= primeFactor;
        }
    }

    return { num: num/biggestCommonFactor, den, biggestCommonFactor };
}

module.exports = simplifyFraction;

console.log(simplifyFraction(240, 360));
