const isPrime = (n) => {
    if (n < 2) return false;
    if (n === 2) return true;
    if (n % 2 === 0) return false;
    const limit = Math.ceil(Math.sqrt(n));
    for (let divisor = 3; divisor <= limit; divisor += 2) {
        if (n % divisor === 0) return false;
    }
    return true;
}

module.exports = isPrime;
