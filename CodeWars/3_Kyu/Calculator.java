import java.util.ArrayList;
import java.util.List;

/*
    3 Kyu - https://www.codewars.com/kata/5235c913397cbf2508000048

    Create a simple calculator that given a string of operators (+ - * and /) and numbers separated by spaces returns the value of that expression

    Example:
    Calculator().evaluate("2 / 2 + 3 * 4 - 6") # => 7

    Remember about the order of operations! Multiplications and divisions have a higher priority and should be performed left-to-right. 
    Additions and subtractions have a lower priority and should also be performed left-to-right.
*/

public class Calculator {
  public static Double evaluate(String expression) {
while(true) {
            String[] exp = expression.split(" ");

            if (exp.length == 1) {
                return Double.parseDouble(expression);
            }

            List<Double> nums = new ArrayList<>();
            List<String> ops = new ArrayList<>();

            for (int i = 0; i < exp.length; i++) {
                if (i % 2 == 0) {
                    nums.add(Double.parseDouble(exp[i]));
                } else {
                    ops.add(exp[i]);
                }
            }

            Boolean hasMult = false;
            Boolean multiply = false;
            int mult = 0;
            for (int i = 0; i < ops.size(); i++) {
                if (ops.get(i).equals("*") || ops.get(i).equals("/")) {
                    hasMult = true;
                    mult = i;
                    if (ops.get(i).equals("*")) {
                        multiply = true;
                    } else {
                        multiply = false;
                    }
                    break;
                }
            }

            Double ans = 0.0;

            if (hasMult) {
                if (multiply) {
                    ans = nums.get(mult) * nums.get(mult + 1);
                } else {
                    ans = nums.get(mult) / nums.get(mult + 1);
                }
                ops.remove(mult);
                nums.set(mult, ans);
                nums.remove(mult + 1);
            } else {
                if (ops.get(0).equals("+")) {
                    ans = nums.get(mult) + nums.get(mult + 1);
                } else {
                    ans = nums.get(mult) - nums.get(mult + 1);
                }
                ops.remove(0);
                nums.set(0, ans);
                nums.remove(1);
            }

            expression = "";
            int countNums = 0;
            int countOps = 0;
            for (int i = 0; i < (nums.size() * 2) - 1; i++) {

                if (i % 2 == 0) {
                    expression += nums.get(countNums);
                    countNums++;
                } else {
                    System.out.println(ops.get(countOps));
                    expression += ops.get(countOps);
                    countOps++;
                }
                expression += " ";
            }

            expression = expression.trim();

            System.out.println("new " + expression);
            if(expression.split(" ").length==1){
                break;
            }

        }
        return round(Double.parseDouble(expression), 5);
    
  }
  
      public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}