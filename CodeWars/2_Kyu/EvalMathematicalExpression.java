import java.util.ArrayList;
import java.util.List;

/*
    2 Kyu - https://www.codewars.com/kata/52a78825cdfc2cfc87000005

    Given a mathematical expression as a string you must return the result as a number.

    Numbers
    Number may be both whole numbers and/or decimal numbers. The same goes for the returned result.

    Operators
    You need to support the following mathematical operators:

    Multiplication *
    Division / (as true division)
    Addition +
    Subtraction -
    Operators are always evaluated from left-to-right, and * and / must be evaluated before + and -.

    Parentheses
    You need to support multiple levels of nested parentheses, ex. (2 / (2 + 3.33) * 4) - -6

    Whitespace
    There may or may not be whitespace between numbers and operators.
*/

public class MathEvaluator {

  public double calculate(String expression) {
  expression = expression.replaceAll(" ","");

                boolean lastTime = false;
                boolean moreCalc = true;
                // start loop here? reset expression
                while(moreCalc) {
                    boolean moreBrackets = false;
                    int startbracket = expression.lastIndexOf('(');
                    String miniexp = expression;

                    int endbracket = 0;
                    if (startbracket >= 0) {
                        moreBrackets = true;
                        endbracket = expression.substring(startbracket).indexOf(')') + startbracket;
                        miniexp = expression.substring(startbracket + 1, endbracket);
                        System.out.println("mini exp = " + miniexp);
                    }

                    if(miniexp.length()>1){
                        if(miniexp.substring(0,2).equals("--")){
                            miniexp = miniexp.substring(2);
                        }
                    }

                    int miniOps = 0;
                    String checkOps = miniexp.replaceAll("[0-9.]+", "");
                    if (checkOps.length() > 1) {
                        miniOps = 1;
                    }else if(checkOps.length() == 1){
                        if(miniexp.charAt(0) !='-'){
                            miniOps = 1;
                        }
                    }

                    //Start here again after an operator has been calulated
                    while (miniOps > 0) {
                        List<Double> dnum = new ArrayList<Double>();



                        boolean firstNeg = false;
                        if (miniexp.charAt(0) == '-') {
                            miniexp = miniexp.substring(1);
                            firstNeg = true;
                        }
                        String[] nums = miniexp.split("[-+*/]+");

                        String[] operator = miniexp.replaceAll("[0-9.]+", " ").trim().split(" ");

                        List<String> ops = new ArrayList<String>();
                        for (int i = 0; i < operator.length; i++) {
                            if(operator[i].length()>2){
                                ops.add(operator[i].substring(0,1));
                            }else {
                                ops.add(operator[i]);
                            }

                        }


                        for (int i = 0; i < nums.length; i++) {
                            dnum.add(Double.parseDouble(nums[i]));
                        }
                        if (firstNeg) {
                            dnum.set(0, dnum.get(0) * -1);
                        }

                        int indexMult = -1;

                        for (int i = 0; i < ops.size(); i++) {
                            if (ops.get(i).charAt(0) == '*' || ops.get(i).charAt(0) == '/') {
                                indexMult = i;
                                break;
                            }
                        }

                        Double ans = 0.0;

                        int indexSum = 0;

                        if (indexMult > -1) {
                            indexSum = indexMult;
                            if (ops.get(indexMult).charAt(0) == '*') {
                                if (ops.get(indexMult).length() > 1) {
                                    //mult by negative 2nd
                                    ans = -dnum.get(indexMult) * dnum.get(indexMult + 1);
                                } else {
                                    //mult normal
                                    ans = dnum.get(indexMult) * dnum.get(indexMult + 1);
                                }
                            } else if (ops.get(indexMult).length() > 1) {
                                //divide by neg
                                ans = -dnum.get(indexMult) / dnum.get(indexMult + 1);
                            } else {
                                //divide normal
                                ans = dnum.get(indexMult) / dnum.get(indexMult + 1);
                            }

                            //end of mult/divide
                        } else if (ops.get(0).charAt(0) == '+') {
                            if (ops.get(0).length() > 1) {
                                //means +-
                                ans = dnum.get(0) - dnum.get(1);
                            } else {
                                //means just +
                                ans = dnum.get(0) + dnum.get(1);
                            }
                        } else if (ops.get(0).length() > 1) {
                            //means --
                            ans = dnum.get(0) + dnum.get(1);
                        } else {
                            ans = dnum.get(0) - dnum.get(1);
                        }

                        dnum.set(indexSum, ans);
                        dnum.remove(indexSum + 1);
                        ops.remove(indexSum);

                        miniOps = ops.size();

                        miniexp = "";

                        for (int i = 0; i < dnum.size(); i++) {
                            miniexp += dnum.get(i);
                            if (i + 1 < dnum.size()) {
                                miniexp += ops.get(i);
                            }
                        }

                        //get miniexp again

                        System.out.println("next mini exp " + miniexp);
                    }


                    if (startbracket >= 0) {
                        expression = expression.substring(0, startbracket) + miniexp + expression.substring(endbracket + 1);
                        System.out.println("new Expression " + expression);

                    }
                    else{
                        if(lastTime){
                            expression = miniexp;
                            break;
                        }
                        lastTime = true;
                    }


                }
                System.out.println("answer " + expression);
                return Double.parseDouble(expression);
  }

}