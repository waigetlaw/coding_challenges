import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* 
    2 Kyu - https://www.codewars.com/kata/582c1092306063791c000c00

    There is a infinite string. You can imagine it's a combination of numbers from 1 to n, like this:
    "123456789101112131415....n-2n-1n"

    Your task is complete function findPosition that accept a digital string num. Returns the position(index) of the digital string(the first appearance).

    For example:

    findPosition("456") == 3
    because "123456789101112131415".indexOf("456") = 3
*/

public class InfiniteDigitalString {
    
    public static long findPosition(final String s) {
        int length = s.length();
        int testDigits = 1;

        if (length == 1) {
            return Long.valueOf(s) - 1;
        }

        long startingDigit = 0;
        long digitFound = 0L;
        int index = -1;

        //start while testDigits < length, each loop testDigit++
        boolean found = false;
        while(!found){

      List<Long> check = new ArrayList<>();
        for (int i = 0; i < testDigits; i++) {
            if (i + testDigits <= length) {
                if(Long.valueOf(s.substring(i, i + testDigits))==0){
                    // for fixing 0 spams, we must add a 1 or the number after 0
                    if(i+testDigits<length){
                        check.add(Long.valueOf(s.charAt(i+testDigits) + s.substring(i, i + testDigits)));
                    }
                    else{
                        check.add(Long.valueOf("1" + s.substring(i, i + testDigits)));
                    }

                }else {
                    check.add(Long.valueOf(s.substring(i, i + testDigits)));
                }//this part is if 9100, [9] then [91][10]
                //then [910][100] then [9100]

                if(testDigits>1) {
                    for (int j = 0; j < testDigits - 1; j++) {
                            check.add(Long.valueOf(s.substring(j+1,j + 1 + (testDigits-j-1)) + s.substring(0,j+1)));
                            check.add(Long.valueOf((Long.valueOf(s.substring(j+1,j+2))-1) + s.substring(j+2,j + 1 + (testDigits-j-1)) + s.substring(0,j+1)));
                    }
                }
            }
        }
        Collections.sort(check);



        for (int i = 0; i < check.size(); i++) {    // check if indexOf in the string exists or not
            String toCheck = "";
            startingDigit = check.get(i) - 1;
            digitFound = startingDigit;
            while (toCheck.length() < length + testDigits * 2) {
                toCheck += startingDigit;
                startingDigit++;
            }
           // System.out.println("Comparing if string " + s + " is in: " + toCheck);
            index = toCheck.indexOf(s);
            if (index > 0) {
                found = true;
                break;
            }

        }

        testDigits++;
        }//end while
       // System.out.println(found);

        long bufferIndex = 0L;
        int lengthOfStartingDigit = String.valueOf(digitFound).length();
        long tens = 1;

        for (int i = 0; i < lengthOfStartingDigit-1; i++) {
            bufferIndex += (9*Math.pow(10,i))*(i+1); //wrong... need 10 for first digit, + 90 ish for 2nd, 900 for 3 etc
           // System.out.println(bufferIndex);
            tens *=10;
        }


       // System.out.println((digitFound-tens)*lengthOfStartingDigit + " + " + index);
        bufferIndex += (digitFound-tens)*lengthOfStartingDigit + index;

        return bufferIndex;
    }    
}