/*
    https://www.codewars.com/kata/52b7ed099cdc285c300001cd/train/javascript
        
    Write a function called sumIntervals/sum_intervals() that accepts an array of intervals, and returns the sum of all the interval lengths. Overlapping intervals should only be counted once.

    Intervals
    Intervals are represented by a pair of integers in the form of an array. The first value of the interval will always be less than the second value. Interval example: [1, 5] is an interval from 1 to 5. The length of this interval is 4.

    Overlapping Intervals
    List containing overlapping intervals:

    [
    [1,4],
    [7, 10],
    [3, 5]
    ]
    The sum of the lengths of these intervals is 7. Since [1, 4] and [3, 5] overlap, we can treat the interval as [1, 5], which has a length of 4.
*/

const sumIntervals = (intervals) => {
    let nonOverlap = [];

    for (let i = 0; i < intervals.length; i++) {
        let currentInterval = intervals[i];
        let changed;
        let reducedIntervals;
        let hasRemoved = false;
        do {
            changed = false;
            reducedIntervals = intervals.map((interval) => {
                // completely outside - therefore keep
                if (interval[1] < currentInterval[0] || interval[0] > currentInterval[1]) return interval;

                // change is required
                changed = true;
                currentInterval = [
                    Math.min(currentInterval[0], interval[0]),
                    Math.max(currentInterval[1], interval[1])
                ];
                return false;
            })
            if (reducedIntervals.some(x => !x)) hasRemoved = true;
            intervals = reducedIntervals.filter(Boolean);
        } while (changed);

    if (hasRemoved) i--;
    nonOverlap.push(currentInterval);
    }

    return nonOverlap.reduce((sum, int) => sum + (int[1] - int[0]), 0)
}

// console.log(sumIntervals([[1,4],[7, 10],[3, 5]]))
console.log(sumIntervals([ [ 1, 5 ], [ 6, 10 ] ]))
