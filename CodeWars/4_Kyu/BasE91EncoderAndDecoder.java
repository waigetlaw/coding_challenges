/*
    4 Kyu - https://www.codewars.com/kata/58a57c6bcebc069d7e0001fe

    BasE91 is a method for encoding binary as ASCII characters. It is more efficient than Base64 and needs 91 characters to represent the encoded data.

    The following ASCII charakters are used:

    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    '!#$%&()*+,./:;<=>?@[]^_`{|}~"'
    Create two functions that encode strings to basE91 string and decodes the other way round.

    b91encode('test') = 'fPNKd'
    b91decode('fPNKd') = 'test'

    b91decode('>OwJh>Io0Tv!8PE') = 'Hello World!'
    b91encode('Hello World!') = '>OwJh>Io0Tv!8PE'
    Input strings are valid.
*/

public class Base91 {
	public static String encode(String data) {
  String basE91 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&()*+,./:;<=>?@[]^_`{|}~\"";
 

        String checking = ">OwJh>Io0Tv!8PE";



        for (int i = 0; i < checking.length(); i++) {
            System.out.print(basE91.indexOf(checking.charAt(i)) + ", ");
        }

        System.out.println("Converting string: " + data + " to basE91.");
        String binary = "";


        for (int i = data.length()-1; i >=0  ; i--) {
            String oneChar = Integer.toBinaryString(data.charAt(i));
            String leadZeros = "";
            for (int j = 0; j < 8-oneChar.length(); j++) {
                leadZeros +="0";
            }
            binary += leadZeros + oneChar;
            System.out.println(leadZeros + oneChar);

        }



        if(!binary.contains("1")){
            return "A";
        }



        //split 13 bit or 14
        String encoded = "";

        int firstOne = binary.indexOf("1");
        int distance = binary.length() - firstOne;  //essentially a try-catch to make sure I don't parse something larger than 2^31-1
        if(distance<16) {
            int oneTry = Integer.parseInt(binary, 2);
            if (oneTry < 91 * 91 - 1) {
                char second = basE91.charAt(oneTry / 91);
                oneTry = oneTry % 91;

                char first = basE91.charAt(oneTry);

                encoded += first + "" + second;

                binary = "0";
            }
        }



        while(binary.length()>13) {



            char[] bit91 = new char[2];

            String bit = binary.substring(binary.length() - 14);
            int value = Integer.parseInt(bit, 2);


                bit = binary.substring(binary.length() - 13);
                value = Integer.parseInt(bit, 2);
                binary = binary.substring(0,binary.length() - 13);





            bit91[0] = basE91.charAt(value/91);
            value = value%91;

            bit91[1] = basE91.charAt(value);

            encoded += bit91[1] +""+ bit91[0];

        }

        System.out.println("end" + binary);

        if (!binary.contains("1")) {
            encoded +="A";
        }
        else{
            int lengthcheck = binary.length();
            binary = "00000000000000" + binary;

            int value = Integer.parseInt(binary, 2);

            encoded += basE91.charAt(value%91) +""+basE91.charAt(value/91);
            if(encoded.charAt(encoded.length()-1)=='A' && lengthcheck<8){
                encoded = encoded.substring(0,encoded.length()-1);
            }

        }

        System.out.println("length: " + encoded.length() + " answer: " +encoded);


        return encoded;
	}

	public static String decode(String data) {
		 String basE91 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&()*+,./:;<=>?@[]^_`{|}~\"";

        if(data.length()%2==1){
            if(data.charAt(data.length()-1)=='A'){
                data = data.substring(0,data.length()-1);
            }
            else{
                data +="A";
            }
        }

        // two blocks at a time back to binary

        String decoded = "";
        String binary = "";

        //0110001001100001

        while(data.length()>0){

            String packet = data.substring(data.length()-2);
            int value = basE91.indexOf(packet.charAt(1))*91 + basE91.indexOf(packet.charAt(0));
            String temp = Integer.toBinaryString(value);

            while(temp.length()<13){
                temp = "0" + temp;
            }
            binary += temp;

            data = data.substring(0, data.length()-2);
        }

        while(binary.length()%8!=0){
            binary = "0" + binary;
        }

        while(binary.contains("1")){
            String bit = binary.substring(binary.length()-8);
            decoded += (char)(Integer.parseInt(bit,2));
            binary = binary.substring(0,binary.length()-8);
        }

        return decoded; // do it!
	}

}