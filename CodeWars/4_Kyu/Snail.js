/*
    https://www.codewars.com/kata/521c2db8ddc89b9b7a0000c1/train/javascript

    For better understanding, please follow the numbers of the next array consecutively:

    array = [[1,2,3],
            [8,9,4],
            [7,6,5]]
    snail(array) #=> [1,2,3,4,5,6,7,8,9]

    NOTE: The idea is not sort the elements from the lowest value to the highest; the idea is to traverse the 2-d array in a clockwise snailshell pattern.

    NOTE 2: The 0x0 (empty matrix) is represented as en empty array inside an array [[]].
*/
const logs = [];
const snail = arr => {
    if (!arr[0].length) return [];

    const { UP, DOWN, LEFT, RIGHT } = { UP: 'UP', DOWN: 'DOWN', LEFT: 'LEFT', RIGHT: 'RIGHT'};
    let x = 0;
    let y = 0;
    let moved = 0;
    let moveLimit = arr[0].length;
    let currentDirection = RIGHT;
    let movesSinceMoveLimitReduced = 0;

    const move = () => {
        switch(currentDirection) {
            case UP: {
                y--;
                break;
            }
            case DOWN: {
                y++;
                break;
            }
            case LEFT: {
                x--;
                break;
            }
            case RIGHT: {
                x++;
                break;
            }
        }
    }

    const changeDirection = () => {
        switch(currentDirection) {
            case UP: {
                currentDirection = RIGHT;
                break;
            }
            case DOWN: {
                currentDirection = LEFT;
                break;
            }
            case LEFT: {
                currentDirection = UP;
                break;
            }
            case RIGHT: {
                currentDirection = DOWN;
                break;
            }
        }
    }

    const limitBeforeChange = arr[0].length * 3 - 2;
    const result = [];
    for (let i = 0; i < arr[0].length ** 2; i++) {
        logs.push({i, x, y, moved, moveLimit, currentDirection})
        result.push(arr[y][x]);
        moved++;
        movesSinceMoveLimitReduced++;
        if (i >= limitBeforeChange && movesSinceMoveLimitReduced >= (moveLimit - 1) * 2) {
            moveLimit--;
            movesSinceMoveLimitReduced = 0;
        }
        if (moved >= moveLimit) {
            moved = 1;
            changeDirection();
        }
        move();
    }
console.table(logs)
    return result;
}

const arr = [
    [ 1, 2, 3, 4, 5 ],
    [ 6, 7, 8, 9, 10 ],
    [ 11, 12, 13, 14, 15 ],
    [ 16, 17, 18, 19, 20 ],
    [ 21, 22, 23, 24, 25 ]
  ];

console.log(snail(arr))

// [ 1, 2, 3, 4, 5, 10, 15, 20, 25, 24, 23, 22, 21, 16, 11, 6, 7, 8, 9, 14, 19, 18, 17, 12, 13 ]