/*
    https://www.codewars.com/kata/51c8e37cee245da6b40000bd/train/javascript

    Complete the solution so that it strips all text that follows any of a set of comment markers passed in. Any whitespace at the end of the line should also be stripped out.

    Example:

    Given an input string of:

    apples, pears # and bananas
    grapes
    bananas !apples
    The output expected would be:

    apples, pears
    grapes
    bananas
    The code would be called like so:

    var result = solution("apples, pears # and bananas\ngrapes\nbananas !apples", ["#", "!"])
    // result should == "apples, pears\ngrapes\nbananas"
*/

const solution = (input, markers) => {
    const lines = input.split('\n');
    const linesWithoutComments = lines.map(line => {
        const idxOfComment = [...line].findIndex(c => markers.includes(c))
        return ~idxOfComment ? line.substring(0, idxOfComment).trim() : line;
    })
    return linesWithoutComments.join('\n');
}

console.log(solution("apples, plums % and bananas\npears\noranges !applesauce", ["%", "!"], "apples, plums\npears\noranges"))