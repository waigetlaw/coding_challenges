/*
    https://www.codewars.com/kata/5bc6f9110ca59325c1000254/train/javascript

    You are given a chessBoard, a 2d integer array that contains only 0 or 1. 0 represents a chess piece and 1 represents a empty grid. It's always square shape.

    Your task is to count the number of squares made of empty grids.

    The smallest size of the square is 2 x 2. The biggest size of the square is n x n, where n is the size of chess board.
*/

const count = chessBoard => {
    console.time()
    const results = {};

    for (let n = 2; n <= chessBoard.length; n++) {
        let squares = 0;
        let cellRow;
        let cellCol;
        let isValidSquare;
        for (let row = 0; row <= (chessBoard.length - n); row++) {
            isValidSquare = true;
            for (let col = 0; col <= chessBoard.length - n; col++) {
                if (!isValidSquare && col + n < cellCol) {
                    continue;
                }
                isValidSquare = true;
                for (let cell = 0; cell < n ** 2; cell++) {
                    const cellMod = cell % n;
                    const cellDiv = Math.floor(cell / n);
                    cellRow = cellDiv + row;
                    cellCol = cellMod + col;
                    if (!chessBoard[cellRow][cellCol]) {
                        isValidSquare = false;
                        break;
                    }
                }
                if (isValidSquare) squares++;
            }
            if (squares) results[n] = squares;
        }
    }
    console.timeEnd()
    return results;
}

const size = 400;
chessBoard=[...Array(size).keys()].map(x => [...Array(size).keys()].map(_ => Math.random() > 0.01 ? 1: 0))
// console.log(chessBoard)
// chessBoard=[
//     [1,1,1],
//     [1,0,1],
//     [1,1,1]
//     ]

// chessBoard = [
//     [
//       1, 1, 1, 1, 1,
//       1, 1, 1, 0, 1
//     ],
//     [
//       0, 1, 1, 1, 1,
//       1, 1, 1, 1, 1
//     ],
//     [
//       1, 1, 1, 1, 1,
//       1, 1, 1, 1, 1
//     ],
//     [
//       1, 1, 1, 1, 1,
//       1, 1, 1, 0, 0
//     ],
//     [
//       0, 1, 1, 1, 1,
//       1, 1, 1, 1, 1
//     ],
//     [
//       1, 1, 1, 1, 1,
//       1, 1, 1, 1, 1
//     ],
//     [
//       1, 1, 1, 1, 1,
//       1, 1, 1, 1, 1
//     ],
//     [
//       1, 1, 0, 1, 1,
//       0, 0, 1, 0, 1
//     ],
//     [
//       1, 0, 1, 1, 1,
//       1, 1, 1, 1, 1
//     ],
//     [
//       1, 1, 1, 1, 1,
//       1, 1, 1, 1, 1
//     ]
//   ]
const ans = count(chessBoard);
// console.log(ans)
// { '2': 54, '3': 27, '4': 16, '5': 9, '6': 4, '7': 1 }