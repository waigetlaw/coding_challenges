/*
    https://www.codewars.com/kata/5bc6f9110ca59325c1000254/train/javascript

    You are given a chessBoard, a 2d integer array that contains only 0 or 1. 0 represents a chess piece and 1 represents a empty grid. It's always square shape.

    Your task is to count the number of squares made of empty grids.

    The smallest size of the square is 2 x 2. The biggest size of the square is n x n, where n is the size of chess board.
*/

const count = chessBoard => {
    console.time()
    const results = {};
    const boardLength = chessBoard.length;
    const fullColsEmpty = chessBoard.map((_, i) => chessBoard.map(row => row[i])).map(col => col.every(Boolean));

    for (let n = chessBoard.length; n >= 2; n--) {
        let squares = 0;
        let colsChecking = [];

        for (let row = 0; row <= (boardLength - n); row++) {
            let errorCol;
            let isValidSquare = true;

            const rowsChecking = chessBoard.slice(row, row + n);
            colsChecking = fullColsEmpty.map((colEmpty, i) => {
               if (colEmpty) return true;
               const isColEmpty = colsChecking[i] ? !!rowsChecking[rowsChecking.length - 1][i] : rowsChecking.every(row => row[i])
               return isColEmpty;
            });

            // if every col in this row section is true, we can jump straight to the results.
            if (colsChecking.every(Boolean)) {
                squares += boardLength - n + 1;
                continue;
            }

            for (let col = 0; col <= boardLength - n; col++) {
                // if previous was error, skip forward to next possible col.
                if (!isValidSquare && errorCol) col += errorCol;
                if (col > boardLength - n) break;
                // check cols from cols to cols + n to see if all 1's or not.
                isValidSquare = true;
                // checking in reverse order provides largest skip
                for (let colCheck = n - 1; colCheck >= 0; colCheck--) {
                    if (!colsChecking[col + colCheck]) {
                        errorCol = colCheck;
                        isValidSquare = false;
                        break;
                    }
                }
                if (isValidSquare) squares++;
            }
        }
        if (squares) results[n] = squares;
    }

    console.timeEnd();
    return results;
}

const size = 400;
chessBoard=[...Array(size).keys()].map(x => [...Array(size).keys()].map(_ => Math.random() > 0.01 ? 1: 0))
// console.log(chessBoard)
// chessBoard=[
//     [0,2,3],
//     [4,5,6],
//     [7,8,9]
//     ]

chessBoard1 = [
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 0, 1
    ],
    [
      0, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 0, 0
    ],
    [
      0, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 0, 1, 1,
      0, 0, 1, 0, 1
    ],
    [
      1, 0, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ]
  ]
// chessBoard=[
//     [0,1,1,1,1],
//     [1,1,1,1,1],
//     [1,1,1,1,1],
//     [0,1,1,0,1],
//     [1,1,1,1,1]
//     ]
const ans = count(chessBoard)
// console.log(ans)
// { '2': 54, '3': 27, '4': 16, '5': 9, '6': 4, '7': 1 }