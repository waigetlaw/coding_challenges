/*
    https://www.codewars.com/kata/540afbe2dc9f615d5e000425/train/javascript

    Given a Sudoku data structure with size NxN, N > 0 and √N == integer, write a method to validate if it has been filled out correctly.
*/

const Sudoku = function(data) 
{
  //   Private methods
  // -------------------------
    const isValidSection = (section) => {
        const sorted = [...section].sort((a, b) => a - b);
        return sorted.every((n, i) => n === i + 1)
    }
    const validSize = () => data.every(row => row.length === data.length)
    const rows = () => data.every(row => isValidSection(row))
    const columns = () => {
        let cols = [];
        for (let i = 0; i < data.length; i++) {
            cols[i] = data.map(row => row[i])
        }
        return cols.every(col => isValidSection(col));
    }
    const squares = () => {
        const miniBoxSize = data.length ** 0.5;
        let boxes = [];
        for (let i = 0; i < data.length; i++) {
            const boxRow = i % miniBoxSize;
            const boxCol = Math.floor(i / miniBoxSize);

            let box = [];
            for (let j = 0; j < data.length; j++) {
                const row = j % miniBoxSize;
                const col = Math.floor(j / miniBoxSize);
                box.push(data[(boxRow * miniBoxSize) + row][(boxCol * miniBoxSize) + col])
            }
            boxes.push(box)
        }
        return boxes.every(box => isValidSection(box))
    }

  //   Public methods
  // -------------------------
  return {
    isValid: function() {
      return validSize() && rows() && columns() && squares();
    }
  };
};

let s1 = new Sudoku([
    [7,8,4, 1,5,9, 3,2,6],
    [5,3,9, 6,7,2, 8,4,1],
    [6,1,2, 4,3,8, 7,5,9],
  
    [9,2,8, 7,1,5, 4,6,3],
    [3,5,7, 8,4,6, 1,9,2],
    [4,6,1, 9,2,3, 5,8,7],
    
    [8,7,6, 3,9,4, 2,1,5],
    [2,4,3, 5,6,1, 9,7,8],
    [1,9,5, 2,8,7, 6,3,4]
  ])

  console.log(s1.isValid())