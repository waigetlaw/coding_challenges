/*
    https://www.codewars.com/kata/5bc6f9110ca59325c1000254/train/javascript

    You are given a chessBoard, a 2d integer array that contains only 0 or 1. 0 represents a chess piece and 1 represents a empty grid. It's always square shape.

    Your task is to count the number of squares made of empty grids.

    The smallest size of the square is 2 x 2. The biggest size of the square is n x n, where n is the size of chess board.
*/

const count = chessBoard => {
    console.time()
    const results = {};
    const occupiedMap = chessBoard.map(row => row.reduce((occupied, cell, i) => cell ? occupied : [...occupied, i], []))
    const nonOccupiedMap = chessBoard.map(row => row.reduce((nonOccupied, cell, i) => !cell ? nonOccupied : [...nonOccupied, i], []))
    // console.timeEnd()
    // console.time()
    for (let n = 2; n <= chessBoard.length; n++) {
        let squares = 0;
        for (let row = 0; row <= chessBoard.length - n; row++) {
            let possibilities = [];
            let marker = 0;
            let totalOcc = 0;

            ;
            // speed up if full rows are empty;
            // for (let x = 0; x < n; x++) {
            //   totalOcc += occupiedMap[row + x].length;
            //   // if (totalOcc > 0) break;
            // }
            // if ([...Array(n)].map((_, i) => i).every(i => occupiedMap[row + i].length === chessBoard.length)) {
            //   continue;
            // }

            // // first row
            // for (const occupied of occupiedMap[row]) {
            //     if ((marker + n > occupied) || occupied - n < 0) {
            //       marker = occupied + 1;
            //       continue;
            //     }
            //     for (let accept = marker; accept < occupied - n + 1; accept++) {
            //       possibilities.push(accept);
            //     }
            //     marker = occupied + 1;
            // }
            // // deal with last cell
            // const lastOccupied = occupiedMap[row].length ? occupiedMap[row][occupiedMap[row].length - 1] : -1;
            // for (let accept = lastOccupied + 1; accept < chessBoard.length - n + 1; accept++) {
            //   possibilities.push(accept);
            // }
          
            // let occupiedRowIndex = 1;
            // // console.log('Initial Possibilities: ', possibilities, 'from ',occupiedMap[row] )
            // while (possibilities.length && occupiedRowIndex < n) {

            //   let toRemove = [];
            //   let copyOcc = [...occupiedMap[row + occupiedRowIndex]]
            //   for (let posIdx = 0; posIdx < possibilities.length; posIdx++) {
            //     const possible = possibilities[posIdx];
            //     const occIndex = copyOcc.findIndex(occ => {
            //       return occ >= possible && occ < possible + n;
            //     })

            //     if (~occIndex) {
            //       if (occIndex > 0) copyOcc.splice(0, occIndex);
            //       toRemove.push(possible);
            //     }
            //   }
            //   possibilities = possibilities.filter(pos => !toRemove.includes(pos))
            //   occupiedRowIndex++;
            // }

            
            // squares += possibilities.length;
            // n = 4
            // possible = 5
            // occ needs to be 5, 6, 7, 8
            // choose strategy - combine all occupied, or combine all nonOccupied and find common

            let sortedAccumCols = [];

            if (occupiedMap[row].length < nonOccupiedMap[row].length) {
              let accumulatedColsSet = new Set();
              for (let rowIdx = 0; rowIdx < n; rowIdx++) {
                  for (let rowCellIdx = 0; rowCellIdx < occupiedMap[rowIdx + row].length; rowCellIdx++) {
                    accumulatedColsSet.add(occupiedMap[rowIdx + row][rowCellIdx]);
                    if (accumulatedColsSet.size === chessBoard.length) break;
                  }
                  if (accumulatedColsSet.size === chessBoard.length) break;
              }
              // const accumulatedColsSet = [...Array(n)].reduce((set, _, i) => {
              //     for (const occupied of occupiedMap[i + row]) set.add(occupied);
              //     return set;
              // }, new Set())
              sortedAccumCols = [...accumulatedColsSet].sort((a, b) => a - b);
            } else {
                const original = [...Array(chessBoard.length)].map((_, i) => i);
              for (let col = 0; col < chessBoard.length; col++) {

                if ([...Array(n)].map((_, i) => i).every(i => nonOccupiedMap[i + row].includes(col))) {
                  original[col] = undefined;
                }
              }

              sortedAccumCols = original.filter(Boolean);
            }
            
            if (sortedAccumCols.length) {
                squares += sortedAccumCols.reduce((tracker, occupiedIdx, i) => {
                    if ((occupiedIdx - tracker.marker) - n <= 0 && i !== sortedAccumCols.length - 1) {
                        tracker.marker = occupiedIdx;
                        return tracker;
                    }
                    tracker.total += Math.max((occupiedIdx - tracker.marker) - n, 0); // only add if enough space to fit n
                    tracker.marker = occupiedIdx;
                    if (i === sortedAccumCols.length - 1) {
                      tracker.total += Math.max((chessBoard.length - occupiedIdx) - n, 0); // add end part too
                    }
                    return tracker;
                }, { marker: -1, total: 0 }).total;
            } else {
                squares += chessBoard.length - n + 1; // if no occupied, all is valid
            }
        }

        if (squares) results[n] = squares;
    }
    console.timeEnd()
    return results;
}


// [1,2,3,5,6,7,8] [4, 9]
// [1,2,3,6,7,9] [4, 8]
// [1,2,3,5,6,7,8,9]
// [4]



// 0, 8, 9
// T F F F F F F F T T
// 0 1 2 3 4 5 6 7 8 9
// therefore: 7 F's in a row = 5 if n =2

// F F T F F T T F T F
// 0 1 2 3 4 5 6 7 8 9


const size = 400;
chessBoard=[...Array(size).keys()].map(x => [...Array(size).keys()].map(_ => Math.random() > 1 ? 1: 0))
// console.log(chessBoard)
// chessBoard=[
//     [1,1,1],
//     [1,1,1],
//     [1,1,1]
//     ]
const show = 0;
chessBoard1= [
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 0, 1
    ],
    [
      0, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 0, 0
    ],
    [
      0, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 0, 1, 1,
      0, 0, 1, 0, 1
    ],
    [
      1, 0, 1, 1, 1,
      1, 1, 1, 1, 1
    ],
    [
      1, 1, 1, 1, 1,
      1, 1, 1, 1, 1
    ]
  ]
const ans = count(chessBoard);
if (show) console.log(ans)
// { '2': 54, '3': 27, '4': 16, '5': 9, '6': 4, '7': 1 } // 0.25ms ~ 0.3ms