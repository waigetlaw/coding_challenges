const topThreeWords = text => {
    const words = text.toLowerCase().split(/[^a-zA-Z']/).filter(word => word && word !== "'")
    const counts = words.reduce((table, word) => {
        if (!table[word]) {
            table[word] = 1;
        } else {
            table[word]++;
        }
        return table;
    }, {})
    const topThree = Object.entries(counts).sort((a, b) => b[1] - a[1]).map(([k, v]) => k).slice(0, 3);
    return topThree
}





const s = "  '  ";

console.log(topThreeWords(s))