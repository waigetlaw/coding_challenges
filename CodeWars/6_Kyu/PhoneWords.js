/*
    https://www.codewars.com/kata/635b8fa500fba2bef9189473/train/javascript

    For example if you get "22" return "b", if you get "222" you will return "c". If you get "2222" return "ca".

    Further details:

    0 is a space in the string.
    1 is used to separate letters with the same number.
    always transform the number to the letter with the maximum value, as long as it does not have a 1 in the middle. So, "777777" -->  "sq" and "7717777" --> "qs".
    you cannot return digits.
    Given a empty string, return empty string.
    Return a lowercase string.

    Examples:
    "443355555566604466690277733099966688"  -->  "hello how are you"
    "55282"                 -->  "kata"
    "22266631339277717777"  -->  "codewars"
    "66885551555"           -->  "null"
    "833998"                -->  "text"
    "000"                   -->  "   "
*/


/*
 66885551555

/ [
  '6', '6', '8', '8',
  '5', '5', '5', '1',
  '5', '5', '5'
]

    ['66', '88', '555', '555']
*/

const phoneWords = (stringOfNums) => {
    if (!stringOfNums) return '';
    const mapping = {
        2: 'abc',
        3: 'def',
        4: 'ghi',
        5: 'jkl',
        6: 'mno',
        7: 'pqrs',
        8: 'tuv',
        9: 'wxyz',
        0: '?'
    };

    if (stringOfNums.length > 0) {
        const chars = [...stringOfNums];
        let answer = '';

        const { grouped } = chars.reduce((arr, char, i) => {
            if (!arr.currentGroup.length || char === arr.currentGroup[0]) {
                arr.currentGroup += char;
            } else {
                arr.grouped.push(arr.currentGroup);
                arr.currentGroup = char;
            }
            if (i === chars.length - 1) arr.grouped.push(arr.currentGroup);
            return arr;
        }, {
            currentGroup: '',
            grouped: []
        })

        const filteredGroup = grouped.filter(group => group[0] !== '1' && group);

        for (const group of filteredGroup) {
            const groupLength = group.length;
            const numberMapping = mapping[group[0]]

            const start = Math.floor(groupLength / numberMapping.length);
            const startLetters = numberMapping[numberMapping.length - 1].repeat(start);

            const mod = groupLength % numberMapping.length;
            let lastLetter = '';
            if (mod) {
                lastLetter = numberMapping[mod - 1];
            }

            answer += startLetters + lastLetter;
        }
       
        return answer;
    }
}




const string = '111';

console.log(phoneWords(string));