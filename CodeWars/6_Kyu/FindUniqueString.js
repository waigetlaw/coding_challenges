/*
    https://www.codewars.com/kata/585d8c8a28bc7403ea0000c3/train/javascript

    There is an array of strings. All strings contains similar letters except one. Try to find it!

    findUniq([ 'Aa', 'aaa', 'aaaaa', 'BbBb', 'Aaaa', 'AaAaAa', 'a' ]) === 'BbBb'
    findUniq([ 'abc', 'acb', 'bac', 'foo', 'bca', 'cab', 'cba' ]) === 'foo'
    Strings may contain spaces. Spaces are not significant, only non-spaces symbols matters. E.g. string that contains only spaces is like empty string.

    It’s guaranteed that array contains more than 2 strings.

    This is the second kata in series:

    Find the unique number
    Find the unique string (this kata)
    Find The Unique
*/



const findUniq = arr => {
    const letterRegex = /[\p{L}/]/u;
    const count = arr.slice(0, 3).map(s => [...new Set(s.toLowerCase())].sort().filter(c => letterRegex.test(c)).join('')).reduce((count, n) => {
        if (!count[n]) {
            count[n] = 1;
        } else {
            count[n]++;
        }
        return count;
    }, {});

    if (Object.keys(count).length === 2) {
        const unique = Object.entries(count).find(([k, v]) => v === 1)[0];
        return arr.find(s => [...new Set(s.toLowerCase())].sort().filter(c => letterRegex.test(c)).join('') === unique);
    }

    const sameString = Object.keys(count)[0];
    return arr.find(s => [...new Set(s.toLowerCase())].sort().filter(c => letterRegex.test(c)).join('') !== sameString);
}

console.log(findUniq([ 'Кот', 'Кто', 'ток', 'Пёс', 'коток', 'токоток' ]))

// console.log(/[\p{L}/]/u.test('о'))