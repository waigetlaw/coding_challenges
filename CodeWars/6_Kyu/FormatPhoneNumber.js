/*
    https://www.codewars.com/kata/61393fd03e441f001ac9c7d4/train/javascript

    Introduction
    You need to write a function that will format a phone number by a template.

    Task
    You're given number and string.

    If there are more digits than needed, they should be ignored

    if there are less digits than needed, should return Invalid phone number

    Examples
    (79052479075, "+# ### ### ## ##") => "+7 905 247 90 75"
    (79052479075, "+# (###) ### ##-##") => "+7 (905) 247 90-75"
    (79052479075, "+# ### ### ## ##") => "+7 905 247 90 75"
    (81237068908090, "+## ### ### ## ##") => "+81 237 068 90 80"
    (8123706890, "+## ### ### ##-##") => "Invalid phone number"
    (911, "###") => "911"
    (112, "+ () -") => "+ () -"
*/

const formatNumber = (number, template) => {
    const stringNum = `${number}`;
    if ([...template].filter(c  => c === '#').length > stringNum.length) return 'Invalid phone number';
    const stringNumArr = [...stringNum];

    return [...template].map(c => c === '#' ? stringNumArr.shift() : c).join('');
}

console.log(formatNumber(81237068908090, "+## ### ### ## ##"))