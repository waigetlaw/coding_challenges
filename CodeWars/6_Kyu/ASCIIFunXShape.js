/*
    6 Kyu - https://www.codewars.com/kata/5906436806d25f846400009b

    This will probably be a little series :)

    X-Shape
    You will get an odd Integer n (>= 3) and your task is to draw a X. Each line is separated with '\n'.

    Use the following characters: ■ □

    For Ruby, Crystal and PHP: whitespace and o

    e.g.:


                                        ■□□□■
                ■□■                      □■□■□
    x(3) =>   □■□              x(5)=>  □□■□□
                ■□■                      □■□■□
                                        ■□□□■
*/

function x(n){
    a = [];
    gap = n-2;
    middle = '□'.repeat((n-1)/2) + '■' + '□'.repeat((n-1)/2);
    for(i = 0; i < (n-1)/2; i++) {
      a.push('□'.repeat(i) + '■' + '□'.repeat(gap) + '■' + '□'.repeat(i));
      gap -= 2;
    }
    return [...a, middle, ...a.reverse()].join('\n');
  }