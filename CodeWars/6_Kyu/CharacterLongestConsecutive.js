/*
    https://www.codewars.com/kata/586d6cefbcc21eed7a001155/train/javascript

    For a given string s find the character c (or C) with longest consecutive repetition and return:

    [c, l]
    where l (or L) is the length of the repetition. If there are two or more characters with the same l return the first in order of appearance.

    For empty string return:

    ["", 0]
    In JavaScript: If you use Array.sort in your solution, you might experience issues with the random tests as Array.sort is not stable in the Node.js version used by CodeWars. This is not a kata issue.

    Happy coding! :)
*/

const longestRepetition = s => {

    const { counts } = [...s].reduce((track, char, i, original) => {
        if (char === track.currentLetter) {
            track.currentCount++;
        } else {
            track.counts.push({
                char: track.currentLetter,
                place: i,
                count: track.currentCount
            })
            track.currentCount = 1;
            track.currentLetter = char;
        }
        if (i === original.length - 1) {
            track.counts.push({
                char: track.currentLetter,
                place: i,
                count: track.currentCount
            })
        }
        return track;
    }, { currentLetter: s[0], currentCount: 0, counts: [] })
    
    const highestCount = counts.reduce((highest, count) => {
        return Math.max(count.count, highest);
    }, 0);

    const answer = counts.find(count => count.count === highestCount);

    return answer ? [answer.char, answer.count] : ['', 0];
}

console.log(longestRepetition('bbbaaabaaaa'))