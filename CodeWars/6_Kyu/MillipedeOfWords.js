/*
    https://www.codewars.com/kata/6344701cd748a12b99c0dbc4/train/javascript

    The set of words is given. Words are joined if the last letter of one word and the first letter of another word are the same. Return true if all words of the set can be combined into one word. Each word can and must be used only once. Otherwise return false.

    Input
    Array of 3 to 7 words of random length. No capital letters.

    Example true
    Set: excavate, endure, desire, screen, theater, excess, night.
    Millipede: desirE EndurE ExcavatE ExcesS ScreeN NighT Theater.

    Example false
    Set: trade, pole, view, grave, ladder, mushroom, president.
    Millipede: presidenT Trade.
*/

function solution(words) {
    let { firsts, lasts, joined } = words.reduce((arr, word) => {
        arr.firsts.push(word[0]);
        arr.lasts.push(word[word.length - 1]);
        arr.joined.push(word[0] + word[word.length - 1])
        return arr;
    }, { firsts: [], lasts: [], joined: [] })

    console.log(firsts, lasts, joined)

    firsts = firsts.filter(c => {
        const idx = lasts.indexOf(c);
        if (~idx) {
            lasts.splice(idx, 1);
            return false;
        } else {
            return true;
        }
    })

    console.log(firsts, lasts)

    return firsts.length < 2 && lasts.length < 2;
  }

  const arr = ["no", "dog", "on", "goof"];

  // no -> on --><-- dog -> good]

  // no -> on -><- dog -> goof

//   [d], [f]



  console.log(solution(arr));